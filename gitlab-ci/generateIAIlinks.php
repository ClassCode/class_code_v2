<?php

function cmpSectionDate($a, $b){
  $sectionA=$a[20];  
  $sectionB=$b[20];
  $dateA=$a[6];  
  $dateB=$b[6];
  if ($sectionA == $sectionB) {
	if ($dateA == $dateB) {
      return 0;
	}
	return ($dateA > $dateB) ? -1 : 1;  
  }
  return ($sectionA < $sectionB) ? -1 : 1;
}

print "Lancement du traitement... \n";
//$file_pointer = '40ainehtml.txt';
$postID = 21718;
$delimiter = "<!--iairessources-->";

$dataArray=array();
$html="";
$htmlFirst=$html;
if (($h = fopen("https://docs.google.com/spreadsheets/d/1xlI91FqGZ1XLltIcPe5_3IvASOVbSWKC5sdlGxrfWVk/gviz/tq?tqx=out:csv", "r")) !== FALSE){
  print "File open! \n";
  while (($data = fgetcsv($h, 1000, ",")) !== FALSE){
	if(count($data)!=0){
	  $dataArray[] = $data;
	}
  }
  fclose($h);
}
$dataArraySize = count($dataArray);
print $dataArraySize." lignes initialement \n";
if($dataArray && $dataArraySize!=0){
  unset($dataArray[0]); //ligne de légendes
  foreach ($dataArray as $key => $tmp_line){ //on nettoie les lignes vides ou incomplètes
	$dataArray[$key][20]=htmlentities(trim($tmp_line[20]), ENT_QUOTES | ENT_COMPAT ); //section
	$dataArray[$key][1]=str_replace("\n", "",trim($tmp_line[1])); //url
	$dataArray[$key][0]=htmlentities(trim($tmp_line[0]), ENT_QUOTES | ENT_COMPAT); //title
	//$dataArray[$key][4]=str_replace("\n", "",trim($tmp_line[4])); //img
	$dataArray[$key][4]=htmlentities(trim($tmp_line[4]), ENT_QUOTES | ENT_COMPAT); //auteur
	$dataArray[$key][5]=htmlentities(trim($tmp_line[5]), ENT_QUOTES | ENT_COMPAT); //editeur
	$dataArray[$key][6]=htmlentities(trim($tmp_line[6]), ENT_QUOTES | ENT_COMPAT); //date
	
	if(($dataArray[$key][20]=='')||($dataArray[$key][1]=='')||($dataArray[$key][0]=='')){
	  unset($dataArray[$key]);	
	}
  }
}
$dataArraySize = count($dataArray);
print $dataArraySize." lignes après nettoyage \n";
if($dataArray && $dataArraySize!=0){
  $currentSection="";
  usort($dataArray, "cmpSectionDate");//on trie le tableau par Section croissante puis Date decroissante
  $colorArray=array("blue","orange","green");
  $sectionNumber = 0;
  foreach ($dataArray as $tmp_line){	    
	$tmp_section=$tmp_line[20];
	$tmp_url=$tmp_line[1];
	$tmp_title=$tmp_line[0];
	$tmp_author=$tmp_line[4];
	$tmp_editor=$tmp_line[5];
	$tmp_date=$tmp_line[6];
	if(($tmp_url!='')&&($tmp_title!='')&&($tmp_section!='')){	
	  if($currentSection != $tmp_section){ //on change de titre
		if($currentSection !=""){ // on est pas sur la première, donc on ferme la div
		  $html.="</ul></div>\n";
		  $html.="<br>\n";
		}
		$sectionNumber++;		
		$colorClass=$colorArray[$sectionNumber%3];
		$html.="<div class='iai-ressources'><h3 class='".$colorClass."'>".$tmp_section."</h3>\n";
		$html.="<ul>";
		$currentSection=$tmp_section ;		
	  }	  
	  $html.="<li><p><a href='".$tmp_url."'>".$tmp_title."</a>";
	  if($tmp_author!=''){
		$html.=", ".$tmp_author;  
	  }
	  if($tmp_editor!=''){
		$html.=", ".$tmp_editor;  
	  }
	  if($tmp_date!=''){
		$html.=", ".$tmp_date;  
	  }
	  $html.="</li>";
	} 
  }
  if($html!=$htmlFirst){ //on ferme si pas vide
	$html.="</ul></div>\n";
    $html.="<br>\n";	
  } 
}

$wp_path = "/appli/www/pixees/";
$cwd = getcwd();
chdir( $wp_path );
include "wp-load.php";
$post = get_post($postID);
$post_content = $post->post_content;
$post_contentArray = explode($delimiter,$post_content);
$post_contentArray[1]=$html;
$post_content = implode($delimiter,$post_contentArray);
$post->post_content = $post_content;
wp_update_post( $post, true );
if (is_wp_error($postID)) {
  $errors = $post_id->get_error_messages();
  foreach ($errors as $error) {
    echo $error;
  }
}
chdir( $cwd );
//file_put_contents($file_pointer, $html); 
print "... fin du traitement, execution réussie! \n";
?>