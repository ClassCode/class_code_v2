<?php

function cmpSection($a, $b){
  $tmpArray = explode(" ", $a[5]);	
  $sectionA=$tmpArray[1];  
  $tmpArray = explode(" ", $b[5]);	
  $sectionB=$tmpArray[1];
  if ($sectionA == $sectionB) {
    return 0;
  }
  return ($sectionA < $sectionB) ? -1 : 1;
}

print "Lancement du traitement... \n";
//$file_pointer = '40ainehtml.txt';
$postID = 23601;
$delimiter = "<!--webinairesCards-->";

$dataArray=array();
$html="<div id='' class='content-category t4'>\n";
$htmlFirst=$html;
if (($h = fopen("https://docs.google.com/spreadsheets/d/13NRyQYTOMo4yY1LoPFC40mTYyC987yJvX0HfJPoNMAQ/gviz/tq?tqx=out:csv&sheet=webinaires", "r")) !== FALSE){
  print "File open! \n";
  while (($data = fgetcsv($h, 1000, ",")) !== FALSE){
	if(count($data)!=0){
	  $dataArray[] = $data;
	}
  }
  fclose($h);
}
$dataArraySize = count($dataArray);
print $dataArraySize." lignes initialement \n";
if($dataArray && $dataArraySize!=0){
  unset($dataArray[0]); //legende
  foreach ($dataArray as $key => $tmp_line){ //on nettoie les lignes vides ou incomplètes
	$dataArray[$key][5]=htmlentities(trim($tmp_line[3]), ENT_QUOTES | ENT_COMPAT ); //section
	$dataArray[$key][2]=str_replace("\n", "",trim($tmp_line[0])); //url
	$dataArray[$key][3]=htmlentities(trim($tmp_line[1]), ENT_QUOTES | ENT_COMPAT); //title
	$dataArray[$key][4]=str_replace("\n", "",trim($tmp_line[2])); //img
	$dataArray[$key][6]=htmlentities(trim($tmp_line[4]), ENT_QUOTES | ENT_COMPAT); //desc
  $dataArray[$key][7]=htmlentities(trim($tmp_line[5]), ENT_QUOTES | ENT_COMPAT); //date
  $dataArray[$key][8]=htmlentities(trim($tmp_line[6]), ENT_QUOTES | ENT_COMPAT); //heure
	if($dataArray[$key][2]==''){
    $dataArray[$key][2]="https://pixees.fr/wp-content/uploads/2016/09/et-la-litteratie-numerique.png";
  }
  if(($dataArray[$key][0]=='')||($dataArray[$key][1]=='')||($dataArray[$key][3]=='')){
	  unset($dataArray[$key]);	
	}
  }
}
$dataArraySize = count($dataArray);
print $dataArraySize." lignes après nettoyage \n";
if($dataArray && $dataArraySize!=0){
  $currentSection="";
  usort($dataArray, "cmpSection");//on trie le tableau
  foreach ($dataArray as $tmp_line){	
	$tmp_section=$tmp_line[5];
	$tmp_url=$tmp_line[2];
	$tmp_title=$tmp_line[3];
	$tmp_img=$tmp_line[4];
	$tmp_desc=$tmp_line[6];
  $tmp_date=$tmp_line[7];
  $tmp_hour=$tmp_line[8];
  if($tmp_hour!=""){
    $tmp_date = $tmp_date." - ".$tmp_hour;
  }
	if(($tmp_url!='')&&($tmp_title!='')&&($tmp_section!='')){	
	  if($currentSection != $tmp_section){ //on change de titre
		if($currentSection !=""){ // on est pas sur la première, donc on ferme la div
		  $html.="</div>\n";
		  $html.="<div class='clear'></div>\n";
		}
		$html.="<p class='content-category-title'>".$tmp_section."<span class='content-category-subtitle'></span></p>\n";
		$html.="<div class='cards'>";
		$currentSection=$tmp_section ;		
	  }	  
	  $html.="[cc-card title='".$tmp_title."' img='".$tmp_img."' url='".$tmp_url."' desc='".$tmp_desc."' date='".$tmp_date."']";
	} 
  }
  if($html!=$htmlFirst){ //on ferme si pas vide
	$html.="</div>\n";
    $html.="<div class='clear'></div>\n";	
  } 
}
$html.="</div>\n";

$wp_path = "/appli/www/pixees/";
$cwd = getcwd();
chdir( $wp_path );
include "wp-load.php";
$post = get_post($postID);
$post_content = $post->post_content;
$post_contentArray = explode($delimiter,$post_content);
$post_contentArray[1]=$html;
$post_content = implode($delimiter,$post_contentArray);
$post->post_content = $post_content;
wp_update_post( $post, true );
if (is_wp_error($postID)) {
  $errors = $post_id->get_error_messages();
  foreach ($errors as $error) {
    echo $error;
  }
}
chdir( $cwd );
//file_put_contents($file_pointer, $html); 
print "... fin du traitement, execution réussie! \n";
?>