<?php

function cmpSection($a, $b){
  $tmpArray = explode(" ", $a[5]);	
  $sectionA=$tmpArray[1];  
  $tmpArray = explode(" ", $b[5]);	
  $sectionB=$tmpArray[1];
  if ($sectionA == $sectionB) {
    return 0;
  }
  return ($sectionA < $sectionB) ? -1 : 1;
}

print "Lancement du traitement... \n";
//$file_pointer = '40ainehtml.txt';
$postID = 21122;
$delimiter = "<!--activitiesCards-->";

$dataArray=array();
$html="<div id='' class='content-category t4 logopartners'>\n";
$htmlFirst=$html;
if (($h = fopen("https://docs.google.com/spreadsheets/d/1q5exdLRoSm-DnE0N4XLpais7jsmm_T3agKbtPvVYkGM/gviz/tq?tqx=out:csv&sheet=Parcours%20ensembles", "r")) !== FALSE){
  print "File open! \n";
  while (($data = fgetcsv($h, 1000, ",")) !== FALSE){
	if(count($data)!=0){
	  $dataArray[] = $data;
	}
  }
  fclose($h);
}
$dataArraySize = count($dataArray);
print $dataArraySize." lignes initialement \n";
if($dataArray && $dataArraySize!=0){
  unset($dataArray[0]); //ligne de légendes
  unset($dataArray[1]); //ligne de légendes
  foreach ($dataArray as $key => $tmp_line){ //on nettoie les lignes vides ou incomplètes
	$dataArray[$key][5]=htmlentities(trim($tmp_line[5]), ENT_QUOTES | ENT_COMPAT ); //section
	$dataArray[$key][2]=str_replace("\n", "",trim($tmp_line[2])); //url
	$dataArray[$key][3]=htmlentities(trim($tmp_line[3]), ENT_QUOTES | ENT_COMPAT); //title
	$dataArray[$key][4]=str_replace("\n", "",trim($tmp_line[4])); //img
	$dataArray[$key][6]=htmlentities(trim($tmp_line[6]), ENT_QUOTES | ENT_COMPAT); //desc
	if(($dataArray[$key][2]=='')||($dataArray[$key][3]=='')||($dataArray[$key][5]=='')){
	  unset($dataArray[$key]);	
	}
  }
}
$dataArraySize = count($dataArray);
print $dataArraySize." lignes après nettoyage \n";
if($dataArray && $dataArraySize!=0){
  $currentSection="";
  usort($dataArray, "cmpSection");//on trie le tableau
  foreach ($dataArray as $tmp_line){	
	$tmp_section=$tmp_line[5];
	$tmp_url=$tmp_line[2];
	$tmp_title=$tmp_line[3];
	$tmp_img=$tmp_line[4];
	$tmp_desc=$tmp_line[6];
	if(($tmp_url!='')&&($tmp_title!='')&&($tmp_section!='')){	
	  if($currentSection != $tmp_section){ //on change de titre
		if($currentSection !=""){ // on est pas sur la première, donc on ferme la div
		  $html.="</div>\n";
		  $html.="<div class='clear'></div>\n";
		}
		$html.="<p class='content-category-title'>".$tmp_section."<span class='content-category-subtitle'></span></p>\n";
		$html.="<div class='cards'>";
		$currentSection=$tmp_section ;		
	  }	  
	  $html.="[cc-card title='".$tmp_title."' img='".$tmp_img."' url='".$tmp_url."' desc='".$tmp_desc."']";
	} 
  }
  if($html!=$htmlFirst){ //on ferme si pas vide
	$html.="</div>\n";
    $html.="<div class='clear'></div>\n";	
  } 
}
$html.="</div>\n";

$wp_path = "/appli/www/pixees/";
$cwd = getcwd();
chdir( $wp_path );
include "wp-load.php";
$post = get_post($postID);
$post_content = $post->post_content;
$post_contentArray = explode($delimiter,$post_content);
$post_contentArray[1]=$html;
$post_content = implode($delimiter,$post_contentArray);
$post->post_content = $post_content;
wp_update_post( $post, true );
if (is_wp_error($postID)) {
  $errors = $post_id->get_error_messages();
  foreach ($errors as $error) {
    echo $error;
  }
}
chdir( $cwd );
//file_put_contents($file_pointer, $html); 
print "... fin du traitement, execution réussie! \n";
?>