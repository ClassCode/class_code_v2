<?php


print "Lancement du traitement... \n";
//$file_pointer = 'logo40ainehtml.txt';
$postID = 21117;
$delimiter = "<!--40logos-->";

$dataArray=array();
$html="<div id='' class='content-category t4'>\n";
if (($h = fopen("https://docs.google.com/spreadsheets/d/1q5exdLRoSm-DnE0N4XLpais7jsmm_T3agKbtPvVYkGM/gviz/tq?tqx=out:csv&sheet=Partenaires%20contributeurs", "r")) !== FALSE){
  print "File open! \n";
  while (($data = fgetcsv($h, 1000, ",")) !== FALSE){
	if(count($data)!=0){
	  $dataArray[] = $data;
	}
  }
  fclose($h);
}
$dataArraySize = count($dataArray);
print $dataArraySize." lignes initialement \n";
if($dataArray && $dataArraySize!=0){
  unset($dataArray[0]); //ligne de légendes
  unset($dataArray[1]); //ligne de légendes
  foreach ($dataArray as $key => $tmp_line){ //on nettoie les lignes vides ou incomplètes
    $dataArray[$key][1]=str_replace("\n", "",trim($tmp_line[1])); //url
	$dataArray[$key][0]=htmlentities(trim($tmp_line[0]), ENT_QUOTES | ENT_COMPAT); //title
	$dataArray[$key][2]=str_replace("\n", "",trim($tmp_line[2])); //img
	if(($dataArray[$key][1]=='')||($dataArray[$key][0]=='')||($dataArray[$key][2]=='')){
	  unset($dataArray[$key]);	
	}
  }
}
$dataArraySize = count($dataArray);
print $dataArraySize." lignes après nettoyage \n";
if($dataArray && $dataArraySize!=0){
  $currentSection="";
  $html.="<p class='content-category-title'>Ils ont mis ces contenus à votre disposition<span class='content-category-subtitle'></span></p>\n";
  $html.="<div class='cards'>";
  $html.= "<div class='cards-container'>";
  foreach ($dataArray as $tmp_line){	
	$tmp_url=$tmp_line[1];
	$tmp_title=$tmp_line[0];
	$tmp_img=$tmp_line[2];
	if(($tmp_url!='')&&($tmp_title!='')&&($tmp_img!='')){	
 
	  $html.= "<div class='card' style='height:100px; margin: auto; vertical-align: middle; text-align: center;'>";
      $html.= "<a href='".$tmp_url."' target='_blank' rel='noopener noreferrer' title='".$tmp_title."'>";
	  $html.= "<img style='margin:3px;height: 92px;' src='".$tmp_img."' alt='".$tmp_title."'>";      
      $html.= "</a>";
	  $html.= "</div>"; 	 
  
	} 
  }
  $html.= "</div>"; 	 
  $html.="</div>\n";
  $html.="<div class='clear'></div>\n";	
}
$html.="</div>\n";

$wp_path = "/appli/www/pixees/";
$cwd = getcwd();
chdir( $wp_path );
include "wp-load.php";
$post = get_post($postID);
$post_content = $post->post_content;
$post_contentArray = explode($delimiter,$post_content);
$post_contentArray[1]=$html;
$post_content = implode($delimiter,$post_contentArray);
$post->post_content = $post_content;
wp_update_post( $post, true );
if (is_wp_error($postID)) {
  $errors = $post_id->get_error_messages();
  foreach ($errors as $error) {
    echo $error;
  }
}

chdir( $cwd );

//file_put_contents($file_pointer, $html); 
print "... fin du traitement, execution réussie! \n";
?>