<?php

require_once( CLASSCODE2_PLUGIN_DIR . '/functions.php' );

class CLASSCODE2_Plugin {

    const VERSION = '1.0.0';
	private static $instance;
    protected $plugin_slug;
	protected $templates;

	
	public static function get_instance() {

		if( null == self::$instance ) {
			self::$instance = new CLASSCODE2_Plugin();
		}

		return self::$instance;
	}

	private function __construct() {
		
		$this->plugin_slug = "classcode2";
		
		$this->templates = array();
		
		// 1. Ajouter le fichier dans le dossier "templates" du plugin
		// 2. Ajouter le nom du fichier dans le tableau qui suit, en précisant le nom de la template
		// 3. Dans l'admin Wordpress, créer une page en utilisant comme modèle la template créée
		// 4. Définir comme parent de cette page la template "ClassCode2 Accueil" pour avoir le header ClassCode V2 (et non le header Pixees)
		
		// Add your templates to this array.
		$this->templates = array(
			'template-accueil.php'          => __( 'ClassCode 2 Accueil'       , $this->plugin_slug ),
			'template-a-la-carte.php'       => __( 'ClassCode 2 À la carte'    , $this->plugin_slug ),
			'template-le-projet.php'        => __( 'ClassCode 2 Le Projet'     , $this->plugin_slug ),
			'template-credits.php'          => __( 'ClassCode 2 Crédits'       , $this->plugin_slug ),
			'template-les-parcours.php'     => __( 'ClassCode 2 Les parcours'  , $this->plugin_slug ),
			'template-les-parcours-invites.php'  => __( 'ClassCode 2 Les parcours invites'  , $this->plugin_slug ),
			'template-faq.php'              => __( 'ClassCode 2 FAQ'           , $this->plugin_slug ),
			'template-contacts.php'         => __( 'ClassCode 2 CONTACTS'      , $this->plugin_slug ),
			'template-coordinateurs.php'    => __( 'ClassCode 2 Coordinateurs' , $this->plugin_slug ),
			'template-creer-rencontre.php'  => __( 'ClassCode 2 Créer une rencontre' , $this->plugin_slug ),
			'template-more.php'             => __( 'ClassCode 2 Page supplémentaire' , $this->plugin_slug ),
			'template-english-post.php'     => __( 'ClassCode 2 Page en anglais'     , $this->plugin_slug ),
            'template-edit-profile.php'     => __( 'ClassCode 2 Edition du profile'  , $this->plugin_slug ),
            'template-iai-module1.php'      => __( 'ClassCode 2 IAI Module1', $this->plugin_slug ),
            'template-iai-module2.php'      => __( 'ClassCode 2 IAI Module2', $this->plugin_slug ),
            'template-iai-module3.php'      => __( 'ClassCode 2 IAI Module3', $this->plugin_slug ),
            'template-iai-post.php'         => __( 'ClassCode 2 IAI Article', $this->plugin_slug ),
            'template-iai-parcours.php'     => __( 'ClassCode 2 IAI Accueil', $this->plugin_slug )
		);
		
		// adding support for theme templates to be merged and shown in dropdown
		$templates = wp_get_theme()->get_page_templates();
		$templates = array_merge( $templates, $this->templates );
		
		// Add a filter to the attributes metabox to inject template into the cache.
		if ( version_compare( floatval( get_bloginfo( 'version' ) ), '4.7', '<' ) ) {

			// 4.6 and older
			add_filter(
				'page_attributes_dropdown_pages_args',
				array( $this, 'register_project_templates' )
			);

		} else {

			// Add a filter to the wp 4.7 version attributes metabox
			add_filter(
				'theme_page_templates', array( $this, 'add_new_template' )
			);

		}
		
		// Save post in order to inject out template into the page cache
		add_filter('wp_insert_post_data', array( $this, 'register_project_templates' ) );

		// Determine if the page has our template assigned and return it's path
		add_filter('template_include', array( $this, 'view_project_template') );

		// When plugin is activated, deactivated, and uninstalled, respectively.
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );
		

		//
		// Support des catégories pour les pages
		//

		function add_taxonomies_to_pages() {
			register_taxonomy_for_object_type( 'post_tag', 'page' );
			register_taxonomy_for_object_type( 'category', 'page' );
		}
		add_action( 'init', 'add_taxonomies_to_pages' );
		
		//
		// Ajout dans le header des fichiers JS et CSS 
		//

		function css_and_js_scripts() {
			
			global $post;
			
			// $templateName = get_page_template_slug($post->ID);
			
			if ( is_classcode2( $post) ) {
				
				// Front CSS 
				wp_enqueue_style('classcode2-front-styles', CLASSCODE2_PLUGIN_URL.'assets/css/style.css', [], null);

                // IAI CSS
                wp_enqueue_style('classcode2-iai-styles', CLASSCODE2_PLUGIN_URL.'assets/css/style-iai.css', [], null);
                wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Raleway:400,700,800|Rambla:400,700', [], null );

                // Front JS Functions
                // ...

				// SLIDER (Temporaire avant l'usage d'un plugin Wordpress)
				wp_enqueue_style('classcode2-slider-styles', CLASSCODE2_PLUGIN_URL.'assets/js/jquery.bxslider/jquery.bxslider.css', [], null);
				wp_enqueue_script('classcode2-slider-js', CLASSCODE2_PLUGIN_URL.'assets/js/jquery.bxslider/jquery.bxslider.min.js', [], null);
			}
		}
		add_action( 'wp_enqueue_scripts', 'css_and_js_scripts' );

        function css_for_admin() {

            // Admin CSS
            wp_enqueue_style('classcode2-admin-styles',  CLASSCODE2_PLUGIN_URL.'assets/css/admin-style.css');

        }
        add_action('admin_enqueue_scripts', 'css_for_admin');


		//
		// Routes
		//
		
		function classcode_v2_rewrite_rules()
		{
			add_rewrite_rule(
				'classcode-v2/a-la-carte/([^/]+)/?$',
				'index.php?pagename=classcode-v2/a-la-carte&activite=$matches[1]',
				'top' );
		}
		add_action( 'init', 'classcode_v2_rewrite_rules' );
		
		// Liste des variables pouvant être passées via les routes ci-dessus
		function classcode_v2_query_vars( $query_vars ){
			$query_vars[] = 'activite';
			return $query_vars;
		}
		add_filter( 'query_vars', 'classcode_v2_query_vars' );


		//
        // Blocks Gutenberg Wordpress 5 : module ClassCode IAI / liens vers des ressources extérieures
        //

        // Active le mode WP5 Gutenberg pour les modules ClassCode IAI  uniquement ( les autres pages ou posts resteront en mode Classic pré-WP5 )
        function shapeSpace_enable_gutenberg_post_ids($can_edit, $post) {
            if (empty($post->ID)) return $can_edit;
            if (has_category( 'iai-modules-avec-blocs-de-liens', $post )) return true;
            return $can_edit;
        }
        add_filter('use_block_editor_for_post', 'shapeSpace_enable_gutenberg_post_ids', 10, 2);

        function gutenberg_register_block() {

            wp_register_script(
                'iai-links-editor',
                plugins_url( 'blocks/iai-links/block.js', __FILE__ ),
                array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore' ),
                filemtime( plugin_dir_path( __FILE__ ) . '\blocks\iai-links\block.js' )
            );

            register_block_type( 'classcode2/iai-links', array(
                'editor_script' => 'iai-links-editor',
            ) );

        }
        add_action( 'init', 'gutenberg_register_block' );

        function my_allowed_block_types( $allowed_block_types, $post ) {
            // On limite les blocs Gutenberg disponibles pour les modules ClassCode IAI
            if ( $post->post_type !== 'page' ) {
                return $allowed_block_types;
            }
            return array(
                'classcode2/iai-links'
            );
        }
        add_filter( 'allowed_block_types', 'my_allowed_block_types', 10, 2);



		//
		// Shortcode LES PARCOURS DE FORMATION ( LES MODULES ) : [classcode2_module module_id="1"]
		//

		function classcode2_module_shortcodes_init()
		{
			add_shortcode('classcode2_module', array( 'CLASSCODE2_Plugin', 'classcode2_module_apply_shortcode' ));
		}
		add_action('init', 'classcode2_module_shortcodes_init');		
	
	
		//
		// Recherche Ajax À LA CARTE
		//

		add_action( 'wp_ajax_classcode_v2_recherche_a_la_carte', 'query_recherche_posts_callback' );
		add_action( 'wp_ajax_nopriv_classcode_v2_recherche_a_la_carte', 'query_recherche_posts_callback' );
		function query_recherche_posts_callback() {
			
			global $wpdb;
			
			$query_params = json_decode( stripslashes( $_POST['query_params'] ));
            $json_search  = $query_params->{"search"};
      
			$nb_posts = -1;
			$offset = 0;
			
			$nb_posts = $query_params->{"nb_posts"};
			$offset = $query_params->{"offset"};

			// Les paramètres passés sont les noms de catégories
			$tax_query = array( 'relation' => 'AND' );

			// On boucle sur les groupes de catégories ( parcours, ressources, thématiques)
			foreach( $json_search as $categoryGroup => $categoryGroupArray )
			{
				$categoriesOfGroup = $categoryGroupArray;
				if ( count($categoryGroupArray) > 0)
				{
					$groupArray = array( 'relation' => 'OR' );
					
					// On boucle sur les expressions de catégories d'un groupe
					// Ex : 
					// 'module-3' ( expression simple : nom de la catégorie )
					// 'activite-debranchee+comprendre+jouer' ( + --> OR )
					// '1-2-3-codez*cycle-3' ( * --> AND )
					foreach( $categoryGroupArray as $categoryExpression )
					{
						if (strpos( $categoryExpression, "+" ) !== FALSE )
						{
							// + OU : Union de catégories
							$categories = explode("+", $categoryExpression);
							$categoriesQueryArgs = array( 'relation' => "OR");
							foreach( $categories as $category )
							{
								array_push ($categoriesQueryArgs, array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => $category
								));
							}
							array_push($groupArray, $categoriesQueryArgs);
						}
						else if (strpos( $categoryExpression, "*" ) !== FALSE )
						{
							// * ET : Intersection de catégories
							$categories = explode("*", $categoryExpression);
							$categoriesQueryArgs = array( 'relation' => "AND");
							foreach( $categories as $category )
							{
								array_push ($categoriesQueryArgs, array(
									'taxonomy' => 'category',
									'field'    => 'slug',
									'terms'    => $category
								));
							}
							array_push($groupArray, $categoriesQueryArgs);
						}
						else if (strpos( $categoryExpression, "!" ) !== FALSE )
						{
							// ! ET PAS : Exclusion de catégories
							$categories = explode("!", $categoryExpression);
							$categoriesQueryArgs = array( 'relation' => "AND");
							$categoryNo = 0;
							foreach( $categories as $category )
							{
								$categoryNo++;
								
								if ($categoryNo === 1)
								{
									array_push ($categoriesQueryArgs, array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => $category
									));
								}
								else
								{
									array_push ($categoriesQueryArgs, array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => array($category),
										'operator' => 'NOT IN'
									));
								}
							}
							array_push($groupArray, $categoriesQueryArgs);
						}
						else
						{
							// Nom de la catégorie
							array_push ($groupArray, array( 
								'taxonomy' => 'category',
								'field'    => 'slug',
								'terms'    => $categoryExpression
							));
						}
					}
					
					array_push( $tax_query , $groupArray);
				}
			}
			
			$query_results = get_posts_with_categories ( $tax_query, $nb_posts, $offset );
			$query_posts   = $query_results->{"posts"};
			
			echo json_encode( array( 
				"query_params"   => $query_params,
				"query_results"  => $query_results,
				"query_taxonomy" => $tax_query
			));

			wp_die();
		};
    
         //Export Ressources in simple json
        add_action( 'wp_ajax_classcode_v2_export_resources_json', 'query_export_ressources_json_posts_callback' );
        add_action( 'wp_ajax_nopriv_classcode_v2_export_resources_json', 'query_export_ressources_json_posts_callback' );
        function query_export_ressources_json_posts_callback() {

            global $wpdb;

            $query_params = json_decode('{"search":{"tous_les_parcours":true,"parcours":["modules","prendre-du-recul","1-2-3-codez","cycles-scolaires","epi-enseignement-pratique-et-interdisciplinaire","module-1","module-2","module-3","module-4","module-5","cycle-1","cycle-2","cycle-3","cycle-4","a-la-carte"],"ressources":[],"thematiques":[]}}');

            $json_search  = $query_params->{"search"};

            $nb_posts = -1;
            $offset = 0;

            // Les paramètres passés sont les noms de catégories
            $tax_query = array( 'relation' => 'AND' );

            // On boucle sur les groupes de catégories ( parcours, ressources, thématiques)
            foreach( $json_search as $categoryGroup => $categoryGroupArray )
            {
                $categoriesOfGroup = $categoryGroupArray;
                if ( count($categoryGroupArray) > 0)
                {
                    $groupArray = array( 'relation' => 'OR' );

                    // On boucle sur les expressions de catégories d'un groupe
                    // Ex :
                    // 'module-3' ( expression simple : nom de la catégorie )
                    // 'activite-debranchee+comprendre+jouer' ( + --> OR )
                    // '1-2-3-codez*cycle-3' ( * --> AND )
                    foreach( $categoryGroupArray as $categoryExpression )
                    {
                        if (strpos( $categoryExpression, "+" ) !== FALSE )
                        {
                            // + OU : Union de catégories
                            $categories = explode("+", $categoryExpression);
                            $categoriesQueryArgs = array( 'relation' => "OR");
                            foreach( $categories as $category )
                            {
                                array_push ($categoriesQueryArgs, array(
                                    'taxonomy' => 'category',
                                    'field'    => 'slug',
                                    'terms'    => $category
                                ));
                            }
                            array_push($groupArray, $categoriesQueryArgs);
                        }
                        else if (strpos( $categoryExpression, "*" ) !== FALSE )
                        {
                            // * ET : Intersection de catégories
                            $categories = explode("*", $categoryExpression);
                            $categoriesQueryArgs = array( 'relation' => "AND");
                            foreach( $categories as $category )
                            {
                                array_push ($categoriesQueryArgs, array(
                                    'taxonomy' => 'category',
                                    'field'    => 'slug',
                                    'terms'    => $category
                                ));
                            }
                            array_push($groupArray, $categoriesQueryArgs);
                        }
                        else if (strpos( $categoryExpression, "!" ) !== FALSE )
                        {
                            // ! ET PAS : Exclusion de catégories
                            $categories = explode("!", $categoryExpression);
                            $categoriesQueryArgs = array( 'relation' => "AND");
                            $categoryNo = 0;
                            foreach( $categories as $category )
                            {
                                $categoryNo++;

                                if ($categoryNo === 1)
                                {
                                    array_push ($categoriesQueryArgs, array(
                                        'taxonomy' => 'category',
                                        'field'    => 'slug',
                                        'terms'    => $category
                                    ));
                                }
                                else
                                {
                                    array_push ($categoriesQueryArgs, array(
                                        'taxonomy' => 'category',
                                        'field'    => 'slug',
                                        'terms'    => array($category),
                                        'operator' => 'NOT IN'
                                    ));
                                }
                            }
                            array_push($groupArray, $categoriesQueryArgs);
                        }
                        else
                        {
                            // Nom de la catégorie
                            array_push ($groupArray, array(
                                'taxonomy' => 'category',
                                'field'    => 'slug',
                                'terms'    => $categoryExpression
                            ));
                        }
                    }

                    array_push( $tax_query , $groupArray);
                }
            }

            $query_results = get_posts_with_categories ( $tax_query, $nb_posts, $offset );
            $query_posts   = $query_results->{"posts"};

            $results = array();
            foreach($query_posts as $post){
                $result['title']=$post['title'];
                $result['description']=$post['permalink'];
                $result['langage']='fr';
                $result['license']='CC-BY 2.0';
                $result['date']=$post['date'];
                //find the ressource type
                $result['resource type']="pixees page";
                if($post['target'] && $post['target']=='_blank'){
                  $result['resource type']="external link";
                }
                if($post['categories_a_la_carte']){
                  if($post['categories_a_la_carte']['ressources']){
                    if($post['categories_a_la_carte']['ressources']['slug']){
                     $result['resource type']= $post['categories_a_la_carte']['ressources']['slug'];
                    }
                  }
                }
                $results[]=$result;
            }

            header('Content-type: application/json');
            echo json_encode($results);
            wp_die();
        }
	}
	
 
	//
	// SHORTCODE
	//
	
	public function classcode2_module_apply_shortcode($atts = [], $content = null, $tag = '')
	{
		$atts = array_change_key_case((array)$atts, CASE_LOWER);

		$shortcode_atts = shortcode_atts([ 'category' => '1' ], $atts, 'category');
		$module_category = $shortcode_atts['category'];

		$query_results = get_posts_by_category( $module_category );
		$query_posts   = $query_results->posts;
		
		//
		
		$resultsHtml = '';

		foreach($query_posts as $query_post)
		{
			$post_categories_str = get_post_links_to_categories( $query_post );

			$resultsHtml .= '<li>';
			
			// Vignette
			$resultsHtml .= '<div class="post-image" style="background-image: url(' . $query_post["thumbnail"] . ')"><a title="" href="' . $query_post["permalink"] . '" target="' . $query_post["target"] . '"  ></a></div>';
			
			// Catégories et Titre
			$resultsHtml .= '<div class="post-content">';
			$resultsHtml .= '<p class="post-text-categories">' . $post_categories_str . '</p>';

			$resultsHtml .= '<p class="post-text-content"><a title="" href="' . $query_post["permalink"] . '" target="' . $query_post["target"] . '"  >' . $query_post["title"] . '</a></p>';

			$resultsHtml .= '<div class="clear"></div>';
			$resultsHtml .= '</div>';
			$resultsHtml .= '</li>';
		}
		
		return $resultsHtml;
	}

    //
    // CUSTOM BLOCS
    //

    public static function renderLinks( $linkThemeFilter = 'se_questionner' ) {
        global $post;
        $blocks = parse_blocks( $post->post_content );
        foreach( $blocks as $block ) {
            if( 'classcode2/iai-links' === $block['blockName'] ) {
                // var_dump($block);
                $blockAttributes = $block['attrs'];
                $blockLinkTheme = $blockAttributes['linkTheme'];
                if ( $blockLinkTheme === $linkThemeFilter)
                {
                    echo '<li class="iai-content-link">';
                    echo '<a href="'. $blockAttributes['link'] .'" target="_blank" class="link-'. $blockAttributes['linkType'] .'">';
                    echo '<p>'. $blockAttributes['title'] .'</p>';
                    echo '<p>'. $blockAttributes['description'] .'</p>';
                    echo '</a>';
                    echo '</li>';

                    // echo render_block( $block );
                }
            }
        }
    }

	//
	// GESTION DES TEMPLATES
	//
	
	public function add_new_template( $posts_templates ) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}
	
	public function register_project_templates( $atts ) {

		// Create the key used for the themes cache
		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		// Retrieve the cache list. If it doesn't exist, or it's empty prepare an array
		$templates = wp_cache_get( $cache_key, 'themes' );
		
		if ( (is_array($templates) === FAlSE ) || empty( $templates ) ) {
			$templates = array();
		} // end if

		// Since we've updated the cache, we need to delete the old cache
		wp_cache_delete( $cache_key , 'themes');

		// Now add our template to the list of templates by merging our templates
		// with the existing templates array from the cache.
		$templates = array_merge( $templates, $this->templates );

		// Add the modified cache to allow WordPress to pick it up for listing
		// available templates
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		return $atts;

	}
	
	public function view_project_template( $template ) {

		global $post;

		// If no posts found, return to
		// avoid "Trying to get property of non-object" error
		if ( !isset( $post ) ) return $template;

		if ( ! isset( $this->templates[ get_post_meta( $post->ID, '_wp_page_template', true ) ] ) ) {
			return $template;
		} // end if

		$file = plugin_dir_path( __FILE__ ) . 'templates/' .  get_post_meta( $post->ID, '_wp_page_template', true );

		// Just to be safe, we check if the file exist first
		if( file_exists( $file ) ) {
			return $file;
		} // end if

		return $template;
	}
	
	static function deactivate( $network_wide ) {
	    /*
		foreach($this as $value) {
			CLASSCODE2_Plugin::delete_template( $value );
		}
		*/
	}

	public function delete_template( $filename ){				
		$theme_path = get_template_directory();
		$template_path = $theme_path . '/' . $filename;  
		if( file_exists( $template_path ) ) {
			unlink( $template_path );
		}

		// we should probably delete the old cache
		// wp_cache_delete( $cache_key , 'themes');
	}
}




