<?php

/**
 * Defines a new type called rencontre to manage Class'Code hybrid formation.Import datas from classcodeadmin
 *
 */
include_once(WP_PLUGIN_DIR.'/class_code_v2/profiles/profile_type.php');

class rencontre_type {
  function __construct() {
    
  }
  
  public static function get_rencontre($id) {
    $json = file_get_contents(get_site_url().'/classcodeadmin/meetinginfo.json?meetingId='.$id);
    $rencontre = json_decode($json,true);
    return $rencontre;
  }
  
  public static function get_rencontres($what = array(), $sort = 'date') {
    $user_id = isset($what['user_id']) ? $what['user_id'] : wp_get_current_user()->ID;
    global $title_module ; // recupération de la liste des modules pour les comparer en cas de choix "other" et calculer l index
    $json = file_get_contents(get_site_url().'/classcodeadmin/querymeetings.json');
    $rencontres = json_decode($json,true);
    if((isset($what['when']) && $what['when'] != 'future')){
      $rencontres = array_reverse($rencontres);      
    }
    $nearby = 20;
    if(isset($what['nearby']) && ($what['nearby'] !== true) ){
      $nearby = $what['nearby'];
    }
    
    //il faut un user_id pour calculer des distances
    $userCoord = false;
    if($user_id){
      $userCoord = profile_type::get_location($user_id,"coordinates");
    }
    
    if(isset($what['where'])){
      $where = $what['where'];
    }else{
      $where = $userCoord;
    } 
    
    foreach($rencontres as $index => $rencontre) {
      $id = $rencontre['id'];

      $rencontres[$index]["module"] = $rencontre['subject'];
      if (preg_match("/^[^#]*#([0-9]).*$/", $rencontres[$index]["module"])){
        $rencontres[$index]["module_id"] = preg_replace("/^[^#]*#([0-9]).*$/", "$1", $rencontres[$index]["module"]);
      }else{
	      $rencontres[$index]["module_id"] = "0";
      }
      $rencontre_coord = array($rencontre['location']['lat'],$rencontre['location']['lng']);

      $rencontres[$index]['distance'] = 0; //valeur si pas de coordonnées définie pour user_id ou pour la rencontre
      //distance par rapport au used_id :
      if($userCoord && $rencontre_coord){
        $rencontres[$index]['distance'] = round(self::get_location_distance($rencontre_coord,$userCoord ),1);  
        $rencontres[$index]["nearby"] = $rencontres[$index]['distance'] <= $nearby ;
      }
      
      $rencontres[$index]['userStatut']="visiteur";
      if($user_id == $rencontre["ownerId"]){
        $rencontres[$index]["mine"] = true;  
        $rencontres[$index]['userStatut']="organisateur";
      }else{
        $rencontres[$index]["mine"] = false;  
        if(in_array($user_id,$rencontre['subscribers'])){
          $rencontres[$index]['userStatut']="participant";
        }
      }
       
      $strucutre_min_sans_accent = suppr_accents($rencontre['structure']);
      if(isset($what['structure'])){
        $structure_what_min_sans_accent = suppr_accents($what['structure']);  
      }else{
        $structure_what_min_sans_accent = "";
      }
      
      if((isset($what['structure']) && !strstr(strtolower($strucutre_min_sans_accent),strtolower($structure_what_min_sans_accent))) ||
	       (isset($what['module']) && ($what['module'] != $rencontre['subject']) && ($what['module'] != 'other') && ($what['module'] != 'all'))||
	       (isset($what['facilitateur']) && ((($what['facilitateur'] =='yes') && !$rencontres[$index]["facilitateur"]) || 
                                           (($what['facilitateur'] =='no') && $rencontres[$index]["facilitateur"] ))) ||
	       (isset($what['when']) && (($what['when'] == 'past' && !$rencontres[$index]["past"]) || ($what['when'] == 'future' && !$rencontres[$index]["future"]))) ||	       
         ($userCoord && isset($what['nearby']) && !$rencontres[$index]["nearby"])) {
        unset($rencontres[$index]);
      }
      //test sur le module "other"
      if(isset($what['module']) && $what['module'] == 'other'){
        foreach($title_module as $title){
          if($rencontres[$index]["module"] == $title){
            unset($rencontres[$index]);
          }
        }
      }
      //test sur "mes rencontres" 
      if(isset($what['mes_rencontres']) && $what['mes_rencontres']!= ""){
        if($what['mes_rencontres'] === true){
          $mes_rencontres_id = $user_id;
        }else{
          $mes_rencontres_id = $what['mes_rencontres'];
        }        
        if($rencontres[$index]['userStatut']=="visiteur"){          
          unset($rencontres[$index]);
        }
      }
    }
    if ($userCoord && ($sort == 'distance')){
      usort($rencontres, function($r1, $r2) {
	      return $r1['distance'] - $r2['distance'];
      });
    }
    return array_values($rencontres);
    
  } 
  
  /** Gets the distance between two earth location coordinates.
   * @param $c1 An array of the form <tt>array(lattitude,longitude)</tt> for the 1st coordinates.
   * @param $c2 An array of the form <tt>array(lattitude,longitude)</tt> for the 2nd coordinates.
   * @return The distance in kilometers.
   */
  public static function get_location_distance($c1, $c2) {
    $lat1 = deg2rad(90 - $c1[0]);
    $lat2 = deg2rad(90 - $c2[0]);
    return 6371 * acos(cos($lat1) * cos($lat2) + sin($lat1) * sin($lat2) * cos(deg2rad($c1[1] - $c2[1])));
  }

  
}  
?>