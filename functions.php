<?php
include_once('profiles/useravatar/class_code_user_avatar.php');

function get_libelles_categories_parcours() {
	return array(
		"1-2-3-codez" => "Module 123 Codez",
		"module-1" => "Module 1",
		"module-2" => "Module 2",
		"module-3" => "Module 3",
		"module-4" => "Module 4",
		"module-5" => "Module 5",
		"mooc-icn-informatique-et-creation-numerique" => "Formation SNT",
		"parcours-mgistere" => "Parcours M@gistere",
        "iai-formation-citoyenne-a-lintelligence-artificielle-intelligente" => "IAI : Formation citoyenne à l’intelligence artificielle intelligente"
	);
};

function get_liens_parcours() {
   return array(
		"1-2-3-codez" => array(
			"title" => "1,2,3... codez ! | Le site de la Fondation La main à la pâte",
			"url"   => "http://www.fondation-lamap.org/123codez"
		),
		"module-1" => array(
			"title" => "Découvrir la programmation créative - OpenClassrooms",
			"url"   => "https://openclassrooms.com/courses/decouvrir-la-programmation-creative"
		),
		"module-2" => array(
			"title" => "Manipuler l'information - OpenClassrooms",
			"url"   => "https://openclassrooms.com/courses/manipuler-l-information"
		),
		"module-3" => array(
			"title" => "S'initier à la robotique - OpenClassrooms",
			"url"   => "https://openclassrooms.com/courses/s-initier-a-la-robotique"
		),
		"module-4" => array(
			"title" => "Connecter le réseau - OpenClassrooms",
			"url"   => "https://openclassrooms.com/courses/connecter-le-reseau"
		),
		"module-5" => array(
			"title" => "Gérer un projet informatique avec des enfants - OpenClassrooms",
			"url"   => "https://openclassrooms.com/courses/gerer-un-projet-informatique-avec-des-enfants"
		),
		"mooc-icn-informatique-et-creation-numerique" => array(
			"title" => "FUN - Se former pour l’ICN Informatique et Création Numérique",
			"url"   => "https://www.fun-mooc.fr/en/cours/lintelligence-artificielle-avec-intelligence/"
		)
   );
}

function get_libelles_categories_parcours_icn() {
	return array(
		"presentation-du-cours"                       => "Présentation du cours",
		"n-le-numerique-et-ses-sciences-dans-le-reel" => "N: Le numérique et ses sciences dans le réel",
		"i-l-informatique-et-ses-fondements"          => "I : L’Informatique et ses fondements",
		"c-creer-des-projets-pour-l-icn"              => "C : Créer des projets pour l’ICN"
		
	);
};

function get_libelles_categories_thematiques() {
	return array(
		"activite-branchee" => "Programmer",
		"thymio" => "Thymio",
		"robotique" => "Robotique",
		"robotique+thymio" => "Robotique",
		"scratch" => "Scratch",
		"activite-debranchee" => "Jouer",
		"prendre-du-recul" => "Découvrir",
		"pedagogie" => "Transmettre",
	);
};

function get_libelles_categories_ressources() {
	return array(
		"video" => "Vidéo",
		"fiche-d-activite" => "Fiche Activité",
		"texte" => "Cours"
	);
};


// WP QUERY : à la carte
// Retourne un tableau avec les infos de chaque post de la query du formulaire de recherche "à la carte"
function get_posts_with_categories ($tax_query, $nb_posts = -1, $offset = 0) {
	
	$query = get_query_a_la_carte ($tax_query, $nb_posts, $offset);
	$query_posts = get_posts_with( $query );
	wp_reset_query();	
	
	return format_query_results( $query_posts, $query->found_posts, $nb_posts, $offset) ;
}

// Retourne  la requête de la page Recherche
function get_query_a_la_carte( $tax_query, $nb_posts = -1, $offset = 0 ) {

	$args = array(
		'posts_per_page' => $nb_posts,
		'offset'	     => $offset,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'post_status'    => "publish",
		'post_type'      => array('post'/*, 'page'*/),
		'tax_query'		 => $tax_query
	);
	
	return new WP_Query( $args );
}


// WP QUERY : parcours
// Retourne un tableau avec les infos de chaque post ayant LA categorie passée en paramètre
function get_posts_by_category ($category, $nb_posts = -1, $offset = 0) {
	
	$query = get_query_by_category ($category, $nb_posts, $offset);
	$query_posts = get_posts_with( $query );
	wp_reset_query();	
	
	return format_query_results( $query_posts, $query->found_posts, $nb_posts, $offset) ;
}

function get_query_by_category( $category, $nb_posts = -1, $offset = 0 ) {

	$args = array(
		'posts_per_page' => $nb_posts,
		'offset'	     => $offset,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'post_status'    => "publish",
		'post_type'      => array('post'),
		'category_name'	 => $category
	);
	
	return new WP_Query( $args );
}

function UR_exists($url){
  $headers=get_headers($url);
  return stripos($headers[0],"200 OK")?true:false;
}

// 'thumbnail_large', 'thumbnail_medium', 'thumbnail_small'
function get_vimeo_image( $vimeoId, $size = 'thumbnail_large' ) {
  $json_url = "http://vimeo.com/api/v2/video/" . $vimeoId . ".json";
  if(UR_exists($json_url)){
    $data = file_get_contents($json_url);
    $data = json_decode($data);
    return $data[0]->{$size};  
  }else{
    return 0;
  }
}

function get_posts_with ( $the_query ) {
	
	$query_posts = array();
	
	if ( $the_query->have_posts() ) {
		
		while ( $the_query->have_posts() ) {
			
			$the_query->the_post();
			
			$post_id = get_the_ID();
			$post_type = get_post_type( $post_id );
			
			$post_infos = array(
				"id"        => $post_id,
				"title"     => get_the_title(),
				"permalink" => get_permalink(),
				"target"    => '',
				"date"      => get_the_date( "d/m/Y", $post_id),
				"type"		=> $post_type
			);
			
			// Première image (ou par défaut : image du post )
			$post_img = get_post_1st_img();
			if (preg_match("/\.svg$/", $post_img)) $post_img = false;
			if ($post_img === false )
			{
				if (has_post_thumbnail( $post_id ) ) {
					$post_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );

					// RQ : post_image = [ url, width, height, is_intermediate ]...
					$post_img = $post_image[0];
				}
			}
			$post_infos["thumbnail"] = $post_img;
			
			// Premier lien du post :
			$first_link = get_post_1st_link(); 
			
			// Cas particulier : contenus "OpenClassRoom" (liens vers l'extérieur)
			if ( ($first_link != FALSE ) && 
			     ( ( strpos ($first_link, 'https://openclassrooms.com/' ) === 0 ) ||
			       ( strpos ($first_link, 'https://static.oc-static.com/prod/courses/files' ) === 0 ) ||
                               ( strpos ($first_link, 'http://exercices.openclassrooms.com' ) === 0 ))) {
				$post_infos[ "permalink" ] = $first_link;
				$post_infos[ "target" ]    = "_blank";
			}
			
			// Catégories du post
			$post_categories = wp_get_post_terms( $post_id, "category" );
			$post_infos["categories"] = $post_categories;
			
			// Slugs des categories:
			$slugs = array();
			foreach($post_categories as $category) {
				array_push ( $slugs, $category->slug );
			}
			
			$post_infos["categories_slugs"] = $slugs;
			$post_infos["categories_a_la_carte"] = get_post_categories_a_la_carte( $slugs );
			
			array_push($query_posts, $post_infos);
		};
	}
	
	return $query_posts;
}

function get_post_categories_a_la_carte( $post_categories_slugs ) {

	// Catégories du moteur de recherche "à la carte"
	$categories_a_la_carte = array();
	$categories_parcours     = get_libelles_categories_parcours();
	$categories_parcours_icn = get_libelles_categories_parcours_icn();
	$categories_thematiques  = get_libelles_categories_thematiques();
	$categories_ressources   = get_libelles_categories_ressources();

	foreach( $categories_parcours as $key => $value ) {
		if ( in_array( $key, $post_categories_slugs) !== FALSE ) {
			$categories_a_la_carte["parcours"] = array ( "slug" => $key, "name" => $value );
			break;
		}
	}

	foreach( $categories_parcours_icn as $key => $value ) {
		if ( in_array( $key, $post_categories_slugs) !== FALSE ) {
			$categories_a_la_carte["parcours_icn"] = array ( "slug" => $key, "name" => $value );
			break;
		}
	}

	foreach( $categories_thematiques as $key => $value ) {
		if ( in_array( $key, $post_categories_slugs) !== FALSE ) {
			$categories_a_la_carte["thematiques"] = array ( "slug" => $key, "name" => $value );
			break;
		}
	} 

	foreach( $categories_ressources as $key => $value ) {
		if ( in_array( $key, $post_categories_slugs) !== FALSE ) {
			$categories_a_la_carte["ressources"] = array ( "slug" => $key, "name" => $value );
			break;
		}
	} 

	return $categories_a_la_carte;
}


function get_post_links_to_categories( $query_post ) {
	
	$aLaCarteUrl = site_url( CLASSCODE2_ROUTE. "/a-la-carte/" );
	
	$post_categories_a_la_carte = $query_post["categories_a_la_carte"];
	$post_categories_labels_array = array();

	$categoryObj = $post_categories_a_la_carte["parcours"];
	if ($categoryObj) {
		$categorySlug = $categoryObj["slug"];
		$categoryLibelle = $categoryObj["name"];
		$categoryLink = '<a href="' . $aLaCarteUrl . $categorySlug . '" >' . $categoryLibelle . '</a>';
		array_push ($post_categories_labels_array, '<span class="parcours">' . $categoryLink . '</span>');
	}
	
	$categoryObj = $post_categories_a_la_carte["parcours_icn"];
	if ($categoryObj) {
		$categorySlug = $categoryObj["slug"];
		$categoryLibelle = $categoryObj["name"];
		$categoryLink = '<a href="' . $aLaCarteUrl . $categorySlug . '" >' . $categoryLibelle . '</a>';
		array_push ($post_categories_labels_array, '<span class="parcours">' . $categoryLink . '</span>');
	}

	$thematiqueObj = $post_categories_a_la_carte["thematiques"];
	if ($thematiqueObj) {
		$thematiqueSlug = $thematiqueObj["slug"];
		$thematiqueLibelle = $thematiqueObj["name"];
		$thematiqueLink = '<a href="' . $aLaCarteUrl . $thematiqueSlug . '" >' . $thematiqueLibelle . '</a>';
		array_push ($post_categories_labels_array, '<span class="thematiques">' . $thematiqueLink . '</span>');
	}

	$ressourcesObj = $post_categories_a_la_carte["ressources"];
	if ($ressourcesObj) {
		$ressourcesSlug = $ressourcesObj["slug"];
		$ressourcesLibelle = $ressourcesObj["name"];
		$ressourcesLink = '<a href="' . $aLaCarteUrl . $ressourcesSlug . '" >' . $ressourcesLibelle . '</a>';
		array_push ($post_categories_labels_array, '<span class="ressources">' . $ressourcesLink . '</span>');
	}

	return implode( " | ", $post_categories_labels_array );
}


function format_query_results ( $posts, $found_posts, $nb_posts, $offset, $except_post_id = -1) {
	
	$o = array(
		"posts"            => $posts,        // Tableau des objets décrivant les posts retournés par une requête (id, titre, visuel, ...)
		"posts_count"      => count($posts), // Nb de posts retournés par la requête (compte-tenu de la limitation, et de l'offset )
		"found_posts"      => $found_posts,  // Nb de post existants répondant aux critères de la requête (indépendamment de la limitation et l'offset)
		"nb_posts"         => $nb_posts,     // Nb de posts demandés (i.e. limitation pour l'affichage)
		"offset"           => $offset,       // Index du premier post à afficher (permet la pagination)
		"voir_plus"        => $nb_posts === - 1 ? false : $found_posts - ($offset + $nb_posts) > 0, // Y'a-t-il plus de résultats ? (boolean )
		"voir_plus_offset" => $offset + count($posts),  // Index du premier post de la prochaine série
		"except_post_id"   => $except_post_id
	);
	
	return (object) $o;
}

function get_children_slug_categories ( $categoryId ) {

	$term_children_ids = get_term_children( $categoryId, "category" );
	$term_children_slugs = array();
	foreach($term_children_ids as $term_child_id) {
		$term_children = get_term_by( "id", $term_child_id, "category");
		array_push($term_children_slugs, $term_children->slug);
	}
	return $term_children_slugs;
}


// PROD :

// Retourne un tableau avec les infos de chaque post de la query du formulaire de recherche "à la carte"
function get_posts_test_categories () {

	$args = array(
		'post_type' => 'post',
		'tax_query' => array(
		
			'relation' => 'AND',
		
			array(
				'relation' => 'OR',
				array(
					'taxonomy' => 'category',
					'field'    => 'slug',
					'terms'    => array( 'module-1','module-2' )
				),
				array(
					'taxonomy' => 'post_tag',
					'field'    => 'slug',
					'terms'    => array( 'module-1','module-2' )
				)
			),
		
			array(
				'relation' => 'OR',
					array(
						'taxonomy' => 'category',
						'field'    => 'slug',
						'terms'    => array('coder','atelier'),
					),
					array(
						'taxonomy' => 'post_tag',
						'field'    => 'slug',
						'terms'    => array('coder','atelier'),
					)
				),
		),
	);
	
	$query = new WP_Query( $args );
	$query_posts = get_posts_with( $query );
	wp_reset_query();	
	
	return format_query_results( $query_posts, $query->found_posts, $nb_posts, $offset) ;
}


function get_the_content_and_slider( $default_slider_path = 'classcode-v2/slider-par-defaut') {
	
	// Texte de la page en cours
	$htmlContent = get_the_content();
	
	// Par défaut, pas de slider;
	$sliderContent = '';
	
	$dom = new DOMDocument();
  if($htmlContent){
	  $dom->loadHTML( $htmlContent );
  }
	
	// Récupération du slider dans le contenu de la page en cours
	$slider = $dom->getElementById('slider');
	if (! $slider) {
		
		// On prend le slider présent dans le contenu d'une page dédiée
		$slider_page_par_default = get_page_by_path( $default_slider_path );
		if ($slider_page_par_default) {
			$pageContent = $slider_page_par_default->post_content;
			$dom->loadHTML( $pageContent );
			
			// On recherche le slider de cette page :
			$slider = $dom->getElementById('slider');
		}
	}
		
	if ($slider) {
		$slider->setAttribute( "class", "t2 slideshow");

		$sliderDom = $dom->saveHTML($slider);
		$slider->parentNode->removeChild($slider); 
		
		$sliderContent = utf8_decode( $sliderDom );
		$htmlContent = utf8_decode( $dom->saveHTML( $dom ));
	}
	
	return (object) array(
		"slider"  => $sliderContent,
		"content" => $htmlContent
	);
}

function createCard($params) {
  extract(
    shortcode_atts(
      array(
	    'title' => '',
        'img' => '',
        'url' => '',
        'desc' => '',
        'date'=> '',
      ),
      $params
    )
  );
  $card = "<div class='card'>";
  $card.= "<a href='".$url."' target='_blank' rel='noopener noreferrer'>";
  $card.= "<div class='card-content'>";
  $card.= "<div class='card-thumbnail' style='background-image: url(".$img.");'>";
  $card.= "<div class='card-thumbnail-description'>".$desc."</div>";
  $card.= "</div>";
  $card.= "<div class='titles'> ";
  if($date !=''){
    $card .="<div class='categories' style='text-align:center;'>".$date."</div>";  
  }
  $card.= "<p class='title'>".$title."</p>";
  $card.= "</div>";
  $card.= "</div>";
  $card.= "</a>";
  $card.= "</div>";  

  return $card;
};
add_shortcode('cc-card', 'createCard');

//Ajout du menu 'attributs de pages' sur les posts pour pouvoir modifier l'ordre (pour le parcours de l'accueil)
add_action( 'admin_init', 'posts_order_attribute' );
function posts_order_attribute() 
{
    add_post_type_support( 'post', 'page-attributes' );
}
?>