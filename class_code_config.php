<?php
/* this file contain all arrays containing informations about ClassCode modules */

// Static const data for rencontres

$classcodeUrl=get_site_url();

global $title_module;
$title_module = array(
		      1 => '#1 Module fondamental : découvrir la programmation créative',
		      2 => '#2 Module thématique : manipuler l’information',
		      3 => '#3 Module thématique : diriger les robots',
		      4 => '#4 Module thématique : connecter le réseau',
		      5 => '#5 Module fondamental : le processus de création de A à Z',
		      );
global $url_module;
$url_module_array = array(
		      1 => 'https://classcode.fr/formations/module1/',
		      2 => 'https://classcode.fr/formations/module2/',
		      3 => 'https://classcode.fr/formations/module3/',
		      4 => 'https://classcode.fr/formations/module4/',
		      5 => 'https://classcode.fr/accueil/#moocs',
		      );
global $kit1_module;
$kit1_module = array(
		     1 => $classcodeUrl.'/wp-content/uploads/2016/04/KIT-Module-01-Rencontre-01.pdf',
		     2 => $classcodeUrl.'/wp-content/uploads/2016/08/KIT-Module-02-Rencontre-01.pdf',
		     3 => $classcodeUrl.'/wp-content/uploads/2016/11/KIT-Module-03-Rencontre-01.pdf',
		     4 => $classcodeUrl.'/wp-content/uploads/2017/03/KIT-Module-04-Rencontre-01.pdf',
		     );
global $kit2_module;
$kit2_module = array(
		     1 => $classcodeUrl.'/wp-content/uploads/2016/04/KIT-Module-01-Rencontre-02.pdf',
		     2 => $classcodeUrl.'/wp-content/uploads/2016/08/KIT-Module-02-Rencontre-02.pdf',
		     3 => $classcodeUrl.'/wp-content/uploads/2016/11/KIT-Module-03-Rencontre-02.pdf',
		     4 => $classcodeUrl.'/wp-content/uploads/2017/03/KIT-Module-04-Rencontre-02.pdf',
		     );
global $kit1_module_display_link;
$kit1_module_display_link="";
global $kit2_module_display_link;
$kit2_module_display_link="";

global $helpTab;
$helpTab['structure'] = "Saisissez la structure de laquelle vous dépendez (exemple: Atelier Canopé 06, Les PEPs 44, ...)";
$helpTab['adresse'] = "Saisissez l'adresse où aura lieu la rencontre elle-même";
$helpTab['cadre'] = "Saisissez le Cadre dans lequel vous suivez cette formation (ex : CERPEP)";

global $data_structures;
include_once(plugin_dir_path( __FILE__ ).'structures/ClassCode_config_structures.php');

global $data_coordinations;
include_once(plugin_dir_path( __FILE__ ).'structures/ClassCode_config_coordinations.php');
?>