<?php 

  // Test looking at https://pixees.fr/wp-content/plugins/class_code/attestation/?raw=tr

include_once(WP_PLUGIN_DIR.'/class_code/oc_api/OpenClassroomsAPI.php'); 
include_once(WP_PLUGIN_DIR.'/class_code_v2/rencontres/rencontre_type.php');

function get_user_learning_data($userID, $withOC) {
  global $OpenClassroomsAPI;
  $classcodeUrl='';
  if(empty($_SERVER["HTTPS"])){
    $classcodeUrl='http://';
  }else{
    $classcodeUrl='https://';
  }
  $classcodeUrl.=$_SERVER["SERVER_NAME"];
   
  $modules = array(
		   1 => array("title" => "#1 Module fondamental : découvrir la programmation créative", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_1_prog-creativ-300x211.png", "rencontres" => array(), "content" => '', "slug" => "decouvrir-la-programmation-creative"),
		   2 => array("title" => "#2 Module thématique : manipuler l’information", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_2_manip-info-300x211.png", "rencontres" => array(), "content" => '', "slug" => "manipuler-l-information"),
		   3 => array("title" => "#3 Module thématique : diriger les robots", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_4_diriger-robot-300x211.png", "rencontres" => array(), "content" => '', "slug" => "s-initier-a-la-robotique"),
		   4 => array("title" => "#4 Module thématique : connecter le réseau", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_3_connecter-reseau-300x211.png", "rencontres" => array(), "content" => '', "slug" => "connecter-le-reseau"),
		   5 => array("title" => "#5 Module fondamental : le processus de création de A à Z", "img" => $classcodeUrl."/wp-content/uploads/2016/03/formation_en_ligne_classcode_module_5_process-crea-300x211.png", "rencontres" => array(), "content" => '', "slug" => "gerer-un-projet-informatique-avec-des-enfants"),
		   0 => array("title" => "#0 Autres activités", "img" => $classcodeUrl."/wp-content/uploads/2017/10/image1.jpg", "rencontres" => array(), "content" => '', "slug" => ""),
		   // This is added because there is a strange PHP the last array element is overwritten by the last before last
		   array("title" => "", "img"=> "", "rencontres" => array(), "content" => '', "slug" => ""));
  
  //- echo "<pre>modules:".print_r($modules, true)."</pre>\n";

  if ($withOC){
    $courses = $OpenClassroomsAPI->getUserLearningActivity($withOC);
  }
  $user_data = get_user_meta($userID, 'OpenClassroomsAPI/UserData', true);
  //- echo "<pre>user_data:".print_r($user_data, true)."</pre>\n";
  if (is_array($user_data)) {
    $user_remote_profile = $user_data['user_public_profile'];
    if (!$withOC){
      $courses = $user_data['user_learning_activity'];
    }
  }
  $userProfile = profile_type::get_profile($user_id);
  if($userProfile['facilitator']=='true'){
    $facilitator = 'oui';
  }else{
    $facilitator = 'non';
  }
  $state="";
  if(isset($userProfile['location'])){
    $location=$userProfile['location'];
    $state=$location['state'];
  }
  $user_local_profile =
    array('ID' =>$userID,
    'avatarImg' => $userProfile['avatarImg'],    
    'displayname' => $userProfile['displayname'],
    'Prénom' => $userProfile['firstname'],
    'Nom' => $userProfile['lastname'],
    'Login' => $userProfile['username'],
    'Email' => $userProfile['email'],
    "Profil de l'apprenant"  => $userProfile['profile'],
    'Discipline informatique'  => $userProfile['computerJob'],
    'Matière enseignée en tant que professeur'  => $userProfile['teaching'],
    'Matière suivie en tant qu´étudiant'  => $userProfile['learning'],
    'Facilitateur' => $facilitator,
    'Compétences partagées ici'  => $userProfile['skills'],
    'Structure d’appartenance' => $userProfile['structure'],
    'Cadre de la formation' => $userProfile['context'],
    'Localisation régionale' => $state,
    'Pourcentage de participation au module #1' => 0,
    'Pourcentage de participation au module #2' => 0,
    'Pourcentage de participation au module #3' => 0,
    'Pourcentage de participation au module #4' => 0,
    'Pourcentage de participation au module #5' => 0,
    'Nombre de participation à une rencontre' => 0,
    'Nombre d´organisation de rencontre' => 0,
    );

  //- echo "<pre>courses:".print_r($courses, true)."</pre>\n";
  if (is_array($courses)){
    foreach($courses as $course){
      foreach($modules as $index => &$module){
        if ($module['slug'] == $course['slug']) {
	        if (isset($course['table-of-content']) && isset($course['table-of-content']['children'])) {
	          $done = 0; 
            $all = 0;
	          foreach($course['table-of-content']['children'] as $section){
	            if (isset($section['children'])){
	              foreach($section['children'] as $unit) {
		              $all++;
		              if ($unit['isCompleted'] || $unit['isDone'] || $unit['isPassed']){
		                $done++;
                  }
	              }
              }
            }
	          if ($all > 0){
	            $module['done'] = round(100*$done/$all);
	          }else{
	            $module['done'] = 0;
            }
	        }else{
	          $module['done'] = 0;
          }
	        $module['content'] .= $module['done'] == 0 ? 
	          '<p>Participation au cours en ligne.</p>' : 
	          '<p>Participation à '.$module['done'].'% du cours en ligne.</p>';
	        $user_local_profile["Pourcentage de participation au module #".$index] = $module['done'];
        }
      }
    }
  }
  $what=array();
  $what['user_id']=$userID;
  $what['mes_rencontres']=true;
  $what['where']='everywhere';
  $what['when']='past';
  $rencontres = rencontre_type::get_rencontres($what);
   
  //- echo "<pre>".print_r($rencontres, true)."</pre>\n";
  foreach($rencontres as $rencontre) {
    $tobeadded = true;
    foreach($modules as &$module){
      if ($module['title'] == $rencontre['module']) {
	      $module['rencontres'][] = $rencontre;
	      $tobeadded = false;
      }
    }
    if ($tobeadded){
      $modules[0]['rencontres'][] = $rencontre;
    }
  }
  //- echo "<pre>modules:".print_r($modules, true)."</pre>\n";
  
  foreach($modules as &$module) {
    foreach($module['rencontres'] as $meeting) {
      $meeting_url= get_site_url()."/classcode-v2/rencontres/?meetingId=".$meeting['id'];
      $meetingfulldate="";
      if((null !== $meeting['date']) && ($meeting['date']!="")){
        $meetingfulldate .= $meeting['date'];
      }
      if(($meetingfulldate != "")&& (null !== $meeting['time']) && ($meeting['time']!="")){
        $meetingfulldate .= " à ".$meeting['time'];
      }
      if($meeting['ownerId'] == $userID){
        $module['content'] .= '<p>Organisation de';
        $user_local_profile['Nombre d´organisation de rencontre']++;
      }else{
        $module['content'] .= '<p>Participation à';
        $user_local_profile['Nombre de participation à une rencontre']++;
      }
      $module['content'] .= ' la rencontre <a href="'.$meeting_url.'">#'.$meeting['id'].'</a>, ';
      if($meetingfulldate != ''){
        $module['content'] .= 'le '.$meetingfulldate;
      }
      if(isset($meeting['location'])&&($meeting['location']!="")){
        $location=$meeting['location'];
        if(isset($location['formattedAddress'])&&($location['formattedAddress']!="")){
          $module['content'] .= " A l'adresse ".$location['fotmattedAddress'];
        }
      }
      $module['content'] .= '</p>';	     
    }
  }
  unset($modules[6]);
  return array('user_local_profile' => $user_local_profile,
	       'user_remote_profile' => $user_remote_profile,
	       'courses' => $courses,
	       'rencontres' => $rencontres,
	       'modules' => $modules);
}
?>