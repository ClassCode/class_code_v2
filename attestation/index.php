<?php 

  //-echo "<pre>Le m&eacute;canisme d'attestion est en maintenance, &agrave; dans quelques jours.</pre>"; exit;

  // https://pixees.fr/wp-content/plugins/class_code_v2/attestation/index.php
  // require_once('../doc/error_for_all.php');
define('WP_USE_THEMES', false);
require_once("../../../../wp-load.php");

$classcodeUrl='';
if(empty($_SERVER["HTTPS"])){
  $classcodeUrl='http://';
}else{
  $classcodeUrl='https://';
}
$classcodeUrl.=$_SERVER["SERVER_NAME"];

// Ici https://pixees.fr/wp-content/plugins/class_code_v2/attestation?who=nicename permet à un administrateur de voir l'attestation de qq
if (in_array('administrator', wp_get_current_user()->roles) && isset($_REQUEST['who'])) {
  $user = get_user_by('slug', $_REQUEST['who']);
  if($user->ID == 0) {
    $user = get_user_by('email', $_REQUEST['who']);
    if($user->ID == 0) {
      echo "Upps '". $_REQUEST['who']."' n'est pas le slug ou email d'un user";
      exit;
    }
  }
  $withOC = false;
 } else {
  $user = wp_get_current_user();
  // Ici on rebondit si il n y a pas d'authentification
  if($user->ID == 0) {
    header('Location: '.wp_login_url(empty($_SERVER["HTTPS"]) ? "http://" : "https://").$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
    exit;
  }
  $withOC = true;
}

// Ici https://pixees.fr/wp-content/plugins/class_code_v2/attestation?noOC=true permet de récupérer l'attestation sans les données de OC
if (isset($_REQUEST['noOC']) && $_REQUEST['noOC'])
  $withOC = false;

// Ici https://pixees.fr/wp-content/plugins/class_code_v2/attestation?clean=true permet de remettre à zéro la syndication avec OC
if (isset($_REQUEST['clean']) && $_REQUEST['clean']) {
  $OpenClassroomsAPI->clearAccessToken();
  header('Location: '.$classcodeUrl.'/wp-content/plugins/class_code_v2/attestation');
}

// Ici https://pixees.fr/wp-content/plugins/class_code_v2/attestation?raw=true permet d afficher les données brutes au lieu de la certification

include_once(get_template_directory().'/_inc/display-functions.php');

require_once('./get_user_learning_data.php');

$user_learning_data = get_user_learning_data($user->ID, $withOC);
$rencontres = $user_learning_data['rencontres'];
$modules = $user_learning_data['modules'];
$userProfile = $user_learning_data['user_local_profile'];
?>
<?php
if (isset($_REQUEST['raw']) && $_REQUEST['raw']) {
  echo "<html><head> <meta charset='UTF-8'/><title>Données Class´Code</title></head><body><h1>User #".$user->ID." learning data:</h1><pre>".json_encode($user_learning_data, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK)."</pre><hr/></body></html>\n";
  exit;
}
?>

<?php header('X-Frame-Options: GOFORIT'); ?>
<!DOCTYPE html>
<html>
 <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>"/>    
  <title>Attestation Class'Code</title>
  <link href="<?php the_theme_file('/classcode.css');?>" type="text/css" rel="stylesheet" />
  <?php wp_head(); ?>
  <style>
  #profilDisplay { min-height: auto; }
  .moocsBloc { min-height:180px; height: auto; }
  #gmw-sl-wrapper-364, #profil_map_6 { display : none; }
 </style>
</head>
<body id="body" style="width:1200px"><table style="padding:0px;margin:0px;background-color:white">
<tr><td colspan="2"><img  style="padding:0px;margin:0px;width:100%" src="<?php echo $classcodeUrl; ?>/wp-content/themes/pixees-theme/_img/classcode_pictos/header-cc-02.png"></td></tr>
<tr>
  <td style="width:400px;padding:10px;font-size:18px;color:#5D5D5D;" valign="top">
    <div class="classCodeAttestationAvatar"> 
      <?php 
        class_code_user_avatar_img_display($userProfile['avatarImg']); 
      ?>
    </div>    
    <div class="classCodeAttestationyEmphasis" style="font-weight: bold;">
      <?php echo $userProfile['displayname']  ; ?>
    </div>
    <div class="classCodeAttestationEmphasisTag" style="color: #276e84;">
      <?php if($userProfile['Facilitateur']=='oui'){ ?>
        Facilitateur
      <?php } ?>
    </div>    
    <div>
      <?php echo $userProfile["Profil de l'apprenant"] ;?>
    </div>
    <div> 
    <?php 
      $skills = $userProfile['Compétences partagées ici'];
      $skillsTab = explode(",",$skills);
      foreach($skillsTab as $skill){ 
        if(trim($skill) != ""){
    ?>
      <div class="classCodeAttestationGreyRectangleSkills" style="height: 20px;text-transform: capitalize;font-weight: bold;display: inline-block;background-color: #CCCCCC;color: #5D5D5D;margin: 2px 0px;padding: 0px 5px;font-size: 14px;">
        <?php echo $skill ;?>
      </div> 
     <?php 
        }
      } 
    ?>
    </div>
<?php //echo cc_profile_shortcode(array("userID" => $user->ID)); ne marche plus en V2 ?>
      <div style="margin:5px 10px 10px;font-size:14px;font-style:italic;"><?php the_author_meta('description', $user->ID); ?></div>
      <hr/>
  </td>
  <td rowspan="2" valign="top">
    <div style="margin:0px" id="certificate">
      <h2 style="color:white;height:80px;font-size:32px;line-height:80px;text-transform:uppercase;">Attestation de participation</h2>
    </div>
    <div>
<?php foreach($modules as $module) if ($module['content'] != '') echo cc_moocblock_shortcode($module, $module['content']); ?>
    </div>
  </td>
</tr>
<tr><td style="padding:10px;max-width:400px;" valign="top">
 <div style="font-size:18px;"><img src="https://classcode.fr/projet/files/2011/12/Label-PIA-150x150.jpg" align="right" width="80"/>Class´Code est un programme de formation à destination de toutes personnes désireuses d’initier les jeunes de 8 à 14 ans à la pensée informatique. Le programme comprend de 1 à 5 modules de formation en ligne, couplé à des temps de rencontre présentielle pour partager, expérimenter et échanger entre apprenants. <br/>
  Cette attestation est validée par les partenaires du consortium et les partenaires du comité pédagogique du projet Class´Code<div>

<a href="http://www.inria.fr/mecsci"><img class="alignnone wp-image-118" src="https://classcode.fr/projet/files/2011/12/Logo_inria_fr.jpg" alt="Logo_inria_fr" width="150"  /></a> &nbsp;&nbsp;
<a href="http://magicmakers.fr"> <img class="alignnone wp-image-128" src="https://classcode.fr/projet/files/2011/12/Logo-MagicMaker.jpg" alt="Logo-MagicMaker" width="91"  /></a> &nbsp;&nbsp;
<a href="https://openclassrooms.com"><img class="alignnone wp-image-120" src="https://classcode.fr/projet/files/2011/12/Logo_OpenClassrooms.png" alt="Logo_OpenClassrooms" width="112"  /></a> <br/>
<a href="http://www.lespetitsdebrouillards.org"><img class="alignnone wp-image-119" src="https://classcode.fr/projet/files/2011/12/Logo_lesptitdeb.png" alt="Logo_lesptitdeb" width="120"  /></a>
<a href="http://www.univ-nantes.fr"><img class="alignnone wp-image-138" src="https://classcode.fr/projet/files/2011/12/Logo-UNantes.jpeg" alt="Logo-UNantes" width="140"  /></a>
<a href="http://www.societe-informatique-de-france.fr"><img class="alignnone wp-image-134" src="https://classcode.fr/projet/files/2011/12/Logo-Sif.png" alt="Logo-Sif" width="140"  /></a> <br/> &nbsp;&nbsp;
<a href="http://www.assopascaline.fr"><img class="alignnone wp-image-130" src="https://classcode.fr/projet/files/2011/12/Logo-Pascaline.jpeg" alt="Logo-Pascaline" width="100"  /></a>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="http://www.cigref.fr"><img class="alignnone wp-image-124" src="https://classcode.fr/projet/files/2011/12/Logo-Cigref.jpeg" alt="Logo-Cigref" width="94"  /></a>  &nbsp;&nbsp;
<a href="http://www.techinfrance.fr/"><img class="alignnone wp-image-935 " src="https://classcode.fr/projet/files/2016/05/techinfrance-e1463669107304.png" alt="" width="130"  /></a>  <br/>
<a href="http://www.fondation-lamap.org"><img class="alignnone wp-image-127" src="https://classcode.fr/projet/files/2011/12/FMP_logo_sans_baseline_CMJN.jpg" alt="Logo-LAMAP-web" width="150"  /></a>
<a href="https://www.reseau-canope.fr"><img class="alignnone wp-image-123" src="https://classcode.fr/projet/files/2011/12/Logo-Canope.jpeg" alt="Logo-Canope" width="100"  /></a> &nbsp;&nbsp;
<a href="http://simplon.co"><img class="alignnone wp-image-121" src="https://classcode.fr/projet/files/2011/12/Logo_simplonco.jpg" alt="Logo_simplonco" width="106"  /></a>
<a href="http://www.laligue.org/"><img class="alignnone wp-image-1088" src="https://project.inria.fr/classcode/files/2016/07/LOGO-LIGUE-BB-CMJN.png" alt="LOGO-LIGUE-BB-CMJN" width="125"  /></a> <br/>

Pour le comité pédagogique de Class´Code, <br/>Claude Terosier: <br/>
<img width="300" src="<?php echo get_site_url(); ?>/wp-content/plugins/class_code_v2/attestation/signature-cterosier.png"/><br/>
Pour le comité de pilotage de Class´Code, <br/>Colin de la Higuera, Président:<br/>
<img width="300" src="<?php echo get_site_url(); ?>/wp-content/plugins/class_code_v2/attestation/signature-cdhiguera.png"/><br/>

</td></tr></table>

</body></html>
