<?php
  function getBaseUrl() 
  {
    $hostName = $_SERVER['HTTP_HOST']; 
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
    // return: http://localhost/
    return $protocol.$hostName;
  }

  $logospage = file_get_contents(getBaseUrl().'/classcodeadmin/shufflestructureslogos');
  echo $logospage;
?>