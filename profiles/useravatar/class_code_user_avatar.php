<?php // Defines the functions to display an avatar in a page

/** \class class_code_user_avatar
 * Displays the user avatar edition/display.
 *
 * Init:
 * - class_code_user_avatar_enqueue_script() is called in his file here to enqueue the required JS scripts
 * It is used in a piece of code of the form: <pre>
 * &lt;form ... onSubmit="document.getElementById('field_<?php echo xprofile_get_field_id_from_name('Avatar'); ?>').value = JSON.stringify(myAvatarValue); document.getElementById('field_<?php echo xprofile_get_field_id_from_name('AvatarImg'); ?>').value = encodeURIComponent(document.getElementById('class_code_avatar_display').innerHTML.replace(/<!--[^>]*>/g, '').replace(/\s+/g,' '));">
 * ../..
 * &lt;div id="editAvatar" class="blueSquare">
 *   &lt;?php class_code_user_avatar_editor(xprofile_get_field_data('Avatar')); ?>
 *   &lt;input required id="field_<?php echo xprofile_get_field_id_from_name('Avatar'); ?>" type="hidden" aria-required="true" value="<?php echo xprofile_get_field_data('Avatar') ?>" name="field_<?php echo xprofile_get_field_id_from_name('Avatar'); ?>">
 *   &lt;input required id="field_<?php echo xprofile_get_field_id_from_name('AvatarImg'); ?>" type="hidden" aria-required="true" value="<?php echo xprofile_get_field_data('AvatarImg') ?>" name="field_<?php echo xprofile_get_field_id_from_name('AvatarImg'); ?>"> 
 * &lt;/div>
 * ../..
 * &lt;/form></pre>
 *
 * Display :
 * - class_code_user_avatar_img_display($avatar) is used to display the avatar as a SVG image.
 *
 * Editor :
 * - class_code_user_avatar_editor($avatar) is used to display the editor
 *
 * \ingroup userprofile
 * \extends Utility
 */

/** This function is to be called to enqueue the JS scripts. */
function class_code_user_avatar_enqueue_script() {
  add_action('wp_enqueue_scripts', function() {
      wp_enqueue_script('class_code_avatar_angular', plugins_url().
			"/class_code_v2/profiles/useravatar/angular.min.js");
      wp_enqueue_script('class_code_avatar', plugins_url().
			"/class_code_v2/profiles/useravatar/avatar.js?v=13");
    });
}
class_code_user_avatar_enqueue_script();
/** This function echoes the avatar editor.
 * @param $avatar A JSON string that specifies the initial avatar properties.
 */
function class_code_user_avatar_editor($avatar='') {
  class_code_user_avatar_set($avatar);
  $avatar_img =  plugins_url()."/class_code_v2/profiles/useravatar/avatar_img";
  echo '
<div id="avatar_classcode_E" ng-app="avatar_classcode_E" ng-controller="AvatarCtrl">
<style>
.noselect {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
<table id="class_code_user_avatar_save" class="noselect">
   <tr>
      <td>
         <div ng-repeat="part in parts" style="width:60px;padding:2px;height:20px;font-weight:bold" ng-style="{color: arrows.color}">
            {{ part.name }}
         </div>
      </td>
      <td>
         <div ng-repeat="part in parts" style="padding:2px;height:20px;">
            <div ng-include="\''.$avatar_img.'/RightArrow.svg\'" ng-click="changePart(part, -1)" style="width:20px;cursor:pointer"></div>
         </div>
      </td>
      <td >
         <div id="class_code_avatar_display" class="big" ng-repeat="transform in [bigTransform]" style="margin-top:-10px;margin-left:0px;width:100px;height:100px;" ng-include="\''.$avatar_img.'/All.svg\'" onload="avatarLoaded(\'big\')">
         </div>
      </td>
   </tr>
   <tr>
      <td colspan="3" style="padding-left:4px;">
         <div ng-repeat="color in colors" style="width:21px;height:20px;display:inline-block;margin:0px;cursor:pointer" ng-include="\''.$avatar_img.'/Paint.svg\'" ng-click="changeColor(color)"></div>
      </td>
   </tr>
</table>
</div>';
}
/** This function echoes the avatar display. 
 * @param $avatar A URI encoded string with the SVG avatar image.
 */
function class_code_user_avatar_img_display($avatar, $return = false) {
  $avatar = strlen($avatar) > 0 ? rawurldecode($avatar) : file_get_contents(plugin_dir_path( __FILE__ ).'/default.svg');
  $html = '<div style="width:100px;height:100px;" class="avatarImg">'.$avatar.'</div>';
  if ($return) return $html; else echo $html; 
}
function class_code_user_avatar_set($avatar = '') {
  if (preg_match('{.*Coiffure.*}', $avatar)) {
    echo '<script>myAvatarValue = '.$avatar.';</script>'."\n";
  }
}
?>