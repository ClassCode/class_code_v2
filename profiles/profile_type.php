<?php

/**
 * Defines a new type called profile to manage Class'Code complete user profile.Import Datas from classcodeadmin.
 *
 */
class profile_type {
  
  private static function getApiAcount(){
    require(WP_PLUGIN_DIR.'/class_code_v2/api_config.php');
    return $apiAccountArray;
  }
  
  function __construct() {
    
  }
  
  public static function set_profile($userInfosArray){
    $postdata = http_build_query($userInfosArray);
    $apiAccountArray = self::getApiAcount();
    $login = $apiAccountArray['apiLogin'];
    $pass = $apiAccountArray['apiPasswd'];
    $opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => "Authorization: Basic " . base64_encode("$login:$pass"),
        'content' => $postdata
      )
    );//'Content-type: application/x-www-form-urlencoded'.
 
    $context  = stream_context_create($opts);
    $result = file_get_contents(get_site_url().'/classcodeadmin/editpeople', false, $context);
    $json_array = json_decode($result,true); 
    return $json_array;
  }
   
  public static function get_location($user_id = false, $location_format = "coordinates"){     
    if(!$user_id){
      $user_id = wp_get_current_user()->ID;
    }
    $apiAccountArray = self::getApiAcount();
    $login = $apiAccountArray['apiLogin'];
    $pass = $apiAccountArray['apiPasswd'];
    $context = stream_context_create(array(
      'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("$login:$pass")
      )
    ));
    $json = file_get_contents(get_site_url().'/classcodeadmin/queryuserloc.json?userId='.$user_id,false, $context);
    $json_array = json_decode($json,true);   
    $location = $json_array['location']    ;
    if($location) {
      $lat = $location['lat'];
      $lng = $location['lng'];  
      if (($lat != "0" ) || ($lng != "0") ){
        if($location_format == "address"){
          return $location['formattedAddress'];
        }else if($location_format == "region"){
          return $location['state'];
        }else{
          return array($location['lat'], $location['lng']);
        }
      }else{
        return false;
      }    
    }else{
      return false;
    }             
  } 

  public static function get_profile($user_id = false){     
    if(!$user_id){
      $user_id = wp_get_current_user()->ID;
    }
    $apiAccountArray = self::getApiAcount();
    $login = $apiAccountArray['apiLogin'];
    $pass = $apiAccountArray['apiPasswd'];
    $context = stream_context_create(array(
      'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("$login:$pass")
      )
    ));
    $json = file_get_contents(get_site_url().'/classcodeadmin/queryuserprofile.json?userId='.$user_id,false, $context);
    $json_array = json_decode($json,true);   
    if(is_array($json_array)){
      return $json_array;
    }else{
      return false;
    }
  }    
  
  public static function get_all_users_with_loc(){     
    $apiAccountArray = self::getApiAcount();
    $login = $apiAccountArray['apiLogin'];
    $pass = $apiAccountArray['apiPasswd'];
    $context = stream_context_create(array(
      'http' => array(
        'header'  => "Authorization: Basic " . base64_encode("$login:$pass")
      )
    ));
    $json = file_get_contents(get_site_url().'/classcodeadmin/queryuserswithloc.json',false, $context);
    $json_array = json_decode($json,true);   
    return $json_array;
  }    
}  
?>