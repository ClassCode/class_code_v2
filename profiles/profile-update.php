<?php

define("WP_USE_THEMES", false);
require_once("../../../../wp-blog-header.php");
include_once(WP_PLUGIN_DIR.'/class_code_v2/profiles/profile_type.php');
if(isset($_POST['modaledit'])&&($_POST['modaledit']==true)){
  if (isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'profilemodal-edit')) {
    $userId = (int) wp_get_current_user()->ID;
    $beforeUpdateEmail = wp_get_current_user()->user_email ;
    $result = array('status'=>'success');
    $array_update = array('ID' => $userId);
    $pass1 = $_POST['password1'];
    $pass2 = $_POST['password2'];
    $email = ltrim(rtrim($_POST['email']));
    //checkPassword
    if(($pass1 != "") && ($pass1!=$pass2)){
      $result=array('status'=>'passwordError','message'=>'Les deux mots de passes ne correspondent pas') ;
    }else if(($pass1 != "" ) && ($pass1 == $pass2)){
      $array_update['user_pass'] = $pass1;
    }
    if(($email != '')&&($email!=$beforeUpdateEmail)){
      $array_update['user_email'] = $email;  
      $array_post = array('userId' => $userId, 'email'=>$email,'origin' => 'classcode','username'=>wp_get_current_user()->user_login);
      $result = profile_type::set_profile($array_post);

      if(!isset($result['status'])){
        $result= array ('status'=>'error','message'=>'API failed');
      }      
    }
    if($result['status']=='success'){
      
      $user_id = wp_update_user( $array_update);          
      if ( is_wp_error( $user_id ) ) {
        // There was an error, probably that user doesn't exist.
        $error = $user_id->get_error_message();   
        $result = array('status'=>'error','message'=> $error);
        //rollback
        $array_post = array('userId' => $userId, 'email'=>$beforeUpdateEmail,'origin' => 'classcode','username'=>wp_get_current_user()->user_login);
        profile_type::set_profile($array_post);
      }else{
        $result['message']="update";
      }
      $result['message']="update";
    }	
  }
}else{
  if (isset($_POST['nonce']) && wp_verify_nonce($_POST['nonce'], 'profile-edit')) {
    $userId = (int) wp_get_current_user()->ID;
    $beforeUpdateFirstName = wp_get_current_user()->first_name;
    $beforeUpdateLastName = wp_get_current_user()->last_name;
    $beforeUpdateNickName = wp_get_current_user()->nickname;
   
    $user_id = wp_update_user( array( 'ID' => $userId, 'first_name' => ltrim(rtrim($_POST['firstname'])),'last_name' => ltrim(rtrim($_POST['lastname'])),'nickname'=> ltrim(rtrim($_POST['nickname'])) ));
          
    $error = "";
    $success = "";
    if ( is_wp_error( $user_id ) ) {
      // There was an error, probably that user doesn't exist.
      $error = $user_id->get_error_message();   
      $result = array('status'=>'error','wp_user update failed');
    } else {
      $_POST['userId'] = $userId;
      $_POST['username'] = wp_get_current_user()->user_login;
      $_POST['origin'] = "classcode";
      $result = profile_type::set_profile($_POST);
      if(!isset($result['status'])){
        $result= array ('status'=>'error','message'=>'API failed');
      }else{
        if($result['status']=='error'){
          //rollback
          $user_id = wp_update_user( array( 'ID' => $userId, 'first_name' => $beforeUpdateFirstName,'last_name' => $beforeUpdateLastName,'nickname'=> $beforeUpdateNickName ) );      
        }
      }        
      // Success!
    }		
  }else{
    $result=array('status'=>'error','message'=>'nonce expired') ;
  }
}
$action='';
if($result['status']=='success'){
  $action=$result['message'];
}elseif($result['status']=='passwordError'){
  $action='passworderror';
}else{
  $action='error';
}
header('Location: '.get_site_url().'/classcode-v2/editer-votre-profile/?action='.$action);

?>