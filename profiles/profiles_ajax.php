<?php

define("WP_USE_THEMES", false);
require_once("../../../../wp-blog-header.php");
include_once(WP_PLUGIN_DIR.'/class_code_v2/profiles/profile_type.php');

if (isset($_GET['ajax_profile']) and isset($_GET['user_id'])) {
  $user_id = $_GET['user_id'];
  if(user_id_exists($user_id)){
    $userProfile = profile_type::get_profile($user_id);
  ?>
    <div class="classCodeMapOverlayContentLogo"> 
      <?php 
        class_code_user_avatar_img_display($userProfile['avatarImg']); 
      ?>
    </div>
    
    <div class="classCodeMapOverlayEmphasis">
      <?php echo $userProfile['displayname']  ; ?>
    </div>
    <div class="classCodeMapOverlayEmphasisTag">
    <?php if($userProfile['facilitator']){ ?>
      Facilitateur
    <?php }else{ ?>   
      
    <?php } ?>   
    </div>    
    <div>
      <?php echo $userProfile['profile'] ;?>
    </div>
    <div> 
    <?php 
      $skills = $userProfile['skills'];
      $skillsTab = explode(",",$skills);
      foreach($skillsTab as $skill){ 
        if(trim($skill) != ""){
    ?>
      <div class="overlayGreyRectangleSkills">
        <?php echo $skill ;?>
      </div> 
    <?php 
        }
      } 
    ?>
    </div>
  <?php 
    if (is_user_logged_in() && $user_id != wp_get_current_user()->ID) { 
      $who = trim($userProfile['firstname']." ".$userProfile['lastname']);
      if($who == ''){
        $who = $userProfile['displayname'];
      }
      echo '<a class="classCodeMapOverlayLink" href="'.cc_mailto_shortcode(array("to" => $userProfile['email'], "who" => $who, "subject" => "À propos de Class´Code","logo"=> "participant")).'"> > Contactez</a>';
    } 
  ?> 
    </div>    
<?php  
  }
}

function user_id_exists($user){
    global $wpdb;
    $count = $wpdb->get_var($wpdb->prepare("SELECT COUNT(ID) FROM $wpdb->users WHERE ID = %d", $user));
    if($count == 1){ return true; }else{ return false; }
}
?>