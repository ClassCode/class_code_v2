<?php

$http_array=array();
$interstices_array=array();

$wp_path = "/appli/www/pixees/";
$cwd = getcwd();
chdir( $wp_path );
include "wp-load.php";
$posts = get_posts(array('numberposts'=>'-1'));
foreach ($posts as $key => $post){
  $post_content = $post->post_content;
  if(strpos($post_content, 'src="http://')){
	$http_array[]= "https://pixees.fr/wp-admin/post.php?post=".$post->ID."&action=edit";
  }
  if(strpos($post_content, "src='http://")){
	$http_array[]= "https://pixees.fr/wp-admin/post.php?post=".$post->ID."&action=edit";
  }
  if(strpos($post_content, 'src="https://interstices.info')){
	$interstices_array[]= "https://pixees.fr/wp-admin/post.php?post=".$post->ID."&action=edit";
  }
  if(strpos($post_content, "src='https://interstices.info")){
	$interstices_array[]= "https://pixees.fr/wp-admin/post.php?post=".$post->ID."&action=edit";
  }
}
print "<!DOCTYPE html>\n<html>\n";
print "<head>\n";
print "  <meta charset='utf-8'>\n";
print "</head>\n";
print "<body>\n";
print "<span style='text-decoration:underline;font-weight:bold;'>src='Http:// ou src=\"Http (".count($http_array).")</span><br>";
foreach ($http_array as $url){
	print "<a href='".$url."' target='_blank'>".$url."</a><br>";
}
print "<br><br><span style='text-decoration:underline;font-weight:bold;'>src='https://interstices.info ou src=\"https://interstices.info  (".count($interstices_array).")</span><br>";
foreach ($interstices_array as $url){
	print "<a href='".$url."' target='_blank'>".$url."</a><br>";
}
print "\n</body>\n</html>\n";
chdir( $cwd );

?>