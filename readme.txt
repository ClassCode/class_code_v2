=== Plugin Name ===
Contributors: chiron.denis@gmail.com, benjamin.ninassi@inria.fr
Donate link: http://sparticipatives.gforge.inria.fr/wp-plugins/#donate
Tags: Class'Code
Requires at least: 3.0.1
Tested up to: 4.8
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin integrates the wordpress Class'Code project V2 functionality

== Description ==

This plugin integrates the wordpress Class'Code project V2 functionality

== Installation ==

*Manual installation*

1. Download the [ZIP](http://sparticipatives.gforge.inria.fr/wp-plugins/index/class_code.zip) file.
2. In your WordPress `Dashboard -> Plugins -> Add new` page choose `upload plugin in .zip format via this page`
3. Browse and select the class_code.zip to upload
4. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

*Any question or suggestion?*

Simply contact us via http://sparticipatives.gforge.inria.fr/wp-plugins

== Screenshots ==

== Changelog ==

= 0.0 =


== Other notes ==

