<?php

function get_temoignages() {
  global $data_temoignages;
  return $data_temoignages;
}
	
$data_temoignages = array(
		array(
			"id"     => "QGMbeVyfqWU",
			"titre"  => "Témoignage d'une participante de Class´Code : les points forts de la formation"
		),
		array(
			"id"     => "_e0WeBpWzT0",
			"titre"  => "Témoignage d'une participante de Class´Code : qu'offre cette formation"
		),
		array(
			"id"     => "bIBw1HCn6rA",
			"titre"  => "Témoignage de facilitateur-e-s de Class´Code : avant la rencontre"
		),
		array(
			"id"     => "G3D9FvIfvOo",
			"titre"  => "Témoignage de facilitateur-e-s de Class´Code : après la rencontre"
		),
		array(
			"id"     => "-MdMyCAtuw8",
			"titre"  => "Témoignage de Class´Code à l'assemblée nationale"
		),
		array(
			"id"     => "-dP7OGAVgJU",
			"titre"  => "Témoignage de Class´Code du secrétariat d'état au numérique"
		),
		array(
			"id"     => "BtHBRp2z0n4",
			"titre"  => "Témoignage de Class´Code à l'exploradome"
		),
		array(
			"id"     => "FjEXxlTeaVI",
			"titre"  => "Témoignage de Class´Code à la direction au numérique de Poitiers"
		),
		array(
		      "id"     => "qRb7cw24Egc",
		      "titre"  => "Témoignage d'une animatrice jeunesse à Laval"
  	        ),

		array(
		      "id"     => "8E7-SUZcd3E",
		      "titre"  => "Témoignage de la directrice du laboratoire LINE en sciences de l'éducation"
  	        ),

		array(
		      "id"     => "f3CKIPv0_BA",
		      "titre"  => "Témoignage d'une enseignante référente aux usages numériques"
  	        ),

		array(
		      "id"     => "1rwWb4CXTSY",
		      "titre"  => "Témoignage d'une professeure des écoles spécialisée"
  	        ),

		array(
		      "id"     => "bJZbcRuig4U",
		      "titre"  => "Témoignage de professeures des écoles formées à Class´Code-MED"
  	        ),

		array(
		      "id"     => "I9EjJmj2sAw",
		      "titre"  => "Témoignage de la coordinatrice des Petits Débrouillards dans les Alpes Maritimes"
  	        ),
		array(
		      "id"     => "leQQg2pT-L8",
		      "titre"  => "Témoignage d'un enseignant chercheur de l'IUT de l'université de Bordeaux"
  	        ),
		array(
		      "id"     => "A5X49uj-l_k",
		      "titre"  => "Témoignage d'une enseignante de l'académie de Bordeaux"
  	        ),
		array(
		      "id"     => "nEs8eIkaUMo",
		      "titre"  => "Témoignage d'un enseignant du centre pilote de la main à la pâte de Bergerac"
  	        ),
		array(
		      "id"     => "pHcpj2s2LfY",
		      "titre"  => "Témoignage de l'Union Départementale des Associations Familiales"
  	        ),
		array(
		      "id"     => "vCvHDDuWVYE",
		      "titre"  => "Témoignage des ateliers Canopé 33 et 19"
  	        ),
		array(
		      "id"     => "GnQuVz6UP1M",
		      "titre"  => "Témoignage de Vincent Besnard, Rectorat de l'académie de Bordeaux"
  	        ),
		array(
		      "id"     => "r_Ehh9vWgbw",
		      "titre"  => "Témoignage de Johanna Gateu, coordinatrice PEP Atlantique Anjou"
  	        ),
		array(
		      "id"     => "Dp1MRpUy1zc",
		      "titre"  => "Témoignage de Béatrix Vincent, Conseillère pédagogique"
  	        ),
		array(
		      "id"     => "zphYMtW-L94",
		      "titre"  => "Témoignage de Yanis blancas, Professeur des écoles"
  	        ),
		array(
		      "id"     => "W94wpeZFqpk",
		      "titre"  => "Témoignage d'ambassadrices du numérique"
  	        ),
		/*
		array(
		      "id"     => "",
		      "titre"  => ""
  	        ),
		*/
		array(
			"id"     => "I0zGkwGlons",
			"titre"  => "Temps de rencontre Class´Code 1/2"
		),
		array(
			"id"     => "04vZ-MIlFRo",
			"titre"  => "Temps de rencontre Class´Code 2/2"
		),
		array(
			"id"     => "OeZQsuSFcxE",
			"titre"  => "Programmation créative avec Scratch ? Visitons une classe."
		),
	);


// echo get_temoignages();
?>
