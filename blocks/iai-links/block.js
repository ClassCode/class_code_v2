( function( blocks, editor, i18n, element, components ) {
	var __ = i18n.__;
	var el = element.createElement;
	var RichText = editor.RichText;
	var Select = components.SelectControl;

	blocks.registerBlockType( 'classcode2/iai-links', {
		title: __( 'Lien ClassCode IAI', 'gutenberg-examples' ),
		icon: 'index-card',
		category: 'layout',
		attributes: {
			title: { type: 'string' },
			description: {type: 'string' },
			link:  {type: 'string' },
			linkType:  {type: 'string' },
			linkTheme:  {type: 'string' }
		},

		example: {
			attributes: {
				title: '',
				description: '',
				link: '',
				linkType: 'article',
				linkTheme: 'se_questionner'
			}
		},

		edit: function( props ) {
			var attributes = props.attributes;

			return el(
				'div',
				{ className: props.className },
				el(
					'div',
					{ className: 'column' },
					el('label', {}, 'Titre du lien :'),
					el( RichText, {
						tagName: 'p',
						inline: true,
						placeholder:'Titre',
						value: attributes.title,
						onChange: function( value ) {
							props.setAttributes( { title: value } );
						}
					}),
					el('label', {}, 'Description :'),
					el( RichText, {
						tagName: 'p',
						inline: true,
						placeholder:'Description',
						value: attributes.description,
						onChange: function( value ) {
							props.setAttributes( { description: value } );
						}
					}),
					el('label', {}, 'URL :'),
					el( RichText, {
						tagName: 'p',
						inline: true,
						placeholder:'Lien',
						value: attributes.link,
						onChange: function( value ) {
							props.setAttributes( { link: value } );
						}
					})
				),
				el(
					'div',
					{ className: 'column' },
					el('label', {}, 'Thème :'),
					el(Select, {
						value: attributes.linkTheme,
						options: [
								{ label: 'Sélectionner une rubrique', value: '' },
								{ label: 'Se questionner', value: 'se_questionner' },
								{ label: 'Expérimenter', value: 'experimenter' },
								{ label: 'Découvrir', value: 'decouvrir' }
						],
						onChange: function( value ) {
							props.setAttributes( { linkTheme: value } );
						}
					}),
					el('label', {}, 'Type de lien :'),
					el(Select, {
						value: attributes.linkType,
						options: [
							    { label: 'Sélectionner un type de lien', value: '' },
								{ label: 'Article', value: 'article' },
								{ label: 'Vidéo', value: 'video' },
								{ label: 'Audio', value: 'audio' },
								{ label: 'Appli', value: 'appli' },
								{ label: 'Fiche', value: 'fiche' }
						],
						onChange: function( value ) {
							props.setAttributes( { linkType: value } );
						}
					})
				)
			);
		},
		save: function( props ) {
			var attributes = props.attributes;

			return el(
				'div',
				{ className: props.className },
				el( 'h3', {}, 'Titre' ),
				el( RichText.Content, {
					tagName: 'p',
					value: attributes.title
				} ),
				el( 'h3', {}, 'Description' ),
				el( RichText.Content, {
					tagName: 'p',
					value: attributes.description
				} ),
				el( 'h3', {}, 'Lien' ),
				el( RichText.Content, {
					tagName: 'p',
					value: attributes.link
				} )
			);
		}
	} );

	/*

	var withInspectorControls = wp.compose.createHigherOrderComponent( function( BlockEdit ) {
		return function( props ) {
			return el(
				wp.element.Fragment,
				{},
				el(
					BlockEdit,
					props
				),
				el(
					wp.editor.InspectorControls,
					{},
					el(
						wp.components.PanelBody,
						{},
						'My custom control'
					)
				)
			);
		};
	}, 'withInspectorControls' );

	wp.hooks.addFilter( 'editor.BlockEdit', 'my-plugin/with-inspector-controls', withInspectorControls );

	*/

} )(
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element,
	window.wp.components,
	window._
);

