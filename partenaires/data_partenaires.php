<?php
 
function get_partenaires($count = false) {
  global $data_partenaires;
  $data = $data_partenaires;
  // Mélange les éléments pour les afficher au hasard
  shuffle($data);
  if ($count)
    $data = array_splic($date, $count);
  return $data;
}
	
$data_partenaires = 
    array(
	  array('name' => 'Inria',
		'logo' => 'Logo_Inria.jpg',
		'url' => 'http://www.inria.fr/mecsci',
		'group' => 'Groupe1-consortium'),
	  array('name' => 'Magic Makers',
		'logo' => 'Logo-MagicMakers.jpg',
		'url' => 'http://magicmakers.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' => 'OpenClassrooms',
		'logo' => 'Logo-OpenClassrooms.jpg',
		'url' => 'https://openclassrooms.com',
		'group' => 'Groupe1-consortium'),
	  array('name' => 'Les Petits Débrouillards',
		'logo' => 'Logo-PetitsDebrouillards.jpg',
		'url' => 'http://www.lespetitsdebrouillards.org',
		'group' => 'Groupe1-consortium'),
	  array('name' => 'Université de Nantes',
		'logo' => 'Logo-UNantes.jpeg',
		'url' => 'http://www.univ-nantes.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' => 'Société Informatique de France',
		'logo' => 'Logo-Sif.jpg',
		'url' => 'http://www.societe-informatique-de-france.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' => 'Pasc@line',
		'logo' => 'Logo-Pascaline.jpg',
		'url' => 'http://www.assopascaline.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'Cigref',
		'logo' =>'Logo-Cigref.jpg',
		'url' => 'http://www.cigref.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'Tech.in France',
		'logo' =>'Logo-TechInFrance.jpg',
		'url' => 'http://www.techinfrance.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'Université d´Évry',
		'logo' =>'Logo-UEvry.jpeg',
		'url' => 'http://www.univ-evry.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'La Main à la pâte',
		'logo' =>'Logo-LaManalaPate.jpg',
		'url' => 'http://www.fondation-lamap.org',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'Canopé',
		'logo' =>'Logo-Canope.jpg',
		'url' => 'https://www.reseau-canope.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'Les PEPs',
		'logo' =>'Logo-Pep.jpg',
		'url' => 'http://www.lespep.org',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'Simplon.co',
		'logo' =>'Logo-Simplon.jpg',
		'url' => 'http://simplon.co',
		'group' => 'Groupe1-consortium'),
	  array('name' => 'Université Côte d´Azur',
		'logo' => 'Logo-UCoteDazur.jpeg',
		'url' => 'http://univ-cotedazur.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'Région Pays de la Loire',
		'logo' =>'Logo-Pdll.jpg',
		'url' => 'Région Provence Alpes Côte d´Azur',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'http://www.paysdelaloire.fr',
		'logo' =>'Logo-Paca.jpg',
		'url' => 'http://www.regionpaca.fr',
		'group' => 'Groupe1-consortium'),
	  array('name' =>'CAI community',
		'logo' =>'Logo-CAI.jpg',
		'url' => 'https://cai.community',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Ligue de l´Enseignement',
		'logo' =>'Logo-LiguedelEnseignement.jpg',
		'url' => 'http://www.laligue.org',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Ceméa',
		'logo' =>'Logo-Cemea.jpg',
		'url' => 'http://www.cemea.asso.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Université de Franche Comté',
		'logo' =>'Logo-UFRancheComte.jpg',
		'url' => 'http://www.univ-fcomte.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Universités des Antilles',
		'logo' =>'Logo-UAntilles.jpg',
		'url' => 'http://www.univ-ag.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Université de Lille',
		'logo' =>'Logo-ULille.jpg',
		'url' => 'http://www.univ-ag.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Ping',
		'logo' =>'Logo-Oing.jpg',
		'url' => 'http://www.pingbase.net',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Maisons pour la science',
		'logo' =>'Logo_MaisonSciences.jpg',
		'url' => 'http://www.maisons-pour-la-science.org',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'La compagnie du code',
		'logo' =>'Logo-CompagnideduCode.jpg',
		'url' => 'http://www.lacompagnieducode.org',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Fantastic<>de',
		'logo' =>'Logo-Fantasticode.jpg',
		'url' => 'http://www.fantasticode.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Espace Mendès France',
		'logo' =>'Logo-EspaceMendesFrance.jpg',
		'url' => 'http://emf.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Centre social du Chemillois',
		'logo' =>'Logo-CentreSocialChemillois.jpg',
		'url' => 'http://centresocial-chemille.asso.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Code Club',
		'logo' =>'Logo-CodeClub.jpg',
		'url' => 'http://www.clubcode.org',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Médiathèque de la CASA',
		'logo' =>'Logo-MediathequeAntibe.jpg',
		'url' => 'http://www.ma-mediatheque.net',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Coding School',
		'logo' =>'Logo-CodingSchool.jpg',
		'url' => 'http://www.codingschool.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Digital Wallonia',
		'logo' =>'Logo-DigitalWallonia.jpg',
		'url' => 'https://www.digitalwallonia.be',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'HatLab',
		'logo' =>'Logo-HatLab.jpg',
		'url' => 'http://www.hatlab.fr',
		'group' => 'Groupe2-maillage'),
	  array('name' =>'Enseignement Public et Informatique',
		'logo' =>'Logo-Epi.jpg',
		'url' => 'http://epi.asso.fr',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Je.code()',
		'logo' =>'Logo-jeCode.jpg',
		'url' => 'http://jecode.org',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Institut National des Sciences Appliquées, Centre Val de Loire',
		'logo' =>'Logo-Insa.jpg',
		'url' => 'http://www.insa-centrevaldeloire.fr',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Fédération des Parent d´Élèves de l´Enseignement Public',
		'logo' =>'Logo-peep.jpg',
		'url' => 'http://peep.asso.fr',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'École Normale Supérieur de Rennes',
		'logo' =>'Logo-EnsRennes.jpg',
		'url' => 'http://www.ens-rennes.fr',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Wild code school',
		'logo' =>'Logo-WildeCodeSchool.jpg',
		'url' => 'http://www.wildcodeschool.fr',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Colombbus',
		'logo' =>'Logo-Columbbus.jpg',
		'url' => 'http://www.colombbus.org',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Tralalere',
		'logo' =>'Logo-Tralalere.jpg',
		'url' => 'http://www.tralalere.com',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Centre National de Recherche Scientifique, INS2I',
		'logo' =>'Logo-Cnrs.jpg',
		'url' => 'http://www.cnrs.fr/ins2i',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Educ Azur',
		'logo' =>'Logo-EducAzur.jpg',
		'url' => 'https://educazur.fr',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Commision Nationale de l´Informatique et des Libertés',
		'logo' =>'Logo-Cnil.jpg',
		'url' => 'https://www.cnil.fr',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'France-IOI',
		'logo' =>'Logo-FranceIOI.jpg',
		'url' => 'http://www.france-ioi.org',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Fondation Blaise Pascal',
		'logo' =>'Logo-FBP.jpg',
		'url' => 'https://www.fondation-blaise-pascal.org',
		'group' => 'Groupe3-soutien'),
	  array('name' =>'Caisse des dépots et consignation',
		'logo' =>'Logo-CDC.jpg',
		'url' => 'http://www.caissedesdepots.fr',
		'group' => 'Groupe4-institutionnels'),
	  array('name' =>'Commissariat Général à l´Investissement',
		'logo' =>'Logo-CGI.jpg',
		'url' => 'http://www.gouvernement.fr/investissements-d-avenir-cgi',
		'group' => 'Groupe4-institutionnels'),
	  array('name' =>'Ministères de l´économie et des finances, dont l´industrie et le numérique',
		'logo' =>'Logo-MEIN.jpg',
		'url' => 'https://www.economie.gouv.fr',
		'group' => 'Groupe4-institutionnels'),
	  array('name' =>'Ministères de l´Éducation Nationale, et de l´Enseignement Supérieur, de la Recherche et de l´Innovation',
		'logo' =>'Logo-MENSR.jpg',
		'url' => 'http://www.education.gouv.fr',
		'group' => 'Groupe4-institutionnels'),
	  );

// ref : https://project.inria.fr/classcode/
// echo get_logos_partenaires();
?>
