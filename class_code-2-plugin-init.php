<?php

/**
 * Makery live
 *
 * @wordpress-plugin
 * Plugin Name:       Class'Code 2
 * Description:       
 * Version:           1.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

if (defined('CLASSCODE2_PLUGIN_URL')) {
   wp_die('Il semble qu\'une autre version du plugin "ClassCode 2" soit active. Merci de la déactiver avant d\'utiliser cette version');
}

define('CLASSCODE2_VERSION', '1.00');
define('CLASSCODE2_ROUTE', '/classcode-v2');
define('CLASSCODE2_IAI_ROUTE', '/iai');
define('CLASSCODE2_PLUGIN_URL', plugin_dir_url(__FILE__));
define('CLASSCODE2_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('CLASSCODE2_PLUGIN_BASE_NAME', plugin_basename(__FILE__));
define('CLASSCODE2_PLUGIN_FILE', basename(__FILE__));
define('CLASSCODE2_PLUGIN_FULL_PATH', __FILE__);

require_once( CLASSCODE2_PLUGIN_DIR . 'class_code-2-plugin-class.php' );
add_action( 'plugins_loaded', array( 'CLASSCODE2_Plugin', 'get_instance' ) );

?>
