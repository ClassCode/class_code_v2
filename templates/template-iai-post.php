<?php

 /*
 
 Template Name: ClassCode IAI Accueil Parcours
 
 */

require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/header.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/header-menu-javascript.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/iai/header-article.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/iai/side-nav-parcours.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/iai/display.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/iai/footer-javascript.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/footer.php');

?>
