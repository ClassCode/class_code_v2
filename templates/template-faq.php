<?php

 /*
 
 Template Name: ClassCode 2 FAQ
 
 */

require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/header.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/header-menu-javascript.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/faq/questions-faq.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/footer.php');

?>
