<?php

 /*
 
 Template Name: ClassCode IAI Module 2
 
 */

require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/header.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/header-menu-javascript.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/iai/header.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/iai/side-nav.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/iai/module2.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/iai/footer-javascript.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/footer.php');

?>
