
	
	</div><!-- fin page-content cf header -->
	
	<footer>
		<div class="footer-column">
			<div class="footer-column-title">Les ressources</div>
			<ul>
			   <li><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>activite-branchee" >Programmer</a></li>
			   <li><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>activite-debranchee" >Jouer</a></li>
			   <li><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>prendre-du-recul" >Découvrir</a></li>
			   <li><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>pedagogie" >Transmettre</a></li>
			</ul>
		</div>
		<div class="footer-column">
			<div class="footer-column-title">Les parcours</div>
			<ul>
				<li><a href="<?php echo site_url( "classcode/formations/module1" ); ?>">#1 Découvrir la programmation créative</a></li>
				<li><a href="<?php echo site_url( "classcode/formations/module2" ); ?>">#2 Manipulez l'information</a></li>
				<li><a href="<?php echo site_url( "classcode/formations/module3" ); ?>">#3 Initiez-vous à la Robotique</a></li>
				<li><a href="<?php echo site_url( "classcode/formations/module4" ); ?>">#4 Connectez le Réseau</a></li>
				<li><a href="<?php echo site_url( "classcode/formations/module5" ); ?>">#5 Gérer un projet informatique avec des enfants</a></li>
				<li><a href="<?php echo site_url( "/mooc-icn-de-linformatique-de-la-creation-du-numerique-des-le-20-fevrier"     );     ?>">Informatique et Création Numérique</a></li>
				<li><a href="<?php echo site_url( "/123-codez-suivez-a-votre-guise-le-guide-2"); ?>">Les ressources «1,2,3…codez!»</a></li>
				<li><a href="https://www.fun-mooc.fr/en/cours/sinitier-a-lenseignement-en-sciences-numeriques-et-technologie/">SNT : Sciences du Numérique et Technologie</a></li>
                <li><a href="<?php echo site_url( "classcode-v2/iai" ); ?>">IAI : Formation citoyenne à l’intelligence artificielle intelligente</a></li>
			</ul>
		</div>
		<div class="footer-column">
			<div class="footer-column-title">Les contacts</div>
			<ul>
				<li><a href="<?php echo $is_accueil_cc_v2 ? "#mapWidget" : site_url( CLASSCODE2_ROUTE. "#mapWidget" ); ?>"  >Coordinations</a></li>
				<li><a href="<?php echo $is_accueil_cc_v2 ? "?structureSearch=true#mapWidget" : site_url( CLASSCODE2_ROUTE . "?structureSearch=true#mapWidget" ); ?>" >Partenaires</a></li>
				<li><a href="<?php echo $is_accueil_cc_v2 ? "?meetingSearch=true#mapWidget" : site_url( CLASSCODE2_ROUTE . "?meetingSearch=true#mapWidget" ); ?>" >Rencontres</a></li>
				<li><a href="<?php echo $is_accueil_cc_v2 ? "?userSearch=true#mapWidget" : site_url( CLASSCODE2_ROUTE . "?userSearch=true#mapWidget" ); ?>" >Participants</a></li>
			</ul>	
<br/>
		<div class="footer-column-title"><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/le-projet/#presse"); ?>">Pour la presse</a></div>

		</div>
		<div class="footer-column">
			<ul>
				<li><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/faq"); ?>">FAQ</a></li>
				<li><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/contacts"); ?>">Contact</a></li>
				<li><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/l-association"); ?>">Qui sommes-nous ?</a></li>
				<li><a href="https://project.inria.fr/classcode/" target="_blank">Le blog du projet</a></li>
			</ul>
			<br/>
			<div class="footer-column-title"><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/credits"); ?>">Mentions légales</a></div>
		</div>
		<div class="footer-column">
			<p style="text-align: center"><img src="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/images/logo_investissement_avenir.png" alt="" width="95" height="95" /></p>
			<p><img src="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/images/logo-classcode.png" alt="" /></p>
		</div>
		<div class="clear"></div>
	</footer>
		
<?php get_footer(); ?>