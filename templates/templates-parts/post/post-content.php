<div class="classcode-v2 content-category" >

	<div class="content-wp blog-post">
	
	<?php
		
		// Filter : URL d'une catégorie
		function changeCategoryPermalinks($permalink, $category) {
		   
		   $term = get_term_by( 'id', $category, 'category' );
		   return site_url( CLASSCODE2_ROUTE. "/a-la-carte/" . $term->slug );
		   
		}
		
		// Filter : liste des catégories du post
		function filterCategories( $post_categories, $post_id ) {
			
			// Slugs des categories du post :
			$slugs = array();
			
			// Tableau associatif : servira à retrouver la catégorie à partir du slug
			$slugs_assoc = array();
			
			foreach($post_categories as $category) {
				
				// Liste linéaire
				array_push ( $slugs, $category->slug );
				
				// Liste associative
				$slugs_assoc[ $category->slug ] = $category;
			}
		   
			// On récupère les catégories du moteur "à la carte" associées au post :
			$categories_a_la_carte = get_post_categories_a_la_carte( $slugs );
		   
			// Elles sont retournées par rubrique :
			$categories_parcours     = $categories_a_la_carte["parcours"];
			$categories_parcours_icn = $categories_a_la_carte["parcours_icn"];
			$categories_thematiques  = $categories_a_la_carte["thematiques"];
			$categories_ressources   = $categories_a_la_carte["ressources"];
			
			// Dans l'ordre d'affichage désiré :
			$categories_a_la_carte_slugs = array();
			
			if (is_array($categories_parcours)){
				array_push( $categories_a_la_carte_slugs, $categories_parcours["slug"]);
			}
			if (is_array($categories_parcours_icn) > 0){
				array_push( $categories_a_la_carte_slugs, $categories_parcours_icn["slug"]);
			}
			if (is_array($categories_thematiques) > 0){
				array_push( $categories_a_la_carte_slugs, $categories_thematiques["slug"]);
			}
			if (is_array($categories_ressources) > 0){
				array_push( $categories_a_la_carte_slugs, $categories_ressources["slug"]);
			}
			
			// Cas particulier des contenus en anglais
			array_push( $categories_a_la_carte_slugs, "informatics-and-digital-creation");
            
			$post_categories = array();
			
			$libelles_categories = array_merge(
				get_libelles_categories_parcours(),
				get_libelles_categories_parcours_icn(),
				get_libelles_categories_thematiques(),
				get_libelles_categories_ressources(),
				array(
				  "informatics-and-digital-creation" => "Informatics and Digital Creation"
                                )
			);
			
			foreach( $categories_a_la_carte_slugs as $cat_slug ) {
				
				$category_from_slug = $slugs_assoc[ $cat_slug ];
				
				if ( array_key_exists( $cat_slug , $libelles_categories )) {
					$category_from_slug->name = $libelles_categories[$cat_slug];
				}
				
				array_push( $post_categories, $category_from_slug);
			}
			
			return $post_categories;
       }
		
		// Filter : liste des catégories du post
		function formatCategories(  $output ) {
			return str_replace(" . ", " | ", $output);
		}
		
		
		// Filter : liste des catégories du post
		function formatTags( $output ) {
			return "";
		}
		
	   add_filter( 'category_link', changeCategoryPermalinks, 10, 2 );
	   add_filter( 'the_category_list', filterCategories, 10, 2 );
	   add_filter( 'the_category', formatCategories, 10, 1 );
	   add_filter( 'the_tags', formatTags, 10, 1 );

       global $post;
		
	   $post_categories = wp_get_post_terms( $post->ID , "category");
       $slugs = array();
		
       foreach($post_categories as $category) {
			array_push ( $slugs, $category->slug );
	   }
       $categories_a_la_carte = get_post_categories_a_la_carte( $slugs );
       $categories_parcours = $categories_a_la_carte["parcours"];
       $categories_parcours_slug = $categories_parcours["slug"];

	   // Liens extérieurs : 1,2,3 Codez, openClassRoom, etc...
       $liens_exterieurs = get_liens_parcours();
		
	   // Libellés courts des parcours
	   $libelles_ressource = get_libelles_categories_parcours();
		
	   $lien_vers_ressource = $liens_exterieurs[$categories_parcours_slug];
	   $libelle_ressource = $libelles_ressource[$categories_parcours_slug];
		
	   switch($categories_parcours_slug) {
		   case "module-1":
		   case "module-2":
		   case "module-3":
		   case "module-4":
		   case "module-5":
			   $banner_parcours = "assets/images/post/ClassCode-v2-ecrans-14.jpg";
			   $lien_vers_module = site_url() . "/classcode/formations/" . str_replace("-", "", $categories_parcours_slug);
			   break;
		   case "mooc-icn-informatique-et-creation-numerique":
			   $banner_parcours = "assets/images/post/ClassCode-v2-ecrans-17.jpg";
			   break;
		   case "1-2-3-codez":
			   $banner_parcours = "assets/images/post/ClassCode-v2-ecrans-16.jpg";
			   break;
	   }
		
	   if ($banner_parcours) {
		   $banner_alt = $categories_parcours->name;
		   echo '<div class="classcode-v2-post-banner"><img src="'. CLASSCODE2_PLUGIN_URL . $banner_parcours .'" alt="'. $banner_alt .'"/></div>';
	   }
		
	   if ($lien_vers_ressource || $lien_vers_module ) {
		   echo '<div class="liens-exterieurs-article">';
		   if ($lien_vers_module) {
			   echo '<span class="lien"><a href="'.$lien_vers_module .'" target="_blank">'. $libelle_ressource .' - Class’Code</a></span>';
		   }
		   if ($lien_vers_ressource) {
			   if ($lien_vers_module) {
			   	  echo " / ";
			   }
			   echo '<span class="lien"><a href="'. $lien_vers_ressource["url"] .'" target="_blank">'. $lien_vers_ressource["title"] .'</a></span>';
		   }
		   echo '</div>';
	   }
		
       the_theme_posts(true, true, true, false); 
		
	   remove_filter( 'category_link', changeCategoryPermalinks );
	   remove_filter( 'the_category_list', filterCategories );
	   remove_filter( 'the_category', formatCategories);
	   remove_filter( 'the_tags', formatTags);
		
	?>
	
	</div>	
</div><!-- classcode-v2 -->