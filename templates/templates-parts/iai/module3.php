<div class="iai-content">
    <div id="se_questionner">
        <h2>Les humains sont-ils bons pour la casse ?</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Se questionner</span> : Vidéo | 1min50 | Sous-titres | Réalisation : 4 minutes 34 |
                    <span class="iai-video-share"><a href="#"></a></span>
                </p>
                <div class="iai-video-parent">
                    <a href="#" data-video="https://www.youtube.com//embed/rEXOzNGAWFQ?enablejsapi=1" data-title="Les humains sont-ils bons pour la casse ?">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/Q3.jpg" alt="" />
                    </a>
                </div>
            </div>
            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    “Dites, vous pensez que les intelligences artificielles pourraient prendre notre boulot ?". Oui, non ? "Peut-on prédire ce qui sera techniquement automatisable ?"
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m3-fichepedago-sequestionner.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Impacts de la révolution technologique et numérique</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links">
            <a href="#" class="iai-content-links-header">
                <p><strong>Décryptons les médias</strong> grâce à des liens vers des ressources complémentaires</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('se_questionner'); ?>
            </ul>
        </div>
    </div>
    <div id="experimenter">
        <h2>Créons avec l'IA</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <div class="iai-tuto-parent">
                    <a href="https://pixees.fr/classcodeiai/app/tuto3/" target="_blank">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/tuto3.png" alt="tutoriel 2" />
                    </a>
                </div>
            </div>
            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    Un condensé d'expériences pour débusquer l'IA sous toutes ses formes !
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m3-fichepedago-experimenter.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Créons avec l’IA.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links iai-content-links-for-tutorial">
            <a href="#" class="iai-content-links-header">
                <p><strong>Expérimentez d'autres logiciels ou ressources pédagogiques</strong> pour vous faire votre propre idée de l'IA</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('experimenter'); ?>
            </ul>
        </div>
    </div>
    <div id="decouvrir">
        <h2>L’intelligence artificielle à notre service&nbsp;?</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Découvrir</span> : Vidéo | 7min40 | Sous-titres | Réalisation : 4 minutes 34 |
                    <span class="iai-video-share"><a href="#"></a></span>
                </p>
                <div class="iai-video-parent">
                    <a href="#" data-video="https://www.youtube.com/embed/4D8XReLj5SQ?rel=0" data-title="L’intelligence artificielle à notre service ? - Se questionner">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/D3.jpg" alt="" />
                    </a>
                </div>
            </div>

            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    L'IA est partout présente autour de nous et pose des questions d'ordre sociétal, démocratique, éthique, politique. Quel rôle va-t-on laisser jouer aux IA ? Dans quelle société voulons-nous vivre ? On y réfléchit ensemble ?
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/IAI_lexique.pdf" target="_blank">
                            <p>Le lexique</p>
                            <p>Une interrogation sur un terme, ce lexique est fait pour vous !</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links">
            <a href="#" class="iai-content-links-header">
                <p><strong>En savoir plus sur une question en particulier</strong> : des liens pour aller plus loin</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('decouvrir'); ?>
            </ul>
        </div>
    </div>
    <div id="debattre">
        <h2>IA et Humains</h2>

        <div class="iai-content-blocks full-width">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Débattre</span> : Conférence du 24 mars à Toulouse | Réalisation : La compagnie du code |
                </p>

                <div class="iai-video-parent iai-video-report-conference">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/conf_temp.jpg" alt="" />
                </div>

                <ul class="iai-report-conference-fiche">
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m3-fichepedago-debattre.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Animer un débat participatif autour de l'IA</p>
                        </a>
                    </li>
                </ul>

                <?php /*
                <p class="iai-content-block-intro">
                    Demain capable d’adapter les apprentissages au plus près des compé́tences réelles des élèves, l’intelligence artificielle suscite fantasmes, enthousiasmes et inquiétudes. Que peut l’IA pour l’éducation ? Et à quelles conditions scientifiques et éthiques imaginer son développement dans nos écoles ?
                </p>
 */ ?>

            </div>
        </div>

        <?php /*
        <div class="iai-content-blocks iai-content-video-blocks">
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/video1.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">Un robot apprenant pour l'éducation à l'IA.</p>
                    <p class="iai-video-name">Thomas Deneux</p>
                    <p class="iai-video-description">Directeur du service calcul scientifique à l'Institut de Neuros- ciences Paris-Saclay (CNRS).</p>
                    <p class="iai-video-duration">12 min</p>
                </div>
            </a>
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/video2.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">Apport du Clustering d'élèves pour l'enseignement</p>
                    <p class="iai-video-name">François Bouchet</p>
                    <p class="iai-video-description">Thomas Deneux Directeur du service calcul scientifique à l'Institut de Neuros- ciences Paris-Saclay (CNRS).</p>
                    <p class="iai-video-duration">8 min</p>
                </div>
            </a>
            <a  href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/video3.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">KidLearn : Evaluation et personalisation des apprentissages</p>
                    <p class="iai-video-name">Benjamin Clément</p>
                    <p class="iai-video-description">Doctorant à l’Inira. Membre de l'équipe Flowers Inria sur le projet projet KidLearn.</p>
                    <p class="iai-video-duration">12 min</p>
                </div>
            </a>
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/video4.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">L’IA un outil au service de l’Education?</p>
                    <p class="iai-video-description">Question réponse avec le Public d’Educatice</p>
                    <p class="iai-video-duration">45 min</p>
                </div>
            </a>
        </div>


        <div class="iai-content-blocks iai-content-video-blocks">
            <div class="iai-content-block iai-content-video-block">
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="iai-content-block iai-content-video-block">
                <ul>
                    <li class="fiche-pedago-icon">
                        <a href="#">
                            <p>La fiche pédagogique</p>
                            <p>Animer un débat participatif autour de l'IA</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

  */ ?>

    </div>
</div>

<div class="iai-footer">
    <ul class="iai-tuto-links">
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module1") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module1-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#1</span>
                    <span>Vous avez dit IA ?</span>
                </div>
            </a>
        </li>
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module2") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module2-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#2</span>
                    <span>Boosté à l’IA !</span>
                </div>
            </a>
        </li>
    </ul>
</div>
