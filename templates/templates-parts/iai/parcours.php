<div class="iai-home-contents">
    <div class="iai-video-parent">
        <a href="#" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
            <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/bande-annonce.jpg" alt="" />
        </a>
    </div>
    <div>
        <div class="iai-content-block">
            <p class="iai-content-block-intro">
                Une formation à la portée de toutes et de tous de 7 à 107 ans pour se questionner, expérimenter et comprendre
                ce qu’est l’Intelligence Artificielle... avec intelligence !
            </p>
            <ul>
                <li class="fun-icon">
                    <a href="https://www.fun-mooc.fr/en/cours/lintelligence-artificielle-avec-intelligence/" target="_blank">
                        <p>Formez-vous !</p>
                        <p>Retrouvez la formation sur Fun Mooc</p>
                    </a>
                </li>
                <li class="parlons-en-icon">
                    <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                        <p>Forum</p>
                        <p>Venez échanger avec nous&nbsp;!</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="iai-home-menu">
    <ul class="iai-tuto-links">
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module1") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module1-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#1</span>
                    <span>Vous avez dit IA ?</span>
                </div>
            </a>
        </li>
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module2") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module2-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#2</span>
                    <span>Boosté à l’IA !</span>
                </div>
            </a>
        </li>
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module3") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module3-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#3</span>
                    <span>Humains et IA…</span>
                </div>
            </a>
        </li>
    </ul>
</div>

<div class="iai-home-logos">
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/inria.png" alt="INRIA" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/magikmakers.jpg" alt="Magik Makers" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/s24B.jpg" alt="s24Bs Sophie de QuatreBarbes" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/class-code.png" alt="Class'Code" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/ministere-educ-jeunesse.png" alt="Ministère de l'éducation et de la jeunesse" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/unit.png" alt="Unit" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/educazur.png" alt="Educazur" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/microsoft.png" alt="Microsoft" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/4min34.png" alt="4 minutes 34" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/databingo.png" alt="Databingo" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/ligue_enseignement.png" alt="Ligue de l'enseignement" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/universite-nantes.png" alt="Université de Nantes" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/lacompagnieducode.png" alt="La compagnie du code" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/leblob.png" alt="Le blob" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/simplon.png" alt="Simplon" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/poppy.png" alt="Poppy" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/tralalere.png" alt="Tralalere" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/line.png" alt="Line" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/uniscite.png" alt="Unicités" />
    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/logos/womeninAI.png" alt="Women in AI" />


</div>

<div class="iai-credits">
    <h2>CRÉDITS</h2>
    <div class="iai-credits-intro">Une co-production Inria, Magic Maker, S24B l’Interactive avec la participation de 4 minutes 34, Data Bingo, Université de Nantes,
        la Compagnie du Code et de la Ligue de l’enseignement et de l’ensemble des partenaires de Class’Code
        et avec le soutien du Ministère de l’Éducation nationale et de la Jeunesse, UNIT et Educazur.
    </div>

    <div class="iai-credits-tout-voir"><a href="#">Voir qui a fait quoi</a></div>

    <div class="columns">
        <div class="column">
            <h3>Conception, production et coordination</h3>
            <div>
                <p>Sophie de Quatrebarbes, S24B L’interactive. Thierry Vieville, Inria.</p>
                <p>Claude Terosier, Magic Makers.</p>
            </div>

            <h3>Conseil scientifique</h3>
            <div>
                <p>Frédéric Alexandre, Neuroscience Computationnelle. Directeur de l’équipe de recherche Mnémosyne, Inria.</p>
                <p>Anne Boyer, Systèmes complexes et intelligence artificielle. Responsable de l’équipe KIwi, Loria.</p>
                <p>Serge Abiteboul, Donnée et réseau. Membre du Collège de l'ARCEP, chercheur à l'ENS Paris et directeur de recherche à l'Inria.</p>
                <p>Gilles Dowek, informaticien, logicien et philosophe. Membre de l’équipe Deducteam, Inria Ens Saclay.</p>
                <p>Colin de la Higuera, chercheur en Machine Learning et traduction automatique, membre du L2N Université de Nantes, ancien président de la SIF, détenteur de la chaire Unesco.</p>
                <p>Gérard Giraudon, Educazur.
            </div>

            <h3>Commission pédagogique & ressources complémentaires</h3>
            <div>
                <p>Nicolas Decoster, la Compagnie du Code. Hanan Salam, Women in AI.</p>
                <p>Bastien Masse, Université de Nantes. Marie Duflot Kremer, Loria.</p>
                <p>Lucie Jague et Bertrand Midot, Simplon.</p>
                <p>Zoé Brunet, Tralalere.</p>
                <p class="iai-credits-et-tous-les-partenaires">
                    <a href="https://pixees.fr/classcode-v2/?structureSearch=true#mapWidget">
                        • Et tous les partenaires de Class’Code ici &gt;
                    </a>
                </p>
            </div>
        </div>
        <div class="column">
            <h3>Se questionner et découvrir : 4minutes34</h3>
            <div>
                <p>Sonia Cruchon, Co-écriture, co-réalisation, voix.</p>
                <p>Nicolas Ledu, Image, montage, co-réalisation, voix.</p>
                <p>Guillaume Clemencin, Jeu.</p>
                <p>Michaël Cixous, Illustrations et animation.</p>
                <p>Jean-Jacques Birgé, Son.</p>
                <p>Sophie de Quatrebarbes, co-écriture et suivi de production.</p>
                <p>Conseil et relecture attentive de Frédéric Alexandre, Inria.</p>
                <p>Avec la participation de leblob.fr</p>
                <p>Production : Isabelle Péricard.</p>
                <p>Direction des programmes : Alain Labouze.</p>
            </div>

            <h3>Expérimenter #1 et #2 : Magic Makers.</h3>
            <div>
                <p>Conception et réalisation : Jade Becker, Magic Makers.</p>
                <p>Supervision : Romain Liblau et Claude Terosier, Magic Makers.</p>
                <p>Veronica Holguin, Graphisme.</p>
                <p>Denis Chiron, Développement.</p>
                <p>Avec le soutien de Microsoft : Béatrice Matlega et Céline Corno.</p>
            </div>

            <h3>Expérimenter #3 : Data Bingo.</h3>
            <div>
                <p>Conception : Bastien Didier, Thu Trinh-Bouvier, Julien Levesque, Albertine Meunier et Sylvie Tissot</p>
                <p>Développement : Bastien Didier et Sylvie Tissot</p>
                <p>Image : Julien Levesque</p>

            </div>

            <h3>Débattre : Class'Code</h3>
            <div>
                <p>#1 Nantes : Bastien Masse, Université de Nantes.</p>
                <p>#2 Paris : Sophie de Quatrebarbes, S24B et Antonin Cois, Ligue de l’enseignement. Antoine Bonfils, Captation.</p>
                <p>#3 Carcassone : Valérie Letard, La Compagnie du Code.</p>
            </div>

            <h3>Fiche pédagogique : Ligue de l'enseignement</h3>
            <div>
                <p>Julie Stein</p>
            </div>

            <h3>Documentation et plateforme : INRIA</h3>
            <div>
                <p>Benjamin Ninassi, Inria - développement. Denis Chiron, développement.</p>
                <p>Veronica Holguin, Graphisme.</p>
                <p>Martine Courbin, Documentation. Marie Hélène Comte, Aurélie Lagarrigue, Mooc Fun.</p>
            </div>
        </div>
    </div>
</div>
