<div class="iai-header iai-home-header <?php if ( is_user_logged_in() ) { echo ' with-userbar'; } ?>">
    <div class="iai-menu-burger"></div>
    <div class="iai-logo"></div>
    <div class="iai-baseline">
        <span>L’Intelligence Artificielle avec Intelligence</span>
    </div>
</div>