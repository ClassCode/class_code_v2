<div class="iai-content iai-content-post" >
	<h2><?php echo get_the_title(); ?></h2>
    <div class="iai-content-blocks">
        <?php
            if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post();
                    the_content();
                }
            }
        ?>
    </div>
</div>