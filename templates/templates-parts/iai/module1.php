<div class="iai-content">
    <div id="se_questionner">
        <h2>Qui a peur de l’IA ?</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Se questionner</span> : Vidéo | 3min | Sous-titres | Réalisation : 4 minutes 34 |
                    <span class="iai-video-share"><a href="#"></a></span>
                </p>
                <div class="iai-video-parent">
                    <a href="#" data-video="https://www.youtube.com/embed/mkfXdzA6B_c?rel=0" data-title="Qui a peur de l’IA ? Se questionner">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/Q1.jpg" alt="" />
                    </a>
                </div>
            </div>
            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    L’intelligence artificielle ! Plus j’en entends parler, moins j’y vois clair… Moi je croyais que c’était top, mais là je vois un article où Stephen Hawking, vous savez le chercheur hyper brillant – déclare : « le développement d’une intelligence artificielle complète pourrait mettre fin à la race humaine ».
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m1-fichepedago-sequestionner.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Partageons des 1iers éléments de définition pour comprendre ce qu’est l’IA et ce que ça n’est pas.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links">
            <a href="#" class="iai-content-links-header">
                <p><strong>Décryptons les médias</strong> grâce à des liens vers des ressources complémentaires</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('se_questionner'); ?>
            </ul>
        </div>
    </div>
    <div id="experimenter">
        <h2>Testons notre premier programme</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <div class="iai-tuto-parent">
                    <a href="https://pixees.fr/classcodeiai/app/tuto1/" target="_blank">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/tuto1.png" alt="tutoriel 1" />
                    </a>
                </div>
            </div>
            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    C'est quoi l'IA et ça fonctionne comment un programme d'IA ? On vous explique que ça n'a rien de magique ;
                    et d'ailleurs allez-y, testez en entraînant une IA !
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m1-fichepedago-experimenter.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Devenez créateur d’une IA.<br/>Pour comprendre comment fonctionne un algorithme de reconnaissance d’images.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links iai-content-links-for-tutorial">
            <a href="#" class="iai-content-links-header">
                <p><strong>Expérimentez d'autres logiciels ou ressources pédagogiques</strong> pour vous faire votre propre idée de l'IA</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('experimenter'); ?>
            </ul>
        </div>
    </div>
    <div id="decouvrir">
        <h2>C’est quoi l’intelligence artificielle ?</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Découvrir</span> : Vidéo | 8min23 | Sous-titres | Réalisation : 4 minutes 34 |
                    <span class="iai-video-share"><a href="#"></a></span>
                </p>
                <div class="iai-video-parent">
                    <a href="#" data-video="https://www.youtube.com/embed/yQLmgw3rClM?rel=0" data-title="C’est quoi l’intelligence artificielle ? Découvrir">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/D1.jpg" alt="" />
                    </a>
                </div>
            </div>

            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    "Nos ordinateurs, ils sont devenus intelligents ou pas ?". Voyons ce qu'est l'intelligence biologique pour pouvoir définir l'IA avec les différentes approches symbolique, numérique... et comprenons que toutes les IA que nous croisons dans la vraie vie sont juste techniques et très spécifiques !
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/IAI_lexique.pdf" target="_blank">
                            <p>Le lexique</p>
                            <p>Une interrogation sur un terme, ce lexique est fait pour vous !</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links">
            <a href="#" class="iai-content-links-header">
                <p><strong>En savoir plus sur une question en particulier</strong> : des liens pour aller plus loin</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('decouvrir'); ?>
            </ul>
        </div>
    </div>
    <div id="debattre">
        <h2>L’IA dans tous ses états</h2>

        <div class="iai-content-blocks full-width">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Débattre</span> : Conférence du 18 mars à la Halle 6 Ouest (Nantes) | Réalisation : Université de Nantes |
                </p>

                <div class="iai-video-parent iai-video-report-conference">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/conf_temp.jpg" alt="" />
                </div>

                <ul class="iai-report-conference-fiche">
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m1-fichepedago-debattre.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Animer un débat participatif autour de l'IA</p>
                        </a>
                    </li>
                </ul>

                <?php /*
                <p class="iai-content-block-intro">
                    Demain capable d’adapter les apprentissages au plus près des compétences réelles des élèves, l’intelligence artificielle suscite fantasmes, enthousiasmes et inquiétudes. Que peut l’IA pour l’éducation ? Et à quelles conditions scientifiques et éthiques imaginer son développement dans nos écoles ?
                </p>
                */ ?>

            </div>
        </div>

        <?php /*
        <div class="iai-content-blocks iai-content-video-blocks">
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/video1.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">Un robot apprenant pour l'éducation à l'IA.</p>
                    <p class="iai-video-name">Thomas Deneux</p>
                    <p class="iai-video-description">Directeur du service calcul scientifique à l'Institut de Neuros- ciences Paris-Saclay (CNRS).</p>
                    <p class="iai-video-duration">12 min</p>
                </div>
            </a>
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/video2.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">Apport du Clustering d'élèves pour l'enseignement</p>
                    <p class="iai-video-name">François Bouchet</p>
                    <p class="iai-video-description">Thomas Deneux Directeur du service calcul scientifique à l'Institut de Neuros- ciences Paris-Saclay (CNRS).</p>
                    <p class="iai-video-duration">8 min</p>
                </div>
            </a>
            <a  href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/video3.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">KidLearn : Evaluation et personalisation des apprentissages</p>
                    <p class="iai-video-name">Benjamin Clément</p>
                    <p class="iai-video-description">Doctorant à l’Inira. Membre de l'équipe Flowers Inria sur le projet projet KidLearn.</p>
                    <p class="iai-video-duration">12 min</p>
                </div>
            </a>
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/881OHsFBJ2Y?rel=0&feature=emb_logo" data-title="Class'Code IAI">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/video4.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">L’IA un outil au service de l’Education?</p>
                    <p class="iai-video-description">Question réponse avec le Public d’Educatice</p>
                    <p class="iai-video-duration">45 min</p>
                </div>
            </a>
        </div>

        <div class="iai-content-blocks iai-content-video-blocks">
            <div class="iai-content-block iai-content-video-block">
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="iai-content-block iai-content-video-block">
                <ul>
                    <li class="fiche-pedago-icon">
                        <a href="#">
                            <p>La fiche pédagogique</p>
                            <p>Animer un débat participatif autour de l'IA</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

         */ ?>

    </div>
</div>

<div class="iai-footer">
    <ul class="iai-tuto-links">
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module2") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module2-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#2</span>
                    <span>Boosté à l’IA !</span>
                </div>
            </a>
        </li>
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module3") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module3-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#3</span>
                    <span>Humains et IA…</span>
                </div>
            </a>
        </li>
    </ul>
</div>
