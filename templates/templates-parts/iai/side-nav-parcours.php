<div class="iai-nav-bar iai-nav-bar-parcours <?php if ( is_user_logged_in() ) { echo ' with-userbar'; } ?>">

     <div class="iai-nav iai-nav-modules">
        <a href="#" class="iai-nav-close"></a>
        <ul class="">
            <li class="iai-nav-module iai-nav-item-active">
                <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module1") ?>" class="<?php echo $module1_selected ?>">
                    #1 Vous avez dit IA ?
                </a>
            </li>
            <li class="iai-nav-module">
                <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module2") ?>" class="<?php echo $module2_selected ?>">
                    #2 Boosté à l’IA !
                </a>
            </li>
            <li class="iai-nav-module">
                <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module3") ?>" class="<?php echo $module3_selected ?>">
                    #3 Humains et IA…
                </a>
            </li>
        </ul>
    </div>

</div>
