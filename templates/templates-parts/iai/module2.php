<div class="iai-content">
    <div id="se_questionner">
        <h2>De l’IA à toutes les sauces ?</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Se questionner</span> : Vidéo | 1min27 | Sous-titres | Réalisation : 4 minutes 34 |
                    <span class="iai-video-share"><a href="#"></a></span>
                </p>
                <div class="iai-video-parent">
                    <a href="#" data-video="https://www.youtube.com/embed/E_3vJwb-wE0?rel=0" data-title="De l’IA à toutes les sauces ? - Se questionner">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/Q2.jpg" alt="" />
                    </a>
                </div>
            </div>
            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    Comment ils font pour mettre de l’intelligence artificielle dans une pizza ?
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m2-fichepedago-sequestionner.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Notre quotidien boosté à l’IA ? Comment ça marche ?</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links">
            <a href="#" class="iai-content-links-header">
                <p><strong>Décryptons les médias</strong> grâce à des liens vers des ressources complémentaires</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('se_questionner'); ?>
            </ul>
        </div>
    </div>
    <div id="experimenter">
        <h2>Jouons avec les données</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <div class="iai-tuto-parent">
                    <a href="https://pixees.fr/classcodeiai/app/tuto2/" target="_blank">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/tuto2.png" alt="tutoriel 2" />
                    </a>
                </div>
            </div>
            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    Rien de mieux pour comprendre comment fonctionne l'apprentissage machine et l'entraînement d'un programme, de voir comment bien préparer son jeu de données. C'est à vous !
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m2-fichepedago-experimenter.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Jouer avec les données. Quand une machine apprend à jouer.</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links iai-content-links-for-tutorial">
            <a href="#" class="iai-content-links-header">
                <p><strong>Expérimentez d'autres logiciels ou ressources pédagogiques</strong> pour vous faire votre propre idée de l'IA</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('experimenter'); ?>
            </ul>
        </div>
    </div>
    <div id="decouvrir">
        <h2>Du Machine Learning et des données</h2>
        <div class="iai-content-blocks">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Découvrir</span> : Vidéo | 9min36 | Sous-titres | Réalisation : 4 minutes 34 |
                    <span class="iai-video-share"><a href="#"></a></span>
                </p>
                <div class="iai-video-parent">
                    <a href="#" data-video="https://www.youtube.com/embed/bLvabh0asQU?rel=0" data-title="Du Machine Learning et des données">
                        <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/D2.jpg" alt="" />
                    </a>
                </div>
            </div>

            <div class="iai-content-block">
                <p class="iai-content-block-intro">
                    Quand on parle d’IA aujourd’hui, on entend le plus souvent “machine learning” ou apprentissage automatique. Mais comment les machines apprennent-elles, et de quoi se nourrissent-elles ? Explication.
                </p>
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/IAI_lexique.pdf" target="_blank">
                            <p>Le lexique</p>
                            <p>Une interrogation sur un terme, ce lexique est fait pour vous !</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="iai-content-links">
            <a href="#" class="iai-content-links-header">
                <p><strong>En savoir plus sur une question en particulier</strong> : des liens pour aller plus loin</p>
                <span class="toggle"></span>
            </a>
            <ul>
                <?php CLASSCODE2_Plugin::renderLinks('decouvrir'); ?>
            </ul>
        </div>
    </div>
    <div id="debattre">
        <h2>L’IA, un outil au service de l’éducation</h2>

        <div class="iai-content-blocks full-width">
            <div class="iai-content-block">
                <p class="iai-content-video-title">
                    <span class="iai-rubrique">Débattre</span> : Conférence du 21/11/2019 sur Educatice (Paris, Porte de Versailles) | Réalisation : La ligue de l’enseignement |
                </p>
                <p class="iai-content-block-intro">
                    Demain capable d’adapter les apprentissages au plus près des compé́tences réelles des élèves, l’intelligence artificielle suscite fantasmes, enthousiasmes et inquiétudes. Que peut l’IA pour l’éducation ? Et à quelles conditions scientifiques et éthiques imaginer son développement dans nos écoles ?
                </p>
            </div>
        </div>

        <div class="iai-content-blocks iai-content-video-blocks">
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/UVgfz7_d3bQ?rel=0" data-title="Neurosciences" >
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/D2V1.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">Un robot apprenant pour l'éducation à l'IA</p>
                    <p class="iai-video-name">Thomas Deneux</p>
                    <p class="iai-video-description">Directeur du service calcul scientifique à l'Institut de Neurosciences Paris-Saclay (CNRS).</p>
                    <p class="iai-video-duration">13 min 35</p>
                </div>
            </a>
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/p3VYAuMi-i4?rel=0" data-title="Apport du Clustering d'é́lèves pour l'enseignement">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/D2V2.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">Apport du Clustering d'é́lèves pour l'enseignement</p>
                    <p class="iai-video-name">François Bouchet</p>
                    <p class="iai-video-description">Maître de conférences en informatique au LIP6, laboratoire d'informatique de Sorbonne Université au sein de l'équipe MOCAH.</p>
                    <p class="iai-video-duration">10 min 38</p>
                </div>
            </a>
            <a  href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/WRndpdkpjOA?rel=0" data-title="KidLearn">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/D2V3.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">KidLearn : Evaluation et personalisation des apprentissages</p>
                    <p class="iai-video-name">Benjamin Clément</p>
                    <p class="iai-video-description">Doctorant Inria, membre de l’équipe Flowers en charge du programme KidLearn (Inria)</p>
                    <p class="iai-video-duration">8 min 12</p>
                </div>
            </a>
            <a href="#" class="iai-video-parent iai-content-video-block" data-video="https://www.youtube.com/embed/DQX4hvSF-4w?rel=0" data-title="L’IA un outil au service de l’Education ?">
                <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/videos/D2V4.jpg" alt="" />
                <div class="video-infos">
                    <p class="iai-video-title">L’IA un outil au service de l’Education ?</p>
                    <p class="iai-video-description">Question réponse avec le Public d’Educatice</p>
                    <p class="iai-video-duration">18 min 15</p>
                </div>
            </a>
        </div>

        <div class="iai-content-blocks iai-content-video-blocks">
            <div class="iai-content-block iai-content-video-block">
                <ul>
                    <li class="parlons-en-icon">
                        <a href="https://mooc-forums.inria.fr/moociai/" target="_blank">
                            <p>Parlons-en !</p>
                            <p>Venez échanger sur le Forum.</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="iai-content-block iai-content-video-block">
                <ul>
                    <li class="fiche-pedago-icon">
                        <a href="https://files.inria.fr/mecsci/classcodeIAI/pdf/fiches_pedagogiques/m2-fichepedago-debattre.pdf" target="_blank">
                            <p>La fiche pédagogique</p>
                            <p>Animer un débat participatif autour de l'IA</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="iai-footer">
    <ul class="iai-tuto-links">
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE .  "/module1") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module1-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#1</span>
                    <span>Vous avez dit IA ?</span>
                </div>
            </a>
        </li>
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE.  "/module3") ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module3-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#3</span>
                    <span>Humains et IA…</span>
                </div>
            </a>
        </li>
    </ul>
</div>
