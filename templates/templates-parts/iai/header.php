<?php

    global $post;

    $post_slug = $post->post_name;
    $module1_selected = $post_slug === "module1" ? "selected" : "";
    $module2_selected = $post_slug === "module2" ? "selected" : "";
    $module3_selected = $post_slug === "module3" ? "selected" : "";

    $route_IAI = CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE;

?>

<div class="iai-header <?php if ( is_user_logged_in() ) { echo ' with-userbar'; } ?>">
    <div class="iai-menu-burger"></div>
    <a class="iai-logo" href="<?php echo site_url( $route_IAI ) ?>"></a>
    <ul class="iai-tuto-links">
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( $route_IAI.  "/module1") ?>" class="<?php echo $module1_selected ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module1-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#1</span>
                    <span>Vous avez dit IA ?</span>
                </div>
            </a>
        </li>
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( $route_IAI.  "/module2") ?>" class="<?php echo $module2_selected ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module2-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#2</span>
                    <span>Boosté à l’IA !</span>
                </div>
            </a>
        </li>
        <li class="iai-tuto-link">
            <a href="<?php echo site_url( $route_IAI.  "/module3") ?>" class="<?php echo $module3_selected ?>">
                <div class="iai-tuto-icon">
                    <img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/iai/module3-320.jpg" alt="" />
                </div>
                <div class="iai-tuto-link-title">
                    <span>#3</span>
                    <span>Humains et IA…</span>
                </div>
            </a>
        </li>
    </ul>
</div>