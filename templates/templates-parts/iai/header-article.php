<?php

$route_IAI = CLASSCODE2_ROUTE . CLASSCODE2_IAI_ROUTE;

?>

<div class="iai-header iai-home-header <?php if ( is_user_logged_in() ) { echo ' with-userbar'; } ?>">
    <div class="iai-menu-burger"></div>
    <a class="iai-logo" href="<?php echo site_url( $route_IAI ) ?>"></a>
    <div class="iai-baseline">
        <span>L’Intelligence Artificielle avec Intelligence</span>
    </div>
</div>