
	<script type="text/javascript">
		
		jQuery(function($){

		    $('.iai-content-links a').attr('target', '_blank');

            $( '.iai-content-links-header' ).on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $(this).closest('.iai-content-links').toggleClass('opened');
            });

            $( '.iai-credits-tout-voir > a' ).on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                $(this).toggleClass('opened');
                $(this).closest('.iai-credits').find('.columns').toggleClass('opened');
            });

            $("[data-video]").on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();

                var $this = $(this);
                var $iframe = $("<iframe>").attr("src", $this.data("video"));
                var $title = $("<h2>").text($this.data("title"));

                var popup = $('#video-popup');

                if (! popup.length ) {
                    $('body').append('<div id="video-popup"></div>')
                }

                $("#video-popup").addClass('iai-video-popup').html($title).append($iframe);
                $iframe.wrap("<div class='iai-video-popup-parent'>");

                $(".iai-video-popup").on('click', function (e) {
                    $(".iai-video-popup").off('click');
                    $("#video-popup").remove();
                });

            });

            if ($('.iai-nav').length)
            {
                $('.iai-nav-items .iai-nav-burger').on('click', function(e){
                    e.preventDefault();
                    $('.iai-nav-items').addClass('opened');
                });

                $('.iai-nav-items .iai-nav-close').on('click', function(e){
                    e.preventDefault();
                    $('.iai-nav-items').removeClass('opened');
                });

                $('.iai-menu-burger').on('click', function(e){
                    e.preventDefault();
                    $('.iai-nav-modules').toggleClass('opened');
                });

                $('.iai-nav-modules .iai-nav-close').on('click', function(e){
                    e.preventDefault();
                    $('.iai-nav-modules').removeClass('opened');
                });

                var anchor1 = $("#se_questionner");
                var anchor2 = $("#experimenter");
                var anchor3 = $("#decouvrir");
                var anchor4 = $("#debattre");

                var $body = $("body");


                $(window).on("resize", function()
                {
                    $('.iai-nav').removeClass('opened');
                });

                if(anchor1.offset() && anchor2.offset() && anchor3.offset() && anchor4.offset()){
                  $(window).on("scroll", function()
                  {
                      var scrolltop = $(this).scrollTop();

                      if ((scrolltop > 0) && ! $body.hasClass('sticky')) {
                          $body.addClass('sticky');
                      } else if (scrolltop === 0) {
                          $body.removeClass('sticky');
                      }

                      scrolltop += 330;

                      var prevActiveMenu = $('.iai-nav-item.iai-nav-item-active').index();
                      var activeMenu = 0;

                      // console.log( $(this).scrollTop()+ 330, anchor2.offset().top );

                      if (scrolltop > anchor4.offset().top ) {
                          activeMenu = 3;
                      } else if (scrolltop > anchor3.offset().top ) {
                          activeMenu = 2;
                      } else if (scrolltop > anchor2.offset().top ) {
                          activeMenu = 1;
                      } else if (scrolltop > anchor1.offset().top ) {
                          activeMenu = 0;
                      }

                      if (prevActiveMenu !== activeMenu) {
                          $('.iai-nav-item').removeClass('iai-nav-item-active');
                          var activeElement = $('.iai-nav-item').get(activeMenu);
                          $(activeElement).addClass('iai-nav-item-active');
                      }

                  });
                }

                $('.iai-nav-item').on('click', function(e){
                    var toScrollTop;
                    switch($(this).index()) {
                        case 4:
                            break;
                        case 3:
                            break;
                        case 2:
                            break;
                    }
                    $("body").animate({scrollTop:toScrollTop}, '500');
                });
            }

            //
            // Ancres --> Smooth scroll ( cf https://css-tricks.com/snippets/jquery/smooth-scrolling/ )
            //

            $('a[href*="#"]')
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function(event) {
                    if (
                        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                        &&
                        location.hostname == this.hostname
                    ) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {

                            event.preventDefault();
                            
                            var adminbaroffset=0;
                            if($('#wpadminbar').height()){
                              adminbaroffset= $('#wpadminbar').height()
                            }
                            var scrollOffset = adminbaroffset + $('.header-classcode-v2').height() + $('.iai-header').height() + 80;
                            // console.log("scrollOffset", scrollOffset);

                            $('html, body').animate({
                                scrollTop: target.offset().top - scrollOffset
                            }, 1000, function() {

                                // Must change focus!
                                /*
                                var $target = $(target);
                                $target.focus();
                                if ($target.is(":focus")) { // Checking if the target was focused
                                    return false;
                                } else {
                                    $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                    $target.focus(); // Set focus again
                                }
                                */
                            });
                        }
                    }
                });


        })

	</script>
