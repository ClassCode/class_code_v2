<?php
include_once(WP_PLUGIN_DIR.'/class_code_v2/profiles/profile_type.php');
?>
<div class="classcode-v2 content-category" >

	
	<p class="content-category-title">CONTACTS</p>
	<a name="contacts"></a>
	<div class="content-wp contacts">
          Une question ? Une suggestion, un avis ? Contacter une personne de Class´Code ? Besoin de conseil quel qu'il soit ?<div class="contacts-mailer">

<center><b>Notre bureau d'accueil est là via <a href="mailto:classcode-accueil@inria.fr">classcode-accueil@inria.fr</a> ou ce formulaire, on vous répond au plus vite !</b></center>

<?php 
$new_form = true;

$prefix = "cc_mailer_shortcode";
// Builds the form predifined fields
$to_whom = array("Bureau d´accueil de Class´Code"  => "thierry.vieville@inria.fr");
foreach($data_coordinations as $data){
  $name = htmlspecialchars_decode($data['name']);
  if ($data['alert']){    
    $to_whom[$name] = $data['email'].",classcode-accueil@inria.fr";
  }
}   
$to_whom_logo = array("Bureau d´accueil de Class´Code"  => get_site_url()."/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png");
foreach($data_coordinations as $data){
  $name = htmlspecialchars_decode($data['name']);
  if ($data['alert']){
    $to_whom_logo[$name] = $data['logo'];  
  }  
} 
?>
<script language="javascript">
to_whom = { <?php foreach($to_whom as $name => $value) echo "\"$name\" : \"$value\", "; ?> "" : ""};
to_whom_logo = { <?php foreach($to_whom_logo as $name => $value) echo "\"$name\" : \"$value\", "; ?> "" : ""};
var for_what_bureau = {
  "J’ai un souci pour me connecter à la plateforme https://classcode.fr" : "",
  "J’ai besoin d’aide par rapport la plateforme https://openclassrooms.com" : "",
  "J’ai trouvé une erreur (typo, correction, …) dans une des pages Class´Code" : ""
};
var for_what_coordination = {
  "Je cherche un temps de rencontre" : "",
  "Je suis une ou un facilitateur" : ""
};
var for_what = {};
<?php 
$script = '
function '.$prefix.'_select_change()
{
  name = document.getElementById("'.$prefix.'_select").value;
  if(name != "") {
    document.getElementById("'.$prefix.'_who").value = name;
    document.getElementById("'.$prefix.'_to").value = to_whom[name];
    document.getElementById("'.$prefix.'_logo").src = to_whom_logo[name];
    for_what = name == "Bureau d´accueil de Class´Code" ? for_what_bureau : for_what_coordination;
    {
      innerSelect = "<option selected disabled>Ma question</option>"; for (var name in for_what) innerSelect += "<option value=\""+name+"\">"+name+"</option>"; document.getElementById("'.$prefix.'_for_what").innerHTML = innerSelect;
    }
  } else
    document.getElementById("'.$prefix.'_logo").src = "'.get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png";
  document.getElementById("'.$prefix.'_select").selectedIndex = 0;
}
function '.$prefix.'_for_what_change()
{
  name = document.getElementById("'.$prefix.'_for_what").value;
  if(name != "") {
    document.getElementById("'.$prefix.'_subject").value = name;
    if (for_what[name] != "")
      document.getElementById("'.$prefix.'_to").value = for_what[name];
  }
  document.getElementById("'.$prefix.'_for_what").selectedIndex = 0;
}';
echo $script;
?>
</script>

<?php
// Here we copy the code of themes/pixees-theme/_inc/display-function.php -> function cc_mailer_shortcode($atts, $content = "")
{
  // Preparation of the message parameters
  $who = preg_replace('/"/', '&quot;', isset($_REQUEST[$prefix.'_who']) ? urldecode($_REQUEST[$prefix.'_who']) : "Bureau d´accueil de Class´Code");
  $to = preg_replace('/"/', '&quot;', isset($_REQUEST[$prefix.'_to']) ? urldecode($_REQUEST[$prefix.'_to']) : "thierry.vieville@inria.fr");
  $logo = isset($_REQUEST[$prefix.'_logo']) ? urldecode(preg_replace("/\\\\?'/", "´",  $_REQUEST[$prefix.'_logo'])) : get_site_url().'/wp-content/themes/pixees-theme/_img/classcode_toplinkbar_blue.png';
  if ($logo == "participant") $logo = get_site_url().'/wp-content/uploads/2014/08/pixee-rouge-all.png';
  $subject = isset($_REQUEST[$prefix.'_subject']) && $_REQUEST[$prefix.'_subject'] != '' ? urldecode(preg_replace("/\\\\?'/", "´",  $_REQUEST[$prefix.'_subject'])) : '';
  $body = isset($_REQUEST[$prefix.'_body']) ? preg_replace("/\\\\?'/", "´",  $_REQUEST[$prefix.'_body']) : '';
  if ((!isset($_REQUEST[$prefix.'_submit'])) && (isset($_REQUEST['body_completion']) || !isset($_REQUEST[$prefix.'_body']))){
    $bodyPart="";
    if(wp_get_current_user()->ID != 0){
      $currentUserProfile = profile_type::get_profile(wp_get_current_user()->ID);
      $bofyPart=$currentUserProfile['firstname']." ".$currentUserProfile['lastname'];
    }
    $body = "Bonjour,&#13;&#10;&#13;&#10;".$body.$bodyPart;
  }
  // Sends the mail if the verify_nonce is correct  
  $sent = "<p style='float:left;color:#266d83;'>&nbsp;</p>";
  if (isset($_REQUEST[$prefix.'_to']) && isset($_REQUEST[$prefix.'_subject']) && isset($_REQUEST[$prefix.'_body'])
      && (wp_get_current_user()->ID != 0) 
      && isset($_POST[$prefix.'_data_nonce']) && wp_verify_nonce($_POST[$prefix.'_data_nonce'], $prefix.'_id_nonce')) {
    //- print_r(array($_REQUEST[$prefix.'_to'], $_REQUEST[$prefix.'_subject'], $_REQUEST[$prefix.'_body'], 'From: '.wp_get_current_user()->user_email."\r\nContent-type: text/plain; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\n"));
    if (mail($_REQUEST[$prefix.'_to'],  iconv( "UTF-8", "ISO-8859-1//IGNORE",$_REQUEST[$prefix.'_subject']), $_REQUEST[$prefix.'_body'], 'From: '.wp_get_current_user()->user_email."\r\nContent-type: text/plain; charset=utf-8\r\nContent-Transfer-Encoding: 8bit\r\n"))
      $sent = "<p style='float:left;color:#266d83;margin-top:20px;font-size:26px;font-weight:bold;'>Le courriel a été envoyé.</p>";
    else
      $sent = "<p style='float:left;color:#266d83;margin-top:20px;font-size:26px;font-weight:bold;'>L'envoi du courriel a échoué.</p>";
  }
  // Displays the form
  $html = "";
  $html .= '<form id="'.$prefix.'_form" onSubmit="return confirm(\'Ok pour l´envoi du message ?\');" action="#'.$prefix.'_form" method="post">';
  $html .= '<!--div class="logo-class-code"><img style="max-width:350px;max-height:50px;height:50px;" id="'.$prefix.'_logo" src="'.$logo.'"/></div--><br/>';
  $html .= wp_nonce_field($prefix.'_id_nonce', $prefix.'_data_nonce', true, false);
  $html .= '<table>
<tr><td valign="top"><label for="to">Destinataire:</label></td><td>';
  if ($new_form) {
    $html .= '<select id="'.$prefix.'_select" onchange="javascript:'.$prefix.'_select_change()"><option selected disabled>Mon contact</option>'; foreach($to_whom as $name => $value) $html.= '<option value="'.$name.'">'.$name.'</option>'; $html .= '</select>';
    $html .='
 <input type="text" readonly id="'.$prefix.'_who" name="'.$prefix.'_who" value="'.$who.'"><input id="'.$prefix.'_to" name="'.$prefix.'_to" type="hidden" value="'.$to.'"/>';
  } else 
    $html .='
 <input type="text" readonly id="'.$prefix.'_who" name="'.$prefix.'_who" value="'.$who.'" class="full-width"><input id="'.$prefix.'_to" name="'.$prefix.'_to" type="hidden" value="'.$to.'"/>';
  $html .='
</td></tr>
<tr><td valign="top"><label for="subject">Sujet:</label></td><td>';
  if ($new_form) {
    $html .= '<select id="'.$prefix.'_for_what" onchange="javascript:'.$prefix.'_for_what_change()"><option selected disabled>Ma question</option></select>';
    $html .='
 <input placeholder="Ma question" id="'.$prefix.'_subject" name="'.$prefix.'_subject" type="text" value="'.preg_replace('/"/', '&quot;', $subject).'"/>';
  } else
    $html .='
 <input placeholder="Ma question" id="'.$prefix.'_subject" name="'.$prefix.'_subject" type="text" value="'.preg_replace('/"/', '&quot;', $subject).'"  class="full-width" />';
  $html .='
</td></tr>
<tr><td valign="top"><label for="body">Message:</label></td><td><textarea style="height:200px;" id="'.$prefix.'_body" name="'.$prefix.'_body">'.preg_replace('/</', '&lt;', $body).'</textarea></td></tr>'.(wp_get_current_user()->ID != 0 ?
  '<tr><td align="right" colspan="2">'.$sent.'<input class="sendmail" type="submit" name="'.$prefix.'_submit" value="&gt; Envoyer"/></td></tr>' :
  '<a style="display:block;float:right;padding:2px 10px;min-width:10px" class="sendmail" href="javascript:'.$prefix.'_on_submit();" class="button">Message</a>').'
</table></form>
<script language="javascript">
function '.$prefix.'_on_submit() {
  window.location.href = "mailto:" + encodeURI(document.getElementById("'.$prefix.'_to").value)+"?subject="+encodeURI(document.getElementById("'.$prefix.'_subject").value)+"&body="+encodeURI(document.getElementById("'.$prefix.'_body").value);
}
function '.$prefix.'_on_request(data) {
  document.getElementById("'.$prefix.'_to").value = decodeURI(data["to"]);
  document.getElementById("'.$prefix.'_who").value = decodeURI(data["who"]);
  document.getElementById("'.$prefix.'_subject").value = decodeURI(data["subject"]);
  document.getElementById("'.$prefix.'_body").value = decodeURI(data["body"]);
  window.location.href = "#aideAction2";
}
</script>'; 
  echo $html;
}
?>
</div>

	</div>
	
</div><!-- classcode-v2 -->