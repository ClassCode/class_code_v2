<!doctype html>
<html>

<head>
	<meta charset="UTF-8">
	<title>ClassCode v2</title>
	<link rel="stylesheet" id="v2" href="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/css/style.css" type="text/css" media="all" />
	<link rel='stylesheet' id='cec-bxslider-styles-css' href='<?php echo CLASSCODE2_PLUGIN_URL ?>assets/js/jquery.bxslider/jquery.bxslider.css' type='text/css' media='all' />
	<script type="text/javascript" src="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/js/jquery.js" ></script>
    <script type='text/javascript' src="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/js/jquery.bxslider/jquery.bxslider.min.js"></script>
</head>

<body>
	<header>
		<div class="toggle-menu">
			<button class="c-hamburger c-hamburger--htx">
			  <span>toggle menu</span>
			</button>
			 <div class="responsive-menu">
				<ul>
				   <li>Accueil</li>           
				   <li>Actualités</li>
				   <li>Documentation</li>
				   <li>Modules</li>
				   <li>Rencontres</li>
				   <li>Contact</li>
				</ul>
			 </div>
		</div>
		<div class="sinscrire"><a href="#">S'INSCRIRE</a></div>
	</header>