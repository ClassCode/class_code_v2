<div class="classcode-v2 content-category" >
	<p class="content-category-title"><?php echo get_the_title(); ?></p>

<br/>
						<?php
							if ( have_posts() ) {
								while ( have_posts() ) {
									the_post();
									the_content();
								}
							}
						?>

</div><!-- classcode-v2 -->
