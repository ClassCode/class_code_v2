<?php the_post(); 

$content_and_slider = get_the_content_and_slider();

echo $content_and_slider->slider;

?>


<div class="classcode-v2 content-category" >

	<!-- FAQ -->
	
	<p class="content-category-title">FOIRE AUX QUESTIONS</p>
	<div class="content-wp faq">
	
        <?php
		
		echo do_shortcode( $content_and_slider->content );
		
		?>
	
	</div>


	<script type="text/javascript">

		jQuery(function($)
		{
			$("ul.faq-questions > li a" ).each( function( index, element ) {
			
				var el = $(element);
				
				if ( el.hasClass("faq-question-mail") || el.hasClass("faq-question-text")  || el.hasClass("faq-question-page") )
				{
					el.before('<span class="faq-question-toggle faq-question-no"></span>');
					el.after('<span class="faq-question-toggle faq-question-arrow"></span>');

					var parentLi = el.parent();
					var reponse = $(".faq-reponse", parentLi);
					parentLi.after(reponse);
				}
			});
			
			$(".faq-questions li  a.faq-question-text").on("click", function(e) {
				e.preventDefault();
				var parentLi = $(this).parent();
				var reponse = parentLi.next();
				reponse.toggleClass("active");
			});
			
		});
			
		</script>

	
</div><!-- classcode-v2 -->