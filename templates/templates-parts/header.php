<?php 

get_header();

if ( is_user_logged_in() ) {
	// TODO _wpnonce
	$logout_url = site_url( "/wp-login.php?action=logout&amp;redirect_to=" . urlencode(site_url( CLASSCODE2_ROUTE )) );
  $edit_profile_url = site_url("/classcode-v2/editer-votre-profile/");
  //$edit_profile_url = get_edit_profile_url(wp_get_current_user()->ID,"classcode");
}

?>

	<header class="header-classcode-v2">
	
		<div class="menus-classcode2">
		 
		 	 <?php 
			
				global $post;
			
				$is_accueil_cc_v2    = $post->post_name == "classcode-v2";
				$is_a_la_carte_cc_v2 = $post->post_name == "a-la-carte";
				$is_parcours_cc_v2   = $post->post_name == "les-parcours";
		
				// https://pixees.fr/classcode/formations/module0/
			?>
		 
			 <div class="responsive-menu left-menu">
				<ul>
			   	   <li>
					   <p><a href="<?php echo $is_a_la_carte_cc_v2 ? "#"  : site_url( "/a-la-carte" ); ?>" >Les ressources à la carte</a></p>
					   <ul>
						   <li><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>activite-branchee" >Programmer</a></li>
						   <li><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>activite-debranchee" >Jouer</a></li>
						   <li><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>prendre-du-recul" >Découvrir</a></li>
						   <li><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>pedagogie" >Transmettre</a></li>
					   </ul>
				   </li>
				   <li>
				       <p><a href="<?php echo $is_parcours_cc_v2 ? "#" : site_url( CLASSCODE2_ROUTE .  "#parcours" );   ?>" >Les parcours de formation</a></p>
				       <ul>
				           <li><a href="<?php echo site_url( "classcode/formations/module1" );  ?>" >Programmation créative</a></li>
				           <li><a href="<?php echo site_url( "classcode/formations/module2" );  ?>" >Manipuler l'information</a></li>
				           <li><a href="<?php echo site_url( "classcode/formations/module3" );  ?>" >Robotique ludique</a></li>
				           <li><a href="<?php echo site_url( "classcode/formations/module4" );  ?>" >Connecter le réseau</a></li>
				           <li><a href="<?php echo site_url( "classcode/formations/module5" );  ?>" >Pédagogie de projet</a></li>
				           <li><a href="<?php echo site_url( "/mooc-icn-de-linformatique-de-la-creation-du-numerique-des-le-20-fevrier"     );  ?>" >Info. et Création Numérique</a></li>
				           <li><a href="<?php echo site_url( "/123-codez-suivez-a-votre-guise-le-guide-2" ); ?>" >Les ressources «1,2,3…codez!»</a></li>
					   </ul>
				   </li>
				   <li>
				       <p><a href="<?php echo $is_accueil_cc_v2 ? "#mapWidget" : site_url( CLASSCODE2_ROUTE . "#mapWidget" ); ?>" >Les contacts près de chez vous</a></p>
				       <ul>
				           <li>
							  <a href="<?php echo $is_accueil_cc_v2 ? "#mapWidget" : site_url( CLASSCODE2_ROUTE. "#mapWidget" ); ?>" >Coordinations</a>
				           </li>
				           <li>
				           	  <a href="<?php echo $is_accueil_cc_v2 ? "?structureSearch=true#mapWidget" : site_url( CLASSCODE2_ROUTE . "?structureSearch=true#mapWidget" ); ?>" >Partenaires</a>
				           	</li>
				           <li>
							  <a href="<?php echo $is_accueil_cc_v2 ? "?meetingSearch=true#mapWidget" : site_url( CLASSCODE2_ROUTE . "?meetingSearch=true#mapWidget" ); ?>" >Rencontres</a>
				           	</li>
				           <li>
							  <a href="<?php echo $is_accueil_cc_v2 ? "?userSearch=true#mapWidget" : site_url( CLASSCODE2_ROUTE . "?userSearch=true#mapWidget" ); ?>" >Participants</a>
				           </li>
					   </ul>
				   </li>
				   <li>
				       <p><a href="<?php echo site_url( CLASSCODE2_ROUTE. "/le-projet"); ?>">Le projet</a></p>
				       <ul>
				           <li>
							  <a href="<?php echo site_url( CLASSCODE2_ROUTE. "/faq"); ?>">Vos questions</a>
				           	</li>
				<li><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/contacts"); ?>">Contacter Class´Code</a></li>				           <li>

				           <li>
							  <a href="<?php echo site_url( CLASSCODE2_ROUTE. "/le-projet"); ?>">Qui sommes-nous ?</a>
				           </li>
					   </ul>
				   </li>
				</ul>
			 </div>
			 			 
		 </div>
		 
		<div class="toggle-menu">
			<button class="icone-menu-principal icone-menu-principal-classcode2">
			  <span>toggle menu</span>
			</button>
			<div class="logo_class_code">
				<a href = "<?php echo site_url ( CLASSCODE2_ROUTE ); ?>">&nbsp;</a>
			</div>
		</div>
		
		<?php if ( isset($logout_url)) { ?>
		  
			<div class="utilisateur-connecte">
				<span class="nom-utilisateur">
					<a href="<?php echo $edit_profile_url; ?>" >
						<?php echo (strlen($current_user->user_login) > 16 ? substr($current_user->user_login, 0, 16).'…' : $current_user->user_login); ?>
					</a>
					<a href="<?php echo $logout_url; ?>">
						<img src="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/images/menu_logout.png" alt="" width="20" height="20" />
					</a>
				</span>
	  		</div>
            
			<?php
			
		} else {

			?>
			
			<div class="sinscrire">
				<a href="<?php echo site_url( "/wp-login.php") . "?redirect_to=" . urlencode( site_url( "classcode-v2") ) ?>">SE CONNECTER</a>
			</div>
			
			<?php
		}
		
		?>
		
		<div class="clear"></div>
		
		<!--div class="avertissement">
			<a href="https://pixees.fr/classcode-v1">Aller vers l'ancienne version du site</a>
		</div-->
		
	</header>
	
	<div class="page-content">
	