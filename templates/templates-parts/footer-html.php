
	<div class="t9">Class’Code est soutenu (sous le nom de MAAISoN) au titre du PIA, suite à l’appel à projets « Culture de l’innovation et de l’entrepreneuriat » lancé dans le cadre du Fonds national de l’Innovation. Cette action est gérée par la Caisse des Dépôts pour le compte de l’Etat avec le concours et le suivi du Commissariat Général à l’Investissement et des ministères chargés de l’Education Nationale de l’Enseignement supérieur et de la Recherche, et de l’Économie, de l’Industrie et du Numérique.</div>
	
	<footer>
	</footer>
	
</body>
</html>
