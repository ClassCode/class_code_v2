
	<script type="text/javascript">
		
		jQuery(function($){

				// Header : Menus
			
				$( '.icone-menu-principal-classcode2' ).click(function(e){
					e.stopPropagation();
				 	$(this).toggleClass('is-active')
				    $('.responsive-menu.left-menu').toggleClass('expand')
					$('.responsive-menu.right-menu').removeClass('expand')
				})
				 
				$( 'html' ).on("click", function(){
				 	$( '.icone-menu-principal-classcode2' ).removeClass('is-active')
				 	$( '.utilisateur-connecte' ).removeClass('is-active')
				    $('.responsive-menu').removeClass('expand')
				});
				
				// Slider
				$(".bxslider").bxSlider({
					mode: "horizontal",
					randomStart: false,
					auto: ($(".bxslider li").length > 1) ? true: false,
					controls: false,
					pager:false,
					speed:800 
				});

			})
	</script>
