<?php
	if ( have_posts() ) {
		while ( have_posts() ) {

			the_post();

			// Contenu du post :
			$htmlContent = get_the_content();
			
			$dom = new DOMDocument();
			// $dom->encoding='UTF-8';
			
			@$dom->loadHTML( $htmlContent );

			// Récupération de la première image
			$imgs = $dom->getElementsByTagName('img');
			foreach ($imgs as $img){
				
				// URL de l'image
				$imgHeaderSrc = $img->getAttribute("src");
					
				// On retire le noeud de l'image du DOM HTML
				$img->parentNode->removeChild($img); 
					
				break;
			}

			// Récupération de description de la coordination
			$divCoordination = $dom->getElementById ('coordination');
			if ($divCoordination ) {
				// On le met de côté
				$htmlCoordination = $dom->saveHTML($divCoordination);

				// On retire le noeud de l'ul
				$divCoordination->parentNode->removeChild($divCoordination); 
			}
			
			
			// Le rendu HTML du DOM est :
			$htmlContent = utf8_decode( $dom->saveHTML( $dom ));
			$htmlContent = do_shortcode( $htmlContent );
		}
	}
?>

<?php if ( $imgHeaderSrc ) { ?>
	<div class="classcode-v2-coordinations-header-image" style="background-image: url( <?php echo CLASSCODE2_PLUGIN_URL . $imgHeaderSrc; ?> )"></div>
<?php } ?>

<div class="classcode-v2 content-category coordinations" >

	<p class="content-category-title"><?php echo get_the_title(); ?></p>

	<?php if ( $htmlCoordination ) { ?>

	<!-- Modules 1 à 5 -->

	<div class="page-header">

		<div class="page-header-columns">

			<div class="page-header-column description-coordination">
				<div class="page-header-column-title">Coordination</div>
				<div class="page-header-column-content">
					<div class="presentation-module">
							<?php echo utf8_decode( $htmlCoordination ); ?>
					</div>
				</div>
			</div>
			
			<div class="page-header-column rencontres">
				<div class="page-header-column-content no-padding" style="background-image: url( <?php echo CLASSCODE2_PLUGIN_URL.'assets/images/coordinateurs/rencontres.png'; ?> )">
					<a href="<?php echo site_url( CLASSCODE2_ROUTE. "?meetingSearch=true#mapWidget" ); ?>">
					</a>
				</div>
				<p class="page-header-column-bottom"><a href="<?php echo site_url( CLASSCODE2_ROUTE. "?meetingSearch=true#mapWidget" ); ?>">Les rencontres en région</a></p>
			</div>
			
			<div class="page-header-column carte-rencontres">
				<div class="page-header-column-content no-padding" style="background-image: url( <?php echo CLASSCODE2_PLUGIN_URL.'assets/images/coordinateurs/carte.png'; ?> )">
					<a href="<?php echo site_url( CLASSCODE2_ROUTE. "?structureSearch=true#mapWidget" ); ?>">

					    </a>
				</div>
				<p class="page-header-column-bottom"><a href="<?php echo site_url( CLASSCODE2_ROUTE. "?structureSearch=true#mapWidget" ); ?>">Cartographie du réseau partenaire</a></p>
			</div>

<?php
{
  $where = preg_replace("|/classcode-v2/([^/]*)/|", "$1", $_SERVER['REQUEST_URI']);
  $newsletters = array(
		       "classcode-en-occitanie" => "http://eepurl.com/ddeHi5",
		       "classcode-en-nouvelle-aquitaine" => "https://sympa.inria.fr/sympa/subscribe/classcode-nouvelle-aquitaine",
		       "classcode-en-paca" => "https://sympa.inria.fr/sympa/subscribe/classcode-sudest",
		       );
  $newsletter = isset($newsletters[$where]) ? $newsletters[$where] : "javascript:alert('Pas encore de newsletter pour cette région !')";
}
?>

			<div class="page-header-column newsletter">
				<div class="page-header-column-content no-padding">
					<a target="_blank" href="<?php echo $newsletter; ?>">
						<img  src="<?php echo CLASSCODE2_PLUGIN_URL; ?>/assets/images/coordinateurs/newsletter.png" alt="abonnez-vous à la newsletter" />
						<br/>Restez informer et échanger avec le réseau Class'Code dans votre région en vous inscrivant à la liste de diffusions 
					</a>
				</div>
			</div>
						
			<div class="clear"></div>
		
		</div><!-- page-header-columns -->
		
	</div--><!-- header -->

	<?php } ?>

	<div class="content-wp page-parcours" >
	<?php 
		// Contenu HTML hors vidéo et objectifs pédago
		echo $htmlContent;
	?>
	</div>


	<script type="text/javascript">

		jQuery(function($){

			
			// Toggle des différentes parties
			var moduleParts = $(".page-parcours h2.module-partie");
			
			moduleParts.on("click", function(e) {
				
				if ( $(this).hasClass("closed") ) {
					$(this).removeClass("closed");
					$(this).nextUntil("h2").css("display", "block");
				} else {
					$(this).addClass("closed");
					$(this).nextUntil("h2").css("display", "none");
				}
				
				
			});
			
			
			// Hauteur des blocs du header
			
			var resizeColumns = function() {

				var maxHeight = 0;

				var columns = $('.page-header-column');

				columns.each(function(index, element){
					 maxHeight = Math.max(maxHeight, $(element).height());
					console.log(index, element, $(element).height())
				});

				columns.css("height", maxHeight+ "px");
			}

			resizeColumns();

			$(window).on("resize", function(){ 
				resizeColumns();
			});
		})

	</script>


	<?php 
	/*

	$permalink = get_permalink();
	$permalink_parts = explode( "/", $permalink);
	if ( count($permalink_parts) > 0) {

		$last_part = array_pop( $permalink_parts );
		if ( strlen( $last_part ) === 0 ) {
			$last_part = array_pop( $permalink_parts );
		}

		$module_no_for_last_parts = array(

			"module1"     => "module-1",
			"module2"     => "module-2",
			"module3"     => "module-3",
			"module4"     => "module-4",
			"module5"     => "module-5",
			"icn"         => "mooc-icn-informatique-et-creation-numerique",
			"1-2-3-codez" => "1-2-3-codez"
		);

		$category = $module_no_for_last_parts[ $last_part ];

		if ( isset ($category)) {
			echo do_shortcode( '[classcode2_module category="'. $category .'"]' );
		}
	}
	
	*/
	?>

</div><!-- classcode-v2 -->
