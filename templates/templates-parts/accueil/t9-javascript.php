
	<div class="t9">Class’Code est soutenu (sous le nom de MAAISoN) au titre du PIA, suite à l’appel à projets « Culture de l’innovation et de l’entrepreneuriat » lancé dans le cadre du Fonds national de l’Innovation. Cette action est gérée par la Caisse des Dépôts pour le compte de l’Etat avec le concours et le suivi du Commissariat Général à l’Investissement et des ministères chargés de l’Education Nationale de l’Enseignement supérieur et de la Recherche, et de l’Économie, de l’Industrie et du Numérique.</div>

	<script type="text/javascript">
		
		jQuery(function($){

				// Header : Menus
			
				$( '.icone-menu-principal-classcode2' ).click(function(e){
					e.stopPropagation();
				 	$(this).toggleClass('is-active')
				    $('.responsive-menu.left-menu').toggleClass('expand')
					$('.responsive-menu.right-menu').removeClass('expand')
				})
				 
				$( 'html' ).on("click", function(){
				 	$( '.icone-menu-principal-classcode2' ).removeClass('is-active')
				 	$( '.utilisateur-connecte' ).removeClass('is-active')
				    $('.responsive-menu').removeClass('expand')
				});
				
				
				// T2 : Slider
				$(".bxslider").bxSlider({
					mode: "horizontal",
					randomStart: true,
					auto: true,
					controls: false,
					pager:false,
					speed:800 
				});
					 

				// T3, T6 et T8 : Fonctionnement des flèches
			
				var getNbCards = function( t_class ) {
					return $(".card", t_class).length;
				}
				
				var getNbCardsByPage = function( t_class ) {
					var cardsParentWidth = $(".cards-with-arrows .cards", t_class).width();
					var cardWidth = $(".cards-with-arrows .card", t_class).width();
					return Math.floor(cardsParentWidth / cardWidth);
				}

				var positionCardsWithOffset = function( classSelector ){
					
					var cards = $(".cards-with-arrows .cards", classSelector);
					var cardsContainer = $(".cards-with-arrows .cards-container", classSelector);
					
					var nbCardsHiddenOnLeft = parseInt( cards.attr("data-offset") );
					if (isNaN(nbCardsHiddenOnLeft)) nbCardsHiddenOnLeft = 0;
					
					var widthFactor = 100 / getNbCardsByPage(classSelector);
					var leftPourcent = - Math.floor(widthFactor * nbCardsHiddenOnLeft) + "%";
					cardsContainer.css("left", leftPourcent);
				}
					
				var displayPreviousCards = function( classSelector ){
					
					var cards = $(".cards-with-arrows .cards", classSelector);
					var cardsContainer = $(".cards-with-arrows .cards-container", classSelector);
					
					var nbCards = getNbCards( classSelector );
					
					// Nombre de cartes maximum affichées sur la page (3, 4, 5, 6...)
					var nbCardsByPage = getNbCardsByPage(classSelector);
					
					// Valeur de l'offset (no de la première carte visible : par défaut = 0)
					var nbCardsHiddenOnLeft = parseInt( cards.attr("data-offset") );
					if (isNaN(nbCardsHiddenOnLeft)) nbCardsHiddenOnLeft = 0;
						
					// Nombre de cartes non visibles à droite
					var nbCardsHiddenOnRight = (nbCards - nbCardsHiddenOnLeft - nbCardsByPage);
					
					if (nbCardsHiddenOnLeft > 0) {
						
						// On ne décale pas plus qu'il n'y a de vignettes affichables au maximum dans la page
						var nbCardsToSlide = Math.min( nbCardsHiddenOnLeft, nbCardsByPage );
						
						// Nombre de cartes non visibles à gauche à la fin de l'opération
						nbCardsHiddenOnLeft -= nbCardsToSlide;
						
						// Nombre de cartes non visibles à droite à la fin de l'opération
						nbCardsHiddenOnRight -= nbCardsToSlide;
						
						var widthFactor = 100 / getNbCardsByPage(classSelector);
						var leftPourcent = - Math.floor(widthFactor * nbCardsHiddenOnLeft) + "%";
						
						cardsContainer.css("left", leftPourcent);
						cards.attr("data-offset", nbCardsHiddenOnLeft);
					}
					
					$(".cards-with-arrows .arrow", classSelector).removeClass("arrow-hidden");
					  
					if (nbCardsHiddenOnLeft === 0) {
						$(".cards-with-arrows .arrow.previous", classSelector).addClass("arrow-hidden");
					}
					
					if (nbCardsHiddenOnRight === 0) {
						$(".cards-with-arrows .arrow.next", classSelector).addClass("arrow-hidden");
					}
				}
				
				var displayNextCards = function( classSelector ){
					
					var cards = $(".cards-with-arrows .cards", classSelector);
					var cardsContainer = $(".cards-with-arrows .cards-container", classSelector);
					
					//
					
					var nbCards = getNbCards( classSelector );
					
					// Nombre de cartes maximum affichées sur la page (3, 4, 5, 6...)
					var nbCardsByPage = getNbCardsByPage(classSelector);
					
					// Valeur de l'offset (no de la première carte visible : par défaut = 0)
					var nbCardsHiddenOnLeft = parseInt( cards.attr("data-offset") );
					if (isNaN(nbCardsHiddenOnLeft)) nbCardsHiddenOnLeft = 0;
					
					// Nombre de cartes non visibles à droite
					var nbCardsHiddenOnRight = (nbCards - nbCardsHiddenOnLeft - nbCardsByPage);
					
					if (nbCardsHiddenOnRight > 0) {
						
						// On ne décale pas plus qu'il n'y a de vignettes affichables dans la page (3 ou 4) 
						var nbCardsToSlide = Math.min( nbCardsHiddenOnRight, nbCardsByPage );
						
						// Nombre de cartes non visibles à gauche à la fin de l'opération
						nbCardsHiddenOnLeft += nbCardsToSlide;
						
						// Nombre de cartes non visibles à droite à la fin de l'opération
						nbCardsHiddenOnRight -= nbCardsToSlide;
						
						var widthFactor = 100 / getNbCardsByPage(classSelector);
						var leftPourcent = - Math.floor(widthFactor * nbCardsHiddenOnLeft) + "%";
						
						/*
						console.log("nbCardsHiddenOnRight", nbCardsHiddenOnRight);
						console.log("nbCardsByPage", nbCardsByPage);
						console.log("nbCardsHiddenOnLeft", nbCardsHiddenOnLeft);
						console.log("widthFactor", widthFactor);
						console.log("leftPourcent", leftPourcent);
						*/
						
						cardsContainer.css("left", leftPourcent);
						cards.attr("data-offset", nbCardsHiddenOnLeft);
					}
					
					$(".cards-with-arrows .arrow", classSelector).removeClass("arrow-hidden");
					  
					if (nbCardsHiddenOnLeft === 0) {
						$(".cards-with-arrows .arrow.previous", classSelector).addClass("arrow-hidden");
					}
					
					if (nbCardsHiddenOnRight === 0) {
						$(".cards-with-arrows .arrow.next", classSelector).addClass("arrow-hidden");
					}
				}
				
				var updateArrowsOfCards = function( classSelector ) {
					
					var nbCards = getNbCards(classSelector);
					var nbCardsByPage = getNbCardsByPage(classSelector);

					$(".cards-with-arrows .arrow.previous", classSelector).removeClass("arrow-hidden").addClass("arrow-hidden");
					
					if (nbCards <= nbCardsByPage) {
						$(".cards-with-arrows .arrow.next", classSelector).removeClass("arrow-hidden").addClass("arrow-hidden");
					}
				}
				
			
				// T3 : À la carte : présence des flèches au chargement
				updateArrowsOfCards(".t3");

			
				// T6 : Témoignages : présence des flèches au chargement 
				updateArrowsOfCards(".t6");

			
				// T6 : Partenaires : présence des flèches au chargement 
				updateArrowsOfCards(".t8");
			
			
				// Fonctionnement des flèches
			
				$(".arrow.previous").on("click", function() {
					var classSelector = "." + $(this).closest(".content-category").attr("data-tid");
					displayPreviousCards( classSelector );
				});
			
				$(".arrow.next").on("click", function(){
					var classSelector = "." + $(this).closest(".content-category").attr("data-tid");
					displayNextCards( classSelector );
				});
			
			
				// Resize : ajustement vertical des flèches
			
				function resizeCards() {
					 
					 var maxHeight = 0;
					
					// T3 : À la carte : impose la même hauteur à tous les blocs 
					var cards = $('.t3 .card');
					 
					cards.each(function(index, element){
						 maxHeight = Math.max(maxHeight, $(".card-content", element).height());
					});
					 
					cards.css("min-height", maxHeight+ "px");
					cards.closest(".cards-with-arrows").children(".arrow").css("height", maxHeight+ "px");
					
					updateArrowsOfCards(".t3");
					positionCardsWithOffset(".t3");
					 
					
					// T6 et T8 : positionne les flèches au centre verticalement 
					$('.t6 .arrow').css("height", $('.t6 .cards').height() + "px");
					$('.t8 .arrow').css("height", $('.t8 .cards').height() + "px");
				 };
			
				resizeCards();
			
				$(window).on("resize", function(){ 
					resizeCards();
				});
				
			
				// Shadow Box
			})
	</script>
