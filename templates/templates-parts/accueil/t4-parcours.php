
	<div id="parcours" class="content-category t4">
		<p class="content-category-title">PARCOURS<span class="content-category-subtitle">des formations conçues par des experts en sciences du numérique à suivre à votre rythme</span></p>
		
		<div class="cards">
		
			<?php 

				$wp_query = new WP_Query( array(
					'posts_per_page'  => 10,
					'offset'	      =>  0,
					'orderby'         => 'menu_order',
					'order'           => 'ASC',
					'post_status'    => "publish",
					'post_type'       => array('post','page'),
					'category_name'   => "les-parcours"
				));

				if ( $wp_query->have_posts() ) {

					while ( $wp_query->have_posts() ) {

						$wp_query->the_post();

						$post_id     = get_the_ID();
						$post_type   = get_post_type( $post_id );
						$post_title  = get_the_title();
						$post_url    = get_permalink();
						$post_target = '';

						$first_link = get_post_1st_link(); 
						if ($first_link != FALSE ) {
							$post_url = $first_link;
						}
					
						if ( strpos ($post_url, 'classcode/formations/module' ) === FALSE ) {
							$post_target = "_blank";
						}
						
						if (has_post_thumbnail( $post_id ) ) {
							// --> post_image = [ url, width, height, is_intermediate ]
							$post_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );
							$post_img = $post_image[0];
						}
						
						// Description du lien
						$post_description = get_the_excerpt();
						if ( !$post_description || $post_description == "") {
							$post_description = get_post_meta( $post_id, "_amt_description", true );
						}
						

					  // Rendu HTML  :
						
					?>		
					
					<div class="card">
						<a href="<?php echo $post_url; ?>"  target="<?php echo $post_target; ?>">
							<div class="card-content">
								<div class="card-thumbnail" style="background-image: url(<?php echo $post_img; ?>);">
									<?php if ( $post_description && strlen($post_description) > 0 ) { ?>
									<div class="card-thumbnail-description">
										<?php echo $post_description; ?>
									</div>
									<?php } ?>
								</div>
								<div class="titles">
									<p class="title"><?php echo $post_title ?></p>
								</div>
							</div>
						</a>
					</div>
					
					<?php
					}
				}
			?>		
			
		</div>
		<div class="clear"></div>
	</div>