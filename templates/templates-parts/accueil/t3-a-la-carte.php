
		<div id="a-la-carte" class="content-category t3" data-tid="t3">

			<p class="content-category-title">À LA CARTE<span class="content-category-subtitle">des dizaines de ressources éducatives libres sur le code et la pensée informatique à utiliser et réutiliser...</span></p>
			
			<div class="cards">

				<?php 

					$categories_for_menu = array( 'activite-branchee' , 'activite-debranchee', 'prendre-du-recul', 'pedagogie' );
					$types_de_contenu = array( 'video', 'activite', 'texte' );
				
					$libelles_categories_thematiques = get_libelles_categories_thematiques();
				
					// On récolte les infos sur chacune des catégories
																						  
					// a) Catégories ( branché, débranché, recul, pédagogie )
					$categories_infos = array();
					foreach($categories_for_menu as $category) {
						
						$term = get_term_by( "slug", $category, "category");
						$termId = $term->term_id;
						$term_children_slugs = get_children_slug_categories( $termId);
						
						$libelle = $libelles_categories_thematiques[$category];
						$libelle = $libelle && strlen($libelle)>0 ? $libelle : $term->name;

						$term_infos = array(
							'ID'       => $termId,
							'slug'     => $category,
							'name'     => $libelle,
							'children' => $term_children_slugs,
							'permalink'=> '<a href="' . site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ). $category . '" >' . $libelle . '</a>'
						);
						
						$categories_infos[ $category ] = $term_infos;
					};
																						  
					// b) Type de contenus ( video, texte, activite )
					$types_de_contenu_infos = array();
					foreach($types_de_contenu as $category) {
						
						$term = get_term_by( "slug", $category, "category");
						$termId = $term->term_id;
						$term_children_slugs = get_children_slug_categories( $termId);
						
						$term_infos = array(
							'ID'       => $termId,
							'slug'     => $category,
							'name'     => $term->name,
							'children' => $term_children_slugs
						);
						
						$types_de_contenu_infos[ $category ] = $term_infos; 
					};
					
																						  
					$posts_for_menu_assoc = array();

																						  
					// 0. Activités "à la carte" (mises en avant éditoriales)
						
					$query = new WP_Query( array(
						'posts_per_page' => -1,
						'offset'	     => 0,
						'orderby'        => 'menu_order',
						'order'          => 'DESC',
						'post_status'    => "publish",
						'post_type'      => array('post', 'page'),
						'category_name'  => 'a-la-une-de-a-la-carte'
						)
					);
																						  
					// TODO Changer pour simplement passer les resultats en revue (pour éviter les recherches d'images, de liens, ...)
					$query_posts = get_posts_with( $query );
					
					// Pour chaque catégorie, on recherche si un post correspond dans les posts "à la une à la carte":
					foreach($categories_for_menu as $category) {
						foreach($query_posts as $query_post) {
							$post_id = $query_post["id"];
							if (has_category( $category, $post_id )) {
								$posts_for_menu_assoc[$category] = $query_post;
								break;
							}
						}
					}
																						  
																						  
					// On a maintenant dans $posts_for_menu les références des éventuels articles mis en avant pour "à la carte"
																						  
					// Si une de ces catégories n'est pas représentée, on recherche parmi tous les posts associés à la catégorie,
					// et on prend celui correspondant à la semaine en cours
																						  
					// No de la semaine relativement à une date fixe (epoch)
				    $week_no = (int) ( time() / (7 * 24 * 60 * 60));
																						  
					foreach($categories_for_menu as $category) {
						if ( ! $posts_for_menu_assoc[$category] ) {
							
							// On récupère le nombre de posts associés à cette catégorie
							$query = new WP_Query( array(
								'posts_per_page' => -1,
								'offset'	     =>  0,
								'orderby'        => 'menu_order',
								'order'          => 'DESC',
								'post_status'    => "publish",
								'post_type'      => array('post', 'page'),
								'tax_query' => array(
									'relation' => 'AND',
									array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => $category,
									),
									array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => "video",
									),
									array(
										'taxonomy' => 'category',
										'field'    => 'slug',
										'terms'    => array( "module-1", "module-2", "module-3", "module-4", "module-5" ),
									)
								)
							));
							
							if ($query->found_posts === 0 ) {
								
								$query = new WP_Query( array(
									'posts_per_page' => -1,
									'offset'	     =>  0,
									'orderby'        => 'menu_order',
									'order'          => 'DESC',
									'post_status'    => "publish",
									'post_type'      => array('post', 'page'),
									'tax_query' => array(
										'relation' => 'AND',
										array(
											'taxonomy' => 'category',
											'field'    => 'slug',
											'terms'    => $category,
										)
									)
								));
								
							}
							
							if ($query->found_posts > 0 ) {
								
								$query_posts = get_posts_with( $query );

								// No du post
								$post_no = $week_no % $query->found_posts;
								$post    = $query_posts[$post_no];
								$post["category-for-menu"] = $category;

								// Post
								$posts_for_menu_assoc[$category] = $post;
								
							}				
						}				
					}

					// Tableau linéaire. On les affiche dans l'ordre du tableau des catégories (cf plus haut):
					$posts_for_menu = array();
					foreach($categories_for_menu as $category) {
						$post = $posts_for_menu_assoc[$category]; 
						if ($post) {
							array_push($posts_for_menu, $post);
						}
					}
																						  
					foreach($posts_for_menu as $post) {
							
							$post_id    = $post["id"];
							$post_type  = $post["type"];
							$post_title = $post["title"];
							$post_url   = $post["permalink"];
							$post_target= $post["target"];
							$post_img   = $post["thumbnail"];
							$post_category = $post["category-for-menu"];
						
							$category_infos = $categories_infos[ $post_category ];
							$category_permalink = $category_infos["permalink"];
						
							$categories_permalinks = get_post_links_to_categories( $post );

						  // Rendu HTML  :

						?><div class="card">
							
								<div class="card-content">
									<a class="post_link" href="<?php echo $post_url; ?>" target="<?php echo $post["target"] ?>">
										<div class="card-thumbnail" style="background-image: url(<?php echo $post_img; ?>);"></div>
									</a>
									<div class="titles">
										<p class="categories"><?php echo $categories_permalinks ?></p>
										<p class="title">
											<a href="<?php echo $post_url; ?>" target="<?php echo $post["target"] ?>">
											<?php echo $post_title ?>
											</a>
										</p>
									</div>
								</div>
						</div><?php
					}
				?>		
					
						<div class="card search">
							<div class="search-content">
								<a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/a-la-carte"); ?>">
									+400<br/>
									RESSOURCES<br/>
									À LA CARTE
								</a>
							</div>
						</div>
			</div>
			<div class="clear"></div>
		</div>
	
	