
	<?php

	include CLASSCODE2_PLUGIN_DIR . "/temoignages/data_temoignages.php";

	$t6_videos = get_temoignages();

	?>


	<div id="temoignages" class="content-category t6" data-tid="t6">
		<p class="content-category-title">TÉMOIGNAGES</p>
		<div class="cards-with-arrows">
		
			<div class="arrow previous"><span class="helper"></span><img src="<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/arrow-previous.png" alt="" /></div>
			
			<div class="cards">
				<div class="cards-container">
				
					<?php  foreach( $t6_videos as $video ) { 
	
						// RQ : il ne doit pas y avoir d'espace entre les divs (sinon il y aura un écart entre les vignettes)
	
					 ?><div class="card card-width">
						<div class="card-thumbnail" style="background-image:url(https://img.youtube.com/vi/<?php echo $video['id']; ?>/hqdefault.jpg)">
						</div>
						<div class="youtube-play-btn card-width">
							<?php 
	
							$http = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443 ? "https" : "http";
	
							$shortCode = '[cc_module_video notext="true" title="' . $video['titre']. '" url="'. $http .'://www.youtube.com/embed/' . $video['id']. '?autoplay=1"]';

							echo do_shortcode($shortCode);
	
							?>
						</div>
					</div><?php } ?>
					
				</div>
			</div>
			<div class="arrow next"><span class="helper"></span><img src="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/images/arrow-next.png" alt="" /></div>
		</div>
		<div class="clear"></div>
	</div>
