	<div id="liens" class="content-category t7">
		<div class="t7_col t7_1">
			<p class="content-category-title">SUR TWITTER <span class="twitter_button" data-lang="fr" style="float: right;font-size:10px"><a target="_blank" href="https://twitter.com/classcode_fr?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-count="false">Suivre @classcode_fr</a></span></p>
			<div class="t7-twitter-feed">
				<div class="widget">
					<!-- widget -->
					<a class="twitter-timeline" href="https://twitter.com/classcode_fr"></a>
					<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
				</div>
				<div class="t7-twitter-fallback">
					<!-- bouton Suivre (fallback) -->
					<a target ="_blank" href="https://twitter.com/classcode_fr" class="twitter-follow-button" data-show-count="false" data-lang="fr" data-size="large"><img style="width:90%; margin: 10px auto 20px auto" src="<?php echo get_site_url();?>/wp-content/uploads/2017/10/classcode-on-twitter.png"/><br/>Suivre Class´Code sur twitter.</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="t7_col t7_2">
			<p class="content-category-title">SUR LE BLOG</p>
			<div class="t7-blog-entries">
			
				<?php

				$shortcode = '[import tag_name="article" url="https://classcode.fr/projet/category/actualite/" noscript="true" ]';
				$renderHtml = do_shortcode($shortcode);
				
				$renderHtml = str_replace( array( '<em>', '</em>', '<em class="">'),  "", $renderHtml);

				echo $renderHtml;

				?>
				
				<article class="consulter_plus"><a href="https://project.inria.fr/classcode/category/archive/" target="_blank">En consulter plus</a></article>
				
			</div>
		</div>
		<div class="t7_col t7_3">
			<p class="content-category-title">En savoir plus ?</p>
			<ul class="t7-aide">
				<li><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/faq"); ?>">La Foire aux questions</a></li>
				<li><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/contacts"); ?>">Contacts</a></li>
				   <li><a target="_blank" href="https://project.inria.fr/classcode/nos-newsletters/">Newsletter</a></li>
				<li><a target="_blank" href="https://www.facebook.com/groups/1865612573684678/">Sur facebook</a></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
