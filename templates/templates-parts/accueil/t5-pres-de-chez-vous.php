<?php
include_once(WP_PLUGIN_DIR.'/class_code_v2/class_code_config.php');
include_once(WP_PLUGIN_DIR.'/class_code_v2/rencontres/rencontre_type.php');
  
global $title_module;
global $data_structures;
global $wpdb;

//Inclusions for OpenStreetMap
wp_enqueue_style('leaflet','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/leaflet.css');
wp_enqueue_style('markercluster','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/plugins/leaflet.markercluster-1.3.0/dist/MarkerCluster.Default.css');  

wp_enqueue_script('leaflet','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/leaflet.js');
wp_enqueue_script('markercluster','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/plugins/leaflet.markercluster-1.3.0/dist/leaflet.markercluster.js');  

$structureFilterClassSelected ="";
$coordinationFilterClassSelected ="";
$meetingFilterClassSelected ="";
$userFilterClassSelected ="";
$userSearch = false;
$meetingSearch = false;
$structureSearch = false;
$cordinationSearch = false;
$JSMap = "";
$htmlCoordList = "";
$htmlStructList ="";
$htmlMeetingForm = "";
$htmlUserForm =  "";

$visitorId = get_current_user_id();  
$visitorProfile = false;
$noVisitorLocation = false;
$visitorLocation = false;

if($visitorId){
  $visitorProfile = profile_type::get_profile($visitorId);
  if(isset($visitorProfile['location'])){
    $visitorLocation = $visitorProfile['location'];  
  }  
}
if(!$visitorLocation){
  $noVisitorLocation = true;
  $visitorLat = '46.52863469527167';
  $visitorLong = '2.43896484375';
  $mapZoom = '5';
}else{
  $visitorLat = $visitorLocation['lat'];
  $visitorLong = $visitorLocation['lng'];
  $mapZoom = '11';
}
if(isset($visitorProfile['displayname'])){
  $visitorDisplayName = addslashes($visitorProfile['displayname']);
}else{
  $visitorDisplayName = 'Visiteur';
}

$globalJSMap = "  map = L.map('classCodeMap', ";
$globalJSMap.= "   {maxZoom: 18}).setView([lat,lng], ".$mapZoom." ), ";
$globalJSMap.= "   markers = L.markerClusterGroup({ ";
$globalJSMap.= "   spiderfyOnMaxZoom: true,";
$globalJSMap.= "   showCoverageOnHover: false,";
$globalJSMap.= "   zoomToBoundsOnClick: true,";
$globalJSMap.= "   polygonOptions: {";
$globalJSMap.= "     fillColor: '#FFFFFF',";
$globalJSMap.= "     color: '#FFFFFF',";
$globalJSMap.= "     weight: 0.5,";
$globalJSMap.= "     opacity: 1,";
$globalJSMap.= "     fillOpacity: 0.5";
$globalJSMap.= "   }";
$globalJSMap.= " }),";
$globalJSMap.= " L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {";
$globalJSMap.= "   maxZoom: 18,";
$globalJSMap.= "   attribution: '&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors'";
$globalJSMap.= " }).addTo(map);";

$globalJSMap.= "  var visitorDisplayName ='<div class=\"meetingMapInfoTitle\">".$visitorDisplayName."</div>';";

if(isset($_POST['meetingSearch'])||isset($_POST['meetingDisplayForm'])|| (isset($_GET['meetingSearch'])&&($_GET['meetingSearch'] == "true" ))){
  $classCodeMeetingListTbody="";
  $what=array();
  if(isset($_POST['mymeeting']) && $_POST['mymeeting'] ){
    $what['mes_rencontres']=true;       
  }
  if(isset($_POST['where'])){
    if($_POST['where']!='everywhere'){
      $what['nearby']=$_POST['where'];  
    }else{
      $what['where']=$_POST['where']; 
    }
  }else{
    //recherche par défaut
    $what['where']='everywhere'; 
  }
  $title_module[]="Présentation de Class'Code";
  $title_module[]="Rencontre de partenaires locaux";
  $title_module[]="Echange entre facilitateurs";
  if(isset($_POST['module']) && $_POST['module'] ){
    $what['module']=stripslashes($_POST['module']);       
  }else{
    //recherche par défaut
    if(isset($_GET['module']) && array_key_exists($_GET['module'],$title_module) && !isset($_POST['meetingSearch'])) {
      $what['module']=$title_module[$_GET['module']];  
    }
  }
  if(isset($_POST['meetingStructure']) && $_POST['meetingStructure'] ){
    $what['structure']=stripslashes($_POST['meetingStructure']);       
  }else if(isset($_GET['meetingStructure']) && $_GET['meetingStructure'] && !isset($_POST['meetingSearch']) ){
    $what['structure']=stripslashes($_GET['meetingStructure']);    
  }
  //Nettoyage du champ de structure : on remplace les " par des ' pour éviter de perdre de l'info dans la balise <option>
  //$what['structure'] = str_replace('"',"'",$what['structure']);

  if(isset($_POST['when']) && $_POST['when'] ){
    $what['when']=$_POST['when'];       
  }else if(isset($_GET['when']) && $_GET['when'] && !isset($_POST['meetingSearch']) ){
    $what['when']=$_GET['when'];       
  }else{
    //recherche par défaut
    $what['when']='future';
  }
  $meetingFilterClassSelected="filterSelected";
  $meetingSearch = true;

}else if(isset($_POST['userSearch'])||isset($_POST['userDisplayForm']) || (isset($_GET['userSearch'])&&($_GET['userSearch'] == "true")) ){
  $userFilterClassSelected="filterSelected";
  $userSearch = true;
 
  $who ='all';
  if(isset($_POST['who']) && $_POST['who'] != '' ){
    $who = $_POST['who'];
  }
}else if(isset($_POST['structureSearch']) || (isset($_GET['structureSearch'])&&($_GET['structureSearch'] == "true" ))){
  $structureFilterClassSelected="filterSelected";
  $structureSearch = true;
  $mapZoom = '5';  
}else{
   //recherche par défaut
  $coordinationSearchFilter="";
  if(isset($_GET['region'])&&($_GET['region'] != "" )){
    $coordinationSearchFilter=$_GET['region'];
  }
  $coordinationFilterClassSelected="filterSelected";
  $coordinationSearch = true;
  $mapZoom = '5';  
} 

if($meetingSearch){
  $JSMap.= 'var meetingArrayMarker = [];';
  $JSMap.= 'function loadOSmap(lat,lng) {';
  $JSMap.=   'var meetingContentString = [];';  
  $JSMap.= $globalJSMap;
  
  if(!$noVisitorLocation){        
    $JSMap.= " meetingArrayMarker[0] = L.marker(L.latLng('".$visitorLat."','".$visitorLong."'), { title: '".$visitorDisplayName."', icon:visitorMarkerIcon, id: '0', userId: ".$visitorId.", desc: '<div id=\"profilDisplay\"></div>' } );";

    $JSMap.= "markers.addLayer(meetingArrayMarker[0]);";
    $JSMap.= " meetingArrayMarker[0].on('click',"; 
    $JSMap.= "   function(event){clickMarker(this);});";
  }else{
    $JSMap.= "  meetingArrayMarker[0]='';";
  }

  $meeting_inc = 1;
  $structure_names_array = array();
  $structure_logos_array = array();
  $structure_url_array = array();
  foreach($data_structures as $structure){
    if(($structure['name']) && ($structure['name'] != '')){
      $struct_name =  htmlspecialchars_decode($structure['name'],ENT_QUOTES);
      $structure_names_array[] = $struct_name;
      $structure_logos_array[$struct_name] = $structure['logo'];
      $structure_url_array[$struct_name] = $structure['url'];
    }
  }
  $meetings = rencontre_type::get_rencontres($what);
  foreach($meetings as $meeting){
    $meeting['url']= get_site_url()."/classcode-v2/rencontres/?meetingId=".$meeting['id'];//a corriger avec la nouvelle interface classcodeadmin
    if(!$meeting['structure'] || $meeting['structure'] == ''){       
      $meeting['structure']= $meeting['ownerDisplayName'];
    }
    $meeting['fulldate']="";
    if((null !== $meeting['date']) && ($meeting['date']!="")){
      $meeting['fulldate'] .= $meeting['date'];
    }
    if(($meeting['fulldate'] != "")&& (null !== $meeting['time']) && ($meeting['time']!="")){
      $meeting['fulldate'] .= " à ".$meeting['time'];
    }
    
    if( ($meeting['location']['lat']!=0) || ($meeting['location']['lng']!=0) ){      
        $JSMap.= "meetingContentString[".$meeting_inc."]= '<div class=\"classCodeMapOverlayEmphasis\">Qui ?</div>'; ";        
        $meeting_struct_for_js = addslashes($meeting['structure']);        
        if((in_array($meeting['structure'], $structure_names_array)) && isset($structure_logos_array[$struct_name]) && ($structure_logos_array[$struct_name] !='')){        
          $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div class=\"classCodeMapOverlayContentLogo\"><a title=\'".$meeting_struct_for_js."\' href=\'".$structure_url_array[$meeting_struct_for_js]."\'><img width=\'180\' src=\'".$structure_logos_array[$meeting_struct_for_js]."\' alt=\'".$meeting_struct_for_js."\'/></a></div>';";               
        }else{
          $JSMap.= "meetingContentString[".$meeting_inc."]+=  '<div>".$meeting_struct_for_js."</div>';";
        }        
        $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div class=\"classCodeMapOverlayEmphasis\">Quand ?</div>'; ";
        if($meeting['fulldate']){
          if($meeting['fulldate']){
            $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div>Le ".$meeting['fulldate']."</div>'; ";  
          }          
        }else{
          $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div>Aucune date n\'a encore été définie par l\'organisateur</div>';";  
        }
          
        $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div class=\"classCodeMapOverlayEmphasis\">Où ?</div>'; ";
        if($meeting['location']['formattedAddress']){
          $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div>".addslashes(str_replace("\\", "",$meeting['location']['formattedAddress']))."</div>'; "; 
        }else{
          $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div>Le lieu n\'a encore été défini par l\'organisateur';";  
        }
        
        $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div class=\"classCodeMapOverlayEmphasis\">Quoi ?</div>'; ";
        if($meeting['module']){
          $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div>".addslashes($meeting['module'])."</div>'; "; 
        }else{
          $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div>Le sujet n\'a encore été défini par l\'organisateur';";  
        }
        
        $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div class=\"classCodeMapOverlayEmphasis\">Commentaire</div>'; ";
        if($meeting['precisions']){
          $meeting['precisions']=trim($meeting['precisions']);
          $meeting['precisions']=preg_replace("#\n|\t|\r#","",$meeting['precisions']);
          if(strlen($meeting['precisions'])>140){
            $meeting['precisions']=substr($meeting['precisions'],0,140);
            $meeting['precisions']=$meeting['precisions']."...";
          }
          $JSMap.= "meetingContentString[".$meeting_inc."]+= '<div>".addslashes($meeting['precisions'])."</div>'; "; 
        }
        
        if (is_user_logged_in()) {          
          $JSMap.= "meetingContentString[".$meeting_inc."]+=  '<a class=\"classCodeMapOverlayLink\" href=\"".$meeting['url']."\"> > Voir la rencontre</a>';";
          if($meeting['userStatut'] =='organisateur'){
            $JSMap.= "meetingContentString[".$meeting_inc."]+=  '<a class=\"classCodeMapOverlayModifyLink\" href=\"".$meeting['url']."&edit=true\"> > Modifier la rencontre</a>';"; 
          }
    
        }
      
        $JSMap.= " meetingArrayMarker[".$meeting_inc."] = L.marker(L.latLng('".$meeting['location']['lat']."','".$meeting['location']['lng']."'), { title: 'Rencontre#".$meeting['id']."', icon: meetingMarkerIcon, id: '".$meeting_inc."', desc: meetingContentString[".$meeting_inc."] } );";
      
        $JSMap.= " markers.addLayer(meetingArrayMarker[".$meeting_inc."]);";
        $JSMap.= " meetingArrayMarker[".$meeting_inc."].on('click',"; 
        $JSMap.= "   function(event){clickMarker(this);});";

    }
    //transformation du module pour être plus syntétique :
    $meeting_module_tmp=$meeting['module'];
    if(strpos($meeting_module_tmp,'#1')!==FALSE){
      $meeting_module_tmp='#1 Découvrir la Programmation créative';
    }else if(strpos($meeting_module_tmp,'#2')!==FALSE){
      $meeting_module_tmp="#2 Manipuler l'Information";
    }else if(strpos($meeting_module_tmp,'#3')!==FALSE){
      $meeting_module_tmp="#3 Initiez-vous à la Robotique";
    }else if(strpos($meeting_module_tmp,'#4')!==FALSE){
      $meeting_module_tmp="#3 Connectez le Réseau";
    }else if(strpos($meeting_module_tmp,'#5')!==FALSE){
      $meeting_module_tmp="#3 Gérer un projet informatique avec des enfants";
    }
    $classCodeMeetingListTbody.="<tr>";
    $classCodeMeetingListTbody.="<td>".$meeting['fulldate']."</td>";
    $classCodeMeetingListTbody.="<td>".$meeting_module_tmp."</td>";
    $classCodeMeetingListTbody.="<td class='meetingListStructure'>".$meeting['structure']."</td>";
    $classCodeMeetingListTbody.="<td>".$meeting['distance']." km</td>";
    $classCodeMeetingListTbody.="<td>";
    if($meeting['userStatut'] =='organisateur'){
      $classCodeMeetingListTbody.="<a href='".$meeting['url']."&edit=true'>Modifier #".$meeting['id']."</a><br>";
      $classCodeMeetingListTbody.="<a href='".$meeting['url']."'>Voir #".$meeting['id']."</a>";
    }else{
      $classCodeMeetingListTbody.="<a href='".$meeting['url']."'>Voir la rencontre #".$meeting['id']."</a>";
    }
    $classCodeMeetingListTbody.="</td>";
    $classCodeMeetingListTbody.="</tr>";
    $meeting_inc ++;
  }
  
  $JSMap.= " map.addLayer(markers);";
  $JSMap.= "}";

  $JSMap.= "function clickMarker(marker){";

  $JSMap.= "var overlay = document.getElementById('classCodeMapOverlay');";
  $JSMap.= "var overlayTitle = document.getElementById('classCodeMapOverlayTitleContent');";
  $JSMap.= "var overlayContent = document.getElementById('classCodeMapOverlayContent');";
  $JSMap.= "if (!overlayShown) {";
  $JSMap.=   "overlayTitle.innerHTML = marker.options.title;";
  $JSMap.=   "overlayContent.innerHTML = marker.options.desc;";
  
  $JSMap.=     "if((lastSelectedMarker !== false)&&(lastSelectedMarker == 0)){";
  $JSMap.=       "meetingArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=     "}else if(lastSelectedMarker !== false){";
  $JSMap.=       "meetingArrayMarker[lastSelectedMarker].setIcon(meetingMarkerIcon);";
  $JSMap.=     "}";
  $JSMap.=     "marker.setIcon(meetingMarkerIconSelected);";
  
  $JSMap.=     "if(typeof marker.options.userId !== 'undefined'){";
  $JSMap.=       "marker.setIcon(usersMarkerIconSelected);";
  $JSMap.=       "jQuery.ajax({";
  $JSMap.=         "type: 'GET',";
  $JSMap.=         "url: '".get_site_url()."'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+marker.options.userId+'&ajax_profile=true',";
  $JSMap.=         "timeout: 3000,";
  $JSMap.=         "success: function(data) {";
  $JSMap.=           "jQuery('#profilDisplay').html(data);     ";
  $JSMap.=         "},";
  $JSMap.=         "error: function(request) {";
  $JSMap.=           "jQuery('#profilDisplay').html('erreur ajax');";
  $JSMap.=         "}";
  $JSMap.=       "});";
  $JSMap.=     "}";
  
  $JSMap.=     'jQuery("#classCodeMapOverlay").toggle("slow");';
  $JSMap.=     "overlayShown = marker.options.id;";
  $JSMap.=     "lastSelectedMarker = marker.options.id;";
  $JSMap.=   "}else {";
  $JSMap.=     "if (overlayShown != marker.options.id) {";
  $JSMap.=       "overlayTitle.innerHTML = marker.options.title;";
  $JSMap.=       "overlayContent.innerHTML = marker.options.desc;";
  
  $JSMap.=       "if(lastSelectedMarker==0){";
  $JSMap.=         "meetingArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=       "}else{";
  $JSMap.=         "meetingArrayMarker[lastSelectedMarker].setIcon(meetingMarkerIcon);";
  $JSMap.=       "}";
  $JSMap.=       "marker.setIcon(meetingMarkerIconSelected);";
  
  $JSMap.=       "if(typeof marker.options.userId !== 'undefined'){";
  $JSMap.=         "marker.setIcon(usersMarkerIconSelected);";
  $JSMap.=         "jQuery.ajax({";
  $JSMap.=           "type: 'GET',";
  $JSMap.=           "url: '".get_site_url()."'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+marker.options.userId+'&ajax_profile=true',";
  $JSMap.=           "timeout: 3000,";
  $JSMap.=           "success: function(data) {";
  $JSMap.=           "jQuery('#profilDisplay').html(data);     ";
  $JSMap.=           "},";
  $JSMap.=           "error: function(request) {";
  $JSMap.=             "jQuery('#profilDisplay').html('erreur ajax');";
  $JSMap.=           "}";
  $JSMap.=         "});";
  $JSMap.=       "}";
   
  $JSMap.=       "overlayShown = marker.options.id;";
  $JSMap.=       "lastSelectedMarker = marker.options.id;";
  $JSMap.=     "}else{";
  $JSMap.=       "hideMapOverlay();";
  $JSMap.=     "}";
  $JSMap.=   "}";
  //on centre la map sur le marker
  $JSMap.=   "map.panTo(marker.getLatLng());";
  $JSMap.= "}";
        
  
  $htmlMeetingForm = '<div class="criteres-item-list">'; 
  $htmlMeetingForm.= '<form method="post" id="meetingSearchForm" action="#mapWidget">';
  $htmlMeetingForm.= '<input type="hidden" value="true" id="meetingSearch" name="meetingSearch">';
  $checked = '';
  $htmlMeetingForm.='<div id="meetingListLink" class="h3 meetingSearchFormMapChangeLink">Liste ></div>';
  $htmlMeetingForm.='<div id="meetingMapLink" class="h3 meetingSearchFormMapChangeLink" style="display:none;">Carte ></div>';
  if(isset($_POST['mymeeting']) && ($_POST['mymeeting'] == 'true')){
    $checked = 'checked';
  }
  if(is_user_logged_in()) {
    $htmlMeetingForm.= '<div class="h3" style="padding-top:5px;">Limiter à mes rencontres<input type="checkbox" name="mymeeting" '.$checked.' value="true"></div>';
  }
  
  //Quand ?
  $htmlMeetingForm.= '<h3>Quand?</h3>';
  $htmlMeetingForm.= '<select class="meetingFormSelect" name="when">';
  $htmlMeetingForm.= '<option value="all">Tout le temps</option>';
  $checked = '';
  if(isset($what['when']) && ($what['when'] == 'past')){
    $checked = 'selected';
  }
  $htmlMeetingForm.= '<option value="past" '.$checked.'>Rencontres passées</option>';
  $checked = '';
  if(isset($what['when']) && ($what['when'] == 'future')){
    $checked = 'selected';
  }
  $htmlMeetingForm.= '<option value="future" '.$checked.'>Rencontres à venir</option>';
  $htmlMeetingForm.= '</select>';
 
  //Qui ?
  $default_structure = htmlentities($what['structure']);
  $htmlMeetingForm.= '<h3>Qui?</h3>';  
  $htmlMeetingForm.= do_completion_field("meetingStructure", $structure_names_array,$default_structure , "", false, "Filtrer par Structure");       
  //Quoi ?
  $htmlMeetingForm.= '<h3>Quoi</h3>';
  $htmlMeetingForm.= '<select class="meetingFormSelect" name="module">';
  $htmlMeetingForm.= '<option value="all">Tous les sujets</option>';
 
  foreach($title_module as $title) { 
    $checked = '';
    if(isset($what['module']) && ($what['module'] == $title)){
      $checked = 'selected';
    }
    $htmlMeetingForm.= '<option value="'.$title.'" '.$checked.'>'.$title.'</option>';    
  }
  $checked = '';
  if(isset($what['module']) && ($what['module'] == 'other')){
    $checked = 'selected';
  }
  $htmlMeetingForm.= '<option value="other" '.$checked.'> Autre </option>';
  $htmlMeetingForm.= '</select>';

  //Où ?
  if(is_user_logged_in()) {
    $htmlMeetingForm.= '<h3>Où ?</h3>';
    $htmlMeetingForm.= '<select class="meetingFormSelect" name="where">';
    $htmlMeetingForm.= '<option value="everywhere">Partout</option>';
    $htmlMeetingForm.= '<option disabled>Près de chez moi :</option>';  
    $checked = '';
    if(isset($what['nearby']) && ($what['nearby'] == '20')){
      $checked = 'selected';
    } 
    $htmlMeetingForm.= '<option value="20" '.$checked.'>Dans les 20 km</option>';
    $checked = '';
    if(isset($what['nearby']) && ($what['nearby'] == '100')){
      $checked = 'selected';
    } 
    $htmlMeetingForm.= '<option value="100" '.$checked.'>Dans les 100 km</option>';
    $checked = '';
    if(isset($what['nearby']) && ($what['nearby'] == '200')){
      $checked = 'selected';
    } 
    $htmlMeetingForm.= '<option value="200" '.$checked.'> 200 km</option>';
    $htmlMeetingForm.= '</select>';
  }

  $htmlMeetingForm.= '<a class="mapSearch" onclick="searchMeeting();">Rechercher</a>';
  if (is_user_logged_in()) {
    $htmlMeetingForm.= '<a class="createMeetingLink" href="'.get_site_url().'/classcode-v2/rencontres/">Créer une rencontre</a>';
  }
  $htmlMeetingForm.= '</form>';
  $htmlMeetingForm.= '</div>';
    
}else if($userSearch){
  $htmlUserForm = '<div class="criteres-item-list criteres_item-list-last">'; 
  $htmlUserForm.= '<form method="post" id="userSearchForm" action="#mapWidget">';
  if($visitorId){
    $pixees_array = profile_type::get_all_users_with_loc();

    $nb_subscribers_with_loc=count($pixees_array)+1;
    $JSMap.= "  var pixeesArrayMarker = [];";  
    $JSMap.= "function loadOSmap(lat,lng) {";
    $JSMap.= "  var pixeesContentString = [];";
    
    $JSMap.= $globalJSMap;
    
    if(!$noVisitorLocation){        
      $JSMap.= " pixeesArrayMarker[0] = L.marker(L.latLng('".$visitorLat."','".$visitorLong."'), { title: '".$visitorDisplayName."', icon:visitorMarkerIcon, id: '0', userId: ".$visitorId.", desc: '<div id=\"profilDisplay\"></div>' } );";

      $JSMap.= "markers.addLayer(pixeesArrayMarker[0]);";
      $JSMap.= " pixeesArrayMarker[0].on('click',"; 
      $JSMap.= "   function(event){clickMarker(this);});";
    }else{
      $JSMap.= "  pixeesArrayMarker[0]='';";
    }

    $pixees_inc = 1;
    $nb_displayed_pixees = 0;
    $avatarTab=array();
    

    foreach($pixees_array as $pixees){    
      $id = $pixees['userId'];
      
      if($visitorId != $id){   
        $pixeeLocation = $pixees['location'];
        $pixeeLat = $pixeeLocation['lat'];
        $pixeeLong = $pixeeLocation['lng'];

        if(($pixeeLat)&&($pixeeLong) && (($pixeeLat!=0)||($pixeeLong!=0))){
          
          $includePixee = false;
          if(($who=='all')||
            (($who == 'facilitateurs')&&($pixees['facilitator']))||
            (($who == 'participants')&&(!$pixees['facilitator']))){
            $includePixee = true;
            $nb_displayed_pixees++;
          }
          
          if($includePixee){     
            $JSMap.= "pixeesContentString[".$pixees_inc."] ='<div id=\"profilDisplay\"></div>';";
        
            if($pixees['facilitator'] == true){
              $markerImage = 'usersMarkerIconFacilitateur';
            }else{
              $markerImage = 'usersMarkerIcon';  
            }
            
            $JSMap.= " pixeesArrayMarker[".$pixees_inc."] = L.marker(L.latLng('".$pixeeLat."','".$pixeeLong."'), { title: '".$pixees['displayname']."', icon: ".$markerImage.", id: '".$pixees_inc."',userId:".$id.", desc: pixeesContentString[".$pixees_inc."] } );";
          
            $JSMap.= " markers.addLayer(pixeesArrayMarker[".$pixees_inc."]);";
            $JSMap.= " pixeesArrayMarker[".$pixees_inc."].on('click',"; 
            $JSMap.= "   function(event){clickMarker(this);});";
          }    
          $pixees_inc++;
        } 
      }      
    } 
    $JSMap.= " map.addLayer(markers);";
    $JSMap.= "}";
        
    $JSMap.= "function clickMarker(marker){";

    $JSMap.= "var overlay = document.getElementById('classCodeMapOverlay');";
    $JSMap.= "var overlayTitle = document.getElementById('classCodeMapOverlayTitleContent');";
    $JSMap.= "var overlayContent = document.getElementById('classCodeMapOverlayContent');";
    $JSMap.= "if (!overlayShown) {";
    $JSMap.=    "overlayTitle.innerHTML = marker.options.title;";
    $JSMap.=    "overlayContent.innerHTML = marker.options.desc;";
    
    $JSMap.=      "if(lastSelectedMarker !== false){";
    $JSMap.=      "  pixeesArrayMarker[lastSelectedMarker].setIcon(lastSelectedImage);";
    $JSMap.=      "}";
    $JSMap.=    "lastSelectedImage = marker.options.icon;";
    $JSMap.=    "marker.setIcon(usersMarkerIconSelected);";
    
    $JSMap.=    "jQuery.ajax({";
    $JSMap.=      "type: 'GET',";
    $JSMap.=      "url: '".get_site_url()."'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+marker.options.userId+'&ajax_profile=true',";
    $JSMap.=      "timeout: 3000,";
    $JSMap.=      "success: function(data) {";
    $JSMap.=      "jQuery('#profilDisplay').html(data);     ";
    $JSMap.=      "},";
    $JSMap.=      "error: function(request) {";
    $JSMap.=        "jQuery('#profilDisplay').html('erreur ajax');";
    $JSMap.=      "}";
    $JSMap.=    "});";
    
    $JSMap.=    'jQuery("#classCodeMapOverlay").toggle("slow");';
    $JSMap.=    "overlayShown = marker.options.id;";
    $JSMap.=    "lastSelectedMarker = marker.options.id;";
    $JSMap.=    "}else {";
    $JSMap.=    "if (overlayShown != marker.options.id) {";
    $JSMap.=      "overlayTitle.innerHTML = marker.options.title;";
    $JSMap.=      "overlayContent.innerHTML = marker.options.desc;";
    
    $JSMap.=      "pixeesArrayMarker[lastSelectedMarker].setIcon(lastSelectedImage);";
    $JSMap.=      "lastSelectedImage = marker.options.icon;";
    $JSMap.=      "marker.setIcon(usersMarkerIconSelected);";  
    $JSMap.=      "jQuery.ajax({";
    $JSMap.=        "type: 'GET',";
    $JSMap.=        "url: '".get_site_url()."'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+marker.options.userId+'&ajax_profile=true',";
    $JSMap.=        "timeout: 3000,";
    $JSMap.=        "success: function(data) {";
    $JSMap.=          "jQuery('#profilDisplay').html(data);     ";
    $JSMap.=        "},";
    $JSMap.=        "error: function(request) {";
    $JSMap.=          "jQuery('#profilDisplay').html('erreur ajax');";
    $JSMap.=        "}";
    $JSMap.=      "});";
    $JSMap.=      "overlayShown = marker.options.id;";
    $JSMap.=      "lastSelectedMarker = marker.options.id;";
    $JSMap.=    "}else{";
    $JSMap.=      "hideMapOverlay();";
    $JSMap.=    "}";
    $JSMap.=  "}";
    //on centre la map sur le marker
    $JSMap.=   "map.panTo(marker.getLatLng());";        
    $JSMap.= "}";
    
   

   // $htmlUserForm.= '<div class="h3" style="padding-top:5px;">'.$nb_displayed_pixees.' résultats</div>';
    $htmlUserForm.= '<input type="hidden" value="true" id="userSearch" name="userSearch">';
   // $htmlUserForm.= '<br>';
   
    $htmlUserForm.= '<div class="h3" style="padding-top:5px;"><input type="radio" name="who" value="all" checked>Tous</div>';
    $checked = '';
    if(isset($who) && ($who == 'facilitateurs')){
      $checked = 'checked';
    } 
    $htmlUserForm.= '<div class="h3"><input type="radio" name="who" value="facilitateurs" '.$checked.'>Facilitateurs <img src="'.get_site_url().'/wp-content/plugins/class_code_v2/assets/images/pictos/map/usersMarkerFacilitateur.png'.'" style="height:20px; vertical-align:top;"></div>';
    $checked = '';
    if(isset($who) && ($who == 'participants')){
      $checked = 'checked';
    } 
    $htmlUserForm.= '<div class="h3"><input type="radio" name="who" value="participants" '.$checked.'>Participants <img src="'.get_site_url().'/wp-content/plugins/class_code_v2/assets/images/pictos/map/usersMarker.png'.'" style="height:20px; vertical-align:top;"></div>';  

    $htmlUserForm.= '<a class="mapSearch" onclick="searchUser();">Rechercher</a>';
  
  }else{
    $JSMap.= "function loadOSmap(lat,lng) {";
    $JSMap.= $globalJSMap;
    $JSMap.= "}";
    $htmlUserForm.= "<div class='h3' style='padding-top:5px;'>Vous devez être connecté pour voir les Participants à Class'Code</div>";
  }
  $htmlUserForm.= '</form>';
  $htmlUserForm.= '</div> ';
    
}else if($structureSearch){
  $JSMap.= "  var structureArrayMarker = [];";  
  $JSMap.= "  var structureArrayNoMarker = [];"; 
  $JSMap.= "function loadOSmap(lat,lng) {";
  $JSMap.= "  var structureContentString = '';";
  
  $JSMap.= $globalJSMap;
  
  if(!$noVisitorLocation){        
    $JSMap.= " structureArrayMarker[0] = L.marker(L.latLng('".$visitorLat."','".$visitorLong."'), { title: '".$visitorDisplayName."', icon:visitorMarkerIcon, id: '0', userId: ".$visitorId.", desc: '<div id=\"profilDisplay\"></div>' } );";

    $JSMap.= "markers.addLayer(structureArrayMarker[0]);";
    $JSMap.= " structureArrayMarker[0].on('click',"; 
    $JSMap.= "   function(event){clickMarker(this);});";
  }else{
    $JSMap.= "  structureArrayMarker[0]='';";
  }
  
  $structure_inc = 1;
  $structure_inc_noloc =1;
  $data_structure_table_all = array();
  $data_structure_table = array();
  $data_structure_table_noloc = array();
  foreach($data_structures as $id_name => $localStructure){
    $JSMap.= "structureContentString= '';";
    $JSMap.= "structureContentString=  '".($localStructure['logo'] ? "<div class=\"classCodeMapOverlayContentLogo\"><img style=\"max-height:180px;\" src=\"".$localStructure['logo']."\"></div>" : "")."<br>';";     
    $JSMap.= "structureContentString+=  '".($localStructure['who'] ? "<div class=\"classCodeMapOverlayContentTitle\">Contact</div><div class=\"classCodeMapOverlayEmphasis\">".addslashes($localStructure['who'])."</div>" : "")."';";     
    $JSMap.= "structureContentString+=  '".($localStructure['address'] ? "<div>".addslashes($localStructure['address'])."</div>" : "")."';";     
    $JSMap.= "structureContentString+=  '".($localStructure['email'] ? "<div><a href=\"mailto:".addslashes($localStructure['email'])."\" target=\"_blank\">".addslashes($localStructure['email'])."</a></div>" : "")."';";         
    $JSMap.= "structureContentString+=  '".($localStructure['url'] ? "<div><a target=\"_blank\" href=\"".addslashes($localStructure['url'])."\">".addslashes($localStructure['url'])."</a></div>" : "")."';";  
    $JSMap.= "structureContentString+= '<a class=\"classCodeMapOverlayLink\" href=\"?meetingSearch=true&when=all&meetingStructure=".addslashes(urlencode(trim($id_name)))."#mapWidget \"> > Voir les rencontres</a>';";
    $JSMap.= "structureContentString+= '<a class=\"classCodeMapOverlayLink\" href=\"".cc_mailto_shortcode(array("to" => addslashes($localStructure['email']), "who" => addslashes($localStructure['who']), "subject" => "À propos de Class´Code","logo"=> $localStructure['logo']))."\"> > Contactez</a>';";

    $structureLocation = $localStructure['location'];
    if($structureLocation && (($structureLocation['lat']!= 0)||($structureLocation['lng']!= 0))){
      $structureLat = $structureLocation['lat'];
      $structureLong = $structureLocation['lng'];

      $structureDisplayName = addslashes(htmlspecialchars_decode($localStructure['name'],ENT_QUOTES));
      $data_structure_table[]=array(stripslashes($structureDisplayName),$structure_inc);
      $data_structure_table_all[]=array(stripslashes($structureDisplayName),$structure_inc,true);

      $JSMap.= " structureArrayMarker[".$structure_inc."] = L.marker(L.latLng('".$structureLat."','".$structureLong."'), { title: '".$structureDisplayName."', icon: structuresMarkerIcon, id: '".$structure_inc."', desc: structureContentString } );";
      
      $JSMap.= " markers.addLayer(structureArrayMarker[".$structure_inc."]);";
      $JSMap.= " structureArrayMarker[".$structure_inc."].on('click',"; 
      $JSMap.= "   function(event){clickMarker(this);});";
      
      $structure_inc++;
    }else{      
      $structureDisplayName = addslashes(htmlspecialchars_decode($localStructure['name']));
      $JSMap.= "var NoMarker = [];";
      $JSMap.= "NoMarker['title'] = '".$structureDisplayName."';";
      $JSMap.= "NoMarker['id'] = 'noloc_".$structure_inc_noloc."';";
      $JSMap.= "NoMarker['desc'] = structureContentString;";     
      $JSMap.= "structureArrayNoMarker[".$structure_inc_noloc."]=NoMarker;";
      
      $data_structure_table_noloc[]=array(stripslashes($structureDisplayName),$structure_inc_noloc);
      $data_structure_table_all[]=array(stripslashes($structureDisplayName),$structure_inc_noloc,false);
      
      $structure_inc_noloc++;
    }
  } 
      
  $JSMap.= " map.addLayer(markers);";
  $JSMap.= "}";
  
  $JSMap.= "function selectMarker(id){";
  $JSMap.=   "clickMarker(structureArrayMarker[id]);";
  $JSMap.=   "return false;";
  $JSMap.= "}";
  
   $JSMap.= "function selectNoMarker(id){";
  
  $JSMap.=   "var NoMarker=structureArrayNoMarker[id];";
  $JSMap.=   "var overlay = document.getElementById('classCodeMapOverlay');";
  $JSMap.=   "var overlayTitle = document.getElementById('classCodeMapOverlayTitleContent');";
  $JSMap.=   "var overlayContent = document.getElementById('classCodeMapOverlayContent');";
  $JSMap.=   "if (!overlayShown) {";
  $JSMap.=     "overlayTitle.innerHTML = NoMarker['title'];";
  $JSMap.=     "overlayContent.innerHTML = NoMarker['desc'];";
  
  $JSMap.=     "if((lastSelectedMarker !== false)&&(lastSelectedMarker == 0)){";
  $JSMap.=       "structureArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=     "}else if((lastSelectedMarker !== false)&& (lastSelectedMarker!=-1)){";
  $JSMap.=       "structureArrayMarker[lastSelectedMarker].setIcon(structuresMarkerIcon);";
  $JSMap.=     "}";
  
  $JSMap.=     'jQuery("#classCodeMapOverlay").toggle("slow");';
  $JSMap.=     "overlayShown = NoMarker['id'];";
  $JSMap.=     "lastSelectedMarker = -1;";
  $JSMap.=   "}else {";
  $JSMap.=     "if (overlayShown != NoMarker['id']) {";
  $JSMap.=       "overlayTitle.innerHTML = NoMarker['title'];";
  $JSMap.=       "overlayContent.innerHTML = NoMarker['desc'];";
  
  $JSMap.=       "if((lastSelectedMarker !== false)&&(lastSelectedMarker==0)){";
  $JSMap.=         "structureArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=       "}else if((lastSelectedMarker !== false) && (lastSelectedMarker!=-1)){";
  $JSMap.=         "structureArrayMarker[lastSelectedMarker].setIcon(structuresMarkerIcon);";
  $JSMap.=       "}";
  
  $JSMap.=       "overlayShown = NoMarker['id'];";
  $JSMap.=       "lastSelectedMarker = -1;";
  $JSMap.=     "}else{";
  $JSMap.=       "hideMapOverlay();";
  $JSMap.=     "}";
  $JSMap.=   "}";
   //gestion de la selection laterale
  $JSMap.=   "var selectedStructList = jQuery('#struct_list_noloc_'+id);";
  $JSMap.=   "if (selectedStructList.length != 0){";
  $JSMap.=   "  selectedStructList.addClass('selected').siblings().removeClass('selected');";
  $JSMap.=   "}else{";
  $JSMap.=   "  jQuery('.struct-navigation li').removeClass('selected');";
  $JSMap.=   "}";
  $JSMap.=   "return false;";
  $JSMap.= "}";
  
  $JSMap.= "function clickMarker(marker){";
  $JSMap.= "var overlay = document.getElementById('classCodeMapOverlay');";
  $JSMap.= "var overlayTitle = document.getElementById('classCodeMapOverlayTitleContent');";
  $JSMap.= "var overlayContent = document.getElementById('classCodeMapOverlayContent');";
  $JSMap.= "if (!overlayShown) {";
  $JSMap.=   'overlayTitle.innerHTML = marker.options.title;';
  $JSMap.=   "overlayContent.innerHTML = marker.options.desc;";
  
  $JSMap.=     "if((lastSelectedMarker !== false)&&(lastSelectedMarker == 0)){";
  $JSMap.=       "structureArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=     "}else if((lastSelectedMarker !== false)&& (lastSelectedMarker!=-1)){";
  $JSMap.=       "structureArrayMarker[lastSelectedMarker].setIcon(structuresMarkerIcon);";
  $JSMap.=     "}";
  $JSMap.=     "marker.setIcon(meetingMarkerIconSelected);";
  
  $JSMap.=     "if(typeof marker.options.userId !== 'undefined'){";
  $JSMap.=       "marker.setIcon(usersMarkerIconSelected);";
  $JSMap.=       "jQuery.ajax({";
  $JSMap.=         "type: 'GET',";
  $JSMap.=         "url: '".get_site_url()."'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+marker.options.userId+'&ajax_profile=true',";
  $JSMap.=         "timeout: 3000,";
  $JSMap.=         "success: function(data) {";
  $JSMap.=           "jQuery('#profilDisplay').html(data);     ";
  $JSMap.=         "},";
  $JSMap.=         "error: function(request) {";
  $JSMap.=           "jQuery('#profilDisplay').html('erreur ajax');";
  $JSMap.=         "}";
  $JSMap.=       "});";
  $JSMap.=     "}";
  
  $JSMap.=     'jQuery("#classCodeMapOverlay").toggle("slow");';
  $JSMap.=     "overlayShown = marker.options.id;";
  $JSMap.=     "lastSelectedMarker = marker.options.id;";
  $JSMap.=   "}else {";
  $JSMap.=     "if (overlayShown != marker.options.id) {";
  $JSMap.=       "overlayTitle.innerHTML = marker.options.title;";
  $JSMap.=       "overlayContent.innerHTML = marker.options.desc;";
  
  $JSMap.=       "if(lastSelectedMarker==0){";
  $JSMap.=         "structureArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=       "}else if(lastSelectedMarker!=-1){";
  $JSMap.=         "structureArrayMarker[lastSelectedMarker].setIcon(structuresMarkerIcon);";
  $JSMap.=       "}";
  $JSMap.=       "marker.setIcon(meetingMarkerIconSelected);";
  
  $JSMap.=       "if(typeof marker.options.userId !== 'undefined'){";
  $JSMap.=         "marker.setIcon(usersMarkerIconSelected);";
  $JSMap.=         "jQuery.ajax({";
  $JSMap.=           "type: 'GET',";
  $JSMap.=           "url: '".get_site_url()."'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+marker.options.userId+'&ajax_profile=true',";
  $JSMap.=           "timeout: 3000,";
  $JSMap.=           "success: function(data) {";
  $JSMap.=           "jQuery('#profilDisplay').html(data);     ";
  $JSMap.=           "},";
  $JSMap.=           "error: function(request) {";
  $JSMap.=             "jQuery('#profilDisplay').html('erreur ajax');";
  $JSMap.=           "}";
  $JSMap.=         "});";
  $JSMap.=       "}";
   
  $JSMap.=       "overlayShown = marker.options.id;";
  $JSMap.=       "lastSelectedMarker = marker.options.id;";
  $JSMap.=     "}else{";
  $JSMap.=       "hideMapOverlay();";
  $JSMap.=     "}";
  $JSMap.=   "}";
  //on centre la map sur le marker
  $JSMap.=   "map.panTo(marker.getLatLng());";
  //gestion de la selection laterale
  $JSMap.=   "var selectedStructList = jQuery('#struct_list_'+marker.options.id);";
  $JSMap.=   "if (selectedStructList.length != 0){";
  $JSMap.=   "  selectedStructList.addClass('selected').siblings().removeClass('selected');";
  $JSMap.=   "}else{";
  $JSMap.=   "  jQuery('.struct-navigation li').removeClass('selected');";
  $JSMap.=   "}";
  $JSMap.= "}";
        


  $htmlStructList =  '<div class="criteres-item-list overflowScroll">';
  $htmlStructList.= '<a target="_blank" href="'.get_site_url().'/classcodeadmin/structures" class="mapMenuLink">Référencer ma structure</a>';
  $htmlStructList.= '<ul class="struct-navigation">';
  foreach($data_structure_table_all  as $structure){
    if($structure[2] == true){
      $htmlStructList.= '<li id="struct_list_'.$structure[1].'">';
      $htmlStructList.= '<a href="" onclick="return selectMarker('.$structure[1].');">'.$structure[0].'</a>';
      $htmlStructList.= '</li>';
    }else{
      $htmlStructList.= '<li id="struct_list_noloc_'.$structure[1].'">';
      $htmlStructList.= '<a href="" onclick="return selectNoMarker('.$structure[1].');">'.$structure[0].'</a>';
      $htmlStructList.= '</li>';
    }
  }
  /* séparation en deux liste (avec loc et sans loc)
  foreach($data_structure_table as $structure){
    $htmlStructList.= '<li id="struct_list_'.$structure[1].'">';
    $htmlStructList.= '<a href="" onclick="return selectMarker('.$structure[1].');">'.$structure[0].'</a>';
    $htmlStructList.= '</li>';
  }
  foreach($data_structure_table_noloc as $structure){
    $htmlStructList.= '<li id="struct_list_noloc_'.$structure[1].'">';
    $htmlStructList.= '<a href="" onclick="return selectNoMarker('.$structure[1].');">'.$structure[0].'</a>';
    $htmlStructList.= '</li>';
  }*/
  $htmlStructList.= '</ul>';
  $htmlStructList.= '<div class="criteres-item-list-footer"></div>';
  $htmlStructList.= '</div>';
  
}else if($coordinationSearch){
  $JSMap = "var coordinationArrayMarker = [];";
  $JSMap.= "var coordinationArrayNoMarker = [];";
  $JSMap.= "function loadOSmap(lat,lng) {";
  $JSMap.= "  var coordinationContentString = '';";
  $JSMap.= $globalJSMap;
  
  if(!$noVisitorLocation){        
    $JSMap.= " coordinationArrayMarker[0] = L.marker(L.latLng('".$visitorLat."','".$visitorLong."'), { title: '".$visitorDisplayName."', icon:visitorMarkerIcon, id: '0', userId: ".$visitorId.", desc: '<div id=\"profilDisplay\"></div>' } );";

    $JSMap.= "markers.addLayer(coordinationArrayMarker[0]);";
    $JSMap.= " coordinationArrayMarker[0].on('click',"; 
    $JSMap.= "   function(event){clickMarker(this);});";
  }else{
    $JSMap.= "  coordinationArrayMarker[0]='';";
  }
  
  $coordination_inc = 1;
  $coordination_inc_noloc = 1;
  $data_coordinations_table = array();
  $data_structure_table = array(); 
  $jscoordinationfocus ='';
  foreach($data_coordinations as $coordinationDisplayName => $regionalCoordination){
 //   var_dump($regionalCoordination['alert']);
    if($regionalCoordination['alert']!=false){
      $coordinationLocation = $regionalCoordination['location'];
      $JSMap.= "coordinationContentString = '';";
      $JSMap.= "coordinationContentString=  '".($regionalCoordination['logo'] ? "<div class=\"classCodeMapOverlayContentLogo\"><img src=\"".$regionalCoordination['logo']."\"></div>" : "")."<br>';";
      $JSMap.= "coordinationContentString+=  '".($regionalCoordination['who'] ? "<div class=\"classCodeMapOverlayContentTitle\">Contact</div><div class=\"classCodeMapOverlayEmphasis\"><b>".addslashes($regionalCoordination['who'])."</b></div>" : "")."';";
      $JSMap.= "coordinationContentString+=  '".($regionalCoordination['address'] ? "<div>".addslashes($regionalCoordination['address'])."</div>" : "")."';";
      $JSMap.= "coordinationContentString+=  '".($regionalCoordination['tel'] ? "<div>tel : ".addslashes($regionalCoordination['tel'])."</div>" : "")."';"; 
      $JSMap.= "coordinationContentString+=  '".($regionalCoordination['email'] ? "<div><a href=\"mailto:".addslashes($regionalCoordination['email'])."\" target=\"_blank\">".addslashes($regionalCoordination['email'])."</a></div>" : "")."';";    
      $JSMap.= "coordinationContentString+=  '".($regionalCoordination['url'] ? "<div><a target=\"_blank\" href=\"".addslashes($regionalCoordination['url'])."\">".addslashes($regionalCoordination['url'])."</a></div>" : "")."';";
      $JSMap.= "coordinationContentString+=  '".($regionalCoordination['classCodePage'] ? "<a class=\"classCodeMapOverlayLink\" target=\"_blank\" href=\"".addslashes($regionalCoordination['classCodePage'])."\">> Page de la Coordination</a>" : "")."';";
      $JSMap.= "coordinationContentString+= '<a class=\"classCodeMapOverlayLink\" href=\"".cc_mailto_shortcode(array("to" => addslashes($regionalCoordination['email']), "who" => addslashes($regionalCoordination['who']), "subject" => "À propos de Class´Code","logo"=> $regionalCoordination['logo']))."\"> > Contactez</a>';";

      if($coordinationLocation && (($coordinationLocation['lat']!=0)||($coordinationLocation['lng']!=0))){
        $coordinationLat = $coordinationLocation['lat'];
        $coordinationLong = $coordinationLocation['lng'];
        
        $coordinationDisplayName = addslashes(htmlspecialchars_decode($regionalCoordination['name']));
        if($regionalCoordination['structure']){
          $data_structure_table[]=array(stripslashes($coordinationDisplayName),$coordination_inc);
        }else{
          $data_coordinations_table[]=array(stripslashes($coordinationDisplayName),$coordination_inc);
        }
        
        $JSMap.= " coordinationArrayMarker[".$coordination_inc."] = L.marker(L.latLng('".$coordinationLat."','".$coordinationLong."'), { title: '".$coordinationDisplayName."', icon: structuresMarkerIcon, id: '".$coordination_inc."', desc: coordinationContentString } );";
      
        $JSMap.= " markers.addLayer(coordinationArrayMarker[".$coordination_inc."]);";
        $JSMap.= " coordinationArrayMarker[".$coordination_inc."].on('click',"; 
        $JSMap.= "   function(event){clickMarker(this);});";

        //si recherche pour une région particulière
        if(($coordinationSearchFilter != '')&&isset($regionalCoordination['region'])&&($regionalCoordination['region']!=false)){
          if(preg_match($regionalCoordination['region'],$coordinationSearchFilter)){
            $jscoordinationfocus=$coordination_inc;
          }
        }
        
        $coordination_inc++;
      }else{      
        $coordinationDisplayName = addslashes(htmlspecialchars_decode($regionalCoordination['name']));
        $JSMap.= "var NoMarker = [];";
        $JSMap.= "NoMarker['title'] = '".$coordinationDisplayName."';";
        $JSMap.= "NoMarker['id'] = 'noloc_".$coordination_inc_noloc."';";
        $JSMap.= "NoMarker['desc'] = coordinationContentString;";        
        $JSMap.= "coordinationArrayNoMarker[".$coordination_inc_noloc."]=NoMarker;";
        
        $data_structure_table_noloc[]=array(stripslashes($coordinationDisplayName),$coordination_inc_noloc);
        $coordination_inc_noloc++;
      }
    }
  } 
  $JSMap.= " map.addLayer(markers);";
  $JSMap.= "}"; 
  $JSMap.= "function selectMarker(id){";
  $JSMap.=   "clickMarker(coordinationArrayMarker[id]);";
  $JSMap.=   "return false;";
  $JSMap.= "}";
  
  $JSMap.= "function selectNoMarker(id){";
  
  $JSMap.=   "var NoMarker=coordinationArrayNoMarker[id];";
  $JSMap.=   "var overlay = document.getElementById('classCodeMapOverlay');";
  $JSMap.=   "var overlayTitle = document.getElementById('classCodeMapOverlayTitleContent');";
  $JSMap.=   "var overlayContent = document.getElementById('classCodeMapOverlayContent');";
  $JSMap.=   "if (!overlayShown) {";
  $JSMap.=     "overlayTitle.innerHTML = NoMarker['title'];";
  $JSMap.=     "overlayContent.innerHTML = NoMarker['desc'];";
  
  $JSMap.=     "if((lastSelectedMarker !== false)&&(lastSelectedMarker == 0)){";
  $JSMap.=       "coordinationArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=     "}else if((lastSelectedMarker !== false)&& (lastSelectedMarker!=-1)){";
  $JSMap.=       "coordinationArrayMarker[lastSelectedMarker].setIcon(structuresMarkerIcon);";
  $JSMap.=     "}";
  
  $JSMap.=     'jQuery("#classCodeMapOverlay").toggle("slow");';
  $JSMap.=     "overlayShown = NoMarker['id'];";
  $JSMap.=     "lastSelectedMarker = -1;";
  $JSMap.=   "}else {";
  $JSMap.=     "if (overlayShown != NoMarker['id']) {";
  $JSMap.=       "overlayTitle.innerHTML = NoMarker['title'];";
  $JSMap.=       "overlayContent.innerHTML = NoMarker['desc'];";
  
  $JSMap.=       "if((lastSelectedMarker !== false)&&(lastSelectedMarker==0)){";
  $JSMap.=         "coordinationArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=       "}else if((lastSelectedMarker !== false) && (lastSelectedMarker!=-1)){";
  $JSMap.=         "coordinationArrayMarker[lastSelectedMarker].setIcon(structuresMarkerIcon);";
  $JSMap.=       "}";
  
  $JSMap.=       "overlayShown = NoMarker['id'];";
  $JSMap.=       "lastSelectedMarker = -1;";
  $JSMap.=     "}else{";
  $JSMap.=       "hideMapOverlay();";
  $JSMap.=     "}";
  $JSMap.=   "}";
   //gestion de la selection laterale
  $JSMap.=   "var selectedStructList = jQuery('#struct_list_noloc_'+id);";
  $JSMap.=   "if (selectedStructList.length != 0){";
  $JSMap.=   "  selectedStructList.addClass('selected').siblings().removeClass('selected');";
  $JSMap.=   "}else{";
  $JSMap.=   "  jQuery('.struct-navigation li').removeClass('selected');";
  $JSMap.=   "}";
  $JSMap.=   "return false;";
  $JSMap.= "}";
  
  
  $JSMap.= "function clickMarker(marker){";
  $JSMap.= "var overlay = document.getElementById('classCodeMapOverlay');";
  $JSMap.= "var overlayTitle = document.getElementById('classCodeMapOverlayTitleContent');";
  $JSMap.= "var overlayContent = document.getElementById('classCodeMapOverlayContent');";
  $JSMap.= "if (!overlayShown) {";
  $JSMap.=   "overlayTitle.innerHTML = marker.options.title;";
  $JSMap.=   "overlayContent.innerHTML = marker.options.desc;";
  
  $JSMap.=     "if((lastSelectedMarker !== false)&&(lastSelectedMarker == 0)){";
  $JSMap.=       "coordinationArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=     "}else if((lastSelectedMarker !== false)&& (lastSelectedMarker!=-1)){";
  $JSMap.=       "coordinationArrayMarker[lastSelectedMarker].setIcon(structuresMarkerIcon);";
  $JSMap.=     "}";
  $JSMap.=     "marker.setIcon(meetingMarkerIconSelected);";
  
  $JSMap.=     "if(typeof marker.options.userId !== 'undefined'){";
  $JSMap.=       "marker.setIcon(usersMarkerIconSelected);";
  $JSMap.=       "jQuery.ajax({";
  $JSMap.=         "type: 'GET',";
  $JSMap.=         "url: '".get_site_url()."'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+marker.options.userId+'&ajax_profile=true',";
  $JSMap.=         "timeout: 3000,";
  $JSMap.=         "success: function(data) {";
  $JSMap.=           "jQuery('#profilDisplay').html(data);     ";
  $JSMap.=         "},";
  $JSMap.=         "error: function(request) {";
  $JSMap.=           "jQuery('#profilDisplay').html('erreur ajax');";
  $JSMap.=         "}";
  $JSMap.=       "});";
  $JSMap.=     "}";
  
  $JSMap.=     'jQuery("#classCodeMapOverlay").toggle("slow");';
  $JSMap.=     "overlayShown = marker.options.id;";
  $JSMap.=     "lastSelectedMarker = marker.options.id;";
  $JSMap.=   "}else {";
  $JSMap.=     "if (overlayShown != marker.options.id) {";
  $JSMap.=       "overlayTitle.innerHTML = marker.options.title;";
  $JSMap.=       "overlayContent.innerHTML = marker.options.desc;";
  
  $JSMap.=       "if(lastSelectedMarker==0){";
  $JSMap.=         "coordinationArrayMarker[lastSelectedMarker].setIcon(visitorMarkerIcon);";
  $JSMap.=       "}else if(lastSelectedMarker!=-1){";
  $JSMap.=         "coordinationArrayMarker[lastSelectedMarker].setIcon(structuresMarkerIcon);";
  $JSMap.=       "}";
  $JSMap.=       "marker.setIcon(meetingMarkerIconSelected);";
  
  $JSMap.=       "if(typeof marker.options.userId !== 'undefined'){";
  $JSMap.=         "marker.setIcon(usersMarkerIconSelected);";
  $JSMap.=         "jQuery.ajax({";
  $JSMap.=           "type: 'GET',";
  $JSMap.=           "url: '".get_site_url()."'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+marker.options.userId+'&ajax_profile=true',";
  $JSMap.=           "timeout: 3000,";
  $JSMap.=           "success: function(data) {";
  $JSMap.=           "jQuery('#profilDisplay').html(data);     ";
  $JSMap.=           "},";
  $JSMap.=           "error: function(request) {";
  $JSMap.=             "jQuery('#profilDisplay').html('erreur ajax');";
  $JSMap.=           "}";
  $JSMap.=         "});";
  $JSMap.=       "}";
   
  $JSMap.=       "overlayShown = marker.options.id;";
  $JSMap.=       "lastSelectedMarker = marker.options.id;";
  $JSMap.=     "}else{";
  $JSMap.=       "hideMapOverlay();";
  $JSMap.=     "}";
  $JSMap.=   "}";
  //on centre la map sur le marker
  $JSMap.=   "map.panTo(marker.getLatLng());";
  //gestion de la selection laterale
  $JSMap.=   "var selectedStructList = jQuery('#struct_list_'+marker.options.id);";
  $JSMap.=   "if (selectedStructList.length != 0){";
  $JSMap.=   "  selectedStructList.addClass('selected').siblings().removeClass('selected');";
  $JSMap.=   "}else{";
  $JSMap.=   "  jQuery('.struct-navigation li').removeClass('selected');";
  $JSMap.=   "}";
  $JSMap.= "}";
  
  $htmlCoordList=  '<div class="criteres-item-list overflowScroll">';
  $htmlCoordList.= '<ul class="struct-navigation">';
  foreach($data_coordinations_table as $region){
    $htmlCoordList.= '<li id="struct_list_'.$region[1].'">';
    $htmlCoordList.= '<a href="" onclick="return selectMarker('.$region[1].');">'.$region[0].'</a>';
    $htmlCoordList.= '</li>';
  }
  $htmlCoordList.= '<li>---</li>';

  foreach($data_structure_table as $structure){
    $htmlCoordList.= '<li id="struct_list_'.$structure[1].'">';
    $htmlCoordList.= '<a href="" onclick="return selectMarker('.$structure[1].');">'.$structure[0].'</a>';
    $htmlCoordList.= '</li>';
  }
  foreach($data_structure_table_noloc as $structure){
    $htmlCoordList.= '<li id="struct_list_noloc_'.$structure[1].'">';
    $htmlCoordList.= '<a href="" onclick="return selectNoMarker('.$structure[1].');">'.$structure[0].'</a>';
    $htmlCoordList.= '</li>';
  }
  $htmlCoordList.= '</ul>';
  $htmlCoordList.= '<div class="criteres-item-list-footer"></div>';
  $htmlCoordList.= '</div>';
  
}


// Javascript pour l'affichage de la carte
$htmlMap = '<script type="text/javascript">';
$htmlMap.= 'var map;';
$htmlMap.= 'var marker;';
$htmlMap.= 'var overlayShown = false;';
$htmlMap.= 'var lastSelectedMarker = false;';
$htmlMap.= 'var lastSelectedImage = "";';

$htmlMap.= "var meetingMarkerImage = '".get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/meetingMarker.png';";
$htmlMap.= "var meetingMarkerImageSelected = '".get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/meetingMarkerSelected.png';";
$htmlMap.= "var visitorMarkerImage = '".get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/visitorMarker.png';";
$htmlMap.= "var usersMarkerImage = '".get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/usersMarker.png';";
$htmlMap.= "var usersMarkerImageSelected = '".get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/usersMarkerSelected.png';";
$htmlMap.= "var usersMarkerImageFacilitateur = '".get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/usersMarkerFacilitateur.png';";
$htmlMap.= "var structuresMarkerImage = '".get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/structuresMarker.png';";


$htmlMap.= 'jQuery( document ).ready(function() {';
$htmlMap.= '  ClassCodeIcon = L.Icon.extend({';
$htmlMap.= '    options: {';
$htmlMap.= '    iconSize:     [33, 45],';
$htmlMap.= '    iconAnchor:   [16, 44],';
$htmlMap.= '    popupAnchor:  [0, -30]';
$htmlMap.= '    }';
$htmlMap.= '  });';
$htmlMap.= '    meetingMarkerIcon = new ClassCodeIcon({iconUrl: meetingMarkerImage});';
$htmlMap.= '    meetingMarkerIconSelected = new ClassCodeIcon({iconUrl: meetingMarkerImageSelected});';
$htmlMap.= '    visitorMarkerIcon = new ClassCodeIcon({iconUrl: visitorMarkerImage});';
$htmlMap.= '    usersMarkerIcon = new ClassCodeIcon({iconUrl:usersMarkerImage});';
$htmlMap.= '    usersMarkerIconSelected = new ClassCodeIcon({iconUrl:usersMarkerImageSelected});';
$htmlMap.= '    usersMarkerIconFacilitateur = new ClassCodeIcon({iconUrl:usersMarkerImageFacilitateur});';
$htmlMap.= '    structuresMarkerIcon = new ClassCodeIcon({iconUrl:structuresMarkerImage});';
    
$htmlMap.= '  loadOSmap('.$visitorLat.','.$visitorLong.');';
$htmlMap.= '  jQuery("#classCodeMapOverlayTitleLink").click(function() {';
$htmlMap.= '    hideMapOverlay();';
$htmlMap.= '  });';
$htmlMap.= '  jQuery(".auto_submit_form").change(function() {';
$htmlMap.= '    this.submit();';
$htmlMap.= '  });';
if(isset($jscoordinationfocus)&&($jscoordinationfocus!="")){
 $htmlMap.= 'selectMarker("'.$jscoordinationfocus.'");';
}
if($meetingSearch){
  $htmlMap.= ' jQuery("#meetingListLink").click(function(){';
  $htmlMap.= '    jQuery("#classCodeMap").hide();';
  $htmlMap.= '    jQuery("#meetingListLink").hide();';
  $htmlMap.= '    jQuery("#meetingListHeaderDate1").click();';
  $htmlMap.= '    jQuery("#classCodeMeetingList").show();';
  $htmlMap.= '    jQuery("#meetingMapLink").show();';
  $htmlMap.= '  });';

  $htmlMap.= ' jQuery("#meetingMapLink").click(function(){';
  $htmlMap.= '    jQuery("#classCodeMeetingList").hide();';
  $htmlMap.= '    jQuery("#meetingMapLink").hide();';
  $htmlMap.= '    jQuery("#classCodeMap").show();';
  $htmlMap.= '    jQuery("#meetingListLink").show();';
  $htmlMap.= '  });';
   
  $htmlMap.= ' jQuery.tablesorter.addParser({ ';
  $htmlMap.= '   id: "classCodeDates", ';
  $htmlMap.= '   is: function(s) { ';
  $htmlMap.= '     return false; ';
  $htmlMap.= '   }, ';
  $htmlMap.= '   format: function(s) { ';
  $htmlMap.= '     sDateReformated = s; ';
  $htmlMap.= '     if(s.length>0){';
  $htmlMap.= '       var sTmp = s.split("à");';
  $htmlMap.= '       sDate = sTmp[0];';
  $htmlMap.= '       sDateArray = sDate.split("/");';
  $htmlMap.= '       sDateReformated = sDateArray[2].trim()+"/"+sDateArray[1].trim()+"/"+sDateArray[0].trim();';
  $htmlMap.= '     }';
  $htmlMap.= '     return sDateReformated;';
  $htmlMap.= '   }, ';
  $htmlMap.= '   type: "text" ';
  $htmlMap.= ' });';
  $htmlMap.= ' jQuery("#meetingListTable").tablesorter( {';
  $htmlMap.= '   sortList: [[0,0]],';
  $htmlMap.= '   widgets: ["zebra"],';
  $htmlMap.= '   dateFormat: "uk",';
  $htmlMap.= '   headers: {0: { sorter:"classCodeDates"}, 4: { sorter: false}},';
  $htmlMap.= '   } ); ';
}
$htmlMap.= '});';

$htmlMap.=  $JSMap;

$htmlMap.= "function searchCoordination(){";    
$htmlMap.= "window.onbeforeunload = null;";
$htmlMap.= "var coordinationSearchForm = document.getElementById('coordinationSearchForm');";
$htmlMap.= "jQuery('<input type=\"submit\">').hide().appendTo(coordinationSearchForm).click().remove();";
$htmlMap.= "return false;";
$htmlMap.= "}";

$htmlMap.= "function searchStructure(){";    
$htmlMap.= "window.onbeforeunload = null;";
$htmlMap.= "var structureSearchForm = document.getElementById('structureSearchForm');";
$htmlMap.= "jQuery('<input type=\"submit\">').hide().appendTo(structureSearchForm).click().remove();";
$htmlMap.= "return false;";
$htmlMap.= "}";

$htmlMap.= "function displayMeetingForm(){";    
$htmlMap.= "window.onbeforeunload = null;";
$htmlMap.= "var meetingDisplayForm = document.getElementById('meetingDisplayForm');";
$htmlMap.= "jQuery('<input type=\"submit\">').hide().appendTo(meetingDisplayForm).click().remove();";
$htmlMap.= "return false;";
$htmlMap.= "}";

$htmlMap.= "function displayUserForm(){";    
$htmlMap.= "window.onbeforeunload = null;";
$htmlMap.= "var userDisplayForm = document.getElementById('userDisplayForm');";
$htmlMap.= "jQuery('<input type=\"submit\">').hide().appendTo(userDisplayForm).click().remove();";
$htmlMap.= "return false;";
$htmlMap.= "}";

$htmlMap.= "function searchMeeting(){";    
$htmlMap.= "window.onbeforeunload = null;";
$htmlMap.= "var meetingSearchForm = document.getElementById('meetingSearchForm');";
$htmlMap.= "jQuery('<input type=\"submit\">').hide().appendTo(meetingSearchForm).click().remove();";
$htmlMap.= "return false;";
$htmlMap.= "}";

$htmlMap.= "function searchUser(){";    
$htmlMap.= "window.onbeforeunload = null;";
$htmlMap.= "var userSearchForm = document.getElementById('userSearchForm');";
$htmlMap.= "jQuery('<input type=\"submit\">').hide().appendTo(userSearchForm).click().remove();";
$htmlMap.= "return false;";
$htmlMap.= "}";

$htmlMap.= "function hideMapOverlay(){";
$htmlMap.=   'jQuery("#classCodeMapOverlay").toggle("slow");';
$htmlMap.=   "overlayShown = false;";
$htmlMap.=   "return false;";
$htmlMap.= "}";

$htmlMap.= "</script>"; 

?>
	<div id="mapWidget" class="content-category t5">
		<p class="content-category-title">PRÈS DE CHEZ VOUS<span class="content-category-subtitle">des rencontres avec d’autres participants pour échanger, partager, expérimenter !</span></p>
		<div class="content-header"></div>
			<div id="criteres" class="criteres" >  
        <div class="criteresGauche">      
          <div id="criteres-groupe-1" class="criteres-groupe-1">
            <div id="coordinationsFilter" class="criteres-item first <?php echo $coordinationFilterClassSelected; ?>">
              <div class="criteres-item-title">
                <a onclick="searchCoordination();">Les coordinations</a>
              </div>
              <?php          
                echo $htmlCoordList;
              ?>
            </div>
            
            <form method="post" id="coordinationSearchForm" action="?meetingSearch=false#mapWidget">
              <input type="hidden" value="true" id="coordinationSearch" name="coordinationSearch">
            </form>
            <div id="structuresFilter" class="criteres-item second <?php echo $structureFilterClassSelected; ?>">
              <div class="criteres-item-title">
                <a onclick="searchStructure();">Les partenaires</a>
              </div>
              <?php          
                echo $htmlStructList;
              ?>
            </div>
            
            <form method="post" id="structureSearchForm" action="?meetingSearch=false#mapWidget">
              <input type="hidden" value="true" id="structureSearch" name="structureSearch">
            </form>
          </div>
          <div id="criteres-groupe-2" class="criteres-groupe-2">
            <div class="criteres-item third <?php echo $meetingFilterClassSelected; ?>">
              <div id="rencontresFilter" class="criteres-item-title">
                <a onclick="displayMeetingForm();">Les rencontres</a>
              </div>
              <?php          
                echo $htmlMeetingForm;
              ?>
            </div>
            
            <form method="post" id="meetingDisplayForm" action="#mapWidget">
              <input type="hidden" value="true" id="meetingDisplayForm" name="meetingDisplayForm">
            </form>
            <div class="criteres-item fourth <?php echo $userFilterClassSelected; ?>">
              <div id="usersFilter" class="criteres-item-title">
                <a onclick="displayUserForm();">Les participants</a>
              </div>
              <?php          
                echo $htmlUserForm;
              ?>
            </div>
            
            <form method="post" id="userDisplayForm" action="?meetingSearch=false#mapWidget">
              <input type="hidden" value="true" id="userDisplayForm" name="userDisplayForm">
            </form>
          </div>
        </div>        
        <div class="clear"></div>
        <div class="content-footer">
          <div class="mapSearch"></div>
        </div>        
			</div>
      <div class="carte-parent">
      <div id="map-wrapper">
			  <div id="classCodeMap" class="carte" >
        </div>
        <div id="classCodeMeetingList" style="display:none;" >     
          <table id="meetingListTable" class="tablesorter">
            <thead id="meetingListHeader">
              <th id="meetingListHeaderDate1" class="meetingListHeaderDefault sortable">Date <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
              <th id="meetingListHeaderModule" class="meetingListHeaderDefault sortable">Module <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
              <th id="meetingListHeaderStructure" class="meetingListHeaderDefault sortable">Structure <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
              <th id="meetingListHeaderDistance" class="meetingListHeaderDefault sortable">Distance (km) <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></th>
              <th class="meetingListHeaderDefault" style="width:120px";>Plus de détails</th>
            </thead>
            <tbody id="classCodeMeetingListTbody">
              <?php echo $classCodeMeetingListTbody; ?>
            </tbody>
          </table>
        </div>
        <div id="classCodeMapOverlay">
          <div id="classCodeMapOverlayTitle">           
            <span id="classCodeMapOverlayTitleContent">
              Retour
            </span>
            <a id="classCodeMapOverlayTitleLink" class="backCriteriaLink">
              <span class="returnIcon">
              </span>
            </a>
          </div>
          <div id="classCodeMapOverlayContent">
          </div>          
        </div>        
      </div>
		</div>
	</div>
<script>
function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }

    alert(out);
}
</script>
<?php
echo $htmlMap;
?>