
	<?php
	
	include CLASSCODE2_PLUGIN_DIR . "/partenaires/data_partenaires.php";

	$t8_partenaires = get_partenaires();

	?>

	<div id="partenaires" class="content-category t8" data-tid="t8">
		<p class="content-category-title">LES PARTENAIRES</p>
		<div class="cards-with-arrows">
			<div class="arrow previous"><span class="helper"></span><img src="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/images/arrow-previous.png" alt="" /></div>
			<div class="cards">
				<div class="cards-container">
					
					<?php  foreach( $t8_partenaires as $partenaire ) { 
	
						$partenaire_logo = CLASSCODE2_PLUGIN_URL . "/partenaires/LogosPartenaires/" . $partenaire['group'] . "/" . $partenaire['logo'];
	
						// RQ : il ne doit pas y avoir d'espace entre les divs (sinon il y aura un écart entre les vignettes)
					 ?><div class="card">
				 			<a title="<?php echo $partenaire['name']; ?>" href="<?php echo site_url( CLASSCODE2_ROUTE.  "/le-projet"); ?>" >
								<img src="<?php echo $partenaire_logo; ?>" alt="<?php echo $partenaire['name']; ?>"/>
							</a>
					 </div><?php } ?>
					 
				</div>
			</div>
			<div class="arrow next"><span class="helper"></span><img src="<?php echo CLASSCODE2_PLUGIN_URL ?>assets/images/arrow-next.png" alt="" /></div>
		</div>
		<div class="clear"></div>
		<p><a href="<?php echo site_url( CLASSCODE2_ROUTE.  "/le-projet"); ?>">En savoir plus</a></p>
	</div>
