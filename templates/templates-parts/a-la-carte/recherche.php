<?php

$checked = "checked";

// Colonne 1
$tousModulesChecked = $checked;
$module1Checked = "";
$module2Checked = "";
$module3Checked = "";
$module4Checked = "";
$module5Checked = "";
$moduleICNChecked = "";
$module123Checked = "";
$moduleIAIChecked = "";

// Colonne 2
$tousTypeChecked = $checked;
$brancheeChecked = "";
$robotChecked = "";
$scratchChecked = "";
$debrancheeChecked = "";
$reculChecked = "";
$pedagoChecked = "";

// Colonne 3
$toutContenuChecked = $checked;
$contenuVideo = "";
$contenuActivite = "";
$contenuFiche = "";

$prefixRecherchePourModules = "Parcours > ";
$prefixRecherchePourTypes   = "Activités > ";
$prefixRecherchePourContenus= "Ressources > ";

$recherchePourModules = "Tous";
$recherchePourTypes   = "Toutes";
$recherchePourContenus= "Toutes";

// Passage de l'activité en paramètre ?activite=....
$query_activite = $_GET["activite"];

if ( ! isset ($query_activite)) {
	// Passage de l'activité dans la réécriture d'URL : a-la-carte/....
	$query_activite = get_query_var('activite');
}

$libellesCategories = array_merge(
	get_libelles_categories_parcours(),
	get_libelles_categories_parcours_icn(),
	get_libelles_categories_thematiques(),
	get_libelles_categories_ressources(),
	array(	"parcours" => $recherchePourModules,
		"ressources" => $recherchePourContenus,
		"thematiques" => $recherchePourTypes,
		"prefix-parcours" => $prefixRecherchePourModules,
		"prefix-ressources" => $prefixRecherchePourContenus,
		"prefix-thematiques" => $prefixRecherchePourTypes
	)
);


if (isset($query_activite) && ($query_activite != "") ) {
	
	switch($query_activite) {
	
		// Colonne 1
		case "module-1":
			$tousModulesChecked = "";
			$module1Checked = $checked;
			$recherchePourModules = $libellesCategories[$query_activite];
			$query_activite_exception = "1-2-3-codez";
			break;
			
		case "module-2":
			$tousModulesChecked = "";
			$module2Checked = $checked;
			$recherchePourModules = $libellesCategories[$query_activite];
			$query_activite_exception = "1-2-3-codez";
			break;
			
		case "module-3":
			$tousModulesChecked = "";
			$module3Checked = $checked;
			$recherchePourModules = $libellesCategories[$query_activite];
			$query_activite_exception = "1-2-3-codez";
			break;
			
		case "module-4":
			$tousModulesChecked = "";
			$module4Checked = $checked;
			$recherchePourModules = $libellesCategories[$query_activite];
			$query_activite_exception = "1-2-3-codez";
			break;
			
		case "module-5":
			$tousModulesChecked = "";
			$module5Checked = $checked;
			$recherchePourModules = $libellesCategories[$query_activite];
			$query_activite_exception = "1-2-3-codez";
			break;
			
		case "mooc-icn-informatique-et-creation-numerique":
			$tousModulesChecked = "";
			$moduleICNChecked = $checked;
			$recherchePourModules = $libellesCategories[$query_activite];
			break;
			
		case "presentation-du-cours":
		case "n-le-numerique-et-ses-sciences-dans-le-reel":
		case "i-l-informatique-et-ses-fondements":
		case "c-creer-des-projets-pour-l-icn":
			$tousModulesChecked = "";
			$moduleICNChecked = $checked;
			$recherchePourModules = $libellesCategories["mooc-icn-informatique-et-creation-numerique"] . ' - ' . $libellesCategories[$query_activite];
			break;
			
		case "1-2-3-codez":
			$tousModulesChecked = "";
			$module123Checked = $checked;
			$recherchePourModules = $libellesCategories[$query_activite];
			break;

		case "iai-formation-citoyenne-a-lintelligence-artificielle-intelligente":
			$tousModulesChecked = "";
            $moduleIAIChecked = $checked;
			$recherchePourModules = $libellesCategories[$query_activite];
			break;

		// Colonne 2
		case "activite-branchee":
		case "programmer":
			$tousTypeChecked = "";
			$brancheeChecked = $checked;
			$recherchePourTypes = $libellesCategories["activite-branchee"];
			break;
			
		case "thymio":
		case "robotique":
			$tousTypeChecked = "";
			$brancheeChecked = $checked;
			$robotChecked = $checked;
			$recherchePourTypes = $libellesCategories[$query_activite];
			break;
			
		case "scratch":
			$tousTypeChecked = "";
			$brancheeChecked = $checked;
			$scratchChecked = $checked;
			$recherchePourTypes = $libellesCategories[$query_activite];
			break;
			
		case "activite-debranchee":
		case "jouer":
			$tousTypeChecked = "";
			$debrancheeChecked = $checked;
			$recherchePourTypes = $libellesCategories["activite-debranchee"];
			break;
			
		case "prendre-du-recul":
		case "decouvrir":
			$tousTypeChecked = "";
			$reculChecked = $checked;
			$recherchePourTypes = $libellesCategories["prendre-du-recul"];
			break;
			
		case "pedagogie":
		case "transmettre":
			$tousTypeChecked = "";
			$pedagoChecked = $checked;
			$recherchePourTypes = $libellesCategories["pedagogie"];
			break;
			
		// Colonne 3
		case "video":
			$toutContenuChecked = "";
			$contenuVideo = $checked;
			$recherchePourContenus = $libellesCategories[$query_activite];
			break;
			
		case "fiche-d-activite":
			$toutContenuChecked = "";
			$contenuFiche = $checked;
			$recherchePourContenus = $libellesCategories[$query_activite];
			break;
			
		case "texte":
			$toutContenuChecked = "";
			$contenuActivite = $checked;
			$recherchePourContenus = $libellesCategories[$query_activite];
			break;
			
		// Sinon :
			
		default :
			$recherchePourModules = "";
			$recherchePourTypes   = "";
			$recherchePourContenus= "";
			
	}
}

?>

<div class="classcode-v2 ressources-a-la-carte content-category" >
	<p class="content-category-title">Les ressources à la carte</p>

	<div class="page-header search">

		<div class="page-header-columns">

			<div class="page-header-column parcours">
				<div class="page-header-column-title">Parcours de formation</div>
				<div class="page-header-column-content">
					<ul>
						<li>
                            <label>
                                <input type="radio" name="parcours" value="" <?php echo $tousModulesChecked ?> >Tous</input>
                            </label>
						</li>
						<li>
                            <label>
                                <input type="radio" name="parcours" value="module-1!1-2-3-codez" <?php echo $module1Checked ?> >#1 Découvrir la programmation créative</input>
                            </label>
						</li>
						<li>
                            <label>
                                <input type="radio" name="parcours" value="module-2!1-2-3-codez" <?php echo $module2Checked ?> >#2 Manipulez l'information</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="parcours" value="module-3!1-2-3-codez" <?php echo $module3Checked ?> >#3 Initiez-vous à la Robotique</input>
                            </label>
                        </li>
						<li>
                            <label>
    							<input type="radio" name="parcours" value="module-4!1-2-3-codez" <?php echo $module4Checked ?> >#4 Connectez le Réseau</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="parcours" value="module-5!1-2-3-codez" <?php echo $module5Checked ?> >#5 Gérer un projet informatique avec des enfants</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="parcours" value="mooc-icn-informatique-et-creation-numerique" <?php echo $moduleICNChecked ?> >SNT : Sciences du Numérique et Technologie</input>
                            </label>
						</li>
						<li class="sous-theme">
                            <label>
                                <input type="radio" name="parcours" value="t-thematiques-en-sciences-du-numerique"  <?php echo $P_ICNChecked ?> >Thématiques en Sciences du numérique</input>
                            </label>
						</li>
						<li class="sous-theme">
                            <label>
    							<input type="radio" name="parcours" value="i-l-informatique-et-ses-fondements"  <?php echo $I_ICNChecked ?> >L’Informatique et ses fondements</input>
                            </label>
						</li>
						<li class="sous-theme">
                            <label>
    							<input type="radio" name="parcours" value="n-le-numerique-et-ses-sciences-dans-le-reel"  <?php echo $N_ICNChecked ?> >Sujets transversaux sur le numérique</input>
                            </label>
						</li>
<!--
						<li class="sous-theme">
							<input type="radio" name="parcours" value="c-creer-des-projets-pour-l-icn"  <?php echo $C_ICNChecked ?> >C : Créer des projets pour l’ICN</input>
						</li>
-->
						<li>
                            <label>
                                <input type="radio" name="parcours" value="1-2-3-codez" <?php echo $module123Checked ?> >1,2,3 Codez - Cycles 3 et 4</input>
                            </label>
						</li>
						<li>
                            <label>
                                <input type="radio" name="parcours" value="iai-formation-citoyenne-a-lintelligence-artificielle-intelligente" <?php echo $moduleIAIChecked ?> >IAI : Formation citoyenne à l’intelligence artificielle intelligente</input>
                            </label>
						</li>
					</ul>
				</div>
			</div>

			<div class="page-header-column thematiques">
				<div class="page-header-column-title">Types d'activités</div>
				<div class="page-header-column-content">
					<ul>
						<li>
                            <label>
    							<input type="radio" name="thematiques" value="" <?php echo $tousTypeChecked ?> >Toutes</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="thematiques" value="activite-branchee" <?php echo $brancheeChecked ?> >Programmer / Branchée</input>
                            </label>
						</li>
						<li class="sous-theme">
                            <label>
    							<input type="radio" name="thematiques-branchee" value="robotique+thymio"  <?php echo $robotChecked ?> >Robotique</input>
                            </label>
						</li>
						<li class="sous-theme">
                            <label>
    							<input type="radio" name="thematiques-branchee" value="scratch" <?php echo $scratchChecked ?> >Scratch</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="thematiques" value="activite-debranchee" <?php echo $debrancheeChecked ?> >Jouer / Débranchée</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="thematiques" value="prendre-du-recul" <?php echo $reculChecked ?> >Découvrir / Recul</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="thematiques" value="pedagogie" <?php echo $pedagoChecked ?> >Transmettre / Pédagogie</input>
                            </label>
						</li>
						<!-- <li><input type="radio" name="thematiques" value="autres">Autres</input></li> -->
					</ul>
				</div>
			</div>

			<div class="page-header-column ressources">
				<div class="page-header-column-title">Types de ressources</div>
				<div class="page-header-column-content">
					<ul>
						<li>
                            <label>
    							<input type="radio" name="ressources" value="" <?php echo $toutContenuChecked ?> >Toutes</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="ressources" value="video" <?php echo $contenuVideo ?> >Video</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="ressources" value="texte" <?php echo $contenuActivite ?> >Cours</input>
                            </label>
						</li>
						<li>
                            <label>
    							<input type="radio" name="ressources" value="fiche-d-activite" <?php echo $contenuFiche ?> >Fiche activité</input>
                            </label>
						</li>
						<!-- <li><input type="radio" name="ressources" value="autres">Autres</input></li> -->
					</ul>
				</div>
			</div>

			<div class="clear"></div>

		</div><!-- page-header-columns -->

		<?php

			// https://stackoverflow.com/questions/6100741/how-to-prevent-browsers-from-remembering-checkbox-state


			require_once( CLASSCODE2_PLUGIN_DIR . '/functions.php' );

			// Recherche par défaut

			// Pas de catégories précisées
			$categories = array();

			// Nombre maximum de posts affichés 
			$nbPostsMax = 20;

			// Offset des posts affichés ( i.e. index du premier affiché )
			$offsetPosts = 0;

			$term_a_la_carte = get_term_by( 'slug', "a-la-carte", "category" );
			$term_a_la_carte_id = $term_a_la_carte->term_id;

			// Liste de toutes les catégories filles de "à la carte", à laquelle on ajoute "à la carte"
			$term_children = get_term_children ($term_a_la_carte_id , "category");
			array_push( $term_children, $term_a_la_carte_id);

			// Pour JS : si on sélectionne "tous les modules", on doit rester dans les catégories enfant de "à la carte"
			$slug_term_children = array();
			foreach($term_children as $term_id ) {
				$term = get_term( $term_id ) ;
				array_push($slug_term_children, "'" . $term->slug . "'");
			}

			//
			// Cas général : tous les contenus "à la carte" ( catégorie parente ou enfant )
			//

			if ( ! isset($query_activite) || ( $query_activite == "" )) {
	
				// Cas général :
				$args = array(
					'tax_query' => array(
						array(
							'taxonomy' => 'category',
							'field'    => 'term_id',
							'terms'    => $term_children,
							'operator' => 'IN',
						),
					)
				);
				
			} else {
				
				// Cas particulier : passage d'un type (branchée, débranchée, recul, pédagogie, robotique, scratch)  de contenu dans l'URL
				
				if ($query_activite_exception) {
					
					$args = array(
						'tax_query' => array(
							"relation" => "AND",
							array(
								'taxonomy' => 'category',
								'field'    => 'slug',
								'terms'    => $query_activite,
							),
							array(
								'taxonomy' => 'category',
								'field'    => 'slug',
								'terms'    => array( $query_activite_exception ),
								'operator' => "NOT IN"
							),
							array(
								'taxonomy' => 'category',
								'field'    => 'term_id',
								'terms'    => $term_children,
								'operator' => 'IN',
							),
						)
					);					
					
				} else {

					$args = array(
						'tax_query' => array(
							"relation" => "AND",
							array(
								'taxonomy' => 'category',
								'field'    => 'slug',
								'terms'    => $query_activite,
							),
							array(
								'taxonomy' => 'category',
								'field'    => 'term_id',
								'terms'    => $term_children,
								'operator' => 'IN',
							),
						)
					);
					
					
				}
				
			}
								   
								   
			$query_results = get_posts_with_categories($args, $nbPostsMax);

			$query_posts = $query_results->posts;
			$nb_found_posts = $query_results->found_posts;
			$more_posts = $query_results->voir_plus;

			$resultsHtml = "";
			$voirPlusText = "plus de résultats...";
			$aLaCarteUrl = site_url( CLASSCODE2_ROUTE. "/a-la-carte/" );

			foreach($query_posts as $query_post)
			{
				$post_categories_str = get_post_links_to_categories($query_post);
				
				/*
				$post_categories_a_la_carte = $query_post["categories_a_la_carte"];
				$post_categories_labels_array = array();

				$categoryObj = $post_categories_a_la_carte["parcours"];
				if ($categoryObj) {
					$categorySlug = $categoryObj["slug"];
					$categoryLibelle = $categoryObj["name"];
					$categoryLink = '<a href="' . $aLaCarteUrl . $categorySlug . '" >' . $categoryLibelle . '</a>';
					array_push ($post_categories_labels_array, '<span class="parcours">' . $categoryLink . '</span>');
				}
						
				$thematiqueObj = $post_categories_a_la_carte["thematiques"];
				if ($thematiqueObj) {
					$thematiqueSlug = $thematiqueObj["slug"];
					$thematiqueLibelle = $thematiqueObj["name"];
					$thematiqueLink = '<a href="' . $aLaCarteUrl . $thematiqueSlug . '" >' . $thematiqueLibelle . '</a>';
					array_push ($post_categories_labels_array, '<span class="thematiques">' . $thematiqueLink . '</span>');
				}
				
				$ressourcesObj = $post_categories_a_la_carte["ressources"];
				if ($ressourcesObj) {
					$ressourcesSlug = $ressourcesObj["slug"];
					$ressourcesLibelle = $ressourcesObj["name"];
					$ressourcesLink = '<a href="' . $aLaCarteUrl . $ressourcesSlug . '" >' . $ressourcesLibelle . '</a>';
					array_push ($post_categories_labels_array, '<span class="ressources">' . $ressourcesLink . '</span>');
				}
				
				$post_categories_str = implode( " | ", $post_categories_labels_array );
				*/
				
				$resultsHtml .= '<li>';
				
				// Vignette
				$resultsHtml .= '<div class="post-image" style="background-image: url(' . $query_post["thumbnail"] . ')"><a title="" href="' . $query_post["permalink"] . '" target="_blank"  ></a></div>';
				
				// Catégorie et titre
				$resultsHtml .= '<div class="post-content">';
				$resultsHtml .= '<p class="post-text-categories">' . $post_categories_str . '</p>';
				
				$resultsHtml .= '<p class="post-text-content"><a title="" href="' . $query_post["permalink"] . '" target="_blank"  >' . $query_post["title"] . '</a></p>';
				
				$resultsHtml .= '<div class="clear"></div>';
				$resultsHtml .= '</div>';
				$resultsHtml .= '</li>';
			}
								   
								   
			// 
			
			if ($recherchePourModules === "") {
				$pour = ' | ' . $query_activite . ' |';
			} else {
				$pour  = ' | ' . $prefixRecherchePourModules . $recherchePourModules;
				$pour .= ' | ' . $prefixRecherchePourTypes . $recherchePourTypes;
				$pour .= ' | ' . $prefixRecherchePourContenus . $recherchePourContenus;
			}

			// Nb de résultats au total
			$resultNbsHtml = $nb_found_posts . " Résultat" . ( $nb_found_posts > 1 ? "s" : "" ) . " pour " . $pour;

			$divHtml = "<div class='search-nb-results-decryptage' id='cec-search-nb-results'>";

			if ($nbResultats === 0 ) {
				$nbResultatsHtml .= "Pas de résultats trouvés";
			} else {
				$nbResultatsHtml .= "<strong>" . $nbResultats . " " . ($nbResultats === 1 ? "résultat</strong> trouvé" : "résultats</strong> trouvés");
			}

			if ($search_param != NULL) {
				$nbResultatsHtml .= " pour <strong>" . $search_param . "</strong>";
			}

			$divHtml .= "<p class='nb-resultats'>" . $nbResultatsHtml . "</p>";
			$divHtml .= "</div>";
								   
								   
			$voirPlustml = "";
			if ( $more_posts !== false ) {
				$voirPlustml .= '<div id="voir_plus_recherche_articles" class="query-voir-plus"';
				$voirPlustml .= 'data-found-posts="' . $nb_found_posts . '"';
				$voirPlustml .= 'data-nb-posts="' . $nbPostsMax . '"';
				$voirPlustml .= 'data-offset="' . $query_results->voir_plus_offset . '"';
				$voirPlustml .= 'data-search-param=""';
				$voirPlustml .= 'data-except=""><a href="#">'. $voirPlusText . '</a></div>';
			}
		?>

		<div class="search-results-container" >
			<p class="search-nb-results" id="search-nb-results"><?php echo $resultNbsHtml ?></p>
			<ul class="search-results" id="search-results"><?php echo $resultsHtml ?></ul>
			<div class="search-pagination" id="search-pagination">
				<?php echo $voirPlustml; ?>
			</div>
		</div>

		<script type="text/javascript">

			jQuery(function($)
			{
				//
				
				function resizeColumns() {
					 
					 var maxHeight = 0;
					
					var columns = $('.page-header-column');
					 
					columns.each(function(index, element){
						 maxHeight = Math.max(maxHeight, $(element).height());
					});
					 
					columns.css("min-height", maxHeight+ "px");
				 };
			
				resizeColumns();
			
				$(window).on("resize", function(){ 
					resizeColumns();
				});
				
				//
				
				var ajaxurl        = "<?php echo admin_url( 'admin-ajax.php' ) ?>";
				var waitingGif     = "<?php echo CLASSCODE2_PLUGIN_URL ?>/assets/images/wait.gif";
				var waitingHTML    = "<img src='" + waitingGif + "' width='30' height='30' alt='chargement en cours...' />";
				var ajaxAction     = "classcode_v2_recherche_a_la_carte";
				var voirPlusText   = "<?php echo $voirPlusText; ?>";
				var aLaCarteUrl    = "<?php echo site_url( CLASSCODE2_ROUTE. "/a-la-carte/" ); ?>";
				var tagsEnfantsALaCarte = [ <?php echo implode( ",", $slug_term_children); ?> ];
				var libellesCategories  = <?php echo json_encode( $libellesCategories ); ?>;


				// Pagination :
				var nbPostsMax = <?php echo $nbPostsMax ?>;

				// Divs (boutons, container où apparaîtront les résultats, ...)
				var resultsNbElement  = $("#search-nb-results");
				var resultsElement    = $("#search-results");
				var paginationElement = $("#search-pagination");

				var getCategoriesForGroup = function ( groupNames ) {

					var categories = [];
					
					var i, n = groupNames.length, groupName, categorie;
					for(i=0; i<n; i++) {
						groupName = groupNames[i];
						categorie = $("input[type=radio][name="+groupName+"]:checked").val();
						if ( categorie && (categorie.length > 0)) {
							// On ne retient que la valeur du dernier groupe (pour conserver robotique, plutôt que son parent)
							categories = [ categorie ];
						}
					}

					return categories;
				}

				var getSelectedCategories = function () {
					
					// 1) Parcours :
					var categoriesParcours = getCategoriesForGroup( ["parcours"] );
					var tousLesParcours = categoriesParcours.length === 0;
					if ( tousLesParcours ) {
						categoriesParcours = tagsEnfantsALaCarte.concat();
					}

					// 2) Types de ressources :
					var categoriesTypesRessources = getCategoriesForGroup( ["ressources"] );

					// 3) Types de thématiques :
					var categoriesThematiques = getCategoriesForGroup( ["thematiques", "thematiques-branchee"] );

					// Critères de recherche combinés
					var categories = {
						"tous_les_parcours": tousLesParcours,
						"parcours": categoriesParcours,
						"ressources": categoriesTypesRessources,
						"thematiques": categoriesThematiques
					};
					
					return categories;
				};
				
				var get_html_for_post = function ( posts ) {
					
					var i, post_infos, n = posts.length, postsHtml = "", post_categories_a_la_carte, post_categories_labels_array, post_categories_str;
					var categoryObj, categorySlug, categoryLibelle, categoryLink, thematiqueObj, thematiqueSlug, thematiqueLibelle, thematiqueLink, ressourcesObj, ressourcesSlug, ressourcesLibelle, ressourcesLink;

					for(i=0; i<n; i++)
					{
						post_infos = posts[i];
						
						// Catégories :
						post_categories_a_la_carte = post_infos["categories_a_la_carte"];
						post_categories_labels_array = [ ];
						
						categoryObj = post_categories_a_la_carte["parcours"];
						if (categoryObj) {
							categorySlug = categoryObj["slug"];
							categoryLibelle = categoryObj["name"];
							categoryLink = '<a href="' + aLaCarteUrl + categorySlug + '" >' + categoryLibelle + '</a>';
							post_categories_labels_array.push ( '<span class="parcours">' + categoryLink + '</span>');
						}
						
						categoryObj = post_categories_a_la_carte["parcours_icn"];
						if (categoryObj) {
							categorySlug = categoryObj["slug"];
							categoryLibelle = categoryObj["name"];
							categoryLink = '<a href="' + aLaCarteUrl + categorySlug + '" >' + categoryLibelle + '</a>';
							post_categories_labels_array.push ( '<span class="parcours">' + categoryLink + '</span>');
						}
						
						thematiqueObj = post_categories_a_la_carte["thematiques"];
						if (thematiqueObj) {
							thematiqueSlug = thematiqueObj["slug"];
							thematiqueLibelle = thematiqueObj["name"];
							thematiqueLink = '<a href="' + aLaCarteUrl + thematiqueSlug + '" >' + thematiqueLibelle + '</a>'
							post_categories_labels_array.push ( '<span class="thematiques">' + thematiqueLink + '</span>');
						}
						
						ressourcesObj = post_categories_a_la_carte["ressources"];
						if (ressourcesObj) {
							ressourcesSlug = ressourcesObj["slug"];
							ressourcesLibelle = ressourcesObj["name"];
							ressourcesLink = '<a href="' + aLaCarteUrl + ressourcesSlug + '" >' + ressourcesLibelle + '</a>'
							post_categories_labels_array.push ( '<span class="ressources">' + ressourcesLink + '</span>');
						}
						
						post_categories_str = post_categories_labels_array.join(" | ");
						
						if (post_categories_str.length === 0 ) post_categories_str = "&nbsp;";

						postsHtml += '<li>';
						
						// Vignette
						postsHtml += '<div class="post-image" style="background-image: url(' + post_infos.thumbnail + ')"><a title="" href="' + post_infos.permalink + '" target="_blank"></a></div>';
						
						// Catégories et titre 
						postsHtml += '<div class="post-content">';
						postsHtml += '<p class="post-text-categories">' + post_categories_str + '</p>';
						postsHtml += '<p class="post-text-content"><a title="" href="' + post_infos.permalink + '" target="_blank">' + post_infos.title + '</a></p>';
						
						postsHtml += '<div class="clear"></div>';
						postsHtml += '</div>';
						postsHtml += '</li>';
					}
					
					return postsHtml;
				}
				
				var changeTimeout;
				
				$(".search input[type=radio]").on("change", function() {
					
					if (changeTimeout) clearTimeout(changeTimeout);
					
					switch( $(this).prop("name") ){
						case "thematiques":
							$(".search input[type=radio][name=thematiques-branchee]").attr('checked',false);
							break;
						case "thematiques-branchee":
							$(".search input[type=radio][value=activite-branchee]").attr('checked',true);
							break;
					}
					
					changeTimeout = setTimeout( function() {
					
						// Critères de recherche combinés
						var jsonCategories = getSelectedCategories();

						
						// On vide les résultats actuels
						$(".search-results").empty();

						resultsNbElement.html(waitingHTML);
						resultsElement.empty();
						paginationElement.empty();

						var query_params = JSON.stringify ({
							nb_posts: nbPostsMax,
							offset: 0,
							search: jsonCategories
						});

						$.ajax({
							 "dataType" : "json",
							 "data"     : { 'action': ajaxAction, 'query_params': query_params },
							 "type"     : "POST",
							 "url"      : ajaxurl,

							 "success"  : function ( response ) {

								// console.log("response :", response);

								var query_results = response.query_results;
								var posts  = query_results.posts;

								var resultsNbHtml = "";
								var resultsHtml = "";

								// Nombre de résultats de la recherche ( hors pagination )
								// Si ce nombre est plus grand que la pagination, un '+' apparaît après les blocs
								var resultsNbTotal = parseInt( query_results.found_posts );

								// Nombre de résultats retournés par la requête paginée (égal ou inférieur aux nombre maximum demandé)
								var resultsNb = posts.length;

								if (resultsNbTotal == 0)
								{
									resultsNbHtml = '<p class="nb-resultats">Pas de résultats</p>';
								}
								else
								{
									// Pour...
									var tousLesParcours      = jsonCategories.tous_les_parcours;
									var categorieParcours    = jsonCategories.parcours;
									var categorieParcoursICN = jsonCategories.parcours_icn;
									var categorieThematiques = jsonCategories.thematiques;
									var categorieRessources  = jsonCategories.ressources;
									
									console.log("categorieParcours", categorieParcours);
									console.log("categorieThematiques", categorieThematiques);
									console.log("categorieRessources", categorieRessources);

									var pour = " pour ";
									
									pour += ' | ' + libellesCategories['prefix-parcours'];
									if (tousLesParcours) {
										pour += libellesCategories['parcours'];
									} else {
										var cat = categorieParcours[0];
										var catPosNot = cat.indexOf("!");
										if (catPosNot != -1) {
											// Cas du type module2!1-2-3-codez :
											pour += libellesCategories[cat.substr(0, catPosNot)];
										} else {
											pour += libellesCategories[cat];
										}
									}
									
									if (categorieParcoursICN && (categorieParcoursICN.length > 0 )) {
										pour += ' - ' + libellesCategories[categorieParcoursICN[0]];
									}
									
									pour += ' | ' + libellesCategories['prefix-thematiques'];
									pour += categorieThematiques.length === 0 ? libellesCategories['thematiques'] : libellesCategories[categorieThematiques[0]];
									
									pour += ' | ' + libellesCategories['prefix-ressources'];
									pour += categorieRessources.length === 0 ? libellesCategories['ressources'] : libellesCategories[categorieRessources[0]];
									
									// Nb de résulats
									var label_results = resultsNbTotal + " Résultat" + (resultsNbTotal === 1 ? "" : "s") + pour;
									resultsNbHtml = '<p class="nb-resultats">' + label_results + '</p>';

									resultsHtml += get_html_for_post( posts );

									if (query_results.voir_plus === true)
									{
										//
										// Ajout du bouton "voir plus" :
										//

										var paginationHtml = '<div id="voir_plus_recherche_articles" class="query-voir-plus bloc-vertical"';
										paginationHtml += 'data-found-posts="'+ query_results.found_posts +'"';
										paginationHtml += 'data-nb-posts="'+ query_results.nb_posts +'"';
										paginationHtml += 'data-offset="'+ query_results.voir_plus_offset +'"';
										paginationHtml += 'data-search-param="'+ escape(jsonCategories) +'"';
										paginationHtml += 'data-except=""><a href="#">'+ voirPlusText + '</a></div>';

										paginationElement.html( paginationHtml );
									}

									resultsHtml += '<div class="clear"></div>';
								}

								resultsNbElement.html(resultsNbHtml);
								resultsElement.html(resultsHtml);

								if ( query_results.voir_plus === true )
								{
									var voirPlusElement = $("#voir_plus_recherche_articles");
									var voirPlusLink = $("#voir_plus_recherche_articles a");

									voirPlusLink.on("click", function(e) {

										e.preventDefault();
										e.stopPropagation();

										// Dans la requête "voir plus de résultats", on demande d'afficher tous les résultats restants
										var query_params = JSON.stringify ({
											nb_posts : voirPlusElement.data("found-posts") - voirPlusElement.data("offset"),
											offset   : voirPlusElement.data("offset"),
											search   : jsonCategories
										});

										$.ajax({

											 "dataType" : "json",
											 "data"     : { 'action': ajaxAction, 'query_params':query_params },
											 "type"     : "POST",
											 "url"      : ajaxurl,

											 "success"  : function ( response ) {

												// console.log("voir + response", response);

												var query_results = response.query_results;
												var posts  = query_results.posts;

												$("#search-results").append( get_html_for_post( posts ) );

												if ( query_results.voir_plus === false)
												{
													voirPlusElement.remove();
												}
												else
												{
													// On retire le Gif de progression, on remet le "+", puis on met à jour l'offset :
													voirPlusLink.html(voirPlusText);
													voirPlusLink.css("pointer-events", "all");
													voirPlusElement.data("offset", query_results.voir_plus_offset);
												}

											 }
										});
									});
								}								 

							 } // 
						});
						
					}, 1050);
					
				});

				var voirPlusElement = $("#voir_plus_recherche_articles");
				var voirPlusLink = $("#voir_plus_recherche_articles a");
				
				voirPlusLink.on("click", function(e) {

					e.preventDefault();
					e.stopPropagation();
					
					var jsonCategories = getSelectedCategories();

					var query_params = JSON.stringify ({
						nb_posts : voirPlusElement.data("found-posts") - voirPlusElement.data("offset"),
						offset   : voirPlusElement.data("offset"),
						search   : jsonCategories
					});

					$.ajax({

						 "dataType" : "json",
						 "data"     : { 'action': ajaxAction, 'query_params':query_params },
						 "type"     : "POST",
						 "url"      : ajaxurl,

						 "success"  : function ( response ) {

							// console.log("voir + response", response);

							var query_results = response.query_results;
							var posts  = query_results.posts;

							$("#search-results").append( get_html_for_post( posts ) );

							if ( query_results.voir_plus === false)
							{
								voirPlusElement.remove();
							}
							else
							{
								// On retire le Gif de progression, on remet le "+", puis on met à jour l'offset :
								voirPlusLink.html(voirPlusText);
								voirPlusLink.css("pointer-events", "all");
								voirPlusElement.data("offset", query_results.voir_plus_offset);
							}

						 }
					});
				});				
				
			});
		</script>

	</div><!-- search -->
</div><!-- ressources-a-la-carte -->
