<div class="classcode-v2 content-category page-credits" >

	<!-- Crédits -->
	
	<p class="content-category-title">Crédits et Mentions légales</p>
	<div class="content-wp credits">

	<div><strong>Dénomination&nbsp;:</strong>«Class’Code.»&nbsp; http://classcode.fr ( avec des pages hébergées sur https://pixees.fr )</div> 
	
	<div><strong>Responsable légal du site</strong>: le Président-directeur général d’Inria, pour Inria, Domaine de Voluceau, Rocquencourt – B.P. 105,&nbsp; 78153 Le Chesnay Cedex – France, Téléphone: +33 1 39 63 55 11</div>
	
	<div><strong>Crédits :</strong> <em>Ergonomie et graphisme ont été conçus et réalisés par <a href="http://www.s24b.com/" class="external" target="_blank">S24B</a>. La conception et le développement sont dus à <a href="https://www.linkedin.com/in/denischiron">Denis Chiron</a> et <a href="https://www.linkedin.com/in/bninassi" class="external" target="_blank">Benjamin Ninassi</a>. Le volet communication et rédactionel est du à <a href="https://www.linkedin.com/in/%C3%A9milie-peinchaud-a978a817">Émilie Peinchaud</a>.</em></div>
	
	<div><strong>Hébergeur</strong> : Médiation Scientifique et Direction des systèmes d’information d’Inria</div>
	
	<div><strong>Données personnelles:&nbsp; </strong>Conformément aux articles 39 et 40 de la loi Informatique et Libertés modifiée par la loi n° 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l’égard des traitements de données à caractère personnel, vous disposez d’un droit d’accès et de rectification des données vous concernant. Le traitement et stockage de ces données a été déclaré au Correspondant Informatique et Liberté Inria.</div>
	
	<div><strong>Contenus de ce site:</strong> Tous les contenus du site sont, sauf avis contraire explicite, soumis à la licence <a title="Licence et copyright des contenus de ce site." href="https://creativecommons.org/licenses/by/2.0/fr/" class="external" target="_blank">CC-BY</a> et les composants logiciels à la licence <a title="Licence et copyright des composants logiciells ce site." href="https://cecill.info/licences/Licence_CeCILL-C_V1-fr.html" class="external" target="_blank">CeCILL-C</a>. On s’efforce d’assurer au mieux de nos possibilités, l’exactitude et la mise à jour des informations diffusées, au moment de leur mise en ligne sur le site. Cependant, on ne garantit en aucune façon l’exactitude, la précision ou l’exhaustivité des informations mises à disposition sur le site. Les informations présentes sur le site sont non-contractuelles et peuvent être modifiées à tout moment.</div>
	
	<div><strong>Statistiques:&nbsp; </strong>Ce site utilise Google Analytics, un service d’analyse de site internet fourni par Google Inc. Les données générées par les cookies concernant votre utilisation du site (y compris votre adresse IP) seront transmises et stockées par Google sur des serveurs situés aux Etats-Unis.&nbsp; Vous pouvez désactiver l’utilisation de cookies en sélectionnant les paramètres appropriés de votre navigateur. En utilisant ce site internet, vous consentez expressément au traitement de vos données nominatives par Google dans les conditions et pour les finalités décrites ci-dessus.<strong>&nbsp; </strong>Ce site utilise <a href="https://www.mediego.com/" class="external" target="_blank">Mediego</a> pour offrir un système de recommandation pour les utilisateurs, actuellement en phase de test.</div>
			
	</div>

</div><!-- classcode-v2 -->
