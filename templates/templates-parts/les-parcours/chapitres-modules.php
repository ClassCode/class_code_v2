<?php
	if ( have_posts() ) {
		while ( have_posts() ) {

			the_post();

			// Contenu du post :
			$htmlContent = get_the_content();
			
			$dom = new DOMDocument();
			// $dom->encoding='UTF-8';
			
			@$dom->loadHTML( $htmlContent );

			// Récupération de la vidéo
			$iframes = $dom->getElementsByTagName('iframe');
			foreach ($iframes as $iframe){
				$src = $iframe->getAttribute("src");
				if ( strpos ( $src, 'https://player.vimeo.com/' ) !== FALSE) {
					$iframe->setAttribute( "width", "100%");
					$iframe->setAttribute( "height", "450px");
					
					// On met de côté l'iframe de la vidéo
					$iframeVimeo = $dom->saveHTML($iframe);
					
					// On retire le noeud de l'iframe du DOM HTML
					$iframe->parentNode->removeChild($iframe); 
					
					break;
				}
			}

			// Récupération des objectifs
			$uls = $dom->getElementsByTagName('ul');
			foreach ($uls as $ul){
				$className = $ul->getAttribute("class");
				if ( $className === "module-objectifs") {
					
					// On met de côté le ul des objectifs pédago 
					$htmlObjectifsPedago = $dom->saveHTML($ul);
					
					// On retire le noeud de l'ul
					$ul->parentNode->removeChild($ul); 
					
					break;
				}
			}
			
			// Titre des objectifs
			$h2s = $dom->getElementsByTagName('h2');
			foreach ($h2s as $h2){
				$className = $h2->getAttribute("class");
				if ( $className === "module-objectifs") {
					// On retire le noeud de l'h2'
					$h2->parentNode->removeChild($h2); 
					break;
				}
			}			

			// Le rendu HTML du DOM est :
			$htmlContent = utf8_decode( $dom->saveHTML( $dom ));
			$htmlContent = do_shortcode( $htmlContent );
		}
	}
?>


<?php if ( $iframeVimeo ) { ?>
	<div class="classcode-v2-header-video" >
		<?php echo $iframeVimeo; ?>
	</div>
<?php } ?>


<div class="classcode-v2 content-category" >

	<p class="content-category-title"><?php echo get_the_title(); ?></p>

	<?php if ( $htmlObjectifsPedago ) { ?>

	<!-- Modules 1 à 5 -->

	<div class="page-header">

		<div class="page-header-columns">

			<div class="page-header-column objectifs-module">
				<div class="page-header-column-title">Objectifs pédagogiques</div>
				<div class="page-header-column-content">
					<div class="presentation-module">
							<?php echo utf8_decode( $htmlObjectifsPedago ); ?>
					</div>
				</div>
			</div>
			
			<div class="page-header-column rencontres">
				<div class="page-header-column-content no-padding" style="background-image: url( <?php echo CLASSCODE2_PLUGIN_URL.'assets/images/parcours/carte_rencontres.jpg'; ?> )">
					<a href="<?php echo site_url( CLASSCODE2_ROUTE. "?meetingSearch=true#mapWidget" ); ?>"></a>
				</div>
			</div>

			<div class="page-header-column attestation">
				<div class="page-header-column-content no-padding">
					<a href="<?php echo get_site_url();?>/wp-content/plugins/class_code/attestation/index.php">
						<img  src="<?php echo CLASSCODE2_PLUGIN_URL; ?>/assets/images/parcours/attestation.png" alt="téléchargez votre attestation" />
						<br/>Téléchargez votre attestation
					</a>
				</div>
			</div>
						
			<div class="clear"></div>
		
		</div><!-- page-header-columns -->
		
	</div--><!-- header -->

	<?php } ?>

	<div class="page-parcours" >
	<?php 
		// Contenu HTML hors vidéo et objectifs pédago
		echo $htmlContent;
	?>
	</div>


	<script type="text/javascript">

		jQuery(function($){

			
			// Toggle des différentes parties
			var moduleParts = $(".page-parcours h2.module-partie");
			
			moduleParts.on("click", function(e) {
				
				if ( $(this).hasClass("closed") ) {
					$(this).removeClass("closed");
					$(this).nextUntil("h2").css("display", "block");
				} else {
					$(this).addClass("closed");
					$(this).nextUntil("h2").css("display", "none");
				}
				
				
			});
			
			
			// Hauteur des blocs du header
			
			var resizeColumns = function() {

				var maxHeight = 0;

				var columns = $('.page-header-column');

				columns.each(function(index, element){
					 maxHeight = Math.max(maxHeight, $(element).height());
					console.log(index, element, $(element).height())
				});

				columns.css("height", maxHeight+ "px");
			}

			resizeColumns();

			$(window).on("resize", function(){ 
				resizeColumns();
			});
		})

	</script>


	<?php 
	/*

	$permalink = get_permalink();
	$permalink_parts = explode( "/", $permalink);
	if ( count($permalink_parts) > 0) {

		$last_part = array_pop( $permalink_parts );
		if ( strlen( $last_part ) === 0 ) {
			$last_part = array_pop( $permalink_parts );
		}

		$module_no_for_last_parts = array(

			"module1"     => "module-1",
			"module2"     => "module-2",
			"module3"     => "module-3",
			"module4"     => "module-4",
			"module5"     => "module-5",
			"icn"         => "mooc-icn-informatique-et-creation-numerique",
			"1-2-3-codez" => "1-2-3-codez"
		);

		$category = $module_no_for_last_parts[ $last_part ];

		if ( isset ($category)) {
			echo do_shortcode( '[classcode2_module category="'. $category .'"]' );
		}
	}
	
	*/
	?>

</div><!-- classcode-v2 -->
