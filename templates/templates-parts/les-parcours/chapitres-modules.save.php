<div class="classcode-v2 content-category" >
	<p class="content-category-title"><?php echo get_the_title(); ?></p>

	<div class="page-header">

		<div class="page-header-columns">

			<div class="page-header-column objectifs-module">
				<div class="page-header-column-title">Objectifs pédagogiques</div>
				<div class="page-header-column-content">
					<div class="content-wp presentation-module">
						<?php
							if ( have_posts() ) {
								while ( have_posts() ) {
									the_post();
									the_content();
								}
							}
						?>
					</div>
				</div>
			</div>
			
			<div class="page-header-column rencontres">
				<div class="page-header-column-content no-padding">
					<img src="<?php echo CLASSCODE2_PLUGIN_URL.'assets/images/parcours/carte_rencontres.jpg'; ?>" alt="" />
				</div>
			</div>

			<div class="page-header-column attestation">
				<div class="page-header-column-content">
				</div>
			</div>
						
			<div class="clear"></div>
		
		</div><!-- page-header-columns -->

	</div><!-- header -->

	<div class="page-parcours" >
		<ul class="page-parcours-grains">
			<?php 
				$permalink = get_permalink();
				$permalink_parts = explode( "/", $permalink);
				if ( count($permalink_parts) > 0) {
				
					$last_part = array_pop( $permalink_parts );
					if ( strlen( $last_part ) === 0 ) {
						$last_part = array_pop( $permalink_parts );
					}

					$module_no_for_last_parts = array(

						"module1"     => "module-1",
						"module2"     => "module-2",
						"module3"     => "module-3",
						"module4"     => "module-4",
						"module5"     => "module-5",
						"icn"         => "mooc-icn-informatique-et-creation-numerique",
						"1-2-3-codez" => "1-2-3-codez"
					);

					$category = $module_no_for_last_parts[ $last_part ];

					if ( isset ($category)) {
						echo do_shortcode( '[classcode2_module category="'. $category .'"]' );
					}
				}
			
			?>
		</ul>
	</div>
	
</div><!-- classcode-v2 -->
