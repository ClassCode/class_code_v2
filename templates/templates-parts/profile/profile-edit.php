<?php

  // Initialisation du tableau des messages d'aide et des strucures
  include_once(plugin_dir_path( __FILE__ ).'/../../../class_code_config.php');  
  include_once(plugin_dir_path( __FILE__ ).'/../../../rencontres/rencontre_type.php');
  
  //Inclusions for OpenStreetMap
  wp_enqueue_style('leaflet','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/leaflet.css');
  wp_enqueue_style('markercluster','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/plugins/leaflet.markercluster-1.3.0/dist/MarkerCluster.Default.css');  

  wp_enqueue_script('leaflet','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/leaflet.js');
  wp_enqueue_script('markercluster','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/plugins/leaflet.markercluster-1.3.0/dist/leaflet.markercluster.js'); 

  // check original profil
  $userProfile = profile_type::get_profile();
  $flashMessage = '';
  $flashmessageclass="";
  if(isset($_REQUEST['action'])){
    $action = $_REQUEST['action'];
    if($action=="update"){
    $flashMessage = "Profile modifié avec succès !";
    }else if($action=="create"){
      $flashMessage = "Profile créé avec succès !";
    }else if($action=="error"){
      $flashmessageclass="error";
      $flashMessage = "Une erreur est survenue lors de l'enregistrement du profile !";
    }else if($action=="passworderror"){
      $flashmessageclass="error";
      $flashMessage = "Les deux mots de passe ne sont pas identiques !";
    }else if($action=="modalupdate"){
      $flashMessage = "Un email vous a été envoyé pour confirmer les changements !";
    }
  }
?>
<!-- Modal for password and mail changes -->
<div id="profilemodal" class="profilemodal">
  <div id="profilemodal-content" class="profilbloc profilemodal-content">
    <div class="profilbloctitle">
      <span class="close">&times;</span>
      <h2>Changement d'addresse mail ou de mot de passe</h2>
    </div>
    <div class="profilbloccontent">
      
      <form action="<?php echo plugins_url( '/../../../profiles/profile-update.php', __FILE__ ); ?>" method="post" id="modal-edit-form" class="">
        <input type="hidden" id="modaledit" name="modaledit" value="true">
        <input type="hidden" id="nonce" name="nonce" value="<?php echo wp_create_nonce( 'profilemodal-edit' ); ?>" >
        <h3> Email du compte</h3>
        <input required id="email" type="email" value="<?php echo wp_get_current_user()->user_email; ?>" name="email" placeholder="Mail" title="">
        <h3> Mot de passe (laissez vide si vous ne voulez pas le changer)</h3>
        <input id="password1" type="password" name="password1" value="">
        <h3> Répétez le mot de passe</h3>
        <input id="password2" type="password" name="password2" value="">    
        <a class="profilButton" onclick="submitModalForm();">Valider mes données</a>        
      </form>      
      <p>La confirmation vous sera envoyée par e-mail.</p>
    </div>    
  </div>

</div>
    
<div id="profilheader">
  <?php
    if($flashMessage!=""){
      echo '<div id="profileflashMessage" class="'.$flashmessageclass.'">';
        echo $flashMessage;
      echo '</div>';
    }  
  ?>
  <p>
  Bonjour <?php echo $userProfile['firstname']; ?> !<br>
  Bienvenue dans votre espace personnel ! Ici, vous êtes à la maison: activités près de chez vous, réponses à vos questions, actualités dans votre région et aussi des accès rapides aux parcours de formation.
  </p>
</div>
<div id="mapPosBox">
  <form action="<?php echo plugins_url( '/../../../profiles/profile-update.php', __FILE__ ); ?>" method="post" id="profile-edit-form" class="" onSubmit="encodeAvatarImg();">
  <input type="hidden" id="nonce" name="nonce" value="<?php echo wp_create_nonce( 'profile-edit' ); ?>" >
  
  <div id="profileFirstRow" class="content-category userprofil">
    <p id="personnal-data-trigger" class="content-category-title">VOS DONNEES PERSONNELLES</p>
    <div id="profileAvatar" class="profilbloc profilblocfirst">
      <div class="profilbloctitle">Votre image</div>
      <div class="profilbloccontent">
        <?php class_code_user_avatar_editor(stripslashes($userProfile['avatar'])); ?>
        <input required id="avatar" type="hidden" aria-required="true" value="<?php echo $userProfile['avatar']; ?>" name="avatar">
        <input required id="avatarImg" type="hidden" aria-required="true" value="<?php echo $userProfile['avatarImg']; ?>" name="avatarImg"> 
      </div>
    </div>
    
    <div id="profilePrimaryFields" class="profilbloc profilblocsecond">
      <div class="profilbloctitle">Vous</div>
      <div class="profilbloccontent">
        <input required id="lastname" type="text" aria-required="true" value="<?php echo $userProfile['lastname']; ?>" name="lastname" placeholder="Nom" title="Mon nom">
        <input required id="firstname" type="text" aria-required="true" value="<?php echo $userProfile['firstname']; ?>" name="firstname" placeholder="Prénom" title="Mon prénom">
        <input disabled id="displayed_email" type="text" value="<?php echo wp_get_current_user()->user_email; ?>" name="displayed_email" placeholder="Mail" title="Mon adresse électronique ne peut être modifée ici, voir ci-dessous">
        <input id="email" type="hidden" value="<?php echo wp_get_current_user()->user_email; ?>" name="email">
        <input required id="nickname" type="text" aria-required="true" value="<?php echo $userProfile['nickname'] ?>" name="nickname" placeholder="Pseudo" title="Mon pseudo (le nom qui se sera affiché)">
        
	      <?php echo do_completion_field("structure", array_keys($data_structures), $userProfile['structure'], "", false,"Structure",$helpTab['structure']); ?>
        
        <input id="context" type="text" value="<?php echo $userProfile['context']; ?>" name="context" placeholder="Cadre de la formation ?" title="<?php echo $helpTab['cadre']; ?>">
    <a class="profilButton" onclick="submitProfilForm();">Valider mes données</a>
    <a class="" id="profilemodalToggle" href="">Modifier mon mail ou mon mot de passe</a>
        
      </div>
    </div>
    <?php 
      $userLocation = $userProfile['location'];
      if(!isset($userLocation) || $userLocation== null){
        $userLocation=array('lat'=>0,'lng'=>0);
      }
      
    ?>    
    <div id="profileLoc" class="profilbloc">   
      <div id="profilblocLocInner">    
        <div id="profilbloctitleLoc">Votre Localisation</div>
        <div class="profilbloccontent">
          Class'Code est une formation hybride qui propose des rencontres un peu partout sur le territoire. Pour rencontrer les autres participants près de chez vous, découvrir les temps de rencontre, etc. nous avons besoin de savoir où vous êtes.
        
          <input class="" id="street" type="text" name="street" value="<?php echo $userLocation['street']; ?>" placeholder="Adresse" title="Numéro et rue" /><br/>
          <input class="firstInline" id="city" type="text" name="city" value="<?php echo $userLocation['city']; ?>" placeholder="Ville" title="Ville" />
          <input class="secondInline" id="zipcode" type="text" name="zipcode" value="<?php echo $userLocation['zipcode']; ?>" placeholder="Code Postal" title="Code Postal" /><br>
          <input class="" id="country" type="text" name="country" value="<?php echo $userLocation['country']; ?>" placeholder="Pays" title="Pays" /><br/>
          <input id="lat" type="hidden" name="lat" value="<?php echo $userLocation['lat']; ?>"/>
          <input id="lng" type="hidden" name="lng" value="<?php echo $userLocation['lng']; ?>"/>  
          <input id="state" type="hidden" name="state" value="<?php echo $userLocation['state']; ?>"/>
          <input id="formatted_address" type="hidden" name="formatted_address" value="<?php echo $userLocation['formattedAddress']; ?>"/>
          <a class="profilButton" onclick="updateOSMap();">Mettre à jour sur la carte </a>
          <a class="profilButton" onclick="submitProfilForm();">Valider mes données</a>
        </div>
      </div>
      <div id="userMap" class="">
        
        
      </div>
      
      <?php
        
      ?> 
    </div>
      
    <div class="clear"></div>
  </div>
  
  <div id="profileSecondRow" class="content-category userprofil">
    <p class="content-category-title">VOTRE PROFIL</p>
    <div id="profileInfosSup" class="profilbloc profilblocfirst">
      <div class="profilbloctitle">Votre Profil</div>
      <div class="profilbloccontent">
        <h3>EDUCATION</h3>
          <input id="profile" type="hidden" value="<?php echo $userProfile['profile']; ?>" name="profile" >
          <?php 

          function profilCheck($value,$userProfile){
            $profilChecked= "";
            if($userProfile == $value ){
              $profilChecked="checked";
            }       
            return $profilChecked;
          }          
          
          ?>
          <?php $profilInputValue = "Animation d'activités péri-scolaires"; ?>
          <input <?php echo profilCheck($profilInputValue,$userProfile['profile']); ?> type="radio" name="radio_Profil" value="<?php echo $profilInputValue; ?> "/><label for="<?php echo $profilInputValue; ?>"><?php echo $profilInputValue; ?></label>  <br />
          <?php $profilInputValue = "Animation d'activités extra-scolaires"; ?> 
          <input <?php echo profilCheck($profilInputValue,$userProfile['profile']); ?> type="radio" name="radio_Profil" value="<?php echo $profilInputValue; ?> "/><label for="<?php echo $profilInputValue; ?>"><?php echo $profilInputValue; ?></label> <br />
          <?php $profilInputValue = "Professeur (ou formateur) en primaire"; ?>
          <input <?php echo profilCheck($profilInputValue,$userProfile['profile']); ?> type="radio" name="radio_Profil" value="<?php echo $profilInputValue; ?> "/><label for="<?php echo $profilInputValue; ?>"><?php echo $profilInputValue; ?></label> <br />
          <?php $profilInputValue = "Professeur (ou formateur) en secondaire"; ?>
          <input <?php echo profilCheck($profilInputValue,$userProfile['profile']); ?> type="radio" name="radio_Profil" value="<?php echo $profilInputValue; ?> "/><label for="<?php echo $profilInputValue; ?>"><?php echo $profilInputValue; ?></label> <br />
          <input id="teaching" type="text" value="<?php echo $userProfile['teaching']; ?>" name="teaching" placeholder="Précision ou matière enseignée">          
        <h3>INFORMATIQUE</h3>
          <?php $profilInputValue = "Professionnel en informatique"; ?>
          <input <?php echo profilCheck($profilInputValue,$userProfile['profile']); ?> type="radio" name="radio_Profil" value="<?php echo $profilInputValue; ?> "/><label for="<?php echo $profilInputValue; ?>"><?php echo $profilInputValue; ?></label> <br />
          <input id="computerJob" type="text" aria-required="true" value="<?php echo $userProfile['computerJob']; ?>" name="computerJob" placeholder="Profession">

          <?php 
            $facilitateurChecked="";
            if($userProfile['facilitator']){
              $facilitateurChecked = "checked";
            }
          ?>
          <input <?php echo $facilitateurChecked; ?> id="facilitator" type="checkbox" value="true" name="facilitator"><label for="facilitator"><a target="_blank" href="<?php echo $classcodeUrl; ?>/classcode/accueil/documentation/classcode-la-documentation-facilitateur/">Je veux aider comme facilitateur</a></label>
          
          <h3>AUTRE</h3>
            <?php $profilInputValue = "Parent d'élèves"; ?>
            <input <?php echo profilCheck($profilInputValue,$userProfile['profile']); ?> type="radio" name="radio_Profil" value="<?php echo $profilInputValue; ?> "/><label for="<?php echo $profilInputValue; ?>"><?php echo $profilInputValue; ?></label> <br />
            <?php $profilInputValue = "Etudiant"; ?>
            <input <?php echo profilCheck($profilInputValue,$userProfile['profile']); ?> type="radio" name="radio_Profil" value="<?php echo $profilInputValue; ?> "/><label for="<?php echo $profilInputValue; ?>"><?php echo $profilInputValue; ?></label> <br />    
            <input id="learning" type="text" value="<?php echo $userProfile['learning']; ?>" name="learning" placeholder="Discipline"><br />
            <?php $profilInputValue = "Simple curieux"; ?>
            <input <?php echo profilCheck($profilInputValue,$userProfile['profile']); ?> type="radio" name="radio_Profil" value="<?php echo $profilInputValue; ?> "/><label for="<?php echo $profilInputValue; ?>"><?php echo $profilInputValue; ?></label>
          
      </div>
    </div>
    <div id="profileSkills" class="profilbloc profilblocsecond">
      <div class="profilbloctitle">Vos Compétences</div>
      <div class="profilbloccontent">
        <textarea rows="12" id="skills" value="" onInput="skillNormalize();" name="skills" placeholder="Compétences"><?php echo $userProfile['skills']; ?></textarea>
        
        <div id="skillExample">
          <?php
          // Voici les mots-clés contrôlés des compétences
          $skills =  array(
          "programmation",
          "pédagogie",
          "histoire informatique",
          "robotique",
          "scratch",
          "snap!",
          "thymio",
          "arduino",
          "réseaux",
          "théorie informatique",
               );
          foreach($skills as $skill) 
            echo '<div class="greyRectangleSkills"><a href="#" onClick="skillAdd(\''.$skill.'\'); return false;">'.$skill.'</a></div> ';
          ?>
          <br />
          <a class="profilButton" onclick="submitProfilForm();">Valider mon profil</a>
        </div>
  
      </div>
    </div> 
    <div id="profileAditionnalLinks" class="profilbloc profilblocthird">
      <div class="profileAditionnalLink profileAditionnalLinkFirst"> 
        <div class="profileAditionnalLinkVideo">
          <a class="module-video" title="Témoignage d'une participante de Class´Code : les points forts de la formation" href="https://www.youtube.com/embed/QGMbeVyfqWU?autoplay=1" rel="shadowbox" onclick="Shadowbox.open({content: this.href, player: 'iframe', title: this.title, width:window.innerWidth, height:(window.innerHeight-120)}); return false;">
          </a>					
        </div>
        <div class="profileAditionnalLinkTitle">FORMEZ-VOUS !</div>
          <div class="profileAditionnalLinkContent">Enseignant, animateurs, éducateurs, professionnels de l'informatique ou simple curieux, formez-vous pour initiez les jeunes à la pensée informatique grâce à Class'Code !<br>
            <a class="profileAditionnalLinkParent" href="<?php echo $classcodeUrl; ?>/classcode-v2/faq/#formation"> 
            > A propos des formations
            </a>
          </div>
        <div class="clear"></div>
      </div>
      <div class="profileAditionnalLink profileAditionnalLinkSecond">
        <div class="profileAditionnalLinkVideo">
          <a class="module-video" title="Témoignage de facilitateur-e-s de Class´Code : avant la rencontre" href="https://www.youtube.com/embed/bIBw1HCn6rA?autoplay=1" rel="shadowbox" onclick="Shadowbox.open({content: this.href, player: 'iframe', title: this.title, width:window.innerWidth, height:(window.innerHeight-120)}); return false;">
          </a>				
        </div>
        <div class="profileAditionnalLinkTitle">DEVENEZ FACILITATEUR</div>
        <div class="profileAditionnalLinkContent">Développeurs, chercheurs, étudiants, professionnel(les) de l'informatique, partagez un peu de votre expérience et aidez à former les formateurs de nos enfants en devenant facilitateur !<br>
        <a class="profileAditionnalLinkParent" href="<?php echo $classcodeUrl; ?>/classcode-v2/faq/#rencontres"> 
          > A propos des rencontres
        </a><br>
        <a target="_blank" class="profileAditionnalLinkParent" href="https://drive.google.com/file/d/0BxArukeaJm9NY1dlUmppdUszQ2c/view"> 
          > Le guide du facilitateur
        </a>
        </div>
        <div class="clear"></div>
      </div>
      <div class="profileAditionnalLink profileAditionnalLinkThird">
        <div class="profileAditionnalLinkVideo">
          <a class="module-video" title="Temps de rencontre Class´Code 2/2" href="https://www.youtube.com/embed/04vZ-MIlFRo?autoplay=1" rel="shadowbox" onclick="Shadowbox.open({content: this.href, player: 'iframe', title: this.title, width:window.innerWidth, height:(window.innerHeight-120)}); return false;"></a>
        </div>
        <div class="profileAditionnalLinkTitle">ORGANISEZ DES FORMATIONS</div>
        <div class="profileAditionnalLinkContent">Responsables de structure, en charge d'organiser des formations, intéressées à proposer des temps de rencontres : profitons de Class'Code.<br>
        <a class="profileAditionnalLinkParent" href="<?php echo $classcodeUrl; ?>/classcode-v2/?meetingSearch=true#mapWidget"> 
          > Organiser des rencontres
        </a><br>
        <a class="profileAditionnalLinkParent" href="<?php echo $classcodeUrl; ?>/classcode-v2/faq/#diffusion"> 
          > Comment vous aider à nous aider
        </a>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  </form>
</div>

  <div id="profileThirdRow" class="content-category userprofil">
    <p class="content-category-title">MON ACTIVITE DANS CLASS'CODE</p>
    <div id="profileMonActivite" class="profilbloc">
      <div class="profilbloctitle">Vos rencontres</div>
      <div class="profilbloccontent">
        <?php
          $what=array();
          $what['mes_rencontres']=true;
          $what['where']='everywhere';
          $meetings = rencontre_type::get_rencontres($what);
          if(sizeof($meetings) == 0){
            echo "<h3>Vous n'êtes inscrit à aucune rencontre.</h3>";
          }else{
            echo "<ul>";
            foreach($meetings as $meeting) { 
              $meeting_url= get_site_url()."/classcode-v2/rencontres/?meetingId=".$meeting['id'];
              $meeting_date ="";
              if((null !== $meeting['date']) && ($meeting['date']!="")){
                $meeting_date .= $meeting['date'];
              }
              if(($meeting_date != "")&& (null !== $meeting['time']) && ($meeting['time']!="")){
                $meeting_date .= " à ".$meeting['time'];
              }

              $meeting_string = $meeting['module'];
              if($meeting_date != ""){
                $meeting_string .= " le ".$meeting_date;
              }
              echo "<li><a href='".$meeting_url."' >".$meeting_string."</a></li>";
            }
            echo "</ul>";
          }
        ?>
      </div>
    </div>
    <div id="profileMonAttestation" class="profilbloc profilAttestationbloc">
    <a target="_blank" class="profileAttestationLinkParent" href=" <?php echo $classcodeUrl; ?>/wp-content/plugins/class_code_v2/attestation/index.php">
      <div id="profileAttestationLink">
        Télécharger mon attestation pour voir ma progression
      </div>
    </a>
    </div>
    <div class="clear"></div>
  </div>
  
  <div id="profileFourRow" class="content-category userprofil">
    <p class="content-category-title">PARTAGER, ECHANGER, S'INFORMER</p>
    <?php 
      $myregionlink=$classcodeUrl."/classcode-v2/#mapWidget";
      if(isset($profilRegion)&&($profilRegion!='')){
        $myregionlink=$classcodeUrl."/classcode-v2/?region=".$profilRegion."#mapWidget";
      }
    ?>
    <a href="<?php echo $myregionlink;?>">
      <div id="" class="profilblocAdditionnalContent profilblocAdditionnalContentFirst">
        <img src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code_v2/assets/images/pictos/profil/ClassCode-v2-img-Profil-19.jpg">
        <div class="profilblocFooter">Dans votre Région</div>
      </div>
    </a>
    <a href="<?php echo $classcodeUrl; ?>/classcode-v2/?meetingSearch=true#mapWidget">
      <div id="" class="profilblocAdditionnalContent profilblocAdditionnalContentSecond">
        <img src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code_v2/assets/images/pictos/profil/ClassCode-v2-img-Profil-22.png">
        <div class="profilblocFooter">Participez à une Rencontre</div>
      </div>
    </a>
    <a href="<?php echo $classcodeUrl; ?>/classcode-v2/faq/">
      <div id="" class="profilblocAdditionnalContent profilblocAdditionnalContentThird">
        <img src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code_v2/assets/images/pictos/profil/ClassCode-v2-img-Profil-20.jpg">
        <div class="profilblocFooter">Foire aux Questions</div>
      </div>
    </a>
    <a href="<?php echo $classcodeUrl; ?>/classcode-v2/contacts">
       <div id="" class="profilblocAdditionnalContent profilblocAdditionnalContentFour">
          <img src="<?php echo $classcodeUrl; ?>/wp-content/plugins/class_code_v2/assets/images/pictos/profil/ClassCode-v2-img-Profil-21.jpg">
          <div class="profilblocFooter">Contactez-nous</div>
      </div>
    </a>
    <div class="profilblocAdditionnalContent profilblocAdditionnalContentFive">
      <div class="widget">
        <a class="twitter-timeline" href="https://twitter.com/classcode_fr"></a>
        <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
      </div>
      <div class="t7-twitter-fallback">
        <a target ="_blank" href="https://twitter.com/classcode_fr" class="twitter-follow-button" data-show-count="false" data-lang="fr" data-size="large"><img style="width:90%; margin: 10px auto 20px auto" src="<?php echo $classcodeUrl; ?>/wp-content/uploads/2017/10/classcode-on-twitter.png"/><br/>Suivre Class´Code sur twitter.</a>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
      </div>
    </div>
    <div class="clear"></div>
  </div>

<script language='javascript'>
  var userMap;
  var userMarker;  
  var visitorMarkerImage = '<?php echo get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/visitorMarker.png";?>';
  var userMapZoom = 11;
  var ClassCodeIcon ;
  var visitorIcon ;
  
  jQuery( document ).ready(function($) {
    ClassCodeIcon = L.Icon.extend({
      options: {               
      iconSize:     [33, 45],
      iconAnchor:   [16, 44],
      popupAnchor:  [0, -30]
      }
    });
    visitorIcon = new ClassCodeIcon({iconUrl: visitorMarkerImage});
    loadOSmap(<?php echo $userLocation['lat']; ?>,<?php echo $userLocation['lng']; ?>)
    
		jQuery(function($){	
			// Toggle des différentes parties
			var moduleParts = $(".content-category-title");
			moduleParts.on("click", function(e) {
				if ( $(this).hasClass("closed") ) {
					$(this).removeClass("closed");
					$(this).nextUntil(".content-category").css("display", "block");
          if($(this).attr('id')== "personnal-data-trigger"){
            $("#profileLoc").css("display", "block");
            if(document.body.clientWidth < 610){
              $("#profileFirstRow").css("height","1640px");
            }else if(document.body.clientWidth < 1145){
              $("#profileFirstRow").css("height","820px");
            }else{
              $("#profileFirstRow").css("height","410px");
            }
          }
				} else {
					$(this).addClass("closed");
					$(this).nextUntil(".content-category").css("display", "none");
          if($(this).attr('id')=="personnal-data-trigger"){
            $("#profileLoc").css("display", "none");
            $("#profileFirstRow").css("height","auto");
          }
				}
			});
		});
    jQuery(window).resize(function($){
      if(!jQuery("#personnal-data-trigger").hasClass("closed")){
        if(document.body.clientWidth < 610){
          jQuery("#profileFirstRow").css("height","1640px");
        }else if(document.body.clientWidth < 1145){
          jQuery("#profileFirstRow").css("height","820px");
        }else{
          jQuery("#profileFirstRow").css("height","410px");
        }
      }
    });

    $('input[type=radio][name=radio_Profil]').change(function() {
      $('#profile').val(this.value);
    });
    
    skillNormalize();
    
    // Get the modal
    var modal = document.getElementById('profilemodal');
    // Get the button that opens the modal
    var btn = document.getElementById("profilemodalToggle");
    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];
    // When the user clicks the button, open the modal 
    btn.onclick = function() {
        modal.style.display = "block";
        return false;
    }     
    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }    
  });  
    
  
  /* main function to conver lat/long to address using Normatim (OpenStreetMap) */
  function returnNormatimAddress( gotLat, gotLng) {
    var address = 'https://nominatim.openstreetmap.org/reverse.php?format=json&lat=' + gotLat + '&lon=' + gotLng;
      // use jQuery to call the API and get the JSON results
      var latlng;
      jQuery.ajax({
        type: 'GET',
        url: address,
        dataType: 'json',
        success: function(data){          
          if((typeof data !== "undefined")){
            breakNormatimAddress(data);
          }else{
            alert("Geocoder failed  : " + address);
          }
        }        
      });
    
  }
  
  //address components
  function breakNormatimAddress(location) {  
    var addressComponent = location.address;
    var formattedAddress = location.display_name;
    var streetAddress = "";
    if(addressComponent !== "undefined"){
      if(addressComponent.house_number){
        streetAddress = addressComponent.house_number+" ";
      }
      if(addressComponent.road){
         streetAddress += addressComponent.road;  
      }
    }
    jQuery("#street").val(streetAddress);
    var city = "";
    if(addressComponent.village){
      city = addressComponent.village;
    }else if(addressComponent.city){
      city = addressComponent.city;
    }else if(addressComponent.town){
      city = addressComponent.town;
    }

    jQuery("#city").val(city);
    var postcode ="";
    if(addressComponent.postcode){
      postcode = addressComponent.postcode;
    }
    jQuery("#zipcode").val(postcode);
    //region
    var state = "";
    if(addressComponent.state){
      state = addressComponent.state;
    }
    jQuery("#state").val(state);
    var country = "";
    if(addressComponent.country){
      country = addressComponent.country;
    }
    jQuery("#country").val(country);
    jQuery("#formatted_address").val(formattedAddress);
  }
  
  function loadOSmap(lat,lng){
    userMap = L.map('userMap',
      {maxZoom: 18}).setView([lat,lng], <?php if($noloc === true){ echo "5";}else{ echo "15"; } ?>),
    markers = L.markerClusterGroup({
      spiderfyOnMaxZoom: true,
      showCoverageOnHover: false,
      zoomToBoundsOnClick: true,
      polygonOptions: {
        fillColor: '#FFFFFF',
        color: '#FFFFFF',
        weight: 0.5,
        opacity: 1,
        fillOpacity: 0.5
      }
    }),
    lat = lat,
    lon = lng;

    var markerDetail = "<p class='h4'><b>Vous</b></p>";
    userMarker = L.marker(L.latLng(lat, lon), { title: 'Vous', draggable: true, icon:visitorIcon,userId:'450'} );        

    markers.addLayer(userMarker);                       
    userMap.addLayer(markers);
      
    userMarker.on('dragend', function(event) {
      var marker = event.target;  // you could also simply access the marker through the closure
      var result = marker.getLatLng();  // but using the passed event is cleaner
      jQuery("#lat").val( result.lat );
      jQuery("#lng").val( result.lng );
      returnNormatimAddress( result.lat, result.lng);        
    });
      
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(userMap);
  }
  
  function updateOSMap(formSubmit){
    formSubmit = typeof formSubmit !== 'undefined' ? formSubmit : false;
    var address = document.getElementById("street").value;
    var city = document.getElementById("city").value;
    var postalCode = document.getElementById("zipcode").value;
    var country = document.getElementById("country").value;
    if((country == "undefined") ||(country == '')){
      country = "France";
    }
    
    var completeAddress = address+","+postalCode+","+city+","+country;
    var simpleAddress = postalCode+","+city+","+country;
    
    var geocode = 'https://nominatim.openstreetmap.org/search.php?format=json&q=' + encodeURI(completeAddress);
    var simplegeocode = 'https://nominatim.openstreetmap.org/search.php?format=json&q=' + encodeURI(simpleAddress);
    // use jQuery to call the API and get the JSON results
    var latlng;
    jQuery.ajax({
      type: 'GET',
      url: geocode,
      dataType: 'json',
      success: function(data){          
        if((typeof data[0] !== "undefined")){
          jQuery("#lat").val(data[0]["lat"]);
          jQuery("#lng").val(data[0]["lon"]);
          latlng = L.latLng(data[0]["lat"], data[0]["lon"]);
          userMarker.setLatLng(latlng);
          userMap.panTo(latlng);
          jQuery("#formatted_address").val(data[0]["display_name"]); 
          if(formSubmit == true){
            window.onbeforeunload = null;
            var meetingForm = document.getElementById('profile-edit-form');
            jQuery('<input type="submit">').hide().appendTo(meetingForm).click().remove();
            return false;
          }
        }else{
          jQuery.ajax({
            type: 'GET',
            url: simplegeocode,
            dataType: 'json',
            success: function(data){          
              if((typeof data[0] !== "undefined")){
                jQuery("#lat").val(data[0]["lat"]);
                jQuery("#lng").val(data[0]["lon"]);
                latlng = L.latLng(data[0]["lat"], data[0]["lon"]);
                userMarker.setLatLng(latlng);
                userMap.panTo(latlng);
                jQuery("#formatted_address").val(data[0]["display_name"]); 
                if(formSubmit == true){
                  window.onbeforeunload = null;
                  var meetingForm = document.getElementById('profile-edit-form');
                  jQuery('<input type="submit">').hide().appendTo(meetingForm).click().remove();
                  return false;
                }
              }else{
                alert("La localisation n'a pu être géocodée, merci de corriger le lieu de rencontre");
              }
            }        
          });
        }
      }        
    });
  }
  
  //avatar encodding at submission
  function encodeAvatarImg(){
    document.getElementById('avatar').value = JSON.stringify(myAvatarValue); 
    document.getElementById('avatarImg').value = encodeURIComponent(document.getElementById('class_code_avatar_display').innerHTML.replace(/<!--[^>]*>/g, '').replace(/\s+/g,' '));
  }
  
  // Adds a skill to the input field
  function skillAdd(skill) {
    document.getElementById(skillInputId).value = document.getElementById(skillInputId).value + ", " + skill;
    skillNormalize();
  }
  // Normalized the skill input field value
  function skillNormalize() {
    var value = document.getElementById(skillInputId).value;
    // Avoids the ' char, normalize space and punctuation
    value = value.toLowerCase().replace(/'/g, "´").replace(/\s+/, " ").replace(/\s*^[,;]\s*(.*)\s*$/, "$1").replace(/\s*[,;]\s*/g, ", ");
    // Avoids skill duplicate
    value = value.split(", ").filter(function(item, pos, self) { return self.indexOf(item) == pos; }).join(", ");
    document.getElementById(skillInputId).value = toTitleCase(value);
  }
  function toTitleCase(str){
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  }
  var skillInputId = "skills";
  
  function submitProfilForm(){
    window.onbeforeunload = null;
    
    var $myForm = document.getElementById('profile-edit-form');
    jQuery('<input type="submit">').hide().appendTo($myForm).click().remove();
    return false;
    
    //updateOSMap(true);  
  }
  
  function submitModalForm(){
    window.onbeforeunload = null;
    
    var $myForm = document.getElementById('modal-edit-form');
    jQuery('<input type="submit">').hide().appendTo($myForm).click().remove();
    return false;
    //updateOSMap(true);  
  }
  
</script>

