<?php // Implements the edition or display of a rencontre post
  
// Usage: link?meetingId=$id 		to display a rencontre
// Usage: link?meetingId=$id&edit=true 	to edit a rencontre
// Usage: link				to create edit a rencontre
  // Creates the post if not yet done
 
//include_once GMW_PT_PATH . 'includes/admin/gmw-pt-metaboxes.php';
//include_once GMW_PATH . '/includes/admin/geo-my-wp-admin-functions.php' ;
include_once(WP_PLUGIN_DIR.'/class_code_v2/class_code_config.php');
include_once(WP_PLUGIN_DIR.'/class_code_v2/rencontres/rencontre_type.php');

wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

wp_enqueue_style('leaflet','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/leaflet.css');
wp_enqueue_style('markercluster','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/plugins/leaflet.markercluster-1.3.0/dist/MarkerCluster.Default.css');  

wp_enqueue_script('leaflet','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/leaflet.js');
wp_enqueue_script('markercluster','/wp-content/plugins/class_code_v2/assets/js/leaflet-1.3.1/plugins/leaflet.markercluster-1.3.0/dist/leaflet.markercluster.js');  

//accueil de la v2
$is_accueil_cc_v2 = "classcode-v2";

// Redirection if the user is not connected
if (!is_user_logged_in()) {
  echo "<div id='notLogged' class='content-category'><h2>Vous devez être connecté pour accéder à cette page !</h2>";
  echo "<a href='".site_url($is_accueil_cc_v2)."' >Retour à l'accueil</h2></a></div>";
  return;
}else{
  $visitorId = get_current_user_id(); 
}


// Visualization du logo de la structure organisatrice de la rencontre
function get_cc_logo_HTML($data_structures, $structure) {  
  foreach($data_structures as $data){
    if($structure == htmlspecialchars_decode($data['name'],ENT_QUOTES) ){
      if(isset($data['logo']) && ($data['logo']!='')){
        return '<div class="classCodeMapOverlayContentLogo"><a title="'.$structure.'" href="'.$data['url'].'"><img width="180" src="'.$data['logo'].'" alt="'.$structure.'"/></a></div>';  
      }else{
        return '';
      }
    }
  }
  return '';
}
$meetingShortLink = get_site_url()."/classcode-v2/rencontres/";
$meetingEditShortLink = $meetingShortLink."?edit=true";

$flashMessage = '';
if(isset($_REQUEST['action'])){
  $action = $_REQUEST['action'];
  if($action=="update"){
  $flashMessage = "Bravo, Rencontre modifiée avec succès !";
  }else if($action=="create"){
    $flashMessage = "Bravo, Rencontre créée avec succès !";
  }else if($action=="duplicate"){
    $flashMessage = "Bravo, Rencontre dupliquée avec succès !";
  }else if($action=="delete"){
    $flashMessage = "Bravo, Rencontre supprimée avec succès !";
  }
}

$firstFilterClassSelected =""; 
$secondFilterClassSelected ="";
$thirdFilterClassSelected ="";
$facilitateurSearch = false;
$participantSearch = false;
$subscriberSearch = false;

$JSMap ="";
if(isset($_POST['facilitateurSearch'])){
  $firstFilterClassSelected="filterSelected";
  $facilitateurSearch = true;  
  $who = 'facilitateurs';
}else if(isset($_POST['participantSearch'])){
  $secondFilterClassSelected="filterSelected";
  $participantSearch = true;
  $who = 'participants';
}else if(isset($_POST['subscriberSearch'])){
  $thirdFilterClassSelected="filterSelected";
  $subscriberSearch = true;
  $who = 'subscriber';
}else{ //recherche par défaut
  $firstFilterClassSelected="filterSelected";
  $facilitateurSearch = true;
  $who = 'facilitateurs';
}

$visualization='create';


global $wpdb;          
// Determination du type de visualisation : edition, creation ou visualisation

if (isset($_REQUEST['meetingId'])){
  $meetingId = $_REQUEST['meetingId'];
  $meeting = rencontre_type::get_rencontre($meetingId);
  if (empty($meeting)) {
    //la rencontre n'existe pas
    $visualization='create';    
  }else{ // la rencontre existe
    $organizerId = $meeting['ownerId'];
    $organizerEmail = $meeting['ownerEmail'];
    
    if(isset($_REQUEST['edit'])){
      if(($visitorId != $organizerId) && !in_array('administrator', wp_get_current_user()->roles)){
        //le visiteur n'est pas l'auteur, on le repasse en visualisation
        $visualization='visualize';    
      }else{
        $visualization='edit';    
      }
    }else{
      $visualization='visualize';  
    }     
    if($visualization=='visualize'){
      $secondFilterClassSelected="";
      $firstFilterClassSelected="filterSelected";
      $who = 'subscriber';
    }
    $organizerProfile = profile_type::get_profile($organizerId);         
    $organizerdisplayName = addslashes($organizerProfile['displayname']);

    $organizerskills = $organizerProfile['skills'];
    $organizerskillsTab = explode(",",$organizerskills);
    
    $organizerLocation = $organizerProfile['location'];
    $organizerLat = $organizerLocation['lat'];
    $organizerLong = $organizerLocation['lng'];
    $meetingLat = $organizerLat;
    $meetingLong = $organizerLong;
            
    $meetingAddress = str_replace("\\", "",$meeting['location']['street']);
    $meetingCity = $meeting['location']['city'];
    $meetingState = $meeting['location']['state'];
    $meetingPostalCode = $meeting['location']['zipcode'];
    $meetingCountry =  $meeting['location']['country'] ;
    if((!$meetingCountry) || ($meetingCountry =='')){
      $meetingCountry = "France";
    }
    if( ($meeting['location']['lat'] != 0) || ($meeting['location']['lat'] != 0)){
      $meetingLat = $meeting['location']['lat'] ;
      $meetingLong = $meeting['location']['lng'] ;        
    }    
    $meetingFormattedAddress = $meeting['location']['formattedAddress'] ;
    $meetingFormattedAddress = str_replace("\\", "",$meetingFormattedAddress);
    $meetingStructure = $meeting['structure'];
    $meetingCapacity = $meeting['capacity'];
    $meetingMore = $meeting['more'];
    $meetingModule = $meeting['subject'];
    $meetingModulePrecisions = $meeting['precisions'];
    $meetingHangout = $meeting['hangoutLink'];

    $module_id = 0; 
    foreach($title_module as $id => $title){
      if ($meetingModule == $title){
        $module_id = $id;  
      } 
    }
    //existance des kits modules
    if (isset($kit1_module[$module_id])){
      $kit1_module_display_link = $kit1_module[$module_id];
    }
    if (isset($kit2_module[$module_id])){
      $kit2_module_display_link = $kit2_module[$module_id];
    }
    //existance de l'url de description du module
    if(isset($url_module_array[$module_id])){
      $url_module_link = $url_module_array[$module_id];
    }
      
    $meetingDate = $meeting['date'];
    $meetingTime = $meeting['time'];  
    $subscribersEmails = $meeting['subscribersMails'];  
    
    $meetingLink = $meetingShortLink."?meetingId=".$_REQUEST['meetingId'];
    
    //on recupère le visiteur
    $noVisitorLocation =false;
    if($organizerId != $visitorId){
      $visitorProfile = profile_type::get_profile($visitorId);        
      $visitorLocation = $visitorProfile['location'];
      $visitorLat = $visitorLocation['lat'];
      $visitorLong = $visitorLocation['lng'];
      $visitorDisplayName = addslashes($visitorProfile['displayname']);
      if(!$visitorDisplayName){
        $visitorDisplayName = 'Visiteur';
      } 
    }else{
      $visitorDisplayName=$organizerdisplayName;
      $visitorLat = $organizerLat;
      $visitorLong = $organizerLong;        
    }
    if($visitorLat =='' || $visitorLong == ''){
      $visitorLat = '46.52863469527167';
      $visitorLong = '2.43896484375';
      $noVisitorLocation = true;
    }

    $JSMap.= "function loadUserOSmap(lat,lng) {";
    $JSMap.= "  userMap = L.map('classCodeUserMap', ";
    $JSMap.= "   {maxZoom: 18}).setView([lat,lng], userMapZoom ), ";
    $JSMap.= "   userMarkers = L.markerClusterGroup({ ";
    $JSMap.= "   spiderfyOnMaxZoom: true,";
    $JSMap.= "   showCoverageOnHover: false,";
    $JSMap.= "   zoomToBoundsOnClick: true,";
    $JSMap.= "   polygonOptions: {";
    $JSMap.= "     fillColor: '#FFFFFF',";
    $JSMap.= "     color: '#FFFFFF',";
    $JSMap.= "     weight: 0.5,";
    $JSMap.= "     opacity: 1,";
    $JSMap.= "     fillOpacity: 0.5";
    $JSMap.= "   }";
    $JSMap.= " }),";
    $JSMap.= " userLat = lat,";
    $JSMap.= " userLon = lng;";
    $JSMap.= " L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {";
    $JSMap.= "   maxZoom: 18,";
    $JSMap.= "   attribution: '&copy; <a href=\"http://osm.org/copyright\">OpenStreetMap</a> contributors'";
    $JSMap.= " }).addTo(userMap);";
    
    $JSMap.= " userMeetingMarker = L.marker(L.latLng(lat, lon), { title: 'Temps de rencontre', icon:meetingIcon} );";
    $JSMap.= " userMarkers.addLayer(userMeetingMarker);";
          
    $JSMap.= " var visitorDisplayName ='<div class=\"meetingMapInfoTitle\">".$visitorDisplayName."</div>';";

    if(!$noVisitorLocation){        
      $JSMap.= " pixeesArrayMarker[0] = L.marker(L.latLng('".$visitorLat."','".$visitorLong."'), { title: '".$visitorDisplayName."', icon:visitorIcon, id: '0', userId: ".$visitorId.", desc: '<div id=\"profilDisplay\"></div>' } );";

      $JSMap.= "userMarkers.addLayer(pixeesArrayMarker[0]);";
    }else{
      $JSMap.= "  pixeesArrayMarker[0]='';";
    }
    
    $pixees_inc = 1;
    $avatarTab=array();
    if($who=='subscriber'){
      $participantCheckArray=array();
      foreach($meeting['subscribers'] as $id){
        $participantCheckArray[$id]=true;
      }
    }
    
    $pixees_array = profile_type::get_all_users_with_loc();
    foreach($pixees_array as $pixees){
      $id = $pixees['userId'];
      if($visitorId != $id){ 
        $pixeeLocation = $pixees['location'];
        $pixeeLat = $pixeeLocation['lat'];
        $pixeeLong = $pixeeLocation['lng']; 

        if(($pixeeLat)&&($pixeeLong)){
          $includePixee = false;
          if(($who=='all')||
            (($who == 'facilitateurs')&&($pixees['facilitator']))||
            (($who == 'participants')&&(!$pixees['facilitator']))){
            $includePixee = true;
          }else if($who=='subscriber'){
            if(($id == $organizerId) || (isset($participantCheckArray[$id]) && ($participantCheckArray[$id]))){
              $includePixee = true;
            }
          }
          
          if($includePixee){        
            $JSMap.= "pixeesContentString[".$pixees_inc."] ='<div id=\"profilDisplay\"></div>';";
            
            if($pixees['facilitator'] == true){
              $markerImage = 'usersMarkerIconFacilitateur';
            }else{
              $markerImage = 'usersMarkerIcon';  
            }
            
            $JSMap.= " pixeesArrayMarker[".$pixees_inc."] = L.marker(L.latLng('".$pixeeLat."','".$pixeeLong."'), { title: '".$pixees['displayname']."', icon:".$markerImage.", id: '".$pixees_inc."', userId: ".$id.", desc: pixeesContentString[".$pixees_inc."] } );";
        
            $JSMap.= " userMarkers.addLayer(pixeesArrayMarker[".$pixees_inc."]);";
            $JSMap.= " pixeesArrayMarker[".$pixees_inc."].on('click',"; $JSMap.= "   function(event){clickOnMapMarker(this);});";
          }         
          $pixees_inc++;
        }  
      }        
    } 
    $JSMap.= " userMap.addLayer(userMarkers);";
    $JSMap.= "}"; 
  }
}

if($visualization=='create'){
  $organizerId = $visitorId;  
  $organizerProfile = profile_type::get_profile($organizerId);  
  $organizerLocation = $organizerProfile['location'];
  $organizerLat = $organizerLocation['lat'];
  $organizerLong = $organizerLocation['lng'];
  $meetingLat = $organizerLat;
  $meetingLong = $organizerLong;
  $meetingAddress = $organizerLocation['street'];
  $meetingCity = $organizerLocation['city'];
  $meetingPostalCode = $organizerLocation['zipcode'];
  $meetingCountry = $organizerLocation['country'];
  if((!$meetingCountry) || ($meetingCountry =='')){
    $meetingCountry = "France";
  }
  $meetingState = $organizerLocation['state'];
  $meetingFormattedAddress = $organizerLocation['formattedAddress'];
}
$noloc = false;
if(!$meetingLat or ($meetingLat == '') or !$meetingLong or ($meetingLong == '')){
  $meetingLat =  '46.52863469527167';
  $meetingLong = '2.43896484375'; 
  $noloc = true;
}  

if($flashMessage!=""){
  echo '<div id="meetingflashMessage">';
    echo $flashMessage;
  echo '</div>';
}
  
if($visualization!='visualize'){ 
 
  // Encapsulates the interaction in a form
  echo '<form action="'.get_site_url().'/classcodeadmin/editmeeting" method="post" id="meetingForm" class="rencontres-edit">';
    echo '<input type="hidden" name="redirect" value="'.$meetingEditShortLink.'"/>';
  // Propagates the meetingId
  if (isset($_REQUEST['meetingId'])){
    echo '<input type="hidden" name="meetingId" value="'.$_REQUEST['meetingId'].'"/>';
  }
}else{
  $is_registred = in_array(wp_get_current_user()->ID,$meeting['subscribers']) ;     
}
?>


  <div id="meetingPratcicalInfos" class="content-category meeting">
    <p class="content-category-title">INFOS PRATIQUES <span class="minimize"><?php if($visualization == 'visualize'){ echo '(visualisation)';}else{ echo '(création modification)';} ?></span></p>
    <div id="createMeeting" class="meetingbloc meetingblocfirst">
      <div class="meetingbloctitle">
      <?php  
        if($visualization != 'create'){
          echo "Rencontre " ; 
          if(isset($_REQUEST['meetingId'])){ 
            echo '#'.$_REQUEST['meetingId']; 
          }     
        }else{
          echo "Créer une rencontre";  
        }
      ?>
      </div>
      <div class="meetingbloccontent">
      <?php
      if($visualization != 'visualize'){

        echo '<input class="firstInline calendrier" id="meeting_date" type="text" onNothing="cc_checkDate(this.value);" name="meeting_date" value="'.$meetingDate.'" placeholder="Date de la rencontre" title="Date de la rencontre" />';       
        echo '<input class="secondInline" id="meeting_time" type="text" onNothing="cc_checkTime(this.value);" name="meeting_time" value="'.$meetingTime.'" placeholder="00:00" title="Heure de la rencontre au format HH:mm" />';
        echo '<div class="clear"></div>';
        $meetingStructure=htmlentities($meetingStructure);
        $structure_names_array = array();
        foreach($data_structures as $structure){
          if(($structure['name']) && ($structure['name'] != '')){
            $structure_names_array[] = htmlspecialchars_decode($structure['name'],ENT_QUOTES);
          }
        }
        echo do_completion_field("structure", $structure_names_array, $meetingStructure, "", false,"Structure d'accueil",$helpTab['structure']);
        echo '<input class="" id="meeting_street" type="text" name="meeting_street" value="'.$meetingAddress.'" placeholder="Adresse" title="'.$helpTab['adresse'].'" /><br/>';
        echo '<input class="firstInline" id="meeting_city" type="text" name="meeting_city" value="'.$meetingCity.'" placeholder="Ville" title="Ville" />';
        echo '<input class="secondInline" id="meeting_zipcode" type="text" name="meeting_zipcode" value="'.$meetingPostalCode.'" placeholder="Code Postal" title="Code Postal" />';
        echo '<input class="" id="meeting_country" type="text" name="meeting_country" value="'.$meetingCountry.'" placeholder="Pays" title="Pays" /><br/>';

        echo '<div class="clear"></div>';
        echo '<br/>';
        echo '<a class="meetingButton" onclick="updateOSMap();">Mettre à jour sur la carte </a>';
        echo '<input class="" id="capacity" type="text" name="capacity" value="'.$meetingCapacity.'" placeholder="Capacité d\'accueil" title="Capacité d\'accueil" /><br/>';
        
        echo '<p>Vous souhaitez faire participer des personnes à distance <a target="_blank" href="'.get_site_url().'/classcode/accueil/documentation/classcode-la-documentation-participant/comment-utiliser-hangout-dans-le-cadre-de-ce-projet/">par hangout</a> ?</p>';
        echo '<input class="" id="meeting_hangout" type="text" name="meeting_hangout" value="'.$meetingHangout.'" placeholder="Lien Hangout pour suivre à distance" title="Lien Hangout pour suivre à distance" /><br/>';
        
        echo '<textarea class="" id="meeting_more" name="meeting_more" placeholder="Commentaires, précisions éventuelles" title="Commentaires, précisions éventuelles">'.$meetingMore.'</textarea><br/>';

        echo '<input id="meeting_lat" type="hidden" name="meeting_lat" value="'.$meetingLat.'"/>';   
        echo '<input id="meeting_lng" type="hidden" name="meeting_lng" value="'.$meetingLong.'"/>';  
        echo '<input id="meeting_formatted_address" type="hidden" name="meeting_formatted_address" value="'.$meetingFormattedAddress.'"/>';
        echo '<input id="meeting_state" type="hidden" name="meeting_state" value="'.$meetingState.'"/>';         
      }else{
        $meeting_struct_logo=get_cc_logo_HTML($data_structures, $meetingStructure);
        
        if($meeting_struct_logo!=''){
          echo $meeting_struct_logo; 
        }
        echo '<h3>Quand ?</h3>';
        if(isset($meetingDate) && ($meetingDate != '')){
          echo "<p class='meetingInfos'>Le ".$meetingDate;
          if(isset($meetingTime) && ($meetingTime != '')){
            echo " à ".$meetingTime;
          }
          echo "</p>";
        }else{
          echo "<p class='meetingInfos'>Non déterminé</p>";
        }
        echo "<h3>Où?</h3>";  
        if(isset($meetingStructure) && ($meetingStructure!='')){
          echo "<p class='meetingInfos'>".$meetingStructure."</p>";
        }
        echo "<p class='meetingInfos'>";
        if($meetingAddress){
          echo $meetingAddress."<br>";
        }
        if($meetingPostalCode){
          echo $meetingPostalCode." ";
        }
        if($meetingCity){
          echo $meetingCity;
        }
        if($meetingCity || $meetingPostalCode ){
          echo "<br>";
        }
        if($meetingCountry){
          echo $meetingCountry."<br>";
        }
        echo "</p>";
        if(isset($meetingCapacity) && ($meetingCapacity!='')){
          echo "<p class='meetingInfos'>Peut accueillir ".$meetingCapacity." personnes</p>";
        }
        if(isset($meetingHangout) && ($meetingHangout!='')){
          echo "<h3>A distance</h3>"; 
          echo "<p class='meetingInfos'><a target='_blank' href='".$meetingHangout."'>".$meetingHangout."</a></p>";
        }
        echo "<h3>Commentaire</h3>";
        $shortmeetingMore=$meetingMore;
        if(strlen($meetingMore)>300){
            $shortmeetingMore=substr($meetingMore,0,300);
            $shortmeetingMore=$shortmeetingMore."...";
          }
        echo "<p class='meetingInfos' title='".$meetingMore."'>".$shortmeetingMore."</p>";      
        echo '<a class="meetingButton" href="'.site_url($is_accueil_cc_v2).'?meetingSearch=true#mapWidget'.'">Trouver une autre rencontre</a>';
      }
      
      ?>
         
        
      </div>    
    </div>
    <div id="createMeetingMap" class="meetingbloc meetingblocsecond">
      
      
    </div>
    <div id="createMeetingSubject" class="meetingbloc meetingblocthird">
      <div class="meetingbloctitle">Sujet</div>
      <div class="meetingbloccontent">
      <?php
        if($visualization != 'visualize'){
      ?>          
        <select class="input" name="meeting_module" id="meeting_module">
          <option value="" disabled selected>Sujet</option>
          <?php
          // Edits the rencontre metatags: generates the proper select option + text input

          $selected = false;
          $title_module[]="Présentation de Class'Code";
          $title_module[]="Rencontre de partenaires locaux";
          $title_module[]="Echange entre facilitateurs";
          for($ii = 1; $ii <= 8; $ii++) {
            $selected_ii = $meetingModule == $title_module[$ii];
            $selected = $selected || $selected_ii;
            if($selected_ii){
              $selected_ii="selected";
            }else{
              $selected_ii="";
            }            
            echo '<option '.$selected_ii.' value="'.$title_module[$ii].'">'.$title_module[$ii].'</option>';
          }
          if((!$selected)&&($meetingModule!="")){
            $selected="selected";
          }else{
            $selected="";
          }
          echo '<option '.$selected.' value="'.$title_module[$ii].'">Autre</option>';
          ?> 
        </select>         
      <?php
          echo '<textarea class="" id="meeting_precisions" name="meeting_precisions" placeholder="Précisions sur le sujet de la rencontre" title="Précisions sur le sujet de la rencontre">'.$meetingModulePrecisions.'</textarea><br/>';
        }else{
          echo "<h3>".$meetingModule."</h3>";
          echo "<p>";
          if(isset($meetingModulePrecisions) && ($meetingModulePrecisions != '')){
            echo $meetingModulePrecisions;
          }
          echo "</p>";
          
        }
        if(isset($_REQUEST['meetingId'])){
          if(isset($url_module_link)){            
            echo '<a target="_blank" class="meetingButton" href="'.$url_module_link.'">Aller à la page du module</a>';
          }        
          $meetingWebpad = "https://annuel.framapad.org/p/classcode_rencontre_".$_REQUEST['meetingId'];
          if(isset($meetingWebpad) && ($meetingWebpad!='')){
            echo '<a class="meetingButton" href="'.$meetingWebpad.'" target="_blank">Pad de la rencontre</a>';
          }
          if($kit1_module_display_link !=''){
            echo '<a class="meetingButton" href="'.$kit1_module_display_link.'">Télécharger le Kit 1</a>';
          }
          if($kit2_module_display_link !=''){
            echo '<a class="meetingButton" href="'.$kit2_module_display_link.'">Télécharger le Kit 2</a>';
          }  
        }        
      ?>
          
      </div>
    </div>
    <div id="createMeetingManagement" class="meetingbloc meetingblocfourth">
      <div class="meetingbloctitle">Gestion</div>
      <div class="meetingbloccontent">
      <?php
      if($visualization != 'visualize'){
        if(isset($_REQUEST['meetingId'])){
        ?>
          <p> Félicitation ! Vous avez créé une rencontre. Vous pouvez désormais gérer la participation à cette rencontre.</p>
          <p> Pour vous aider dans l'organisation de cette rencontre vous pouvez <a href="">consulter la documentation ici</a>.</p>
          <a class="meetingButton" target="_blank" href="<?php echo site_url($is_accueil_cc_v2)."?meetingSearch=true#mapWidget" ?>" >Contacter la coordination</a>
          <a class="meetingButton" onclick="submitMeetingForm();">Modifier la rencontre</a>
          <a class="meetingButton" onclick="submitMeetingForm('delete');">Supprimer la rencontre</a>
          <a class="meetingButton" onclick="submitMeetingForm('duplicate');">Dupliquer la rencontre</a>
          <a class="meetingButton" target="_blank" href="<?php echo cc_mailto_shortcode(array("subject" => "À propos de la rencontre numéro ".$_REQUEST['meetingId'], "body" => "")); ?>" >Besoin d'aide...</a>
        <?php
        }else{
        ?>
          <p>Pour créer une rencontre il vous suffit de renseigner une date, un lieu et un sujet.</p>
          <p>Si vous ne possédez pas de lieu, n'hésitez pas <a target='_blank' href="<?php echo site_url($is_accueil_cc_v2)."?structureSearch=true#mapWidget" ?>">à contacter nos partenaires</a> et/ou le <a target='_blank' href="<?php echo site_url($is_accueil_cc_v2)."#mapWidget" ?>">coordinateur de votre région</a>.</p>
          <p>Une fois la rencontre créée vous pourrez gérer la participation et entrer en contact avec un facilitateur.</p>
          <p>Vous pourrez également modifier, supprimer ou dupliquer cette rencontre.</p>
          <p>En tant qu'organisateur de la rencontre nous vous offrons un abonnement premium de 3 mois à OpenClassrooms.</p>
          <a class="meetingButton" onclick="submitMeetingForm();">CREER LA RENCONTRE</a>
          <a class="meetingButton" target="_blank" href="<?php echo cc_mailto_shortcode(array("subject" => "À propos de la rencontre numéro ".$_REQUEST['meetingId']."", "body" => "")); ?>" >Besoin d'aide...</a>
        <?php
        }
        
      }else{
      ?>
        <p>La formation Class'Code repose sur deux aspects : une partie en ligne et une partie présentielle.</p>
        <p>Ces temps de rencontre sont essentiels pour tester les activités, bénéficier de temps d'échanges avec des professionnels de l'éducation et de l'informatique et être ensuite accompagné.</p>
        <p>Si la rencontre correspond à votre besoin, inscrivez-vous d'un simple clic. N'hésitez pas à <a target="_blank" href="<?php echo cc_mailto_shortcode(array("who" => "Organisateur-e du temps de rencontre #".$_REQUEST['meetingId'], "to" => $organizerEmail, "subject" => "À propos de la rencontre #".$_REQUEST['meetingId'], "body" => "Relativement à la rencontre ".$meetingLink)); ?>">contacter son organisateur</a> si vous avez des questions.</p>
        <p>Vous pourrez ensuite solliciter une attestation auprès de l'organisateur et gagner 1 mois d'abonnement premium à OpenClassrooms.</p>
        <?php
          if($visitorId == $organizerId){
            echo '<a class="meetingButton" href="'.$meetingLink.'&edit=true">> Modifier la rencontre</a>';
          }else if($is_registred){
            echo '<a class="meetingButton" onclick="register(true);">> Se désinscrire</a>';
            echo '<a target="_blank" class="meetingButton" href="'.get_site_url().'/wp-content/plugins/class_code/attestation/inscription.php?rencontre_id='.$_REQUEST['meetingId'].'">Obtenir une attestation</a>'; 

          }else{
            echo "<a class='meetingButton'  onclick='register();'>> S'inscrire</a>";
          }
        ?>
        <a class="meetingButton" target="_blank" href="<?php echo cc_mailto_shortcode(array("subject" => "À propos de la rencontre numéro ".$_REQUEST['meetingId']."", "body" => "")); ?>" >Besoin d'aide...</a>
      <?php
      }
      ?>
      </div>
    </div>
    <div class="clear"></div>
  </div>   
  </form>
<script type="text/javascript">   
  var map;
  var marker;  
  var userMap;
  var overlayShown = false;
  var lastSelectedMarker = false;
  var lastSelectedImage = "";
  var meetingMarkerImage = '<?php echo get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/meetingMarker.png";?>';
  var meetingMarkerImageSelected = '<?php echo get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/meetingMarkerSelected.png";?>';
  var visitorMarkerImage = '<?php echo get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/visitorMarker.png";?>';
  var usersMarkerImage = '<?php echo get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/usersMarker.png";?>';
  var usersMarkerImageSelected = '<?php echo get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/usersMarkerSelected.png";?>';
  var usersMarkerImageFacilitateur = '<?php echo get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/usersMarkerFacilitateur.png";?>';
  var structuresMarkerImage = '<?php echo get_site_url()."/wp-content/plugins/class_code_v2/assets/images/pictos/map/structuresMarker.png";?>';
  
  var userMapZoom = 11;
  var pixeesArrayMarker = [];  
  var pixeesContentString = [];
  var ClassCodeIcon ;
  var meetingIcon ;
  var visitorIcon ;
  var usersMarkerIconFacilitateur ;
  var usersMarkerIcon ;
  var usersMarkerIconSelected ;
  
   
  jQuery( document ).ready(function() {
    ClassCodeIcon = L.Icon.extend({
      options: {               
      iconSize:     [33, 45],
      iconAnchor:   [16, 44],
      popupAnchor:  [0, -30]
      }
    });
    meetingIcon = new ClassCodeIcon({iconUrl: meetingMarkerImage});
    visitorIcon = new ClassCodeIcon({iconUrl: visitorMarkerImage});
    usersMarkerIconFacilitateur = new ClassCodeIcon({iconUrl: usersMarkerImageFacilitateur});
    usersMarkerIcon = new ClassCodeIcon({iconUrl:usersMarkerImage});
    usersMarkerIconSelected = new ClassCodeIcon({iconUrl:usersMarkerImageSelected});
    
    loadOSmap(<?php echo $meetingLat; ?>,<?php echo $meetingLong; ?>)

    jQuery('#meeting_date').datepicker({
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        dateFormat : 'dd/mm/yy'
    });
    
    if(typeof(loadUserOSmap)=="function"){
      loadUserOSmap(<?php echo $meetingLat; ?>,<?php echo $meetingLong; ?>);
    }
  
    jQuery("#classCodeMapOverlayTitleLink").click(function() {
      hideMapOverlay();
    });
    
    
  });        
  
  
  /* main function to conver lat/long to address using Normatim (OpenStreetMap) */
  function returnNormatimAddress( gotLat, gotLng) {
    var address = 'https://nominatim.openstreetmap.org/reverse.php?format=json&lat=' + gotLat + '&lon=' + gotLng;
      // use jQuery to call the API and get the JSON results
      var latlng;
      jQuery.ajax({
        type: 'GET',
        url: address,
        dataType: 'json',
        success: function(data){          
          if((typeof data !== "undefined")){
            breakNormatimAddress(data);
          }else{
            alert("Geocoder failed  : " + address);
          }
        }        
      });
    
  }
  
  //address components
  function breakNormatimAddress(location) {  
    var addressComponent = location.address;
    var formattedAddress = location.display_name;
    var streetAddress = "";
    if(addressComponent !== "undefined"){
      if(addressComponent.house_number){
        streetAddress = addressComponent.house_number+" ";
      }
      if(addressComponent.road){
         streetAddress += addressComponent.road;  
      }
    }
    jQuery("#meeting_street").val(streetAddress);
    var city = "";
    if(addressComponent.village){
      city = addressComponent.village;
    }else if(addressComponent.city){
      city = addressComponent.city;
    }else if(addressComponent.town){
      city = addressComponent.town;
    }

    jQuery("#meeting_city").val(city);
    var postcode ="";
    if(addressComponent.postcode){
      postcode = addressComponent.postcode;
    }
    jQuery("#meeting_zipcode").val(postcode);
    //region
    var state = "";
    if(addressComponent.state){
      state = addressComponent.state;
    }
    jQuery("#meeting_state").val(state);
    var country = "";
    if(addressComponent.country){
      country = addressComponent.country;
    }
    jQuery("#meeting_country").val(country);
    jQuery("#meeting_formatted_address").val(formattedAddress);
    jQuery("#post_title").val(formattedAddress);

  }
  
  /* function to handle click on Map for bottom Map */   
  function clickOnMapMarker(marker){
    var id = marker.options.id;
    var userId = marker.options.userId;
    var desc = marker.options.desc;
    var title = marker.options.title;
    var icon = marker.options.icon;
    if(id != '-1'){
      var overlay = document.getElementById('classCodeMapOverlay');
      var overlayTitle = document.getElementById('classCodeMapOverlayTitleContent');
      var overlayContent = document.getElementById('classCodeMapOverlayContent');
      if (!overlayShown) {        
        overlayTitle.innerHTML = title;
        overlayContent.innerHTML = desc;
        if(lastSelectedMarker !== false){
          pixeesArrayMarker[lastSelectedMarker].setIcon(lastSelectedImage);
        }
        lastSelectedImage = icon;
        marker.setIcon(usersMarkerIconSelected);
        jQuery.ajax({
          type: 'GET',
          url: '<?php echo get_site_url(); ?>'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+userId+'&ajax_profile=true',
          timeout: 3000,
          success: function(data) {
            jQuery('#profilDisplay').html(data);
          },
          error: function(request) {
            jQuery('#profilDisplay').html('erreur ajax');
          }
        });
        
        jQuery("#classCodeMapOverlay").toggle("slow");
        overlayShown = id;
        lastSelectedMarker = id;
      }else{
        if (overlayShown != id) {
          overlayTitle.innerHTML = title;
          overlayContent.innerHTML = desc;
          pixeesArrayMarker[lastSelectedMarker].setIcon(lastSelectedImage);
          lastSelectedImage = icon;
          marker.setIcon(usersMarkerIconSelected);
          
          jQuery.ajax({
            type: 'GET',
            url: '<?php echo get_site_url(); ?>'+'/wp-content/plugins/class_code_v2/profiles/profiles_ajax.php?user_id='+userId+'&ajax_profile=true',
            timeout: 3000,
            success: function(data) {
              jQuery('#profilDisplay').html(data);
            },
            error: function(request) {
              jQuery('#profilDisplay').html('erreur ajax');
            }
          });
          overlayShown = id;
          lastSelectedMarker = id;
        }else{
          hideMapOverlay();
        }
      }
    }
    userMap.panTo(marker.getLatLng());
  }
  
  
  function loadOSmap(lat,lng){
    meetingMap = L.map('createMeetingMap',
      {maxZoom: 18}).setView([lat,lng], <?php if($noloc === true){ echo "5";}else{ echo "15"; } ?>),
    markers = L.markerClusterGroup({
      spiderfyOnMaxZoom: true,
      showCoverageOnHover: false,
      zoomToBoundsOnClick: true,
      polygonOptions: {
        fillColor: '#FFFFFF',
        color: '#FFFFFF',
        weight: 0.5,
        opacity: 1,
        fillOpacity: 0.5
      }
    }),
    lat = lat,
    lon = lng;

    var markerDetail = "<p class='h4'><b>Temps de rencontre</b></p>";
    //meetingMarker = L.marker(L.latLng(lat, lon), { title: 'Temps de rencontre', draggable: true, icon:meetingIcon} )./bindPopup(markerDetail).openPopup();
    meetingMarker = L.marker(L.latLng(lat, lon), { title: 'Temps de rencontre', <?php if($visualization!='visualize'){ echo 'draggable: true,';}?> icon:meetingIcon,userId:'450'} );        

    markers.addLayer(meetingMarker);                       
    meetingMap.addLayer(markers);
      
    meetingMarker.on('dragend', function(event) {
      var marker = event.target;  // you could also simply access the marker through the closure
      var result = marker.getLatLng();  // but using the passed event is cleaner
      jQuery("#meeting_lat").val( result.lat );
      jQuery("#meeting_lng").val( result.lng );
      returnNormatimAddress( result.lat, result.lng);        
    });
      
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          maxZoom: 18,
          attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(meetingMap);
  }
  
  
  
  function updateOSMap(formSubmit){
    formSubmit = typeof formSubmit !== 'undefined' ? formSubmit : false;
    var location = document.getElementById("structure").value;
    var address = document.getElementById("meeting_street").value;
    var city = document.getElementById("meeting_city").value;
    var postalCode = document.getElementById("meeting_zipcode").value;
    var country = document.getElementById("meeting_country").value;
    if((country == "undefined") ||(country == '')){
      country = "France";
    }
    
    var completeAddress = address+","+postalCode+","+city+","+country;
    var simpleAddress = postalCode+","+city+","+country;
    
    var geocode = 'https://nominatim.openstreetmap.org/search.php?format=json&q=' + encodeURI(completeAddress);
    var simplegeocode = 'https://nominatim.openstreetmap.org/search.php?format=json&q=' + encodeURI(simpleAddress);
    // use jQuery to call the API and get the JSON results
    var latlng;
    jQuery.ajax({
      type: 'GET',
      url: geocode,
      dataType: 'json',
      success: function(data){          
        if((typeof data[0] !== "undefined")){
          jQuery("#meeting_lat").val(data[0]["lat"]);
          jQuery("#meeting_lng").val(data[0]["lon"]);
          latlng = L.latLng(data[0]["lat"], data[0]["lon"]);
          meetingMarker.setLatLng(latlng);
          meetingMap.panTo(latlng);
          jQuery("#meeting_formatted_address").val(data[0]["display_name"]); 
          jQuery("#post_title").val(data[0]["display_name"]);
          if(formSubmit == true){
            window.onbeforeunload = null;
            var meetingForm = document.getElementById('meetingForm');
            jQuery('<input type="submit">').hide().appendTo(meetingForm).click().remove();
            return false;
          }
        }else{
          jQuery.ajax({
            type: 'GET',
            url: simplegeocode,
            dataType: 'json',
            success: function(data){          
              if((typeof data[0] !== "undefined")){
                jQuery("#meeting_lat").val(data[0]["lat"]);
                jQuery("#meeting_lng").val(data[0]["lon"]);
                latlng = L.latLng(data[0]["lat"], data[0]["lon"]);
                meetingMarker.setLatLng(latlng);
                meetingMap.panTo(latlng);
                jQuery("#meeting_formatted_address").val(data[0]["display_name"]); 
                jQuery("#post_title").val(data[0]["display_name"]);
                if(formSubmit == true){
                  window.onbeforeunload = null;
                  var meetingForm = document.getElementById('meetingForm');
                  jQuery('<input type="submit">').hide().appendTo(meetingForm).click().remove();
                  return false;
                }
              }else{
                alert("La localisation n'a pu être géocodée, merci de corriger le lieu de rencontre");
              }
            }        
          });
        }
      }        
    });
  }
  
  // Fonctions de hurlement si les dates/heures ne sont pas au bon format, mais ca cree un conflit avec le calendrier
  function cc_checkDate(value) {
    if (!value.match(/\d{2}\/\d{2}\/\d{4}/))
      alert("Le format de date ``"+value+"´´ n'est pas correct: utiliser JJ/MM/YYYY (jour, mois, année), par exemple 01/04/2016 (1er avril 2016)");
  }
  function cc_checkTime(value) {
    if (!value.match(/\d{2}:\d{2}/))
      alert("Le format d'heure ``"+value+"´´ n'est pas correct: utiliser HH:MM (heures, minutes), par exemple 12:00 (midi)");
  }
  
  //fonction pour l'affichage de la carte de participation
  <?php
    if($_REQUEST['meetingId']){
      echo $JSMap;
    }
  ?>
  
  function searchFacilitateur(){  
    window.onbeforeunload = null;
    var facilitateurSearchForm = document.getElementById('facilitateurSearchForm');
    jQuery('<input type=\"submit\">').hide().appendTo(facilitateurSearchForm).click().remove();
    return false;
  }
  
  function searchParticipant(){  
    window.onbeforeunload = null;
    var participantSearchForm = document.getElementById('participantSearchForm');
    jQuery('<input type=\"submit\">').hide().appendTo(participantSearchForm).click().remove();
    return false;
  }
  
  function searchSubscriber(){  
    window.onbeforeunload = null;
    var subscriberSearchForm = document.getElementById('subscriberSearchForm');
    jQuery('<input type=\"submit\">').hide().appendTo(subscriberSearchForm).click().remove();
    return false;
  }
  
   function searchOrganizer(){  
    window.onbeforeunload = null;
    var organizerSearchForm = document.getElementById('organizerSearchForm');
    jQuery('<input type=\"submit\">').hide().appendTo(organizerSearchForm).click().remove();
    return false;
  }
  
 function register(unregister=false){
    window.onbeforeunload = null;
    var registerMeetingForm = document.getElementById('registerMeetingForm');
    if(unregister){
      jQuery('<input type="hidden" name="unregister" value="true"/>').appendTo(registerMeetingForm);  
    }
    jQuery('<input type=\"submit\">').hide().appendTo(registerMeetingForm).click().remove();
    return false;
  }
  
  function hideMapOverlay(){
    jQuery("#classCodeMapOverlay").toggle("slow");
    overlayShown = false;
    return false;
  }

  jQuery('#meeting_time').focus(function(){
    jQuery('#meeting_time').attr('placeholder', '');
  });
  jQuery('#meeting_time').focusout(function(){
    jQuery('#meeting_time').attr('placeholder', '00:00');
  });

  function submitMeetingForm(action){
    if(action == 'duplicate'){
      var meetingForm = document.getElementById('meetingForm');
      var input = document.createElement('input');
      input.type = 'hidden';
      input.name = 'duplicate';
      input.value = 'true';
      meetingForm.appendChild(input);
    }else if(action == 'delete'){
      var meetingForm = document.getElementById('meetingForm');
      var input = document.createElement('input');
      input.type = 'hidden';
      input.name = 'delete';
      input.value = 'true';
      meetingForm.appendChild(input);
    }
    updateOSMap(true);              
  }
  
  function sendClassCodeInvit(){
    var email = encodeURIComponent(jQuery('#emailInvit').val());
    window.open("<?php echo get_site_url();?>/classcode-v2/contacts/?cc_mailer_shortcode_subject="+encodeURIComponent("Bienvenue à une rencontre Class´Code")+"&cc_mailer_shortcode_to="+email+"&cc_mailer_shortcode_who="+email+"&cc_mailer_shortcode_body="+encodeURIComponent("Bonjour,&#13;&#10;&#13;&#10;La rencontre <?php echo $_REQUEST['meetingId']; ?> pourrait vous intéresser, vous pouvez vous inscrire d'un simple clic ici <?php echo $meetingLink; ?>&#13;&#10;&#13;&#10;Bien Cordialement.")+"#aideAction2");
  }
    
  jQuery('#emailInvit').focus(function(){
    jQuery('#emailInvit').attr('placeholder', '');
  });
  jQuery('#emailInvit').focusout(function(){
    jQuery('#emailInvit').attr('placeholder', "Mail d'une personne que vous souhaitez inviter dans Class'Code");
  });
     
  function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    alert(out);
  }
   
</script>
<?php
  if($visualization != 'create'){
?>
  <div id="mapWidget" class="content-category meeting">
    <?php 
    if($visualization != 'visualize'){
    ?>
		  <p class="content-category-title">GERER LA PARTICIPATION</p>
    <?php 
    }else{
    ?>
      <p class="content-category-title">PARTICIPATION</p>
    <?php    
    }
    ?>
		<div class="content-header"></div>
			<div id="criteres" class="criteres" >  
        <div class="criteresGauche">      
          <div id="criteres-groupe-1" class="criteres-groupe-1">
            <div id="firstFilter" class="criteres-item first <?php echo $firstFilterClassSelected; if($firstFilterClassSelected &&($visualization=='visualize')){ echo " visualize";} ?>">
              <div class="criteres-item-title">
                <?php 
                if($visualization != 'visualize'){
                ?>
                  <a onclick="searchFacilitateur();">Trouver un facilitateur</a>
                <?php 
                }else{
                ?>
                  Organisateur
                <?php    
                }
                ?>
              </div>
              <?php
              if(($visualization != 'visualize') && $facilitateurSearch){
              ?>
              <div class="criteres-item-list">
                <p style="padding-top:10px;">Le Facilitateur est un professionnel de l'informatique qui donne un peu de son temps pour partager avec d'autres un peu de son expertise.</p>
                <p>Le Facilitateur n'est pas un formateur. Il est là pour partager, chercher, apprendre, comme vous !</p>
                <p>N'hésitez pas à contacter les Facilitateurs à proximité de la rencontre.</p>
                <p>Si il n'y en a pas ou qu'il ne sont pas disponibles, n'hésitez pas à <a target="_blank" href="<?php echo site_url($is_accueil_cc_v2)."#mapWidget" ?>">contacter la coordination régionale</a> qui pourra vous épauler dans votre recherche.</p>
              </div>
              <?php
              }
              if($visualization == 'visualize'){
              ?>                
              <div class="criteres-item-list criteres_item-list-uniq">
                <br/>
                <?php
                  echo class_code_user_avatar_img_display($organizerProfile['avatarImg']); 
                ?>
                <h3><?php echo $organizerdisplayName  ; ?></h3>
                <div class="classCodeMapOverlayEmphasisTag">
                <?php if($organizerProfile['facilitator']){ ?>
                  Facilitateur
                <?php } ?>   
                
                </div>    
                <div>
                <?php echo $organizerProfile['profile'] ;?>
                </div>
                <div> 
                <?php foreach($organizerskillsTab as $skill){ 
                if(trim($skill) != ""){
                ?>
                  <div class="overlayGreyRectangleSkills">
                  <?php echo $skill ;?>
                  </div> 
                <?php 
                  }
                } 
                ?>
                </div>
                <br/>
                <a class="meetingButton" target="_blank" href="<?php echo cc_mailto_shortcode(array("who" => "Organisateur-e du temps de rencontre #".$_REQUEST['meetingId'], "to" => $organizerEmail, "subject" => "À propos de la rencontre #".$_REQUEST['meetingId'], "body" => "Relativement à la rencontre ".$meetingLink)); ?>">> Contacter l'Organisateur</a>
                <p>Il y a <?php echo count($meeting['subscribers'])+1; ?> inscrits à la rencontre.</p>
                <p>N'hésitez pas à les contacter pour faire connaissance avant la rencontre, affiner le programme, proposer de remplir le PAD, etc.</p>
                <a class="meetingButton" target="_blank" href="<?php echo cc_mailto_shortcode(array("who" => "Groupe du temps de rencontre #".$_REQUEST['meetingId'], "to" => $subscribersEmails, "subject" => "À propos de la rencontre #".$_REQUEST['meetingId'], "body" => "Relativement à la rencontre ".$meetingLink)); ?>">> Contactez les inscrits</a>
                <?php
                  // Encapsulates the interaction of registration/unregistration in a form
                  echo '<form action="'.get_site_url().'/classcodeadmin/registermeeting" method="post" id="registerMeetingForm">';
                     
                    // Propagates the meetingId
                    echo '<input type="hidden" name="meetingId" value="'.$_REQUEST['meetingId'].'"/>';
                    echo '<input type="hidden" name="redirect" value="'.$meetingLink.'"/>';
                  echo '</form>';
                 ?>      
              </div>
              <?php              
              }
              ?>
            </div>
            <form method="post" id="facilitateurSearchForm" action="<?php echo $meetingEditShortLink."&meetingId=".$_REQUEST['meetingId']."#mapWidget";?>">
              <input type="hidden" value="true" id="facilitateurSearch" name="facilitateurSearch">
            </form>
            <?php 
            if($visualization != 'visualize'){
            ?>
            <div id="secondFilter" class="criteres-item second <?php echo $secondFilterClassSelected; if(($secondFilterClassSelected) && ($visualization=='visualize')){ echo " visualize";} ?>">
              <div class="criteres-item-title">
                <a onclick="searchParticipant();">Trouver les participants</a>
              </div>
              <?php
              if($participantSearch){
              ?>
              <div class="criteres-item-list">
                <p style="padding-top:10px;">Pour que la rencontre soit un succès, il faut des participants.</p>
                <p>D'autre participant Class'Code se trouve probablement dans les environs. Peut-être seront-ils intéressés ?</p>
                <p>N'hésitez pas à inviter les gens que vous connaissez; C'est votre réseau qu'il faut activer.</p>
                <p>Vous connaissez leur mail ? Envoyer leur une invitation à participer à Class'Code.</p>
                <input type="text" id="emailInvit" name="emailInvit" placeholder="Mail d'une personne que vous souhaitez inviter">
                <a target="_blank" class="meetingButton" onClick='sendClassCodeInvit();'>> Envoyer une invitation</a>
              </div>
              <?php
              }
              ?>
            </div>
            <form method="post" id="participantSearchForm" action="<?php echo $meetingEditShortLink."&meetingId=".$_REQUEST['meetingId']."#mapWidget";?>">
              <input type="hidden" value="true" id="participantSearch" name="participantSearch">
            </form>
            <?php    
            }
            ?>
          </div>
          <?php 
          if($visualization != 'visualize'){
          ?>
          <div id="criteres-groupe-2" class="criteres-groupe-2">
            <div class="criteres-item fourth <?php echo $thirdFilterClassSelected; ?>">
              <div id="thirdFilter" class="criteres-item-title">
                  <a onclick="searchSubscriber();">Les inscrits</a>
              </div>
              <?php
              if($subscriberSearch){
              ?>
              <div class="criteres-item-list criteres_item-list-last">
                <p style="padding-top:10px;">Voici les personnes qui se sont inscrit à la rencontre.</p>
                <p>N'hésitez pas à les contacter pour faire connaissance avant la rencontre, affiner le programme, proposer de remplir le PAD, etc.</p>
                <p>Si vous souhaitez envoyer un mail à tous les participants, utiliser le bouton ci-dessous.</p>
                <a class="meetingButton" target="_blank href="<?php echo cc_mailto_shortcode(array("who" => "Groupe du temps de rencontre #".$_REQUEST['meetingId'], "to" => $subscribersEmails, "subject" => "À propos de la rencontre #".$_REQUEST['meetingId'], "body" => "Relativement à la rencontre ".$meetingLink)); ?>">> Contactez les tous</a>
              </div>
              <?php
              }
              ?>
            </div>
            <form method="post" id="subscriberSearchForm" action="#mapWidget">
              <input type="hidden" value="true" id="subscriberSearch" name="subscriberSearch">
            </form>
          </div>
          <?php    
          }
          ?>
        </div>        
        <div class="clear"></div>
        <div class="content-footer">
          <div class="mapSearch"></div>
        </div>        
			</div>
      <div class="carte-parent">
      <div id="map-wrapper">
			  <div id="classCodeUserMap" class="carte">
        </div>
        <div id="classCodeMapOverlay">
          <div id="classCodeMapOverlayTitle">           
            <span id="classCodeMapOverlayTitleContent">
              Retour
            </span>
            <a id="classCodeMapOverlayTitleLink" class="backCriteriaLink">
              <span class="returnIcon">
              </span>
            </a>
          </div>
          <div id="classCodeMapOverlayContent">
          </div>          
        </div>        
      </div>
		</div>
	</div>
  
  
<?php       
  } // if(isset($_REQUEST['meetingId'])){
?> 