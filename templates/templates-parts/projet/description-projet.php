<div class="classcode-v2 content-category page-projet" >

	<!-- Description du projet -->
	
	<p class="content-category-title">L'association</p>
	<div class="content-wp description-projet">
        <?php
			the_post();
		
			// Permet la transformation des shortCodes des vidéos YouTube
			//echo apply_filters('the_content', get_the_content());
                        echo get_the_content();
		?>
	</div>

	<!-- Les partenaires -->
	
	<p class="content-category-title">Les partenaires</p>
	

	<div class="partenaires">
	
	<?php

 try {
      echo $http_syndication->import_shortcode(array("url"=>"https://classcode.fr/projet", "tag_id"=>"logos"), "");
 } catch(Error $error) { 
      echo "<hr/><pre>Upps: ".$error->getMessage()."</pre><hr/>";
 }

echo "<div style='float:right'><b><a target='_blank' href='https://classcode.fr/projet#partenaires'>Tous les partenaires sur notre blog</a></b></div>\n";



   /*

		include CLASSCODE2_PLUGIN_DIR . "/partenaires/data_partenaires.php";
		
		$partenaires = get_partenaires();
		
		foreach( $partenaires as $partenaire ) { 
		
			$partenaire_logo = CLASSCODE2_PLUGIN_URL . "/partenaires/LogosPartenaires/" . $partenaire['group'] . "/" . $partenaire['logo']; ?>
			
			<div class="card">
					<a title="<?php echo $partenaire['name']; ?>" href="<?php echo $partenaire['url']; ?>" target="_blank">
						<img src="<?php echo $partenaire_logo; ?>" alt="<?php echo $partenaire['name']; ?>"/>
					</a>
			 </div>
<?php } */ ?>

		<div class="clear"></div>
	</div>

	<!-- Les autres partenaires -->
	
	<p  id="autres" class="content-category-title">Ils sont en lien avec Class´Code</p>

	<div class="partenaires">

		<table cellspacing="0" cellpadding="4">
		<tbody>
		<tr valign="top">
		<td><img
		src="https://project.inria.fr/classcode/files/2017/06/logo-voyageur-du-code-code-decode.png"
		alt="" width="100"></td>
		<td><img
		src="https://project.inria.fr/classcode/files/2017/05/a1706d9.png"
		alt="" width="120"></td>
		<td><img
		src="https://project.inria.fr/classcode/files/2017/05/a1706d7.png"
		alt="" width="120"></td>
		</tr>
		<tr valign="top">
		<td><a href="http://voyageursducode.fr/">http://voyageursducode.fr</a><br>
		<a href="https://www.code-decode.net/">https://www.code-decode.net</a></td>
		<td><a href="http://d-clicsnumeriques.org/">http://d-clicsnumeriques.org</a></td>
		<td><a href="http://capprio.fr/">http://capprio.fr</a></td>
		</tr>
		<tr>
		<td valign="top">Avec les Voyageurs du Code, l'engagement citoyen et le bénévolat est mis au coeur d'une stratégie d'initiation et de formation à grande échelle de la jeunesse française au numérique : clubs citoyens d'initiation pour comprendre notre quotidien numérique, voir ouvrir des perspectives professionnelles nouvelles à travers le code. Le programme Code´Decode fournit des ressources en lien avec cette initiative.
		</td>
		<td valign="top">Projet d’éducation active auprès d’enfants de 8 à 14 ans, proposant également des formations courtes adaptées aux enseignants et aux animateurs des collectivités et des associations gestionnaires de structures de loisirs périscolaires, ainsi que des actions d’accompagnement et de diffusion des outils.
		</td>
		<td valign="top">Ce programme s’adresse en priorité aux jeunes de 16 à 24 ans, issus des quartiers prioritaires de politique de la ville. Des efforts particuliers sont déployés à destination des jeunes femmes, sous-représentées dans l’entrepreneuriat et l’innovation numérique. Son objectif est de former ces jeunes sans emploi et/ou sans formation aux métiers du digital et à l’animation numérique. L’objectif recherché est double : favoriser l’insertion sociale et professionnelle des jeunes formés et sensibiliser les enfants aux enjeux du numérique.<br>
		</td>
		</tr>
		</tbody>
		</table>

  </div>

	<p id="presse" class="content-category-title">Pour la presse</p>
	<div align="center" class="content-wp description-projet">

<a title="Kit de presse" href="https://project.inria.fr/classcode/kit-de-presse/" rel="bookmark"><img style="padding:0 20px 0 20px" src="https://project.inria.fr/classcode/files/2016/05/kitpresse-200x150.jpg" alt="Kit de presse"/></a>

<a title="Ils-elles parlent de Class’Code : notre revue de presse" href="https://project.inria.fr/classcode/ils-elles-parlent-de-classcode/" rel="bookmark"><img style="padding:0 20px 0 20px;" src="https://project.inria.fr/classcode/files/2016/03/presse-200x150.jpg" alt="Revue de presse"/></a>

<a title="Outil de communication: parler de Class´Code" href="https://project.inria.fr/classcode/classcode-en-un-flyer"><img style="padding:0 20px 0 20px;width:200px;height:150px" src="https://classcode.fr/projet/files/2015/10/Com.jpg" alt="Outils de Com'"/></a>

<div>Contact presse : <i><a href="mailto:classcode-accueil@inria.fr?subject=Contact presse pour Class´Code">classcode-accueil<img style="width:15px;padding:0 5px 0 5px" src="<?php echo get_site_url(); ?>/wp-content/plugins/class_code_v2/assets/images/faq/arobas.png" alt="arobas"/>inria.fr</i></a></div>

	</div>

</div><!-- classcode-v2 -->