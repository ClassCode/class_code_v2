<?php

 /*
 
 Template Name: ClassCode2 Accueil
 
 */

require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/header.php');
?>

<div id="preslider" class="content-category t4">
<center style="float:right;font-size:small"><a href="https://pixees.fr"><img src="https://pixees.fr/wp-content/uploads/2015/04/pixees-ressources.png" width="60"/><br>avec pixees.fr</a></center> <br/> 
Bienvenue dans la communauté apprenante de Class'Code !
Consultez nos ressources, suivez nos formations et déployez avec nous vos projets sur les territoires.
<br/>
</div>
<div id="" class="content-category t4"> <p class="content-category-title"></p>
</div>
<?php

require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/slider.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/accueil/t3-a-la-carte.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/accueil/t4-parcours.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/accueil/t5-pres-de-chez-vous.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/accueil/t6-temoignages.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/accueil/t7-liens.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/accueil/t8-partenaires.php');
require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/accueil/t9-javascript.php');

require_once( CLASSCODE2_PLUGIN_DIR . '/templates/templates-parts/footer.php'); 

?>
		
	


