<?php

/**
 * Makery live
 *
 * @wordpress-plugin
 * Plugin Name:       Class'Code 2
 * Description:       
 * Version:           1.5.0
 * Plugin URI: https://gitlab.inria.fr/ClassCode/class_code_v2
 * Author: benjamin.ninassi@inria.fr, thierry.vieville@inria.fr
 * Author URI: http://classcode.fr
 * License: GPLv2 or later
 
 */

?>
